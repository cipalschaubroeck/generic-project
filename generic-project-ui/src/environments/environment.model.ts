export interface EnvironmentModel {
  enableTracing: boolean;
  keycloak?: any;
  production: boolean;
}
