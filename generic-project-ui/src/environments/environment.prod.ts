import {EnvironmentModel} from './environment.model';

export const environment: EnvironmentModel = {
  enableTracing: false,
  keycloak: {
    url: 'http://localhost:8180/auth',
    realm: 'cstender',
    clientId: 'cstender'
  },
  production: true
};
