import { CommonModule, PlatformLocation } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { APP_INITIALIZER, NgModule, Optional, SkipSelf } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';

import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

import { ConfirmationService } from 'primeng/api';

import { SharedModule } from '../shared/shared.module';
import { AppBreadcrumbComponent } from './components/app.breadcrumb.component';
import { AppFooterComponent } from './components/app.footer.component';
import { AppTopBarComponent } from './components/app.topbar.component';
import { throwIfAlreadyLoaded } from './guards/module-import.guard';
import { RedirectToHomepageGuard } from './guards/redirect-to-homepage.guard';
import { ConfigurationService } from './services/configuration.service';
import { appInitializer } from './utils/app-initializer';

const components = [
  AppBreadcrumbComponent,
  AppFooterComponent,
  AppTopBarComponent
];

export function HttpLoaderFactory (http: HttpClient) {
  return new TranslateHttpLoader(http);
}

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule,
    BrowserAnimationsModule,
    SharedModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    })
  ],
  declarations: components,
  exports: components,
  providers: [
    ConfirmationService,
    RedirectToHomepageGuard,
    ConfigurationService,
    {
      provide: APP_INITIALIZER,
      useFactory: appInitializer,
      multi: true,
      deps: [
        ConfigurationService,
        HttpClient,
        PlatformLocation
      ]
    },
  ]
})
export class CoreModule {

  constructor (@Optional() @SkipSelf() parentModule: CoreModule) {
    throwIfAlreadyLoaded(parentModule, 'CoreModule');
  }

}
