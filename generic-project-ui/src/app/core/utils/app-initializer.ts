import { throwError } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import { BaseHttpService } from '../services/base-http.service';

import { Config, ConfigurationService } from '../services/configuration.service';

export function appInitializer (configurationService: ConfigurationService): () => Promise<any> {
  return () => {
    return configurationService.load().pipe(
      map(config => createAPI_BASE(config)),
      tap(API_BASE => BaseHttpService.API_BASE = API_BASE),
      catchError(error => {
        const _error = error.message || error;
        return throwError(_error);
      })
    ).toPromise();
  };
}

function createAPI_BASE (config: Config): string {
  return `${config.connection}${config.api_base}`;
}
