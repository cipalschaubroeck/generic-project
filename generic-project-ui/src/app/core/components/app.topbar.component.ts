import { Component, forwardRef, Inject, OnInit } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';

import { AppComponent } from '../../app.component';

@Component({
  selector: 'app-topbar',
  templateUrl: './app.topbar.component.html',
  styleUrls: ['./app.topbar.component.scss']
})
export class AppTopBarComponent implements OnInit {

  constructor (@Inject(forwardRef(() => AppComponent)) public app: AppComponent,
               public translateService: TranslateService) {
  }

  ngOnInit () {
  }

  changeLanguage (lang: string) {
    this.app.changeLanguage(lang);
  }

}
