import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './app.footer.component.html',
  styleUrls: ['./app.footer.component.scss']
})
export class AppFooterComponent implements OnInit {

  appVersion: string;

  constructor () {
  }

  ngOnInit () {
    this.getVersion();
  }

  getVersion () {
    // TODO Include a deprecatedservice to call to the backend to get the version
    this.appVersion = '0.0.1';
  }

}
