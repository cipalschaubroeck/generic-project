import { Injectable } from '@angular/core';

@Injectable()
export abstract class BaseHttpService {

  static API_BASE: string;
  protected endpoint: string;

  protected constructor (path: string) {
    this.endpoint = `${BaseHttpService.API_BASE}` + path;
  }

}
