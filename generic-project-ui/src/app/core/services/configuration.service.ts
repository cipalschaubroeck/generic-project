import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';

import { Observable } from 'rxjs';
import { shareReplay, tap } from 'rxjs/operators';

@Injectable()
export class ConfigurationService {

  config: Config;

  constructor (private httpClient: HttpClient,
               private translateService: TranslateService) {
  }

  public load (): Observable<any> {
    const configUrl = './props/properties.json';
    return this.httpClient.get(configUrl).pipe(
      tap(config => {
        this.config = JSON.parse(JSON.stringify(config || undefined));
        this.initTranslate();
      }),
      shareReplay(1)
    );
  }

  private initTranslate () {
    const languages = <string []>this.config['languages'];
    this.translateService.addLangs(languages);
    this.translateService.setDefaultLang(languages[0]);
    const browserLang = localStorage.getItem('lang') ? localStorage.getItem('lang') : this.translateService.getBrowserLang();
    console.log('********** Checking lang browser => '.concat(browserLang));
    const regexp: RegExp = new RegExp(languages.join('|'));
    const lang = browserLang.match(regexp) ? browserLang : this.translateService.getDefaultLang();
    this.translateService.use(lang);
    console.log('********** Using lang => '.concat(lang));
  }
}

export interface Config {
  [key: string]: string | string[];
}
