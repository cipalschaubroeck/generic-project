export interface DocumentLinkModel {
  addresseeList: string;
  attachments: string;
  body: string;
  heading: string;
  self: string;
  signingOfficerData: string;
}
