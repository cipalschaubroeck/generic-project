export interface EmailLinkModel {
  attachments: string;
  body: string;
  self: string;
}
