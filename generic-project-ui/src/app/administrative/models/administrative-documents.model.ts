import { DocumentStatus } from '../enums/document-status.enum';
import { DocumentProcessType } from '../enums/document-process-type.enum';
import { DocumentLinkModel } from './document-link.model';
import { EmailLinkModel } from './email-link.model';
import { UploadLinkModel } from './upload-link.model';

export interface AdministrativeDocumentsModel {
  date: string;
  name: string;
  type: DocumentProcessType;
  status: DocumentStatus;
  links: DocumentLinkModel | EmailLinkModel | UploadLinkModel;
}
