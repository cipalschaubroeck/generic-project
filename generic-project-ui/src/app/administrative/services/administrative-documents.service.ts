import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';

import { BaseHttpService } from '../../core/services/base-http.service';
import { Utils } from '../../shared/classes/utils';
import { DocumentProcessType } from '../enums/document-process-type.enum';
import { AdministrativeDocumentsModel } from '../models/administrative-documents.model';
import { DocumentLinkModel } from '../models/document-link.model';
import { EmailLinkModel } from '../models/email-link.model';
import { UploadLinkModel } from '../models/upload-link.model';

@Injectable()
export class AdministrativeDocumentsService extends BaseHttpService {

  showDuplicateDialog$: BehaviorSubject<boolean> = new BehaviorSubject(false);
  showAddDialog$: BehaviorSubject<boolean> = new BehaviorSubject(false);
  selected$: BehaviorSubject<AdministrativeDocumentsModel> = new BehaviorSubject(null);
  list$: BehaviorSubject<AdministrativeDocumentsModel[]> = new BehaviorSubject<AdministrativeDocumentsModel[]>([]);

  constructor (private httpClient: HttpClient) {
    super('document-process');
  }

  private static generateDocumentLinks (links): DocumentLinkModel {
    return {
      addresseeList: Utils.cleanUrl(links.addresseeList.href),
      attachments: Utils.cleanUrl(links.attachments.href),
      body: Utils.cleanUrl(links.body.href),
      heading: Utils.cleanUrl(links.heading.href),
      self: Utils.cleanUrl(links.self.href),
      signingOfficerData: Utils.cleanUrl(links.signingOfficerData.href)
    };
  }

  private static generateEmailLinks (links): EmailLinkModel {
    return {
      attachments: Utils.cleanUrl(links.attachments.href),
      body: Utils.cleanUrl(links.body.href),
      self: Utils.cleanUrl(links.self.href)
    };
  }

  private static generateUploadLinks (links): UploadLinkModel {
    return {
      self: Utils.cleanUrl(links.self.href)
    };
  }

  private static generateLinks (item): DocumentLinkModel | EmailLinkModel | UploadLinkModel {
    let links: DocumentLinkModel | EmailLinkModel | UploadLinkModel;
    if (item.type === DocumentProcessType.DOCUMENT) {
      links = AdministrativeDocumentsService.generateDocumentLinks(item._links);
    } else if (item.type === DocumentProcessType.EMAIL) {
      links = AdministrativeDocumentsService.generateEmailLinks(item._links);
    } else if (item.type === DocumentProcessType.UPLOAD) {
      links = AdministrativeDocumentsService.generateUploadLinks(item._links);
    } else {
      console.error('unknown document type ' + item.type);
    }
    return links;
  }

  getAllList (): BehaviorSubject<AdministrativeDocumentsModel[]> {
    // TODO CSPROC-1655 Remove params for this issue
    this.httpClient.get<AdministrativeDocumentsModel[]>(this.endpoint, {
      params: {
        size: '100'
      }
    }).pipe(
      map((data: any) => {
        const documentProcesses: AdministrativeDocumentsModel[] = [];
        for (const item of data._embedded.documentProcesses) {
          documentProcesses.push({
            name: item.name,
            date: item.date,
            status: item.status,
            type: item.type,
            links: AdministrativeDocumentsService.generateLinks(item)
          });
        }
        return documentProcesses;
      })
    ).subscribe(documentProcesses => {
      this.list$.next(documentProcesses);
      this.selected$.next(null);
    }, error => {
      console.log(error);
    });

    return this.list$;
  }

  addItem (itemToAdd: AdministrativeDocumentsModel) {
    this.httpClient.post<AdministrativeDocumentsModel>(this.endpoint, itemToAdd).subscribe(itemAdded => {
      this.list$.next([...this.list$.getValue(), {
        name: itemAdded.name,
        date: itemAdded.date,
        status: itemAdded.status,
        type: itemAdded.type,
        links: AdministrativeDocumentsService.generateLinks(itemAdded)
      }]);
      this.selected$.next(null);
    }, error => {
      console.log(error);
    });
  }

  duplicateItem (name: string) {
    if (this.selected$ !== null) {
      this.httpClient.post<AdministrativeDocumentsModel>(this.selected$.value.links.self, name).subscribe(administrativeItem => {
        this.list$.next([...this.list$.getValue(), {
          name: administrativeItem.name,
          date: administrativeItem.date,
          status: administrativeItem.status,
          type: administrativeItem.type,
          links: AdministrativeDocumentsService.generateLinks(administrativeItem)
        }]);
        this.selected$.next(null);
      }, error => {
        console.log(error);
      });
    }
  }

  removeItem () {
    if (this.selected$ !== null) {
      this.httpClient.delete(this.selected$.value.links.self).subscribe(() => {
        const documentList = this.list$.getValue();
        const indexToRemove = documentList.indexOf(this.selected$.value);
        documentList.splice(indexToRemove, 1);
        this.list$.next([...documentList]);
        this.selected$.next(null);
      }, error => {
        console.log(error);
      });
    }
  }

  updateName (name: string) {
    if (this.selected$ !== null) {
      this.httpClient.patch<AdministrativeDocumentsModel>(this.selected$.value.links.self, {
        name: name
      }).subscribe(patchItem => {
        const documentList = this.list$.getValue();
        const indexToUpdate = documentList.indexOf(this.selected$.value);

        documentList[indexToUpdate] = {
          name: patchItem.name,
          date: patchItem.date,
          status: patchItem.status,
          type: patchItem.type,
          links: AdministrativeDocumentsService.generateLinks(patchItem)
        };
        this.list$.next([...documentList]);
        this.selected$.next(documentList[indexToUpdate]);
      }, error => {
        console.log(error);
      });
    }
  }

}
