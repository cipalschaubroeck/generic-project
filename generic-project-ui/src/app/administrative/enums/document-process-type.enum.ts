export enum DocumentProcessType {
  DOCUMENT = 'DOCUMENT',
  EMAIL = 'EMAIL',
  UPLOAD = 'UPLOAD'
}
