import { Component, OnInit } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';

import { BehaviorSubject } from 'rxjs';

import { DocumentProcessType } from '../../../enums/document-process-type.enum';

import { AdministrativeDocumentsModel } from '../../../models/administrative-documents.model';
import { AdministrativeDocumentsService } from '../../../services/administrative-documents.service';
import { AdministrativeDocumentsFormGroup } from '../sidebar/administrative-documents-form-group';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css']
})
export class DetailComponent implements OnInit {

  selected$: BehaviorSubject<AdministrativeDocumentsModel>;
  detail: AdministrativeDocumentsModel;
  formDetail: AdministrativeDocumentsFormGroup;

  constructor (private translateService: TranslateService,
               private administrativeDocumentsService: AdministrativeDocumentsService) {
  }

  ngOnInit () {
    this.detail = null;
    this.formDetail = new AdministrativeDocumentsFormGroup();
    this.formDetail.removeValidators('type', []);
    this.selected$ = this.administrativeDocumentsService.selected$;
    this.selected$.subscribe(value => {
      this.detail = value;
      this.reset();
    });
  }

  save () {
    this.administrativeDocumentsService.updateName(this.formDetail.name.value);
  }

  reset () {
    this.formDetail.reset();
    if (this.detail) {
      this.formDetail.name.setValue(this.detail.name);
    }
  }

  isDocument () {
    return this.detail.type === DocumentProcessType.DOCUMENT;
  }

  isEmail () {
    return this.detail.type === DocumentProcessType.EMAIL;
  }

  isUpload () {
    return this.detail.type === DocumentProcessType.UPLOAD;
  }
}
