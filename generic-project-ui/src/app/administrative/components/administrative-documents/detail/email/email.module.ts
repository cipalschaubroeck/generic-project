import { HttpClient } from '@angular/common/http';
import { NgModule } from '@angular/core';

import { TranslateLoader, TranslateModule } from '@ngx-translate/core';

import { HttpLoaderFactory } from '../../../../../core/core.module';
import { SharedModule } from '../../../../../shared/shared.module';
import { AdministrativePipeModule } from '../../../../pipes/administrative.pipe.module';
import { AdministrativeDocumentsService } from '../../../../services/administrative-documents.service';
import { AddresseesComponent } from './addressees/addressees.component';
import { AttachmentsComponent } from './attachments/attachments.component';
import { BodyComponent } from './body/body.component';
import { CommunicationComponent } from './communication/communication.component';
import { EmailComponent } from './email.component';

const components = [
  EmailComponent,
  AddresseesComponent,
  AttachmentsComponent,
  BodyComponent,
  CommunicationComponent
];

@NgModule({
  declarations: [
    components
  ],
  imports: [
    AdministrativePipeModule,
    SharedModule,
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    })
  ],
  exports: [
    components
  ],
  providers: [
    AdministrativeDocumentsService
  ]
})
export class EmailModule {
}
