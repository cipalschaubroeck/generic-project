import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddresseesComponent } from './addressees.component';

describe('AddresseesComponent', () => {
  let component: AddresseesComponent;
  let fixture: ComponentFixture<AddresseesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddresseesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddresseesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
