import { HttpClient } from '@angular/common/http';
import { NgModule } from '@angular/core';

import { TranslateLoader, TranslateModule } from '@ngx-translate/core';

import { HttpLoaderFactory } from '../../../../core/core.module';
import { SharedModule } from '../../../../shared/shared.module';
import { AdministrativePipeModule } from '../../../pipes/administrative.pipe.module';
import { AdministrativeDocumentsService } from '../../../services/administrative-documents.service';
import { DetailComponent } from './detail.component';
import { DocumentModule } from './document/document.module';
import { EmailModule } from './email/email.module';
import { UploadModule } from './upload/upload.module';

const components = [
  DetailComponent
];

@NgModule({
  declarations: [
    components
  ],
  imports: [
    AdministrativePipeModule,
    DocumentModule,
    EmailModule,
    UploadModule,
    SharedModule,
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    })
  ],
  exports: [
    components
  ],
  providers: [
    AdministrativeDocumentsService
  ]
})
export class DetailModule {
}
