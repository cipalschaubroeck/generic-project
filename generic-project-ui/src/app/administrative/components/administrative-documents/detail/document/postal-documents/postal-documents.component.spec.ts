import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PostalDocumentsComponent } from './postal-documents.component';

describe('PostalDocumentsComponent', () => {
  let component: PostalDocumentsComponent;
  let fixture: ComponentFixture<PostalDocumentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PostalDocumentsComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PostalDocumentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
