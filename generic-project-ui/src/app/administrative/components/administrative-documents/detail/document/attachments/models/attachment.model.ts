export interface AttachmentLinkModel {
  itemInAssociation: string;
  self: string;
}

export interface AttachmentModel {
  name: string;
  links: AttachmentLinkModel;
}
