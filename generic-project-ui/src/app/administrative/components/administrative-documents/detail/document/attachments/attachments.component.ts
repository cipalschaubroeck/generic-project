import { Component, OnInit } from '@angular/core';

import { ConfirmationService } from 'primeng/api';

import { BehaviorSubject, Observable } from 'rxjs';
import { filter, map, mergeMap } from 'rxjs/operators';

import { SimpleTableComponent } from '../../../../../../shared/components/simple-table/simple-table.component';

import { DocumentProcessType } from '../../../../../enums/document-process-type.enum';
import { AdministrativeDocumentsModel } from '../../../../../models/administrative-documents.model';
import { DocumentLinkModel } from '../../../../../models/document-link.model';
import { AdministrativeDocumentsService } from '../../../../../services/administrative-documents.service';
import { AttachmentModel } from './models/attachment.model';
import { AttachmentsService } from './services/attachments.service';

@Component({
  selector: 'app-attachments',
  templateUrl: './attachments.component.html',
  styleUrls: ['./attachments.component.css']
})
export class AttachmentsComponent extends SimpleTableComponent implements OnInit {

  attachments$: BehaviorSubject<AttachmentModel[]>;
  selected$: BehaviorSubject<AttachmentModel>;
  selected: AttachmentModel;

  constructor (private attachmentsService: AttachmentsService,
               private confirmationService: ConfirmationService,
               private administrativeDocumentService: AdministrativeDocumentsService) {
    super();
  }

  ngOnInit () {
    this.selected = null;
    this.selected$ = this.attachmentsService.selected$;
    this.selected$.subscribe(value => this.selected = value);
    this.initAttachments();
    this.initCols([
      {
        field: 'name',
        text: 'ADMINISTRATIVE.DOCUMENTS.DETAIL.DOCUMENT.ATTACHMENTS.DocumentName',
        class: null,
        hasSort: true
      }
    ]);
    this.initButtons([
      {
        icon: 'ui-icon-file-upload',
        disabled: false,
        title: null,
        click: this.uploadFile,
        scope: this
      },
      {
        icon: 'ui-icon-link',
        disabled: new Observable(subscriber => subscriber.next(true)), // TODO CSPROC-1718
        title: null,
        click: this.linkFile,
        scope: this
      },
      {
        icon: 'ui-icon-trash',
        disabled: this.selected$.pipe(map(() => this.isDisabledRemove())),
        title: 'ADMINISTRATIVE.DOCUMENTS.DETAIL.DOCUMENT.ATTACHMENTS.RemoveItemTitle',
        click: this.confirmRemove,
        scope: this
      }
    ]);
    this.initTableConfig({
      containerClass: 'ui-g-12 only-padding-top',
      noResultsMessage: 'ADMINISTRATIVE.DOCUMENTS.DETAIL.DOCUMENT.ATTACHMENTS.NoResults',
      paginator: {
        rows: 10,
        rowsPerPageOptions: [10, 20, 30]
      }
    });
  }

  private initAttachments () {
    this.attachments$ = this.administrativeDocumentService.selected$
      .pipe(filter(documentSelected => documentSelected && documentSelected.type === DocumentProcessType.DOCUMENT))
      .pipe(mergeMap(documentSelected => this.getAllList(documentSelected))) as BehaviorSubject<AttachmentModel[]>;
  }

  private getAllList (documentSelected: AdministrativeDocumentsModel) {
    return this.attachmentsService.getAllList((<DocumentLinkModel>documentSelected.links).attachments);
  }

  uploadFile () {
    this.attachmentsService.showUploadDialog$.next(true);
  }

  linkFile () {
    console.log('TODO CSPROC-1718 link file');
  }

  confirmRemove () {
    this.confirmationService.confirm({
      message: this.translateService.instant('ADMINISTRATIVE.DOCUMENTS.DETAIL.DOCUMENT.ATTACHMENTS.REMOVE.Message'),
      header: this.translateService.instant('ADMINISTRATIVE.DOCUMENTS.DETAIL.DOCUMENT.ATTACHMENTS.REMOVE.Header'),
      acceptLabel: this.translateService.instant('COMMON.BUTTONS.Confirm'),
      rejectLabel: this.translateService.instant('COMMON.BUTTONS.Cancel'),
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.attachmentsService.removeItem();
      }
    });
  }

  isDisabledRemove (): boolean {
    return this.selected === null;
  }
}
