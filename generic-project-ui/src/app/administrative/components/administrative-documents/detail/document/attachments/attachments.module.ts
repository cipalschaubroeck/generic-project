import { HttpClient } from '@angular/common/http';
import { NgModule } from '@angular/core';

import { TranslateLoader, TranslateModule } from '@ngx-translate/core';

import { HttpLoaderFactory } from '../../../../../../core/core.module';
import { SharedModule } from '../../../../../../shared/shared.module';
import { AttachmentsComponent } from './attachments.component';
import { UploadDialogComponent } from './components/upload-dialog/upload-dialog.component';
import { AttachmentsService } from './services/attachments.service';

const components = [
  AttachmentsComponent,
  UploadDialogComponent
];

@NgModule({
  declarations: [
    components
  ],
  imports: [
    SharedModule,
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    })
  ],
  exports: [
    components
  ],
  providers: [
    AttachmentsService
  ]
})
export class AttachmentsModule {
}
