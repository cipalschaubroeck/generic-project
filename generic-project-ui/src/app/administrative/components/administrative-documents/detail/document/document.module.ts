import { HttpClient } from '@angular/common/http';
import { NgModule } from '@angular/core';

import { TranslateLoader, TranslateModule } from '@ngx-translate/core';

import { HttpLoaderFactory } from '../../../../../core/core.module';
import { SharedModule } from '../../../../../shared/shared.module';
import { AdministrativePipeModule } from '../../../../pipes/administrative.pipe.module';
import { AdministrativeDocumentsService } from '../../../../services/administrative-documents.service';
import { AddresseesModule } from './addressees/addressees.module';
import { AttachmentsModule } from './attachments/attachments.module';
import { BodyComponent } from './body/body.component';
import { DocumentComponent } from './document.component';
import { HeadingComponent } from './heading/heading.component';
import { PostalDocumentsComponent } from './postal-documents/postal-documents.component';
import { SigningOfficersComponent } from './signing-officers/signing-officers.component';

const components = [
  DocumentComponent,
  HeadingComponent,
  BodyComponent,
  SigningOfficersComponent,
  PostalDocumentsComponent
];

@NgModule({
  declarations: components,
  imports: [
    AdministrativePipeModule,
    AddresseesModule,
    AttachmentsModule,
    SharedModule,
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    })
  ],
  exports: components,
  providers: [
    AdministrativeDocumentsService
  ]
})
export class DocumentModule {
}
