import { HttpClient } from '@angular/common/http';
import { NgModule } from '@angular/core';

import { TranslateLoader, TranslateModule } from '@ngx-translate/core';

import { HttpLoaderFactory } from '../../../../../../core/core.module';
import { SharedModule } from '../../../../../../shared/shared.module';
import { AddresseesComponent } from './addressees.component';
import { PostalAddresseeService } from './services/postal-addressee.service';

const components = [
  AddresseesComponent
];

@NgModule({
  declarations: components,
  imports: [
    SharedModule,
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    })
  ],
  exports: components,
  providers: [
    PostalAddresseeService
  ]
})
export class AddresseesModule {
}
