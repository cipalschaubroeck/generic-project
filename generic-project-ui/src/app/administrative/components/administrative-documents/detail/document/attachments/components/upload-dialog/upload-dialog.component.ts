import { HttpEvent, HttpEventType, HttpProgressEvent, HttpResponse } from '@angular/common/http';
import { Component, OnInit, ViewChild } from '@angular/core';

import { FileUpload } from 'primeng/primeng';

import { BehaviorSubject } from 'rxjs';

import { DocumentLinkModel } from '../../../../../../../models/document-link.model';

import { AdministrativeDocumentsService } from '../../../../../../../services/administrative-documents.service';
import { AttachmentsService } from '../../services/attachments.service';

@Component({
  selector: 'app-upload-dialog',
  templateUrl: './upload-dialog.component.html',
  styleUrls: ['./upload-dialog.component.css']
})
export class UploadDialogComponent implements OnInit {
  showDialog$: BehaviorSubject<boolean>;
  showDialog: boolean;
  progress: number;

  @ViewChild(FileUpload) fileUpload: FileUpload;

  constructor (private attachmentsService: AttachmentsService,
               private administrativeDocumentService: AdministrativeDocumentsService) {
  }

  ngOnInit () {
    this.showDialog = false;
    this.showDialog$ = this.attachmentsService.showUploadDialog$;
    this.showDialog$.subscribe(value => this.showDialog = value);
  }

  closeDialog () {
    this.fileUpload.clear();
    this.progress = null;
    this.showDialog$.next(false);
  }

  myUploader ($event: any) {
    const url: string = (<DocumentLinkModel>this.administrativeDocumentService.selected$.getValue().links).attachments;
    this.attachmentsService.uploadFiles(url, $event.files).subscribe((httpEvent: HttpEvent<any>) => {
      if (httpEvent.type === HttpEventType.UploadProgress) {
        this.handleProgress(httpEvent);
      } else if (httpEvent instanceof HttpResponse) {
        this.attachmentsService.uploadFilesSuccess(httpEvent.body);
        this.closeDialog();
      }
    });
  }

  private handleProgress (event: HttpProgressEvent) {
    this.progress = Math.round((event.loaded * 100) / event.total);
  }

}
