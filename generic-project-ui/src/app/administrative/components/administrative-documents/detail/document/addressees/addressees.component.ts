import { Component, OnInit } from '@angular/core';

import { ConfirmationService } from 'primeng/api';

import { BehaviorSubject } from 'rxjs';
import { filter, map, mergeMap } from 'rxjs/operators';

import { Utils } from '../../../../../../shared/classes/utils';

import { ConstantsEnum } from '../../../../../../shared/enums/constants.enum';
import { ProjectionEnum } from '../../../../../../shared/enums/projection.enum';
import { DocumentProcessType } from '../../../../../enums/document-process-type.enum';
import { AdministrativeDocumentsModel } from '../../../../../models/administrative-documents.model';
import { DocumentLinkModel } from '../../../../../models/document-link.model';
import { AdministrativeDocumentsService } from '../../../../../services/administrative-documents.service';
import { PostalAddresseeFormGroup } from './models/postal-addressee-form-group';
import { PostalAddresseeModel } from './models/postal-addressee.model';
import { PostalAddresseeService } from './services/postal-addressee.service';
import { SimpleTableComponent } from '../../../../../../shared/components/simple-table/simple-table.component';

@Component({
  selector: 'app-addressees',
  templateUrl: './addressees.component.html',
  styleUrls: ['./addressees.component.css']
})
export class AddresseesComponent extends SimpleTableComponent implements OnInit {
  addressees$: BehaviorSubject<PostalAddresseeModel[]>;
  selected$: BehaviorSubject<PostalAddresseeModel>;
  selected: PostalAddresseeModel;
  formDetail: PostalAddresseeFormGroup;

  constructor (private postalAddressService: PostalAddresseeService,
               private confirmationService: ConfirmationService,
               private administrativeDocumentService: AdministrativeDocumentsService) {
    super();
  }

  ngOnInit () {
    this.selected = null;
    this.selected$ = this.postalAddressService.selected$;
    this.selected$.subscribe(value => this.selected = value);
    this.initAddressees();
    this.initCols([
      {
        field: 'name',
        text: 'ADMINISTRATIVE.DOCUMENTS.DETAIL.DOCUMENT.ADDRESSEES.Name',
        class: null,
        hasSort: true
      },
      {
        field: 'contactName',
        text: 'ADMINISTRATIVE.DOCUMENTS.DETAIL.DOCUMENT.ADDRESSEES.ContactName',
        class: null,
        hasSort: true
      },
      {
        field: 'email',
        text: 'ADMINISTRATIVE.DOCUMENTS.DETAIL.DOCUMENT.ADDRESSEES.Email',
        class: null,
        hasSort: true
      },
      {
        field: 'phone',
        text: 'ADMINISTRATIVE.DOCUMENTS.DETAIL.DOCUMENT.ADDRESSEES.Phone',
        class: null,
        hasSort: true
      }
    ]);
    this.initButtons([
      {
        icon: 'ui-icon-plus',
        disabled: false,
        title: null,
        click: this.showAddDialog,
        scope: this
      },
      {
        icon: 'ui-icon-trash',
        disabled: this.selected$.pipe(map(() => this.isDisabledRemove())),
        title: null,
        click: this.confirmRemove,
        scope: this
      }
    ]);
    this.initTableConfig({
      containerClass: 'ui-g-12 only-padding-top',
      noResultsMessage: 'ADMINISTRATIVE.DOCUMENTS.DETAIL.DOCUMENT.ADDRESSEES.NoResults',
      paginator: {
        rows: 10,
        rowsPerPageOptions: [10, 20, 30]
      },
      sortField: this.cols[0].field,
      sortOrder: -1
    });
    this.formDetail = new PostalAddresseeFormGroup();
  }

  private initAddressees () {
    this.addressees$ = this.administrativeDocumentService.selected$
      .pipe(filter(documentSelected => documentSelected && documentSelected.type === DocumentProcessType.DOCUMENT))
      .pipe(mergeMap(documentSelected => this.getAllList(documentSelected))) as BehaviorSubject<PostalAddresseeModel[]>;
  }

  private getAllList (documentSelected: AdministrativeDocumentsModel) {
    return this.postalAddressService.getAllList((<DocumentLinkModel>documentSelected.links).addresseeList
      + Utils.buildQuery([
        {
          key: ConstantsEnum.PROJECTION.valueOf(),
          value: ProjectionEnum.POSTAL_ADDRESSEE_TABLE_ITEM.valueOf()
        }
      ]));
  }

  confirmRemove () {
    this.confirmationService.confirm({
      message: this.translateService.instant('ADMINISTRATIVE.DOCUMENTS.DETAIL.DOCUMENT.ADDRESSEES.REMOVE.Message'),
      header: this.translateService.instant('ADMINISTRATIVE.DOCUMENTS.DETAIL.DOCUMENT.ADDRESSEES.REMOVE.Header'),
      acceptLabel: this.translateService.instant('COMMON.BUTTONS.Confirm'),
      rejectLabel: this.translateService.instant('COMMON.BUTTONS.Cancel'),
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.postalAddressService.removeItem();
      }
    });
  }

  isDisabledRemove (): boolean {
    return this.selected === null;
  }

  showAddDialog () {
    console.log('TODO CSPROC-1751 implement showAddDialog');
  }

  save () {
    console.log('TODO CSPROC-1709 save');
  }

  reset () {
    console.log('TODO CSPROC-1709 reset');
  }

}
