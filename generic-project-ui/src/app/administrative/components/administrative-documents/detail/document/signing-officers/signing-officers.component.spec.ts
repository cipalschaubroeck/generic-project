import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SigningOfficersComponent } from './signing-officers.component';

describe('SigningOfficersComponent', () => {
  let component: SigningOfficersComponent;
  let fixture: ComponentFixture<SigningOfficersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SigningOfficersComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SigningOfficersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
