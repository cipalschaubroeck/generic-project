import { TestBed } from '@angular/core/testing';

import { PostalAddresseeService } from './postal-addressee.service';

describe('PostalAddresseeService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PostalAddresseeService = TestBed.get(PostalAddresseeService);
    expect(service).toBeTruthy();
  });
});
