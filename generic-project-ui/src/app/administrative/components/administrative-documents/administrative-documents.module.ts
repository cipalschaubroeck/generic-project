import { HttpClient } from '@angular/common/http';
import { NgModule } from '@angular/core';

import { TranslateLoader, TranslateModule } from '@ngx-translate/core';

import { HttpLoaderFactory } from '../../../core/core.module';
import { SharedModule } from '../../../shared/shared.module';
import { AdministrativePipeModule } from '../../pipes/administrative.pipe.module';
import { AdministrativeDocumentsService } from '../../services/administrative-documents.service';
import { AdministrativeDocumentsComponent } from './administrative-documents.component';
import { DetailModule } from './detail/detail.module';
import { SidebarModule } from './sidebar/sidebar.module';

const components = [
  AdministrativeDocumentsComponent
];

@NgModule({
  declarations: [
    components
  ],
  imports: [
    AdministrativePipeModule,
    SidebarModule,
    DetailModule,
    SharedModule,
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    })
  ],
  exports: [
    components
  ],
  providers: [
    AdministrativeDocumentsService
  ]
})
export class AdministrativeDocumentsModule {
}
