import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DuplicateItemDialogComponent } from './duplicate-item-dialog.component';

describe('DuplicateItemDialogComponent', () => {
  let component: DuplicateItemDialogComponent;
  let fixture: ComponentFixture<DuplicateItemDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DuplicateItemDialogComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DuplicateItemDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
