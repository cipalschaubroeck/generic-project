import { Component, OnInit } from '@angular/core';

import { BehaviorSubject } from 'rxjs';

import { AdministrativeDocumentsService } from '../../../../services/administrative-documents.service';
import { AdministrativeDocumentsFormGroup } from '../administrative-documents-form-group';

@Component({
  selector: 'app-duplicate-item-dialog',
  templateUrl: './duplicate-item-dialog.component.html',
  styleUrls: ['./duplicate-item-dialog.component.css']
})
export class DuplicateItemDialogComponent implements OnInit {

  showDialog$: BehaviorSubject<boolean>;
  showDialog: boolean;
  formDuplicate: AdministrativeDocumentsFormGroup;

  constructor (private administrativeDocumentsService: AdministrativeDocumentsService) {
  }

  ngOnInit () {
    this.formDuplicate = new AdministrativeDocumentsFormGroup();
    this.formDuplicate.removeValidators('type', []);
    this.showDialog = false;
    this.showDialog$ = this.administrativeDocumentsService.showDuplicateDialog$;
    this.showDialog$.subscribe(value => this.showDialog = value);
  }

  duplicateItem () {
    this.administrativeDocumentsService.duplicateItem(this.formDuplicate.name.value);
    this.closeDialog();
  }

  closeDialog () {
    this.formDuplicate.reset();
    this.showDialog$.next(false);
  }

}
