import { Component, OnInit } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';

import { SelectItem } from 'primeng/api';

import { BehaviorSubject } from 'rxjs';

import { DocumentProcessType } from '../../../../enums/document-process-type.enum';
import { AdministrativeDocumentsService } from '../../../../services/administrative-documents.service';
import { AdministrativeDocumentsFormGroup } from '../administrative-documents-form-group';

@Component({
  selector: 'app-add-item-dialog',
  templateUrl: './add-item-dialog.component.html',
  styleUrls: ['./add-item-dialog.component.css']
})
export class AddItemDialogComponent implements OnInit {

  showDialog$: BehaviorSubject<boolean>;
  showDialog: boolean;
  formAdd: AdministrativeDocumentsFormGroup;
  types: SelectItem[];

  constructor (private administrativeDocumentsService: AdministrativeDocumentsService,
               private translateService: TranslateService) {
  }

  ngOnInit () {
    this.formAdd = new AdministrativeDocumentsFormGroup();
    this.showDialog = false;
    this.showDialog$ = this.administrativeDocumentsService.showAddDialog$;
    this.showDialog$.subscribe(value => this.showDialog = value);
    this.initTypes();
  }

  addItem () {
    const name = this.formAdd.name.value;
    const type = this.formAdd.type.value;
    this.administrativeDocumentsService.addItem({
      name: name,
      type: type,
      date: null,
      status: null,
      links: null
    });
    this.closeDialog();
  }

  closeDialog () {
    this.formAdd.reset();
    this.showDialog$.next(false);
  }

  private initTypes () {
    this.types = [
      {
        label: this.translateService.instant('ADMINISTRATIVE.DOCUMENTS.ADD.TYPE.Document'),
        value: DocumentProcessType.DOCUMENT
      },
      {
        label: this.translateService.instant('ADMINISTRATIVE.DOCUMENTS.ADD.TYPE.Email'),
        value: DocumentProcessType.EMAIL
      },
      {
        label: this.translateService.instant('ADMINISTRATIVE.DOCUMENTS.ADD.TYPE.Upload'),
        value: DocumentProcessType.UPLOAD
      }
    ];
  }
}
