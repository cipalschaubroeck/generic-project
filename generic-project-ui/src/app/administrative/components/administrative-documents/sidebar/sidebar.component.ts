import { Component, OnInit, ViewChild } from '@angular/core';

import { ConfirmationService } from 'primeng/api';

import { BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';

import { FilterTableComponent } from '../../../../shared/components/filter-table/filter-table.component';

import { GenericTableComponent } from '../../../../shared/components/generic-table/generic-table.component';
import { DocumentProcessType } from '../../../enums/document-process-type.enum';
import { DocumentStatus } from '../../../enums/document-status.enum';
import { AdministrativeDocumentsModel } from '../../../models/administrative-documents.model';
import { AdministrativeDocumentsService } from '../../../services/administrative-documents.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent extends FilterTableComponent implements OnInit {

  documents$: BehaviorSubject<AdministrativeDocumentsModel[]>;
  selected$: BehaviorSubject<AdministrativeDocumentsModel>;
  selected: AdministrativeDocumentsModel;

  @ViewChild(GenericTableComponent) table: GenericTableComponent;

  constructor (private administrativeDocumentsService: AdministrativeDocumentsService,
               private confirmationService: ConfirmationService) {
    super();
  }

  ngOnInit () {
    this.selected = null;
    this.selected$ = this.administrativeDocumentsService.selected$;
    this.selected$.subscribe(value => this.selected = value);
    this.documents$ = this.administrativeDocumentsService.getAllList();
    this.initCols([
      {
        field: 'date',
        text: 'ADMINISTRATIVE.DOCUMENTS.Date',
        class: 'no-padding small-size',
        hasSort: true
      },
      {
        field: 'name',
        text: 'ADMINISTRATIVE.DOCUMENTS.DocumentProcess',
        class: null,
        hasSort: true
      },
      {
        field: 'status',
        text: 'ADMINISTRATIVE.DOCUMENTS.Status',
        class: 'no-padding icon-size',
        hasSort: false
      },
      {
        field: 'type',
        text: null,
        class: 'hidden',
        hasSort: false
      }
    ]);
    this.initFilters([
      {
        label: 'ADMINISTRATIVE.DOCUMENTS.FILTER.Document',
        value: DocumentProcessType.DOCUMENT.valueOf()
      },
      {
        label: 'ADMINISTRATIVE.DOCUMENTS.FILTER.Email',
        value: DocumentProcessType.EMAIL.valueOf()
      },
      {
        label: 'ADMINISTRATIVE.DOCUMENTS.FILTER.Upload',
        value: DocumentProcessType.UPLOAD.valueOf()
      }
    ]);
    this.initFiltersConfig({
      defaultLabel: 'ADMINISTRATIVE.DOCUMENTS.FILTER.All',
      scope: this,
      onChange: this.onFilterChange
    });
    this.initButtons([
      {
        icon: 'ui-icon-plus',
        disabled: false,
        title: null,
        click: this.showAddDialog,
        scope: this
      },
      {
        icon: 'ui-icon-content-copy',
        disabled: this.selected$.pipe(map(selectedItem => selectedItem === null)),
        title: 'ADMINISTRATIVE.DOCUMENTS.DuplicateItemTitle',
        click: this.showDuplicateDialog,
        scope: this
      },
      {
        icon: 'ui-icon-trash',
        disabled: this.selected$.pipe(map(() => this.isDisabledRemove())),
        title: 'ADMINISTRATIVE.DOCUMENTS.RemoveItemTitle',
        click: this.confirmRemove,
        scope: this
      }
    ]);
    this.initTableConfig({
      containerClass: 'ui-g-12 smaller no-padding-right',
      noResultsMessage: 'ADMINISTRATIVE.DOCUMENTS.NoResults',
      paginator: {
        rows: 10,
        rowsPerPageOptions: [10, 20, 30]
      },
      sortField: this.cols[0].field,
      sortOrder: -1
    });
  }

  onFilterChange ($event) {
    this.selected$.next(null);
    this.table.filter($event.value, this.cols[3].field, 'in');
  }

  confirmRemove () {
    this.confirmationService.confirm({
      message: this.translateService.instant('ADMINISTRATIVE.DOCUMENTS.REMOVE.Message'),
      header: this.translateService.instant('ADMINISTRATIVE.DOCUMENTS.REMOVE.Header'),
      acceptLabel: this.translateService.instant('COMMON.BUTTONS.Confirm'),
      rejectLabel: this.translateService.instant('COMMON.BUTTONS.Cancel'),
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.administrativeDocumentsService.removeItem();
      }
    });
  }

  isDisabledRemove (): boolean {
    return this.selected === null || this.selected.status !== DocumentStatus.IN_PROGRESS;
  }

  showAddDialog () {
    this.administrativeDocumentsService.showAddDialog$.next(true);
  }

  showDuplicateDialog () {
    this.selected$.next(this.selected);
    this.administrativeDocumentsService.showDuplicateDialog$.next(true);
  }

}
