import { HttpClient } from '@angular/common/http';
import { NgModule } from '@angular/core';

import { TranslateLoader, TranslateModule } from '@ngx-translate/core';

import { HttpLoaderFactory } from '../../../../core/core.module';
import { SharedModule } from '../../../../shared/shared.module';
import { AdministrativePipeModule } from '../../../pipes/administrative.pipe.module';
import { AdministrativeDocumentsService } from '../../../services/administrative-documents.service';
import { AddItemDialogComponent } from './add-item-dialog/add-item-dialog.component';
import { DuplicateItemDialogComponent } from './duplicate-item-dialog/duplicate-item-dialog.component';
import { SidebarComponent } from './sidebar.component';

const components = [
  AddItemDialogComponent,
  DuplicateItemDialogComponent,
  SidebarComponent
];

@NgModule({
  declarations: [
    components
  ],
  imports: [
    AdministrativePipeModule,
    SharedModule,
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    })
  ],
  exports: [
    components
  ],
  providers: [
    AdministrativeDocumentsService
  ]
})
export class SidebarModule {
}
