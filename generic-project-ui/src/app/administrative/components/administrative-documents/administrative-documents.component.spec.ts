import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdministrativeDocumentsComponent } from './administrative-documents.component';

describe('administrativeDocumentsComponent', () => {
  let component: AdministrativeDocumentsComponent;
  let fixture: ComponentFixture<AdministrativeDocumentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AdministrativeDocumentsComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdministrativeDocumentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
