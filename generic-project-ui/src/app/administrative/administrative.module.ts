import { HttpClient } from '@angular/common/http';
import { NgModule } from '@angular/core';

import { TranslateLoader, TranslateModule } from '@ngx-translate/core';

import { HttpLoaderFactory } from '../core/core.module';
import { SharedModule } from '../shared/shared.module';
import { AdministrativeRoutingModule } from './administrative.routing';
import { AdministrativeDocumentsModule } from './components/administrative-documents/administrative-documents.module';
import { BailComponent } from './components/bail/bail.component';

import { AdministrativeComponent } from './containers/administrative/administrative.component';
import { AdministrativePipeModule } from './pipes/administrative.pipe.module';
import { AdministrativeDocumentsService } from './services/administrative-documents.service';

const components = [
  AdministrativeComponent,
  BailComponent
];

@NgModule({
  declarations: components,
  imports: [
    AdministrativePipeModule,
    AdministrativeDocumentsModule,
    SharedModule,
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    AdministrativeRoutingModule
  ],
  providers: [
    AdministrativeDocumentsService
  ]
})
export class AdministrativeModule {
}
