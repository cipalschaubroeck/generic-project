import { Component, OnInit } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';

import { MenuItem } from 'primeng/primeng';

const administrativeTabs: MenuItem[] = [
  {
    label: 'ADMINISTRATIVE.Bail',
    title: null,
    routerLink: '/administrative/bail'
  },
  {
    label: 'ADMINISTRATIVE.Documents',
    title: null,
    routerLink: '/administrative/documents'
  }
];

@Component({
  selector: 'app-administrative',
  templateUrl: './administrative.component.html',
  styleUrls: ['./administrative.component.css']
})
export class AdministrativeComponent implements OnInit {

  tabItems: MenuItem[];

  constructor (private translateService: TranslateService) {
    this.createTabs();
  }

  ngOnInit () {
  }

  private createTabs () {
    this.tabItems = [];
    for (const tab of administrativeTabs) {
      const label = this.translateService.instant(tab.label);
      const title = tab.title !== null ? this.translateService.instant(tab.title) : null;
      this.tabItems.push({
        label: label,
        title: title,
        routerLink: tab.routerLink
      });
    }
  }
}
