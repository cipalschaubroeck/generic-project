import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AdministrativeDocumentsComponent } from './components/administrative-documents/administrative-documents.component';
import { BailComponent } from './components/bail/bail.component';

import { AdministrativeComponent } from './containers/administrative/administrative.component';

const routes: Routes = [
  {
    path: '',
    component: AdministrativeComponent,
    data: {
      breadcrumb: 'PAGE_SPECIFIC.Administrative'
    },
    children: [
      {
        path: '',
        redirectTo: 'documents',
        pathMatch: 'full'
      },
      {
        path: 'bail',
        component: BailComponent,
        data: {
          breadcrumb: 'ADMINISTRATIVE.Bail'
        }
      },
      {
        path: 'documents',
        component: AdministrativeDocumentsComponent,
        data: {
          breadcrumb: 'ADMINISTRATIVE.Documents'
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdministrativeRoutingModule {
}
