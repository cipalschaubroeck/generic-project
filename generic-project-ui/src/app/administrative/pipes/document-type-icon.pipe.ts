import { Pipe, PipeTransform } from '@angular/core';

import { DocumentProcessType } from '../enums/document-process-type.enum';

@Pipe({
  name: 'documentTypeIcon'
})
export class DocumentTypeIconPipe implements PipeTransform {

  transform (value: DocumentProcessType): string {
    switch (value) {
      case DocumentProcessType.DOCUMENT: {
        return 'assignment';
      }
      case DocumentProcessType.EMAIL: {
        return 'email';
      }
      case DocumentProcessType.UPLOAD: {
        return 'file_upload';
      }
      default:
        throw new Error(`documentTypeIcon unable to map a value for: ${value}`);
    }
  }

}
