import { Pipe, PipeTransform } from '@angular/core';

import { DocumentStatus } from '../enums/document-status.enum';

@Pipe({
  name: 'documentStatusIcon'
})
export class DocumentStatusIconPipe implements PipeTransform {

  transform (value: DocumentStatus): string {
    switch (value) {
      case DocumentStatus.DISPATCHED: {
        return 'email';
      }
      case DocumentStatus.FOR_SIGNATURE: {
        return 'remove_red_eye';
      }
      case DocumentStatus.IN_PROGRESS: {
        return 'edit';
      }
      case DocumentStatus.UPLOADED: {
        return 'file_upload';
      }
      case DocumentStatus.VALIDATED: {
        return 'check_box';
      }
      default:
        throw new Error(`documentTypeIcon unable to map a value for: ${value}`);
    }
  }

}
