import { NgModule } from '@angular/core';

import { DocumentStatusIconPipe } from './document-status-icon.pipe';
import { DocumentTypeIconPipe } from './document-type-icon.pipe';

const pipes = [
  DocumentTypeIconPipe,
  DocumentStatusIconPipe
];

@NgModule({
  declarations: pipes,
  exports: pipes
})
export class AdministrativePipeModule {
}
