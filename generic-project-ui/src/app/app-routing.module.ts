import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { environment } from '../environments/environment';

import { RedirectToHomepageGuard } from './core/guards/redirect-to-homepage.guard';
import { DashboardComponent } from './dashboard/dashboard.component';
import { HomeComponent } from './home/home.component';
import { authenticateIfActive } from './keycloak/keycloak-initializer';

const routes: Routes = [
  {
    path: 'home',
    component: HomeComponent,
    data: {
      breadcrumb: 'PAGE_SPECIFIC.Home'
    },
    canActivate: authenticateIfActive([])
  },
  {
    path: 'administrative',
    loadChildren: './administrative/administrative.module#AdministrativeModule',
    data: {
      breadcrumb: 'PAGE_SPECIFIC.Administrative'
    },
    canActivate: authenticateIfActive([])
  },
  {
    path: 'dashboard',
    component: DashboardComponent,
    data: {
      breadcrumb: 'PAGE_SPECIFIC.Dashboard'
    },
    canActivate: authenticateIfActive([])
  },
  {
    path: '',
    pathMatch: 'full',
    redirectTo: '', // redundant, but this is required for the route configuration to work
    canActivate: [RedirectToHomepageGuard]
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(
      routes,
      {enableTracing: environment.enableTracing} // <-- debugging purposes only
    )
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {
}
