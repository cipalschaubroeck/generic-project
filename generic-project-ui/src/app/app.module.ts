import { HttpClientModule } from '@angular/common/http';
import { LOCALE_ID, NgModule, Provider } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { CoreModule } from './core/core.module';
import { DashboardComponent } from './dashboard/dashboard.component';
import { HomeComponent } from './home/home.component';
import { importKeycloakIfActive, provideKeycloakIfActive } from './keycloak/keycloak-initializer';

import { SharedModule } from './shared/shared.module';

const providers: Provider[] = [
  {provide: LOCALE_ID, useValue: 'nl-BE'}
];
provideKeycloakIfActive(providers);

const imports = [
  BrowserModule,
  HttpClientModule,
  CoreModule,
  SharedModule,
  AppRoutingModule
];
importKeycloakIfActive(imports);

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    HomeComponent
  ],
  imports: imports,
  providers: providers,
  bootstrap: [AppComponent]
})
export class AppModule {
}
