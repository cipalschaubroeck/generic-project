import { Component } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';

import { AppInjector } from '../../services/app-injector.service';
import { GenericTableButtonModel, GenericTableColumnModel, GenericTableConfigModel } from '../generic-table/models/generic-table.models';

@Component({
  template: ''
})
export class SimpleTableComponent {

  cols: GenericTableColumnModel[];
  buttons: GenericTableButtonModel[];
  tableConfig: GenericTableConfigModel;

  protected translateService: TranslateService;

  constructor () {
    const injector = AppInjector.getInjector();
    this.translateService = injector.get(TranslateService);
  }

  initCols (configColumnModel: GenericTableColumnModel[]) {
    configColumnModel.forEach(item => {
      if (item.text) {
        item.text = this.translateService.instant(item.text);
      }
    });
    this.cols = configColumnModel;
  }

  initButtons (configButtonModel: GenericTableButtonModel[]) {
    configButtonModel.forEach(item => {
      if (item.title) {
        item.title = this.translateService.instant(item.title);
      }
    });
    this.buttons = configButtonModel;
  }

  initTableConfig (configModel: GenericTableConfigModel) {
    configModel.noResultsMessage = this.translateService.instant(configModel.noResultsMessage);
    this.tableConfig = configModel;
  }
}
