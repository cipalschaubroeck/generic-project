import { Observable } from 'rxjs';

export interface GenericTableButtonModel {
  disabled: boolean | Observable<boolean>;
  title: string;
  icon: string;
  click: (...any) => any;
  scope: any;
}

export interface GenericTableColumnModel {
  field: string;
  text: string;
  class: string;
  hasSort: boolean;
}

export interface GenericTableConfigModel {
  containerClass?: string;
  noResultsMessage: string;
  paginator?: GenericTablePaginatorModel;
  sortField?: string;
  sortOrder?: number;
}

export interface GenericTableFilterModel {
  label: string;
  value: string;
}

export interface FilterConfigModel {
  defaultLabel: string;
  onChange: ($event, tableName) => any;
  scope: any;
}

export interface GenericTablePaginatorModel {
  rows: number;
  rowsPerPageOptions: number[];
}

