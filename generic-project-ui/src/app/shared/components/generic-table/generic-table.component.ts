import { Component, Input, OnInit, TemplateRef, ViewChild } from '@angular/core';

import { Table } from 'primeng/table';

import { BehaviorSubject } from 'rxjs';

import {
  FilterConfigModel,
  GenericTableButtonModel,
  GenericTableColumnModel,
  GenericTableConfigModel,
  GenericTableFilterModel
} from './models/generic-table.models';

@Component({
  selector: 'app-generic-table',
  templateUrl: './generic-table.component.html',
  styleUrls: ['./generic-table.component.css']
})
export class GenericTableComponent implements OnInit {

  @Input()
  buttons: GenericTableButtonModel[];

  @Input()
  filters?: GenericTableFilterModel[];

  @Input()
  filterConfig?: FilterConfigModel;

  @Input()
  tableConfig: GenericTableConfigModel;

  @Input()
  columns: GenericTableColumnModel[];

  @Input()
  elements$: BehaviorSubject<any[]>;

  @Input()
  selected$: BehaviorSubject<any>;

  @Input()
  templateRef: TemplateRef<any>;

  @Input()
  selected: any;

  @ViewChild('genericTable') table: Table;

  constructor () {
  }

  ngOnInit () {
  }

  hasButtons () {
    return this.buttons && this.buttons.length > 0;
  }

  hasFilters () {
    return this.filters && this.filters.length > 0;
  }

  filter (value, field, matchMode) {
    this.table.filter(value, field, matchMode);
  }
}
