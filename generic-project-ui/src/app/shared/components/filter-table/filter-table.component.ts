import { Component } from '@angular/core';

import { FilterConfigModel, GenericTableFilterModel } from '../generic-table/models/generic-table.models';
import { SimpleTableComponent } from '../simple-table/simple-table.component';

@Component({
  template: ''
})
export class FilterTableComponent extends SimpleTableComponent {

  filters: GenericTableFilterModel[];
  filterConfig: FilterConfigModel;

  constructor () {
    super();
  }

  initFilters (filterModel: GenericTableFilterModel[]) {
    filterModel.forEach(filter => filter.label = this.translateService.instant(filter.label));
    this.filters = filterModel;
  }

  initFiltersConfig (filterConfigModel: FilterConfigModel) {
    filterConfigModel.defaultLabel = this.translateService.instant(filterConfigModel.defaultLabel);
    this.filterConfig = filterConfigModel;
  }
}
