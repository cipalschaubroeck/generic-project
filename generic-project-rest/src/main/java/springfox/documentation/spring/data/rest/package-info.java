/**
 * springfox's 2.9.2 (latest at the time of writing) library for data rest includes several
 * incompatibilities with spring data rest 3.1.4. The classes in this package correct those problems
 * so we can use swagger without reducing our the functional abilities by using the compatible version
 * of spring.
 * <p>
 * Once the springfox team solves these problems, we could delete this package and, in SwaggerConfig,
 * import SpringDataRestConfiguration from springfox
 */
package springfox.documentation.spring.data.rest;