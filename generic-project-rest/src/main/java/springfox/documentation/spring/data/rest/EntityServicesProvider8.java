package springfox.documentation.spring.data.rest;

import com.fasterxml.classmate.TypeResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mapping.context.PersistentEntities;
import org.springframework.data.repository.support.Repositories;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.core.mapping.ResourceMappings;
import org.springframework.data.rest.core.mapping.ResourceMetadata;
import org.springframework.data.rest.webmvc.mapping.Associations;
import springfox.documentation.RequestHandler;
import springfox.documentation.spi.service.RequestHandlerProvider;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;


public class EntityServicesProvider8 implements RequestHandlerProvider {
    @Autowired
    private RepositoryRestConfiguration configuration;
    @Autowired
    private ResourceMappings mappings;
    @Autowired
    private Repositories repositories;
    @Autowired
    private TypeResolver typeResolver;
    @Autowired
    private PersistentEntities entities;
    @Autowired
    private Associations associations;

    @Autowired(required = false)
    private RequestHandlerExtractorConfiguration extractorConfiguration;

    @PostConstruct
    public void init() {
        if (extractorConfiguration == null) {
            extractorConfiguration = new DefaultExtractorConfiguration();
        }
    }

    @Override
    public List<RequestHandler> requestHandlers() {
        List<EntityContext> contexts = new ArrayList<>();
        repositories.forEach(aClass -> repositories.getRepositoryInformationFor(aClass).ifPresent(
                repositoryInformation -> repositories.getRepositoryFor(aClass).ifPresent(repositoryInstance -> {
                    ResourceMetadata resource = mappings.getMetadataFor(aClass);
                    if (resource.isExported()) {
                        contexts.add(new EntityContext8(
                                typeResolver,
                                configuration,
                                repositoryInformation,
                                repositoryInstance,
                                resource,
                                mappings,
                                entities,
                                associations,
                                extractorConfiguration));
                    }
                })));

        List<RequestHandler> handlers = new ArrayList<>();
        contexts.forEach(
                entityContext -> extractorConfiguration.getEntityExtractors().stream()
                        .flatMap(input -> input.extract(entityContext).stream()).forEach(handlers::add));
        return handlers;
    }
}
