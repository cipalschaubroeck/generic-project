/*
 *
 *  Copyright 2017-2019 the original author or authors.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 *
 */
package springfox.documentation.spring.data.rest;

import com.fasterxml.classmate.TypeResolver;
import org.springframework.data.mapping.PersistentEntity;
import org.springframework.data.repository.core.CrudMethods;
import org.springframework.data.repository.core.RepositoryMetadata;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.method.HandlerMethod;
import springfox.documentation.RequestHandler;
import springfox.documentation.service.ResolvedMethodParameter;

import java.lang.reflect.Method;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;

import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Sets.newHashSet;
import static springfox.documentation.spring.data.rest.RequestExtractionUtils.actionName;
import static springfox.documentation.spring.data.rest.RequestExtractionUtils.pathAnnotations;

public class EntityDeleteExtractor8 implements EntityOperationsExtractor {
    @Override
    public List<RequestHandler> extract(EntityContext context) {
        return ExtractorSupport.extract(context, this::fillHandlers);
    }

    private void fillHandlers(EntityContext context, List<RequestHandler> handlers, PersistentEntity<?, ?> entity, CrudMethods crudMethods, TypeResolver resolver, RepositoryMetadata repository) {
        Optional<Method> deleteMethod = crudMethods.getDeleteMethod();
        if (crudMethods.hasDelete() && deleteMethod.isPresent()) {
            Method actualDeleteMethod = deleteMethod.get();
            HandlerMethod handler = new HandlerMethod(
                    context.getRepositoryInstance(),
                    actualDeleteMethod);
            ActionSpecification spec = new ActionSpecification(
                    actionName(entity, actualDeleteMethod),
                    String.format("%s%s/{id}",
                            context.basePath(),
                            context.resourcePath()),
                    newHashSet(RequestMethod.DELETE),
                    new HashSet<>(),
                    new HashSet<>(),
                    handler,
                    newArrayList(new ResolvedMethodParameter(
                            0,
                            "id",
                            pathAnnotations("id", handler),
                            resolver.resolve(repository.getIdType()))),
                    resolver.resolve(Void.TYPE));
            handlers.add(new SpringDataRestRequestHandler(context, spec));
        }
    }
}
