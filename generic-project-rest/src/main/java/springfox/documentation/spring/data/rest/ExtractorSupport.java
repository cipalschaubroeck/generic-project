package springfox.documentation.spring.data.rest;

import com.fasterxml.classmate.TypeResolver;
import org.springframework.data.mapping.PersistentEntity;
import org.springframework.data.repository.core.CrudMethods;
import org.springframework.data.repository.core.RepositoryMetadata;
import springfox.documentation.RequestHandler;

import java.util.ArrayList;
import java.util.List;

class ExtractorSupport {
    private ExtractorSupport() {
        // utility class
    }

    static List<RequestHandler> extract(EntityContext context, HandlerFiller filler) {
        final List<RequestHandler> handlers = new ArrayList<>();
        final PersistentEntity<?, ?> entity = context.entity();
        CrudMethods crudMethods = context.crudMethods();
        TypeResolver resolver = context.getTypeResolver();
        RepositoryMetadata repository = context.getRepositoryMetadata();
        filler.fill(context, handlers, entity, crudMethods, resolver, repository);
        return handlers;
    }

    interface HandlerFiller {
        void fill(EntityContext context, List<RequestHandler> handlers, PersistentEntity<?, ?> entity,
                  CrudMethods crudMethods, TypeResolver resolver, RepositoryMetadata repository);
    }
}
