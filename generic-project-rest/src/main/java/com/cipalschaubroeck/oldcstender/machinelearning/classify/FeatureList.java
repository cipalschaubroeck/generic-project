package com.cipalschaubroeck.oldcstender.machinelearning.classify;

import lombok.Getter;
import lombok.Setter;

import java.util.HashMap;

@Getter
@Setter
public class FeatureList extends HashMap<Integer, Feature> {
    private HashMap<String, Integer> featureIndex = new HashMap<String, Integer>();

    public Feature getClassFeature() {
        return get(size() - 1);
    }
}
