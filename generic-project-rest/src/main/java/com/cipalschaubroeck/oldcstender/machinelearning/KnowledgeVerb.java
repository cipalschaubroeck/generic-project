package com.cipalschaubroeck.oldcstender.machinelearning;

import com.cipalschaubroeck.oldcstender.machinelearning.shared.MachineLearningTextUtils;
import lombok.Getter;
import lombok.Setter;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * A verb representation
 */
@Getter
@Setter
public class KnowledgeVerb {
    private List<String> synonymList;
    private KnowledgeBase knowledgeBase;

    public KnowledgeVerb(String info, KnowledgeBase knowledgeBase) {
        try {
            String[] array = info.split("(?<!\\\\),");
            for (int i = 0; i < array.length; i++) {
                array[i] = array[i].replaceAll("\\\\,", ",");
            }
            synonymList = Arrays.asList(array);
            this.knowledgeBase = knowledgeBase;
        } catch (Exception e) {
            System.out.println("error in VerbPhrase db "+info);
            e.printStackTrace();
            System.exit(0);
        }
    }

    public String getVerb() {
        return synonymList.get(0);
    }

    public AbstractMap.SimpleEntry<String, Integer> nextVerb(String text, List<KnowledgeVerb> verbPhraseList) {
        for (KnowledgeVerb verbPhrase : verbPhraseList) {
            for (String syn : verbPhrase.synonymList) {
                if (text.startsWith(syn+" ") || text.startsWith(syn+":")) return new AbstractMap.SimpleEntry(verbPhrase.synonymList.get(0),syn.length()+1);
            }
        }
        return null;
    }

    /**
     * Find relations in the text based on rules.
     * eg. Het bouwen van een huis in het park.
     * All verbs with the suffix " van een NE" will be a relation -> REL_bouwen_huis
     *
     * @param text
     * @param namedEntityList
     * @param namedAdjectiveList
     * @param adjectiveList
     * @param verbPhraseList
     * @return a list of relations contained in the text
     */
    public List<KnowledgeRelation> hasRelation(String text, List<KnowledgeEntity> namedEntityList,List<KnowledgeAdjective> namedAdjectiveList, List<KnowledgeAdjective> adjectiveList, List<KnowledgeVerb> verbPhraseList) {
        List<KnowledgeRelation> relationList = null;
        for (String c : synonymList) {
            String t = text.replaceAll(",", " en ");
            t = KnowledgeBase.removeBrackets(t.toLowerCase()).replaceAll(" en (het|de|een) ", " en ");
            int pos = KnowledgeBase.indexOf(t,c);
            while (pos != -1) {
                List<String> verbs = new ArrayList<String>();
                verbs.add(getVerb());
                String e = t.substring(pos+c.length()+1);
                if (e.startsWith(" en ")) {
                    e = e.substring(4);
                    AbstractMap.SimpleEntry<String, Integer> verb2 = nextVerb(e, verbPhraseList);
                    if (verb2 != null) {
                        verbs.add(verb2.getKey());
                        e = e.substring(verb2.getValue()-1);
                    }
                }
                if (e.startsWith(" en ")) {
                    e = e.substring(4);
                    AbstractMap.SimpleEntry<String, Integer> verb2 = nextVerb(e, verbPhraseList);
                    if (verb2 != null) {
                        verbs.add(verb2.getKey());
                        e = e.substring(verb2.getValue()-1);
                    }
                }
                if (relationList == null ) relationList = new ArrayList<KnowledgeRelation>();
                List<KnowledgeEntity> entityList = constructNamedEntitiesViaPattern(namedEntityList,namedAdjectiveList, adjectiveList,e);
                if (entityList == null || entityList.size() == 0) entityList = constructNamedEntities(namedEntityList,namedAdjectiveList, adjectiveList,e);
                KnowledgeRelation kr = new KnowledgeRelation(verbs, entityList, e);
                if (!relationList.contains(kr)) relationList.add(kr);
                t = e;
                pos = KnowledgeBase.indexOf(t,c);
            }
        }
        return relationList;
    }

    public List<KnowledgeEntity> constructNamedEntities(List<KnowledgeEntity> namedEntityList, List<KnowledgeAdjective> namedAdjectiveList, List<KnowledgeAdjective> adjectiveList, String text) {
        List<KnowledgeEntity> entities = new ArrayList<KnowledgeEntity>();
        String[] words =  KnowledgeBase.constructWordBagReplaceColan(text.trim().toLowerCase());
        String noun = null;
        boolean foundEntity = true;
        boolean andFound = true;
        int c = 0;
        while (words.length > c && foundEntity && andFound) {
            String namedAdjective = null;
            String namedAdjective2 = null;
            foundEntity = false;
            andFound = false;
            if (words[c].matches(".*[0-9].*")) {
                c++;
            }
            if (words.length > c) {
                if (words.length > c+1 && knowledgeBase.isAdjective(words[c]))  c++;
                if (words.length > c+1 && (namedAdjective = knowledgeBase.namedAdjectiveMulti(words[c],words[c+1])) != null)  c++;
                if (words.length > c+1 && knowledgeBase.isAdjective(words[c]))  c++;
                if (words.length > c+1 && (namedAdjective2 = knowledgeBase.namedAdjectiveMulti(words[c],words[c+1])) != null)  c++;
                noun = words[c];
                String group = null;
                if ((group = knowledgeBase.namedEntity(noun)) != null) {
                    if (namedAdjective != null)
                        entities.add(new KnowledgeEntity(namedAdjective.trim(),group));
                    if (namedAdjective2 != null)
                        entities.add(new KnowledgeEntity(namedAdjective2.trim(),group));
                    if (namedAdjective == null && namedAdjective2 == null)
                        entities.add(new KnowledgeEntity("",group));
                    foundEntity = true;
                    if (words.length > c+1 && words[c+1].equals("en")) {
                        c++;c++;
                        andFound = true;
                    }
                }
            }
        }
        return entities;
    }

    private boolean isAdjective(String word, List<KnowledgeAdjective> adjectiveList) {
        boolean isAdjective = false;
        for (KnowledgeAdjective adj : adjectiveList) {
            if (word.equals(adj.getAdjective())) {
                isAdjective = true;
                break;
            }
        }
        return isAdjective;
    }

    private boolean isNamedAdjective(String word, List<KnowledgeAdjective> namedAdjectiveList) {
        boolean isAdjective = false;
        for (KnowledgeAdjective adj : namedAdjectiveList) {
            if (word.equals(adj.getAdjective())) {
                isAdjective = true;
                break;
            }
        }
        return isAdjective;
    }

    private List<KnowledgeEntity> nextNoun(String text, List<KnowledgeEntity> namedEntityList, List<KnowledgeAdjective> namedAdjectiveList, List<KnowledgeAdjective> adjectiveList) {
        text = text.replaceAll(" of ", " en ");
        text = text.replaceAll(" en/of ", " en ");
        text = text.replaceAll("\\,", " en ");
        text = text.replaceAll("\\-", " en ");
        text = text.replaceAll(" en het ", " en ");
        text = text.replaceAll("   ", " ");
        text = text.replaceAll("  ", " ");

        String[] words =  MachineLearningTextUtils.constructWordBag(text.trim().toLowerCase());
        List<KnowledgeEntity> nouns = new ArrayList<KnowledgeEntity>();
        String namedAdjective = "";
        //String adjective = "";
        int c = 0;
        for (String word : words) {
            boolean isAdjective = isAdjective(word, adjectiveList);
            //if (isAdjective) adjective = word+" ";
            boolean isNamedAdjective = isNamedAdjective(word, namedAdjectiveList);
            if (isNamedAdjective) namedAdjective = word+" ";
            boolean isNumber = false;
            if (word.matches(".*[0-9].*")) isNumber = true;
            String noun = null;
            if (!isNumber && !isAdjective && !isNamedAdjective) {
                if (word.endsWith("-") && c < words.length-2 && ("en".equals(words[c+1]) || "/".equals(words[c+1]) || "en/of".equals(words[c+1]))) {
                    noun = filterNoun(word);
                    if (noun != null) nouns.add(new KnowledgeEntity(namedAdjective.trim(),noun));
                    noun = filterNoun(words[c+2]);
                    if (noun != null) nouns.add(new KnowledgeEntity(null,noun));
                } else {
                    noun = filterNoun(word);
                    if (noun != null) nouns.add(new KnowledgeEntity(namedAdjective.trim(),noun));
                }
                if (c < words.length-2 && "en".equals(words[c+1])) {
                    addNounList(words,c+2,nouns,  namedEntityList,  namedAdjectiveList,  adjectiveList);
                }
                break;
            } else if (isNamedAdjective && c < words.length-3 && "en".equals(words[c+1]) ) { // plaatselijke en lijnvormige elementen
                noun = filterNoun(words[c+3]);
                boolean namedAdjective2 = isNamedAdjective(words[c+2].trim(), namedAdjectiveList);
                if (noun != null && (isAdjective(words[c+2].trim(), adjectiveList) || namedAdjective2)) {
                    nouns.add(new KnowledgeEntity(namedAdjective.trim(),noun));
                    nouns.add(new KnowledgeEntity(namedAdjective2 ? words[c+2].trim() : null,noun));
                }
                break;
            } else if (isAdjective && c < words.length-3 && "en".equals(words[c+1]) ) { // uitvoeren van snelle en tijdelijke verkeersveiligheidsmaatregelen
                noun = filterNoun(words[c+3]);
                boolean namedAdjective2 = isNamedAdjective(words[c+2].trim(), namedAdjectiveList);
                if (noun != null && (isAdjective(words[c+2].trim(), adjectiveList) || namedAdjective2)) {
                    nouns.add(new KnowledgeEntity(null,noun));
                    if (namedAdjective2) nouns.add(new KnowledgeEntity(words[c+2].trim(),noun));
                }
                break;
            }
            c++;
        }
        return nouns;
    }

    private void addNounList(String[] words, int c, List<KnowledgeEntity> nouns, List<KnowledgeEntity> namedEntityList, List<KnowledgeAdjective> namedAdjectiveList, List<KnowledgeAdjective> adjectiveList) {
        String word1 = words[c];
        boolean found = false;
        for (KnowledgeEntity namedEntity : namedEntityList) {
            if (word1.equals(namedEntity.getEntity())) {
                nouns.add(new KnowledgeEntity(null,word1));
                found = true;
                break;
            }
        }
        if (!found && c < words.length-2) {
            String adjective = null;
            for (KnowledgeAdjective adj : namedAdjectiveList) {
                if (word1.equals(adj.getAdjective())) {
                    adjective = adj.getAdjective();
                    found = true;
                    break;
                }
            }
            if (!found) {
                for (KnowledgeAdjective adj : adjectiveList) {
                    if (word1.equals(adj.getAdjective())) {
                        adjective = adj.getAdjective();
                        found = true;
                        break;
                    }
                }
            }
            if (found) {
                word1 = words[c+1];
                for (KnowledgeEntity namedEntity : namedEntityList) {
                    if (word1.equals(namedEntity.getEntity())) {
                        String e = knowledgeBase.namedEntity(word1);
                        if (e == null) e = word1;
                        nouns.add(new KnowledgeEntity(adjective,e ));
                        found = true;
                        c++;
                        break;
                    }
                }
            }
        }

        if (c < words.length-2 && "en".equals(words[c+1])) {
            addNounList(words,c+2,nouns,  namedEntityList,  namedAdjectiveList,  adjectiveList);
        }
    }

    private String filterNoun(String noun) {
        if (noun != null && noun.endsWith(":")) noun = noun.substring(0,noun.length()-1);
        if (noun != null && noun.endsWith(";")) noun = noun.substring(0,noun.length()-1);
        if (noun != null && noun.indexOf("(") != -1) noun = null;
        if (noun != null && noun.indexOf(")") != -1) noun = null;
        if (noun != null && "en".equals(noun)) noun = null;
        if (noun != null && "al".equals(noun)) noun = null;
        if (noun != null && "de".equals(noun)) noun = null;
        if (noun != null && "te".equals(noun)) noun = null;
        if (noun != null && "het".equals(noun)) noun = null;
        if (noun != null && "van".equals(noun)) noun = null;
        if (noun != null && "op".equals(noun)) noun = null;
        if (noun != null && "in".equals(noun)) noun = null;
        if (noun != null && "of".equals(noun)) noun = null;
        if (noun != null && "null".equals(noun)) noun = null;
        if (noun != null && "naar".equals(noun)) noun = null;

        if (noun != null && noun.length() ==1) noun = null;
        if (noun != null && noun.matches(".*[0-9].*")) noun = null;
        KnowledgeEntity ke = knowledgeBase.getNamedEntityEntityMap().get(noun);
        if (ke != null) noun = ke.getGroup();
        return noun;
    }

    public List<KnowledgeEntity> constructNamedEntitiesViaPattern(List<KnowledgeEntity> namedEntityList, List<KnowledgeAdjective> namedAdjectiveList, List<KnowledgeAdjective> adjectiveList, String text) {
        List<KnowledgeEntity> entities = new ArrayList<KnowledgeEntity>();
        text = text.trim();
        if (text.startsWith("van de ")) entities.addAll(nextNoun(text.substring(6), namedEntityList, namedAdjectiveList, adjectiveList));
        else if (text.startsWith("van een ")) entities.addAll(nextNoun(text.substring(7), namedEntityList, namedAdjectiveList, adjectiveList));
        else if (text.startsWith("van het ")) entities.addAll(nextNoun(text.substring(7), namedEntityList, namedAdjectiveList, adjectiveList));
        else if (text.startsWith("door een ")) entities.addAll(nextNoun(text.substring(8), namedEntityList, namedAdjectiveList, adjectiveList));
        else if (text.startsWith("door het ")) entities.addAll(nextNoun(text.substring(8), namedEntityList, namedAdjectiveList, adjectiveList));
        else if (text.startsWith("door de ")) entities.addAll(nextNoun(text.substring(7), namedEntityList, namedAdjectiveList, adjectiveList));
        else if (text.startsWith("aan de ")) entities.addAll(nextNoun(text.substring(6), namedEntityList, namedAdjectiveList, adjectiveList));
        else if (text.startsWith("aan een ")) entities.addAll(nextNoun(text.substring(7), namedEntityList, namedAdjectiveList, adjectiveList));
        else if (text.startsWith("aan het ")) entities.addAll(nextNoun(text.substring(7), namedEntityList, namedAdjectiveList, adjectiveList));
        else if (text.startsWith("aan ")) entities.addAll(nextNoun(text.substring(3), namedEntityList, namedAdjectiveList, adjectiveList));
        else if (text.startsWith("van ")) entities.addAll(nextNoun(text.substring(3), namedEntityList, namedAdjectiveList, adjectiveList));
        else if (text.startsWith("der ")) entities.addAll(nextNoun(text.substring(3), namedEntityList, namedAdjectiveList, adjectiveList));
        else if (text.startsWith("door ")) entities.addAll(nextNoun(text.substring(4), namedEntityList, namedAdjectiveList, adjectiveList));

        return entities;
    }

}
