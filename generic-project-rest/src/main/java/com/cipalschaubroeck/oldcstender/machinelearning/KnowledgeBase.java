package com.cipalschaubroeck.oldcstender.machinelearning;

import com.cipalschaubroeck.oldcstender.machinelearning.shared.CpvUpgradeElement;
import com.cipalschaubroeck.oldcstender.machinelearning.shared.MachineLearningFileUtils;
import com.cipalschaubroeck.oldcstender.machinelearning.shared.MachineLearningTextUtils;
import com.cipalschaubroeck.oldcstender.machinelearning.shared.MachineLearningUtils;
import com.cipalschaubroeck.oldcstender.shared.Util;
import lombok.Getter;
import lombok.Setter;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Entry point for all resource files.
 * Entry point for feature extraction from plain text.
 */
@Getter
@Setter
public class KnowledgeBase {
    public static final String rootResourceFolder = constructRootResourceFolder();
    public static final String serverName = constructServerName();
    public static final String serverPort = constructServerPort();
    public static final String serverProtocol = constructServerProtocol();

    private String m_knowledgebaseFile = rootResourceFolder + "pattern_features_nl_be.txt";
    private String m_verbphrases = rootResourceFolder + "verbphrases.txt";
    private String m_namedEntities = rootResourceFolder + "namedEntities.txt";
    private String m_adjectives = rootResourceFolder + "adjectives.txt";
    private String m_namedAdjectives = rootResourceFolder + "namedAdjectives.txt";
    private String m_cpvTranslations = rootResourceFolder + "cpvTranslations.txt";
    private String m_zipcodes_be = rootResourceFolder + "zipcodes_nuts_be.txt";
    private String m_stopwords_nl_be = rootResourceFolder + "stopwords_nl_be.txt";
    private String m_nuts_nl_be = rootResourceFolder + "nuts_nl_be.txt";
    private String m_category_nl_be = rootResourceFolder + "category_nl_be.txt";
    private String m_discipline_nl_be = rootResourceFolder + "discipline_nl_be.txt";
    private String m_class_nl_be = rootResourceFolder + "class_nl_be.txt";
    private String m_categoryCpv_be = rootResourceFolder + "categoryToCpv_be.txt";
    private String m_cpvUpgrade_ = rootResourceFolder + "cpvUpgrade_";
    private String m_disciplineStopword_ = rootResourceFolder + "disciplineStopword_";
    private String m_disciplineKeyword_ = rootResourceFolder + "disciplineKeyword_";
    private String m_disciplineDiscarded_ = rootResourceFolder + "disciplineDiscarded_";

    private List<KnowledgeConcept> conceptList = new ArrayList<KnowledgeConcept>();
    private List<KnowledgeVerb> verbPhraseList = new ArrayList<KnowledgeVerb>();
    private Map<String, KnowledgeVerb> verbGroupMap = new HashMap<String, KnowledgeVerb>();
    private List<KnowledgeEntity> namedEntityList = new ArrayList<KnowledgeEntity>();
    private Map<String, KnowledgeEntity> namedEntityEntityMap = new HashMap<String, KnowledgeEntity>();
    private Map<String, KnowledgeEntity> namedEntityMap = new HashMap<String, KnowledgeEntity>();
    private Map<String, KnowledgeEntity> namedEntityGroupMap = new HashMap<String, KnowledgeEntity>();
    private List<KnowledgeCity> cityList = new ArrayList<KnowledgeCity>();
    private Map<String, KnowledgeCity> zipCodeCityMap = new HashMap<String, KnowledgeCity>();
    private Map<String, String> nutsTranslationMap = new HashMap<>();
    private Map<String, String> cpvUpgradeMap = new HashMap<>();
    private Map<String, String> featureLotMap = new HashMap<>();
    private List<String> stopWordList = new ArrayList<>();
    private List<String> modelCpvList = null;
    private Map<String, String> categoryCpvMap = new HashMap<>();
    private Map<String, String> categoryTranslationMap = new HashMap<>();
    private Map<String, String> disciplineTranslationMap = new HashMap<>();
    private Map<String, String> classTranslationMap = new HashMap<>();
    private List<KnowledgeAdjective> namedAdjectiveList = new ArrayList<KnowledgeAdjective>();
    private Map<String, KnowledgeAdjective> namedAdjectiveMap = new HashMap<String, KnowledgeAdjective>();
    private List<KnowledgeAdjective> adjectiveList = new ArrayList<KnowledgeAdjective>();
    private Map<String, KnowledgeAdjective> adjectiveMap = new HashMap<String, KnowledgeAdjective>();
    private Map<String, Integer> entityList = new HashMap<String, Integer>();
    private Map<String, Integer> lotEntityList = new HashMap<String, Integer>();
    private Map<String, String> cpvTranslations = new HashMap<String, String>();
    private KnowledgeNormalize knowledgeNormalize = null;
    private Map<String, Map<String, CpvUpgradeElement>> disciplineKeywordMap = null;
    private Map<String, Map<String,CpvUpgradeElement>> disciplineAtomicKeywordMap = null;
    private Map<String, Map<String,CpvUpgradeElement>> disciplineStopwordMap = null;

    private Map<String,List<String>> discardedTenderDisciplineMap = null;

    private HashMap<String, List<String>> cpvDisciplineMap = null;

    public static String RELATIONPATTERN = "([^a-zA-Z]@1@ )([^,\\.]{1,100})";
    public static String[] loten = { "lot", "loten", "perceel", "percelen", "raamcontract", "deel", "deelperceel","deelpercelen" };

    public static String constructRootResourceFolder() {
        HashMap<String,String> map = new HashMap<String, String>();
        MachineLearningFileUtils.readResourceIntoMap("com/sygel/tender/config.txt",  map); // TODO JAL: ** REFACTOR ** Change path
        System.out.println("rootResourceFolder "+map.get("rootResourceFolder"));
        return map.get("rootResourceFolder");
    }

    public static String constructServerName() {
        HashMap<String,String> map = new HashMap<String, String>();
        MachineLearningFileUtils.readResourceIntoMap("com/sygel/tender/config.txt",  map); // TODO JAL: ** REFACTOR ** Change path
        System.out.println("serverName "+map.get("serverName"));
        return map.get("serverName");
    }

    public static String constructServerPort() {
        HashMap<String,String> map = new HashMap<String, String>();
        MachineLearningFileUtils.readResourceIntoMap("com/sygel/tender/config.txt",  map); // TODO JAL: ** REFACTOR ** Change path
        System.out.println("serverPort "+map.get("serverPort"));
        return map.get("serverPort");
    }

    public static String constructServerProtocol() {
        HashMap<String,String> map = new HashMap<String, String>();
        MachineLearningFileUtils.readResourceIntoMap("com/sygel/tender/config.txt",  map); // TODO JAL: ** REFACTOR ** Change path
        System.out.println("serverProtocol "+map.get("serverProtocol"));
        return map.get("serverProtocol");
    }

/*    public static void main(String[] args) {
        KnowledgeBase kb = new KnowledgeBase();
        kb.constructNewEntitiesAdjectivesFromCorpus();
    }*/

    public KnowledgeBase() {
        try (BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(m_categoryCpv_be), StandardCharsets.UTF_8))) {
            String line = br.readLine();
            while (line != null) {
                if (!line.startsWith("#")) {
                    String[] info = line.split("=");
                    String category = info[0];
                    String cpv = info[1].substring(0, 8);
                    if (categoryCpvMap.get(category) == null)
                        categoryCpvMap.put(category, cpv);
                }
                line = br.readLine();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try (BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(m_zipcodes_be), StandardCharsets.UTF_8))) {
            String line = br.readLine();
            while (line != null) {
                // System.out.println(line);
                if (!line.startsWith("#")) {
                    KnowledgeCity city = new KnowledgeCity();
                    city.parseBE(line);
                    cityList.add(city);
                    KnowledgeCity hcity = zipCodeCityMap.get(city.getZipCode());
                    if (hcity == null || !hcity.isHeadcity())
                        zipCodeCityMap.put(city.getZipCode(), city);
                }
                line = br.readLine();
            }
            Collections.sort(cityList, new KnowledgeCity.LengthComparator());

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        MachineLearningFileUtils.readFileIntoList(m_stopwords_nl_be, stopWordList,";");
        MachineLearningFileUtils.readFileIntoMap(m_nuts_nl_be, nutsTranslationMap);
        MachineLearningFileUtils.readFileIntoMap(m_category_nl_be, categoryTranslationMap);
        MachineLearningFileUtils.readFileIntoMap(m_discipline_nl_be, disciplineTranslationMap);

        constructUpgradeMap();

        MachineLearningFileUtils.readFileIntoMap(m_class_nl_be, classTranslationMap);

        try (BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(m_knowledgebaseFile), StandardCharsets.UTF_8))) {
            String line = br.readLine();
            while (line != null) {
                if (!line.startsWith("#")) {
                    KnowledgeConcept concept = new KnowledgeConcept(line);
                    conceptList.add(concept);
                }
                line = br.readLine();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try (BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(m_verbphrases), StandardCharsets.UTF_8))) {
            String line = br.readLine();
            while (line != null) {
                if (!line.startsWith("#")) {
                    KnowledgeVerb concept = new KnowledgeVerb(line, this);
                    verbPhraseList.add(concept);
                    verbGroupMap.put(concept.getVerb(), concept);
                }
                line = br.readLine();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try (BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(m_namedEntities), StandardCharsets.UTF_8))) {
            String line = br.readLine();
            while (line != null) {
                //System.out.println(line);
                String occ = "";
                if (!line.startsWith("#")) {
                    int stop = line.lastIndexOf(" ");
                    occ = line.substring(stop + 1);
                    String entity = line.substring(0, stop);
                    if (entity.indexOf(";") != -1 && !";".equals(entity)) {
                        KnowledgeEntity concept = new KnowledgeEntity(entity);
                        if (concept.getEntity() != null) {
                            namedEntityList.add(concept);
                            if (concept.getAdjective() == null)
                                namedEntityEntityMap.put(concept.getEntity(), concept);
                            namedEntityMap.put(concept.toString(), concept);
                            KnowledgeEntity entitySyn = namedEntityGroupMap.get(concept.getGroup());
                            if (entitySyn == null)
                                namedEntityGroupMap.put(concept.getGroup(), concept);
                            else
                                entitySyn.addSynonym(concept.toString());
                            // namedEntityCategoryMap.put(concept.toString(),concept.getCategory());
                        }
                    }
                }
                line = br.readLine();
                if ("1".equals(occ)) {
                    System.out.println("named entity line end " + line);
                    line = null;
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try (BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(m_namedAdjectives), StandardCharsets.UTF_8))) {
            String line = br.readLine();
            while (line != null) {
                if (!line.startsWith("#")) {
                    KnowledgeAdjective concept = new KnowledgeAdjective(line);
                    namedAdjectiveList.add(concept);
                    namedAdjectiveMap.put(concept.getAdjective(), concept);
                }
                line = br.readLine();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try (BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(m_adjectives), StandardCharsets.UTF_8))) {
            String line = br.readLine();
            while (line != null) {
                if (!line.startsWith("#")) {
                    KnowledgeAdjective concept = new KnowledgeAdjective(line);
                    adjectiveList.add(concept);
                    adjectiveMap.put(concept.getAdjective(), concept);
                }
                line = br.readLine();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try (BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(m_cpvTranslations), StandardCharsets.UTF_8))) {
            String line = br.readLine();
            while (line != null) {
                if (!line.startsWith("#")) {
                    String[] c = line.split("@");
                    cpvTranslations.put(c[0], c[1]);
                }
                line = br.readLine();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        constructUpgradeKeywordsMap();
        constructUpgradeStopwordsMap();
        modelCpvList = constructModelCPVList();

        constructCpvDisciplineMap();
        knowledgeNormalize = new KnowledgeNormalize(this);
    }

    protected void constructCpvDisciplineMap()  {
        cpvDisciplineMap = new HashMap<String, List<String>>();
        for (String discipline : MLearningModels.DISCIPLINES)
            cpvDisciplineMap.put(discipline, KnowledgeBase.constructCpvDisciplineList(discipline));
    }

    static public List<String> constructCPVList(String discipline) {
        List<String> cpvExeptionList = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(BuildModels.rootDataFolder + "data/model/exceptions_" + discipline.toLowerCase() + "_verified.txt"),
                StandardCharsets.UTF_8))) {
            String line = br.readLine();
            while (line != null) {
                String cpv = line.substring(0, line.indexOf(" "));
                cpvExeptionList.add(cpv);
                line = br.readLine();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return cpvExeptionList;
    }

    public List<String> constructCPVUpgradeList() {
        List<String> cpvUpgradeList = new ArrayList<>();
        for (String cpv : getCpvUpgradeMap().values()) {
            if (!cpvUpgradeList.contains(cpv))
                cpvUpgradeList.add(cpv);
        }
        return cpvUpgradeList;
    }

    public List<String> getModelCpvList() {
        return modelCpvList;
    }



    public Map<String, Map<String, CpvUpgradeElement>> getDisciplineKeywordMap() {
        return disciplineKeywordMap;
    }

    public Map<String, Map<String, CpvUpgradeElement>> getDisciplineAtomicKeywordMap() {
        return disciplineAtomicKeywordMap;
    }
    private List<String> constructModelCPVList() {
        List<String> cpvList = new ArrayList<>();
        List<String> cpvUpgradeList = constructCPVUpgradeList();
        for (String dsicipline : MLearningModels.DISCIPLINES) {
            for (String c : KnowledgeBase.constructCpvDisciplineList(dsicipline))
                if (!c.endsWith("000000") // DO NOT ADD GENERAL CATEGORIES, TOO GENERAL
                        // FOR AI MAPPING
                        && !cpvList.contains(c)
                )
                    cpvList.add(c);
        }

        Collections.sort(cpvList);
        return cpvList;
    }

    static public List<String> constructCpvDisciplineList(String discipline) {
        List<String> cpvExeptionList = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(rootResourceFolder + "exceptions_" + discipline.toLowerCase() + "_verified.txt"), StandardCharsets.UTF_8))) {
            String line = br.readLine();
            while (line != null) {
                if (!line.startsWith("#")) {
                    String cpv = line.substring(line.indexOf(":") + 1, line.indexOf(" "));
                    cpvExeptionList.add(cpv);
                }
                line = br.readLine();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return cpvExeptionList;
    }

    public Map<String, List<String>> getDiscardedTenderDisciplineMap() {
        return discardedTenderDisciplineMap;
    }

    protected void constructUpgradeKeywordsMap() {
        disciplineKeywordMap = new HashMap<String, Map<String,CpvUpgradeElement>>();
        disciplineAtomicKeywordMap = new HashMap<String, Map<String,CpvUpgradeElement>>();
        discardedTenderDisciplineMap = new HashMap<String, List<String>>();
        for (String discipline : MLearningModels.DISCIPLINES) {
            Map<String,CpvUpgradeElement> keywordMap = new HashMap<String, CpvUpgradeElement>();
            Map<String,CpvUpgradeElement> atomicKeywordMap = new HashMap<String, CpvUpgradeElement>();
            List<String> discardedTenders = new ArrayList<String>();
            disciplineKeywordMap.put(discipline, keywordMap);
            disciplineAtomicKeywordMap.put(discipline, atomicKeywordMap);
            discardedTenderDisciplineMap.put(discipline,discardedTenders);
            //System.out.println(m_cpvUpgrade_ + discipline.toUpperCase() + "_nl_be.txt");
            try (BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(m_cpvUpgrade_ + discipline.toUpperCase() + "_nl_be.txt"), StandardCharsets.UTF_8))) {
                String line = br.readLine();
                while (line != null) {
                    if (!line.startsWith("#")) {
                        CpvUpgradeElement cpvUpgradeElement = new CpvUpgradeElement(line);
                        List<String> keywordList = cpvUpgradeElement.constructKeywordList();
                        for (String keyword : keywordList) {
                            keywordMap.put(keyword, cpvUpgradeElement);
                        }
                        List<String> atomickeywordList = cpvUpgradeElement.constructAtomicKeywordList();
                        for (String keyword : atomickeywordList) {
                            atomicKeywordMap.put(keyword, cpvUpgradeElement);
                        }
                    }
                    line = br.readLine();
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try (BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(m_disciplineKeyword_ + discipline.toUpperCase() + "_nl_be.txt"), StandardCharsets.UTF_8))) {
                String line = br.readLine();
                while (line != null) {
                    if (!line.startsWith("#")) {
                        CpvUpgradeElement cpvUpgradeElement = new CpvUpgradeElement(line);
                        List<String> keywordList = cpvUpgradeElement.constructKeywordList();
                        for (String keyword : keywordList) {
                            keywordMap.put(keyword, cpvUpgradeElement);
                        }
                        List<String> atomickeywordList = cpvUpgradeElement.constructAtomicKeywordList();
                        for (String keyword : atomickeywordList) {
                            CpvUpgradeElement atomicCpvUpgradeElement = atomicKeywordMap.get(keyword);
                            if (atomicCpvUpgradeElement == null) {
                                atomicCpvUpgradeElement = new CpvUpgradeElement(keyword, cpvUpgradeElement.getCpv(), cpvUpgradeElement.getAtomicOccurence());
                            } else {
                                atomicCpvUpgradeElement.setOccurence(atomicCpvUpgradeElement.getOccurence()+cpvUpgradeElement.getAtomicOccurence());
                                if (atomicCpvUpgradeElement.getCpv().indexOf(cpvUpgradeElement.getCpv()) == -1)
                                    atomicCpvUpgradeElement.setCpv(atomicCpvUpgradeElement.getCpv()+ "_"+cpvUpgradeElement.getCpv());
                            }
                            atomicKeywordMap.put(keyword, atomicCpvUpgradeElement);
                        }
                    }
                    line = br.readLine();
                }
            } catch (IOException e) {
                //
            }
            //Map<String,CpvUpgradeElement> mergedKeywordMap = CpvUpgrade.sortByComparator(CpvUpgrade.constructMergedCpvUpgradeElements(atomicKeywordMap),false);
            //MachineLearningFileUtils.writeList(CpvUpgrade.convertToList(mergedKeywordMap), BuildModels.rootDataFolder + "data"+"/amergedDisciplineKeyword_"+discipline+"_nl_be.txt");

            MachineLearningFileUtils.readFileIntoList(m_disciplineDiscarded_ + discipline.toUpperCase() + "_nl_be.txt", discardedTenders);
        }
    }

    protected void constructUpgradeStopwordsMap() {
        disciplineStopwordMap = new HashMap<String, Map<String,CpvUpgradeElement>>();
        for (String discipline : MLearningModels.DISCIPLINES) {
            Map<String,CpvUpgradeElement> stopwordMap = new HashMap<String, CpvUpgradeElement>();
            disciplineStopwordMap.put(discipline, stopwordMap);
            try (BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(m_disciplineStopword_ + discipline.toUpperCase() + "_nl_be.txt"), StandardCharsets.UTF_8))) {
                String line = br.readLine();
                while (line != null) {
                    if (!line.startsWith("#")) {
                        CpvUpgradeElement cpvUpgradeElement = new CpvUpgradeElement(line);
                        List<String> keywordList = cpvUpgradeElement.constructAtomicKeywordList();
                        for (String keyword : keywordList) {
                            stopwordMap.put(keyword, cpvUpgradeElement);
                        }
                    }
                    line = br.readLine();
                }
            } catch (IOException e) {
                //e.printStackTrace();
            }
        }
    }

    protected void constructUpgradeMap() {
        for (String discipline : MLearningModels.DISCIPLINES) {
            try (BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(m_cpvUpgrade_ + discipline.toUpperCase() + "_nl_be.txt"), StandardCharsets.UTF_8))) {
                String line = br.readLine();
                while (line != null) {
                    if (!line.startsWith("#")) {
                        //System.out.println(line);
                        String[] info = line.split(" ");
                        updateCpvUpgradeMap(info[0], info[1], cpvUpgradeMap);
                    }
                    line = br.readLine();
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Add concepts and regular expression features.
     *
     * @param text
     * @param features
     */
    public void concepts(String text, HashMap<String, Integer> features) {
        text = text.toLowerCase() + " ";
        for (KnowledgeConcept concept : conceptList) {
            boolean result = concept.hasConcept(text);
            if (result) {
                String key = concept.getKey();
                updateFeatureList(features, key.startsWith("PA_") || key.startsWith("REL_") ? key : "CON_" + key);
                List<String> excludeList = concept.getExcludeList();
                if (excludeList != null) {
                    for (String exclude : excludeList) {
                        features.remove(exclude);
                    }
                }
            }
        }
    }

    public void updateCpvUpgradeMap(String key, String cpv, Map<String, String> cpvUpgrade) {
        if (key.startsWith("#"))
            return;
        int pos = key.indexOf("REL_LOT_");
        if (pos != -1) {
            featureLotMap.put(key.substring(pos), key.substring(pos));
        }
        pos = key.indexOf("(");
        if (pos != -1) {
            int end = key.indexOf(")", pos);
            if (end != -1) {
                String prefix = key.substring(0, pos);
                String[] keys = key.substring(pos + 1, end).split("\\|");
                String ne = key.substring(end + 2);
                for (String k : keys) {
                    cpvUpgrade.put(prefix + k + "_" + ne, cpv);
                }
            }
        } else {
            cpvUpgrade.put(key, cpv);
        }
    }

    public void updateEntityList(List<KnowledgeEntity> entities) {
        for (KnowledgeEntity entity : entities) {
            Integer entityCount = entityList.get(entity.toString());
            if (entityCount == null) {
                entityList.put(entity.toString(), 1);
            } else {
                entityList.put(entity.toString(), entityCount + 1);
            }
        }
    }

    public void updateLotEntityList(KnowledgeEntity entity) {
        Integer entityCount = lotEntityList.get(entity.toString());
        if (entityCount == null) {
            lotEntityList.put(entity.toString(), 1);
        } else {
            lotEntityList.put(entity.toString(), entityCount + 1);
        }
    }

    public static String[] constructWordBagReplaceColan(String text) {
        text = text.replaceAll(",", " en ");
        text = text.replaceAll("    ", " ");
        text = text.replaceAll("   ", " ");
        text = text.replaceAll("  ", " ");

        String[] bag = text.split("[\\s,\\.;]");
        return bag;
    }

    public static String[] constructWordSeparatorBag(String text) {
        text = text.replaceAll(",", " , ");
        // text = text.replaceAll("-", " , ");
        text = text.replaceAll("\\.", " . ");
        text = text.replaceAll("   ", " ");
        text = text.replaceAll("  ", " ");

        String[] bag = text.split("[\\s]");
        return bag;
    }

    public static int indexOf(String text, String pattern) {
        Pattern p = Pattern.compile("[^a-zA-Z]" + pattern + " ");
        Matcher m = p.matcher(text);
        if (m.find()) {
            // if (m.start() == 0) return -1;
            return m.start();
        } else {
            return -1;
        }
    }

    public static int indexOf2(String text, String pattern) {
        Pattern p = Pattern.compile("[^a-zA-Z0-9éèçàäëöüôâêïÉÈÇÀÄËÖÜÔÂÊÏ]" + pattern + "[^a-zA-Z0-9éèçàäëöüôâêïÉÈÇÀÄËÖÜÔÂÊÏ]"); 																																																																																																																																																																																									// here
        Matcher m = p.matcher(text);
        if (m.find()) {
            // if (m.start() == 0) return -1;
            return m.start();
        } else {
            return -1;
        }
    }

    public static String removeBrackets(String text) {
        text = text.replaceAll("\\(", " ");
        text = text.replaceAll("\\)", " ");
        text = text.replaceAll("`", "'");
        text = text.replaceAll("/", ",");
        // text = text.replaceAll("[-]", ",");
        text = text.replaceAll("\\+", " en ");
        text = text.replaceAll("[^a-zA-Z0-9\\s-,.'\téèçàäëöüôâêïÉÈÇÀÄËÖÜÔÂÊÏ]", " ");
        text = text.replaceAll("   ", " ");
        text = text.replaceAll("  ", " ");
        return text;
    }

    public static void updateFeatureList(Map<String, Integer> featureMap, Set<String> features) {
        for (String feature : features) {
            updateFeatureList(featureMap, feature);
        }
    }

    public static void updateFeatureList(Map<String, Integer> features, String feature) {
        Integer entityCount = features.get(feature);
        if (entityCount == null) {
            features.put(feature, 1);
        } else {
            features.put(feature, entityCount + 1);
        }
    }

    public String namedEntity(String word) {
        String e2 = null;
        if (word == null)
            return null;
        KnowledgeEntity namedEntity = namedEntityMap.get(word);
        if (namedEntity != null) {
            e2 = namedEntity.getGroup();
        }
        return e2;
    }

    public String verbPhrase(String word) {
        for (KnowledgeVerb verbPhrase : verbPhraseList) {
            for (String syn : verbPhrase.getSynonymList()) {
                if (syn.equals(word)) {
                    return verbPhrase.getVerb();
                }
            }
        }
        return null;
    }

    public String namedAdjective(String word, String nextWord) {
        KnowledgeAdjective adjective = namedAdjectiveMap.get(word);
        if (adjective != null && nextWord != null && namedEntity(nextWord) != null)
            return word;
        else
            return null;
    }

    public String namedAdjectiveMulti(String word, String nextWord) {
        KnowledgeAdjective adjective = namedAdjectiveMap.get(word);
        if (adjective != null && nextWord != null && (namedEntity(nextWord) != null || isNamedAdjective(nextWord) || isAdjective(nextWord)))
            return word;
        else
            return null;
    }

    public boolean isNamedAdjective(String word) {
        return namedAdjectiveMap.get(word) != null;
    }

    public boolean isAdjective(String word) {
        return adjectiveMap.get(word) != null;
    }

    public boolean isStopword(String word) {
        return stopWordList.contains(word);
    }

    public List<String> getSynonymVerbs(String verb) {
        KnowledgeVerb verbPhrase = verbGroupMap.get(verb);
        if (verbPhrase != null)
            return verbPhrase.getSynonymList();
        List<String> list = new ArrayList<>();
        list.add(verb);
        return list;
    }

    public List<String> getSynonymNE(String ne) {
        KnowledgeEntity entity = namedEntityGroupMap.get(ne);
        if (entity != null)
            return entity.getSynonymList();
        List<String> list = new ArrayList<>();
        list.add(ne);
        return list;
    }

    public List<String> getSynonymsFeature(String feature) {
        List<String> syn = new ArrayList<>();
        feature = feature.replaceAll("_CAT_", "_");
        if (feature.startsWith("REL_LOT_")) {
            String ne = feature.substring(8);
            for (String l : loten) {
                List<String> sl = getSynonymNE(ne);
                if (sl != null)
                    for (String sn : sl) {
                        syn.add(l + " " + sn);
                        syn.add(sn);
                    }
                syn.add(l + " " + ne);
                syn.add(ne);
            }
        } else if (feature.startsWith("REL_") /* && feature.indexOf("_CAT_") == -1 */) {

            String[] info = feature.split("_");
            String verb = info[1];
            String ne = info[2];
            for (String sv : getSynonymVerbs(verb)) {
                List<String> sl = getSynonymNE(ne);
                if (sl != null)
                    for (String sn : sl) {
                        syn.add(sv + " " + sn);
                    }
                syn.add(sv + " " + ne);
            }
        }
        return syn;
    }

    public Set<String> extractFeatures(String line) {
        HashMap<String, Integer> features = new HashMap<String, Integer>();
        extractFeatures(line, features);
        return features.keySet();
    }

    /**
     * Extract features and concepts from a String
     * 1° extract abbreviations
     * 2° extract features (Relations and Entities)
     * 3° extract concepts and regular expression concepts
     * 4° extract relations not found with step 2
     * 5° remove non LOT features if the there are LOT features in the String
     *
     * @param line
     * @return features
     */
    public Set<String> extractFeaturesAndConcepts(String line) {
        HashMap<String, Integer> all = new HashMap<String, Integer>();
        if (line != null) {
            line = line.replaceAll("' ", " ");
            line = extractAbbreviation(line);
            extractFeatures(line, all);
            concepts(line, all);
            addRelations(line, all);
            removeFeaturesNotLotFeatures(all);
        }
        return all.keySet();
    }

    /**
     * Add relations not found with the rule parser.
     * This method tries to find an NE for each orphan VERB by looking in a specific window (size 10) before and after the verb.
     *
     * @param line
     * @param all
     */
    private void addRelations(String line, HashMap<String, Integer> all) {

        List<String> relationEntities = extractEntitiesFromRelations(all);
        List<String> relationVerbs =  extractVerbsFromRelations(all);
        List<String> entities = new ArrayList<>();
        List<String> verbs = new ArrayList<>();

        String augmentedLine = augmentText(line);
        String[] words = augmentedLine.split(" ");
        for (String word : words) {
            if (word.startsWith("NE_") && !relationEntities.contains(word)) entities.add(word);
            if (word.startsWith("VERB_") && !relationVerbs.contains(word)) verbs.add(word);
        }
        for (String entity : entities) {
            for (String verb : verbs)
                addRelation(entity,verb,words,all);
        }
    }

    private void addRelation(String entity, String verb, String[] words, HashMap<String, Integer> all) {
        final int WINDOW = 10;
        boolean verbFound = false;
        boolean entityFound = false;
        int counter = 0;
        for (String word : words) {
            if (word.equals(entity)) {entityFound = true;if (!verbFound) counter = 0;}
            else if (word.equals(verb)) {verbFound = true;if (!entityFound) counter = 0;}
            if (word.equals(".")|| word.equals("LOT")) {
                entityFound = false;
                verbFound = false;
            }
            if (entityFound && verbFound && counter < WINDOW) {
                updateFeatureList(all, "REL_"+verb.substring(5)+"_"+entity.substring(3));
                break;
            }
            counter++;
        }
    }

    private List<String> extractEntitiesFromRelations(HashMap<String, Integer> all) {
        List<String> entities = new ArrayList<>();
        for (String o : all.keySet()) {
            if (o.startsWith("REL_LOT_")) entities.add("NE_"+o.substring(8));
            else if (o.startsWith("REL_")) entities.add("NE_"+o.substring(o.indexOf("_",4)+1));
        }
        return entities;
    }
    private List<String> extractVerbsFromRelations(HashMap<String, Integer> all) {
        List<String> entities = new ArrayList<>();
        for (String o : all.keySet())
            if (o.startsWith("REL_") && !o.startsWith("REL_LOT_")) entities.add("VERB_"+o.substring(4,o.indexOf("_",4)));
        return entities;
    }

    /**
     * remove all none LOT features if one LOT feature is present that results in
     * a Discipline eg bouwen gebouw, LOT sanitair -> should remove the
     * REL_bouwen_gebouw so we only get the discipline SA and not AB SA
     *
     * @param features
     */
    private void removeFeaturesNotLotFeatures(Map<String, Integer> features) {
        boolean found = false;
        for (String feature : features.keySet()) {
            if (feature.startsWith("REL_LOT_") && featureLotMap.get(feature) != null) {
                found = true;
            }
            if (feature.startsWith("PA_excl")) {
                found = false;
                break;
            }
        }
        if (found) {
            features.entrySet().removeIf(e -> !e.getKey().startsWith("REL_LOT_") && !e.getKey().startsWith("NE_") && !e.getKey().startsWith("PA_"));
        }
    }

    private String extractNumbers(String line) {
        line = line.replaceAll("(?<=\\d)\\.(?=\\d)", "");
        return line;
    }

    /**
     * Replace the common abbreviations
     * TODO move to config file.
     *
     * @param line
     * @return String without abbreviations
     */
    private String extractAbbreviation(String line) {
        line = line.replaceAll("H\\.V\\.A\\.C\\.", "hvac");
        line = line.replaceAll("L\\.D\\.P\\.E\\.", "ldpe");
        line = line.replaceAll("P\\.M\\.D\\.", "pmd");
        line = line.replaceAll("st\\.", "sint");
        line = line.replaceAll("St\\.", "sint");
        line = line.replaceAll("Electr\\.", "elektriciteit");
        line = line.replaceAll("electr\\.", "elektriciteit");

        return line;
    }

    private String augmentWithVerbs(String line) {
        for (KnowledgeVerb verb : verbPhraseList) {
            for (String synonym : verb.getSynonymList())
                line = line.replaceAll(" "+synonym+ " ", " VERB_" + verb.getVerb()+" ");
        }
        return line;
    }
    private String augmentWithLot(String line) {
        for (String lot : loten) {
            line = line.replaceAll(" "+lot+ " ", " LOT ");
        }
        return line;
    }

    public String augmentText(String line) {
        line = line.replaceAll("' ", " ");
        line = extractAbbreviation(line);
        line = extractNumbers(line);
        line = normalizeLine(line);
        line = line.replaceAll("/", " . ");
        line = line.replaceAll("\\.", " . ");
        line = line.replaceAll("    ", " ");
        line = line.replaceAll("   ", " ");
        line = line.replaceAll("  ", " ").toLowerCase();
        String[] words = MachineLearningTextUtils.constructWordBagWithDot(line);
        StringBuilder augmentedtext = new StringBuilder(" ");
        for (int c = 0; c < words.length; c++) {
            augmentedtext.append(words[c]+" ");
        }
        line = augmentWithLot(augmentedtext.toString());
        line = augmentWithVerbs(line);
        line = augmentWithEntity(line);
        line = line.replaceAll("    ", " ");
        line = line.replaceAll("   ", " ");
        return line.trim();
    }

    private String augmentWithEntity(String line) {
        String[] words = MachineLearningTextUtils.constructWordBagWithDot(line);
        StringBuilder augmentedtext = new StringBuilder();
        for (int c = 0; c < words.length; c++) {
            String word = words[c];
            boolean commonWord = true;
            String namedAdjective = null;
            if (c > 0) {
                namedAdjective = namedAdjective(words[c - 1], word);
            }
            String nextWord = null;
            if (c < words.length - 1)
                nextWord = words[c + 1];

            if (word != null && word.length() > 0 && ((nextWord == null) || (nextWord != null && namedEntityMap.get(word + " " + nextWord) == null))) {
                KnowledgeEntity ne = new KnowledgeEntity(namedAdjective, word);
                KnowledgeEntity ke = namedEntityMap.get(ne.toString());
                if (ke == null) {
                    ke = namedEntityMap.get(word);
                    if (ke != null) {
                        commonWord = false;
                        augmentedtext.append("NE_"+(namedAdjective != null ? namedAdjective + "_" : "")+ke.getGroup().replace(' ', '_')+ " ");
                    }
                } else {
                    commonWord = false;
                    augmentedtext.append("NE_"+ke.getGroup().replace(' ', '_')+ " ");
                }
            }
            if (commonWord && !isNamedAdjective(word))
                augmentedtext.append(word+ " ");
        }
        return augmentedtext.toString();
    }

    private void extractNamedEntityFeatures(String line, Map<String, Integer> features) {
        line = line.replaceAll("/", " ");
        line = line.replaceAll("   ", " ");
        line = line.replaceAll("  ", " ");

        String[] words = MachineLearningTextUtils.constructWordBag(line.toLowerCase());
        List<KnowledgeEntity> wordList = new ArrayList<>();

        for (int c = 0; c < words.length; c++) {
            String word = words[c];
            String namedAdjective = null;
            if (c > 0) {
                namedAdjective = namedAdjective(words[c - 1], word);
            }
            String nextWord = null;
            if (c < words.length - 1)
                nextWord = words[c + 1];

            if (word != null && word.length() > 0 && ((nextWord == null) || (nextWord != null && namedEntityMap.get(word + " " + nextWord) == null))) {
                KnowledgeEntity ne = new KnowledgeEntity(namedAdjective, word);
                wordList.add(ne);
            }
        }
        List<KnowledgeEntity> wordList2 = new ArrayList<>(wordList);

        for (KnowledgeEntity entity : wordList) {
            KnowledgeEntity ke = namedEntityMap.get(entity.toString());
            if (ke != null) {
                updateFeatureList(features, "NE_" + ke.getGroup().replace(' ', '_'));
                wordList2.remove(entity);
            }
        }
        for (KnowledgeEntity entity : wordList2) {
            String word = entity.getEntity();
            String namedAdjective = entity.getAdjective();
            KnowledgeEntity ke = namedEntityMap.get(word);
            if (ke != null) {
                updateFeatureList(features, "NE_" + (namedAdjective != null ? namedAdjective + "_" : "") + ke.getGroup());
            }
        }
    }

    private void extractLotFeatures(String line, Map<String, Integer> features) {

        for (String lot : loten) {
            String c = removeBrackets(line).toLowerCase();
            // System.out.println("extract loten "+c);
            int pos = KnowledgeBase.indexOf(c, lot);
            while (pos != -1) {
                c = c.substring(pos + lot.length() + 1);
                String[] words = constructWordSeparatorBag(c.trim().toLowerCase());

                if (words != null && words.length > 0) {
                    int w = 0;
                    String word = words[w];

                    while ((word.equals(",") || word.equals("-") || word.equals("") || word.equals("i") || word.equals("ii") || word.equals("iii") || word.equals("iv")) && words.length > w + 1)
                        word = words[++w];
                    while (word.matches(".*[0-9].*") && words.length > w + 1)
                        word = words[++w];
                    while (word.matches("[A-Za-z]") && words.length > w + 1)
                        word = words[++w];
                    while ((word.equals(",") || word.equals("-") || word.equals("") || word.equals("i") || word.equals("ii") || word.equals("iii") || word.equals("iv")) && words.length > w + 1)
                        word = words[++w];
                    while ("lot".equals(word) && words.length > w + 1)
                        word = words[++w];
                    while ("perceel".equals(word) && words.length > w + 1)
                        word = words[++w];
                    while ("loten".equals(word) && words.length > w + 1)
                        word = words[++w];
                    while ("percelen".equals(word) && words.length > w + 1)
                        word = words[++w];
                    while (word.matches(".*[0-9].*") && words.length > w + 1)
                        word = words[++w];
                    while (word.matches("[A-Za-z]") && words.length > w + 1)
                        word = words[++w];
                    if (isAdjective(word) && words.length > w + 1)
                        word = words[++w];
                    String adjective = namedAdjective(word, words.length > w + 1 ? words[w + 1] : null);
                    if (adjective != null && words.length > w + 1)
                        word = words[++w];
                    while (word.matches(".*[0-9].*") && words.length > w + 1)
                        word = words[++w];
                    while (word.matches("[A-Za-z]") && words.length > w + 1)
                        word = words[++w];
                    while ((word.equals(",") || word.equals("-") || word.equals("") || word.equals("i") || word.equals("ii") || word.equals("iii") || word.equals("iv")) && words.length > w + 1)
                        word = words[++w];
                    String entity = namedEntity(word);

                    if (entity != null) {
                        updateFeatureList(features, "REL_LOT_" + (adjective != null ? adjective + "_" : "") + entity);

                        while (words.length > w + 2 && (",".equals(words[w + 1]) || "en".equals(words[w + 1]))) {
                            w += 2;
                            word = words[w];
                            adjective = null;
                            if (words.length > w + 1) {
                                adjective = namedAdjective(word, words[w + 1]);
                                if (adjective != null) {
                                    w++;
                                    word = words[w];
                                }
                            }
                            entity = namedEntity(word);
                            if (entity != null) {
                                updateFeatureList(features, "REL_LOT_" + (adjective != null ? adjective + "_" : "") + entity);
                            }
                        }
                        // updateLotEntityList(new KnowledgeEntity(adjective,word));

                    } else {
                        // System.out.println("Lot entity not found "+word);
                        if (!"".equals(word) && !word.matches("[A-Za-z]"))
                            updateLotEntityList(new KnowledgeEntity(null, word));
                    }
                }

                pos = KnowledgeBase.indexOf(c, lot);
            }
        }
    }

    private void extractRelationFeatures(String line, Map<String, Integer> features) {
        for (KnowledgeVerb vps : verbPhraseList) {
            List<KnowledgeRelation> relalationList = vps.hasRelation(line, namedEntityList, namedAdjectiveList, adjectiveList, verbPhraseList);
            if (relalationList != null) {
                for (KnowledgeRelation rel : relalationList) {
                    updateEntityList(rel.getEntities());
                    // System.out.println(rel.constructVerbs() + ";" +
                    // rel.constructEntities() + " --> "+rel.getEntitieString());
                    rel.addFeatures(features, namedEntityMap);
                }
            }
        }
    }

    public void extractFeatures(String line, Map<String, Integer> features) {
        line = normalizeLine(line);
        extractRelationFeatures(line, features);
        extractLotFeatures(line, features);
        extractNamedEntityFeatures(line, features);
    }

    public Map<String, Integer> extractFeatures() {
        Map<String, Integer> features = new HashMap<String, Integer>();
        String corpusFile = BuildModels.rootDataFolder + "data/omschrijvingCorpus.txt";
        String outF = BuildModels.rootDataFolder + "data/knowledgebase/namedEntitiesPattern.txt";
        String outLotEntities = BuildModels.rootDataFolder + "data/knowledgebase/lotEntitiesPattern.txt";
        try (Writer out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outF), StandardCharsets.UTF_8));
             Writer outLot = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outLotEntities), StandardCharsets.UTF_8));
             BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(corpusFile), StandardCharsets.UTF_8))) {
            String line = br.readLine();

            while (line != null) {
                line = normalizeLine(line);
                extractFeatures(line, features);
                line = br.readLine();
            }
            Map<String, Integer> sortedMapAsc = MachineLearningUtils.sortByComparator(entityList, false);
            //MachineLearningUtils.printMap(sortedMapAsc);
            for (String key : sortedMapAsc.keySet()) {
                if (key != null && !"null".equals(key))
                    out.write(key + ";" + key + " " + sortedMapAsc.get(key) + "\n");
            }

            sortedMapAsc =MachineLearningUtils.sortByComparator(lotEntityList, false);
            //MachineLearningUtils.printMap(sortedMapAsc);
            for (String key : sortedMapAsc.keySet()) {
                if (key != null && !"null".equals(key))
                    outLot.write(key + ";" + key + " " + sortedMapAsc.get(key) + "\n");
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return features;
    }

    public String translateCPV(String cpvCodes) {
        StringBuilder cpvString = new StringBuilder();
        if (cpvCodes != null && cpvCodes.length() > 0) {
            String[] cpvArray = cpvCodes.split(" ");
            for (String cpv : cpvArray) {
                cpv = cpv.trim();
                cpvString.append((cpvString.length() > 0 ? "; " : "") + cpv + " " + cpvTranslations.get(cpv));
            }
        }
        return cpvString.toString();
    }

    public String translateCategory(String category, boolean addHtmlCat) {
        int catPrefixLength = Util.CATEGORY_PREFIX.length();
        int classPrefixLength = Util.CLASS_PREFIX.length();
        StringBuilder categoryBuilder = new StringBuilder();
        if (category != null && category.length() > 0) {
            String[] cats = category.split(":");
            for (String cat : cats) {
                cat = cat.trim();
                if (cat.startsWith(Util.CATEGORY_PREFIX))
                    cat = cat.substring(catPrefixLength);
                String ct = categoryTranslationMap.get(cat);
                if (ct != null)
                    categoryBuilder.append((categoryBuilder.length() > 0 ? "; " : "") + (addHtmlCat ? "<b>" + cat + "</b> " : "") + ct);
            }
            for (String cat : cats) {
                cat = cat.trim();
                if (cat.startsWith(Util.CLASS_PREFIX))
                    cat = cat.substring(classPrefixLength);
                String ct = classTranslationMap.get(cat);
                if (ct != null)
                    categoryBuilder.append((categoryBuilder.length() > 0 ? "; " : "") + "<b>klasse " + ct + "</b>");
            }
        }
        return categoryBuilder.toString();
    }

    public String translateDiscipline(String discipline) {
        int prefixLength = Util.DISCIPLINE_PREFIX.length();
        StringBuilder disciplineBuilder = new StringBuilder();
        if (discipline != null && discipline.length() > 0) {
            String[] ds = discipline.split(":");
            for (String d : ds) {
                d = d.trim();
                if (d.startsWith(Util.DISCIPLINE_PREFIX))
                    d = d.substring(prefixLength);
                String dt = disciplineTranslationMap.get(d);
                if (dt != null)
                    disciplineBuilder.append((disciplineBuilder.length() > 0 ? ", " : "") + dt);
            }
        }
        return disciplineBuilder.toString();
    }

    public KnowledgeCity getCity(String zipcode, String country) {
        if (country.equals("BE")) {
            if (zipcode != null && zipcode.length() > 3 && zipcode.startsWith("B-"))
                zipcode = zipcode.substring(2, zipcode.length());
            return zipCodeCityMap.get(zipcode);
        }
        return null;
    }

    public String translateLocation(String location, String nuts) {
        StringBuilder locationBuilder = new StringBuilder();
        if (location == null)
            location = "";
        int locationEnd = location.indexOf(" (");
        if (locationEnd != -1)
            location = location.substring(0, locationEnd).trim();
        String[] info = location.split(":");
        boolean cityFound = false;
        for (String token : info) {
            if (token.startsWith(Util.LOCATION_PREFIX))
                token = token.substring(1);
            KnowledgeCity city = zipCodeCityMap.get(token);
            if (city != null) {
                cityFound = true;
                locationBuilder.append((locationBuilder.length() == 0 ? "" : ", ") + city.toZipCodeNameString());
            }
        }
        if (!cityFound && nuts != null) {
            boolean arrFound = false;
            info = nuts.split(" ");
            for (String token : info) {
                if (token.startsWith("BE") && token.length() == 5) {
                    String l = nutsTranslationMap.get(token);
                    if (l != null) {
                        arrFound = true;
                        locationBuilder.append((locationBuilder.length() == 0 ? "" : ", ") + l);
                    }
                }
            }
            if (!arrFound) {
                boolean provFound = false;
                for (String token : info) {
                    if (token.startsWith("BE") && token.length() == 4) {
                        String l = nutsTranslationMap.get(token);
                        if (l != null) {
                            provFound = true;
                            locationBuilder.append((locationBuilder.length() == 0 ? "" : ", ") + l);
                        }
                    }
                }
                if (!provFound) {
                    boolean gewestFound = false;
                    StringBuilder country = new StringBuilder();
                    for (String token : info) {
                        if (!token.equals("BEL") && token.startsWith("BE") && token.length() == 3) {
                            String l = nutsTranslationMap.get(token);
                            if (l != null) {
                                gewestFound = true;
                                locationBuilder.append((locationBuilder.length() == 0 ? "" : ", ") + l);
                            }
                        }
                        if (token.equals("BEL") || token.length() == 2)
                            country.append((country.length() == 0 ? "" : " ") + token);
                    }
                    if (!gewestFound)
                        locationBuilder.append((locationBuilder.length() == 0 ? "" : ", ") + country.toString());
                }
            }

        }
        return locationBuilder.toString();
    }

    public String normalizeLine(String line) {
        return " " + knowledgeNormalize.normalizeAdjectiveAndConcat(knowledgeNormalize.normalizeHyphenConcat(knowledgeNormalize.normalizeNamedEntity(knowledgeNormalize.normalizeMultipleAdjectiveAndConcat(line)), null));
    }

    public void showHyphens() {
        String corpusFile = BuildModels.rootDataFolder + "data/omschrijvingCorpus.txt";
        KnowledgeNormalize kn = new KnowledgeNormalize(this);
        Map<String, Integer> hyphenWords = new HashMap<String, Integer>();

        try (BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(corpusFile), StandardCharsets.UTF_8))) {
            String line = br.readLine();
            while (line != null) {
                line = kn.normalizeHyphenConcat(line, hyphenWords);
                line = br.readLine();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Map<String, Integer> sortedMapAsc =MachineLearningUtils.sortByComparator(hyphenWords, false);
       MachineLearningUtils.printMap(sortedMapAsc);
    }

    public boolean isUnknownWord(String word) {
        return !isStopword(word) && !isAdjective(word) && !isNamedAdjective(word) && namedEntity(word) == null && verbPhrase(word) == null && !word.trim().equals("");
    }

    /**
     * Construct entities and adjectives from the CPV translation text.
     */
    public void constructEntitiesAdjectivesFromCpv() {
        List<String> words = new ArrayList<>();
        String newEntities = BuildModels.rootDataFolder + "data/newEntities_nl_be.txt";
        String newAdjectives = BuildModels.rootDataFolder + "data/newAdjectives_nl_be.txt";
        for (String c : getCpvTranslations().keySet()) {
            String translation = getCpvTranslations().get(c);
            // System.out.println(translation);
            translation = normalizeLine(translation).toLowerCase();
            for (String t : MachineLearningTextUtils.tokens(translation)) {
                if (isUnknownWord(t)) {
                    System.out.println(translation + " :: " + t);
                    if (!words.contains(t))
                        words.add(t);
                }
            }
        }
        Collections.sort(words);
        List<String> entityList = new ArrayList<>();
        List<String> adjectiveList = new ArrayList<>();
        String single = "";
        for (String word : words) {
            if (word.endsWith("e") && !word.endsWith("ie") && !word.endsWith("tje")) {
                adjectiveList.add(word);
            } else {
                if (word.endsWith("'s")) {
                    single = word.substring(0, word.length() - 2);
                    entityList.add(word + ";" + single + " 3");
                    if (isUnknownWord(single))
                        entityList.add(single + ";" + single + " 3");
                } else if (word.endsWith("s")) {
                    single = word.substring(0, word.length() - 1);
                    entityList.add(word + ";" + single + " 3");
                    if (isUnknownWord(single))
                        entityList.add(single + ";" + single + " 3");
                } else if (word.endsWith("nnen") || word.endsWith("ssen") || word.endsWith("rren") || word.endsWith("ppen") || word.endsWith("ggen") || word.endsWith("kken") || word.endsWith("dden")
                        || word.endsWith("tten") || word.endsWith("bben") || word.endsWith("ffen") || word.endsWith("llen") || word.endsWith("mmen")) {
                    single = word.substring(0, word.length() - 3);
                    entityList.add(word + ";" + single + " 3");
                    if (isUnknownWord(single))
                        entityList.add(single + ";" + single + " 3");
                } else if (word.endsWith("enen")) {
                    single = word.substring(0, word.length() - 3) + "en";
                    entityList.add(word + ";" + single + " 3");
                    if (isUnknownWord(single))
                        entityList.add(single + ";" + single + " 3");
                } else if (word.endsWith("anen")) {
                    single = word.substring(0, word.length() - 3) + "an";
                    entityList.add(word + ";" + single + " 3");
                    if (isUnknownWord(single))
                        entityList.add(single + ";" + single + " 3");
                } else if (word.endsWith("onen")) {
                    single = word.substring(0, word.length() - 3) + "on";
                    entityList.add(word + ";" + single + " 3");
                    if (isUnknownWord(single))
                        entityList.add(single + ";" + single + " 3");
                } else if (word.endsWith("jnen") || word.endsWith("inen")) {
                    single = word.substring(0, word.length() - 2);
                    entityList.add(word + ";" + single + " 3");
                    if (isUnknownWord(single))
                        entityList.add(single + ";" + single + " 3");
                } else if (word.endsWith("open")) {
                    single = word.substring(0, word.length() - 3) + "op";
                    entityList.add(word + ";" + single + " 3");
                    if (isUnknownWord(single))
                        entityList.add(single + ";" + single + " 3");
                } else if (word.endsWith("heden")) {
                    single = word.substring(0, word.length() - 3) + "id";
                    entityList.add(word + ";" + single + " 3");
                    if (isUnknownWord(single))
                        entityList.add(single + ";" + single + " 3");
                } else if (word.endsWith("en") && !(word.endsWith("ien") || word.endsWith("een") || word.endsWith("oen") || word.endsWith("aen"))) {
                    single = word.substring(0, word.length() - 2);
                    entityList.add(word + ";" + single + " 3");
                    if (isUnknownWord(single))
                        entityList.add(single + ";" + single + " 3");
                } else {
                    entityList.add(word + ";" + word + " 3");
                }
            }
        }

        MachineLearningFileUtils.writeList(entityList, newEntities);
        MachineLearningFileUtils.writeList(adjectiveList, newAdjectives);
    }
    public static boolean isPunctuation(String s) {
        if (s.equals("(") || s.equals(")") || s.equals(",")|| s.equals("#")) return true;
        return false;
    }
    public static boolean isNumber(String s) {
        return s.matches("[0-9]*");
    }
    /**
     * Construct entities and adjectives from the corpus which are not in the stopword list
     *
     */
    public void constructNewEntitiesAdjectivesFromCorpus() {
        List<String> words = new ArrayList<>();
        File corpusFile = new File(BuildModels.rootDataFolder + "data/", "tender_corpus.txt");

        String newEntities = BuildModels.rootDataFolder + "data/newEntities_nl_be.txt";
        String newAdjectives = BuildModels.rootDataFolder + "data/newAdjectives_nl_be.txt";

        try (BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(corpusFile), StandardCharsets.UTF_8))) {
            String line = br.readLine();
            while (line != null) {
                if (!line.startsWith("#")) {
                    for (String t : MachineLearningTextUtils.tokens(line.toLowerCase())) {
                        if (t.trim().length() > 1 && !isPunctuation(t) && !isNumber(t) && isUnknownWord(t)) {
                            words.add(t);
                        }
                    }
                }
                line = br.readLine();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


        //Collections.sort(words);
        Map<String, Integer> entityList = new HashMap<>();
        Map<String, Integer> adjectiveList = new HashMap<>();
        String single = "";
        for (String word : words) {
            if (word.endsWith("e") && !word.endsWith("ie") && !word.endsWith("tje")) {
                updateFeatureList(adjectiveList,word);
            } else {
                if (word.endsWith("'s")) {
                    single = word.substring(0, word.length() - 2);
                    updateFeatureList(entityList,word + ";" + single);
                    if (isUnknownWord(single))
                        updateFeatureList(entityList,single + ";" + single);
                } else if (word.endsWith("s")) {
                    single = word.substring(0, word.length() - 1);
                    updateFeatureList(entityList,word + ";" + single);
                    if (isUnknownWord(single))
                        updateFeatureList(entityList,single + ";" + single);
                } else if (word.endsWith("nnen") || word.endsWith("ssen") || word.endsWith("rren") || word.endsWith("ppen") || word.endsWith("ggen") || word.endsWith("kken") || word.endsWith("dden")
                        || word.endsWith("tten") || word.endsWith("bben") || word.endsWith("ffen") || word.endsWith("llen") || word.endsWith("mmen")) {
                    single = word.substring(0, word.length() - 3);
                    updateFeatureList(entityList,word + ";" + single);
                    if (isUnknownWord(single))
                        updateFeatureList(entityList,single + ";" + single);
                } else if (word.endsWith("enen")) {
                    single = word.substring(0, word.length() - 3) + "en";
                    updateFeatureList(entityList,word + ";" + single);
                    if (isUnknownWord(single))
                        updateFeatureList(entityList,single + ";" + single);
                } else if (word.endsWith("anen")) {
                    single = word.substring(0, word.length() - 3) + "an";
                    updateFeatureList(entityList,word + ";" + single);
                    if (isUnknownWord(single))
                        updateFeatureList(entityList,single + ";" + single);
                } else if (word.endsWith("onen")) {
                    single = word.substring(0, word.length() - 3) + "on";
                    updateFeatureList(entityList,word + ";" + single);
                    if (isUnknownWord(single))
                        updateFeatureList(entityList,single + ";" + single);
                } else if (word.endsWith("jnen") || word.endsWith("inen")) {
                    single = word.substring(0, word.length() - 2);
                    updateFeatureList(entityList,word + ";" + single);
                    if (isUnknownWord(single))
                        updateFeatureList(entityList,single + ";" + single);
                } else if (word.endsWith("open")) {
                    single = word.substring(0, word.length() - 3) + "op";
                    updateFeatureList(entityList,word + ";" + single);
                    if (isUnknownWord(single))
                        updateFeatureList(entityList,single + ";" + single);
                } else if (word.endsWith("heden")) {
                    single = word.substring(0, word.length() - 3) + "id";
                    updateFeatureList(entityList,word + ";" + single);
                    if (isUnknownWord(single))
                        updateFeatureList(entityList,single + ";" + single);
                } else if (word.endsWith("en") && !(word.endsWith("ien") || word.endsWith("een") || word.endsWith("oen") || word.endsWith("aen"))) {
                    single = word.substring(0, word.length() - 2);
                    updateFeatureList(entityList,word + ";" + single);
                    if (isUnknownWord(single))
                        updateFeatureList(entityList,single + ";" + single);
                } else {
                    updateFeatureList(entityList,word + ";" + word);
                }
            }
        }
        entityList =MachineLearningUtils.sortByComparator(entityList, false);
        adjectiveList =MachineLearningUtils.sortByComparator(adjectiveList, false);

        MachineLearningFileUtils.writeMap(entityList, newEntities);
        MachineLearningFileUtils.writeMap(adjectiveList, newAdjectives);
    }
}
