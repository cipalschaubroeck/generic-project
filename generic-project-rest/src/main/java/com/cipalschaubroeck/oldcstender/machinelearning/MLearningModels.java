package com.cipalschaubroeck.oldcstender.machinelearning;

import com.cipalschaubroeck.cstender.domain.Tender;
import com.cipalschaubroeck.oldcstender.machinelearning.classify.ClassifierInterface;
import com.cipalschaubroeck.oldcstender.machinelearning.classify.FeatureList;
import com.cipalschaubroeck.oldcstender.machinelearning.classify.FormatConversions;
import com.cipalschaubroeck.oldcstender.machinelearning.classify.Instance;
import com.cipalschaubroeck.oldcstender.machinelearning.classify.LibSvmClassifier;
import com.cipalschaubroeck.oldcstender.machinelearning.shared.AkteUtilities;
import com.cipalschaubroeck.oldcstender.shared.Util;
import com.cipalschaubroeck.oldcstender.shared.xml.Lot;
import com.cipalschaubroeck.oldcstender.shared.xml.Loten;
import libsvm.svm;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * MachineLearning models utility class
 */

public class MLearningModels {
    public static String[] DISCIPLINES = { "sa","bt","wr","gr","cv","bep","el","af","afv","bo","dt","gw","ho","ijm","inf"
            ,"mhe","mm","sg","sm","sp","spi","fin","stu","tex","vm","wa","ab","div"};

    public static String[] DISCARD_CPV_DISCIPLINE = { "sa","el"};

    public static FeatureList initCpvClassifierFeatureStructure() throws IOException {
        return FormatConversions.readFeatureIndexList(BuildModels.rootExtFolder + "tender/tender_cpv.feature");
    }

    /**
     * Load the SVM models into a HashMap
     *
     * @param cpvList
     * @return SVM model HashMap
     * @throws FileNotFoundException
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public static HashMap<String, ClassifierInterface> initCpvClassifierLibSvmMap(List<String> cpvList) throws FileNotFoundException, IOException, ClassNotFoundException {
        HashMap<String, ClassifierInterface> cpvClassifierMap = new HashMap<String, ClassifierInterface>();
        int start = 0;
        int end = cpvList.size();
        for (int i = start; i < end; i++) {
            String cpv = cpvList.get(i);
            System.out.println(i + " cpv "+cpv);
            try {
                LibSvmClassifier classifier =  loadLibSvmModel(BuildModels.rootExtFolder + "tender/model/smo_" + cpv + ".model");
                cpvClassifierMap.put(cpv, classifier);
            } catch (Exception e) {
                System.out.println("Classifier not found for " + cpv);
            }
        }
        return cpvClassifierMap;
    }




    static public void updateTenderAI(Tender tender, KnowledgeBase filter, FeatureList dataStructure, HashMap<String, ClassifierInterface> classifiers,
                                      KnowledgeBase knowledgeBase) {
        StringBuilder instaceString = new StringBuilder();
        StringBuilder cpvBuilder = new StringBuilder(tender.getCpvNoParentAI());
        StringBuilder cpvPureAIBuilder = new StringBuilder(tender.getCpvPureAI());
        tender.setLanguage("NL");
        String[] clist = null;
        if (tender.getCpvNoParent() != null && tender.getCpvNoParent().length() > 0)
            clist = tender.getCpvNoParent().split(" ");

        String title = tender.getTitle();
        String essence = tender.getEssence();
        updateAI(title, essence, filter, dataStructure, classifiers, instaceString, clist, cpvBuilder, cpvPureAIBuilder, knowledgeBase);

        Loten loten = tender.getLoten();
        if (loten != null) {
            for (Lot lot : loten.getLotList()) {
                title = lot.getTitle();
                essence = lot.getEssence();
                //System.out.println("updateAI LOT "+title + " ::"+essence);
                updateAI(title, essence, filter, dataStructure, classifiers, instaceString, clist, cpvBuilder, cpvPureAIBuilder, knowledgeBase);
            }
        }

        tender.setFeatures("NL", instaceString.toString());
        tender.setCpvNoParentAI(cpvBuilder.toString());
        tender.setCpvPureAI(cpvPureAIBuilder.toString());
        tender.setCpvAI(AkteUtilities.addCpvParents(tender.getCpvNoParentAI()));
    }

    /**
     * eg. 45430000 -> 45400000
     * @param cpvCode
     * @return
     */
    static public String constructParentCpv(String cpvCode) {
        String parent = null;
        if (cpvCode.length() == 8) {
            String last = cpvCode.substring(2);
            if (!"000000".equals(last)) {
                for (int i = 5; i > -1; i--) {
                    if (cpvCode.charAt(2+i) != '0') {
                        if (i == 5) {
                            parent = cpvCode.substring(0,i+2)+"0";
                            break;
                        } else {
                            parent = cpvCode.substring(0,i+2)+"0"+cpvCode.substring(i+3);
                            break;
                        }
                    }
                }
            }
        }
        return parent;
    }
    /**
     * Update the tender discipline based on the CpvNoParentAI in case ai is true, CpvNoParent in case ai is false
     *
     * @param tender
     * @param cpvDisciplineMap
     * @param categoryCpvMap
     * @param ai if true the DisciplineAI will be updated, false will update the Discipline of tender
     */
    static public void updateDiscipline(Tender tender, HashMap<String, List<String>> cpvDisciplineMap, Map<String, String> categoryCpvMap, boolean ai) {
        String[] clist = null;
        if (ai)
            clist = tender.getCpvNoParentAI().trim().split(" ");
        else
            clist = tender.getCpvNoParent().trim().split(" ");
        StringBuilder disciplineBuilder = new StringBuilder();

        if (clist != null) {
            for (String c : clist) {
                boolean found = updateDiscipline(cpvDisciplineMap, disciplineBuilder, c);
                while (!found && c != null) {
                    c = constructParentCpv(c);
                    if (c != null)
                        found = updateDiscipline(cpvDisciplineMap, disciplineBuilder, c);
                }
            }
        }

        if (disciplineBuilder.length() > 0)
            disciplineBuilder.append(":");

        if (ai) {
            tender.setDisciplinesCpvAI(disciplineBuilder.toString().trim());
            upgradeCpvAndDisciplineWithCategory(tender,categoryCpvMap);

        } else
            tender.setDisciplines(disciplineBuilder.toString().trim());
    }

    private static boolean updateDiscipline(HashMap<String, List<String>> cpvDisciplineMap, StringBuilder disciplineBuilder, String c) {
        boolean found = false;
        for (String discipline : DISCIPLINES) {
            List<String> cpvDisciplineList = cpvDisciplineMap.get(discipline);
            if (cpvDisciplineList.contains(c)) {
                found = true;
                if (disciplineBuilder.indexOf(":" + Util.DISCIPLINE_PREFIX + discipline.toUpperCase()) == -1)
                    disciplineBuilder.append(":" + Util.DISCIPLINE_PREFIX + discipline.toUpperCase());
            }
        }
        return found;
    }



    static public void upgradeCpvAndDisciplineWithCategory(Tender tender, Map<String, String> categoryCpvMap) {
        String category = tender.getCategoryAI();
        String discipline = tender.getDisciplinesCpvAI();
        if (category != null && category.length() > 0) {
            String[] categories = category.split(":");
            for (String cat : categories) {
                if (cat.startsWith(Util.CATEGORY_PREFIX)) {
                    cat = cat.substring(Util.CATEGORY_PREFIX.length());

                    String dis = AkteUtilities.getDisciplineTable().get(cat);
                    if (discipline.indexOf(":"+Util.DISCIPLINE_PREFIX+dis+":") == -1) {
                        // add discipline
                        discipline += ("".equals(discipline) ? ":" : "" )+Util.DISCIPLINE_PREFIX+dis+":";
                        tender.setDisciplinesCpvAI(discipline);
                        // add cpv for the discipline

                        String cpv = categoryCpvMap.get(cat);

                        String currentCpv = tender.getCpvNoParentAI();
                        String pureAICpv = tender.getCpvPureAI();
                        //System.out.println(cat +":"+cpv+ ":"+currentCpv);
                        if (cpv != null && currentCpv.indexOf(cpv) == -1) {
                            if (currentCpv == null || currentCpv.length() == 0)
                                currentCpv = cpv;
                            else
                                currentCpv += " "+cpv;
                            if (pureAICpv == null || pureAICpv.length() == 0)
                                pureAICpv = cpv;
                            else
                                pureAICpv += " "+cpv;
                            tender.setCpvNoParentAI(currentCpv);
                            tender.setCpvPureAI(pureAICpv);
                            tender.setCpvAI(AkteUtilities.addCpvParents(currentCpv));
                        }
                    }
                }
            }
        }
        if ((tender.getDisciplinesCpvAI() == null || tender.getDisciplinesCpvAI().length() == 0) && tender.getCpvNoParentAI().indexOf("45000000") != -1 )
            tender.setDisciplinesCpvAI(":" + Util.DISCIPLINE_PREFIX + "AB"+":");
        if ((tender.getDisciplinesCpvAI() == null || tender.getDisciplinesCpvAI().length() == 0) )
            tender.setDisciplinesCpvAI(":" + Util.DISCIPLINE_PREFIX + "DIV"+":");

    }

    static private void updateAI(String title, String essence, KnowledgeBase filter, FeatureList dataStructure, HashMap<String, ClassifierInterface> classifiers,
                                 StringBuilder instaceString, String[] clist, StringBuilder cpvBuilder, StringBuilder cpvPureAIBuilder, KnowledgeBase knowledgeBase) {
        //System.out.println("updateAI:"+title);
        //Instance instance = createEssentieInstanceNominal(title, essence, dataStructure, filter, instaceString);
        com.cipalschaubroeck.oldcstender.machinelearning.classify.Instance instance = createEssentieSparseInstanceNominal(title, essence, dataStructure, filter, instaceString);
        for (String cpv : classifiers.keySet()) {
            try {

                if (instance != null && classifiers.get(cpv).classifyInstance(instance) == 1) {
                    if (cpvBuilder.indexOf(cpv) == -1) {
                        cpvBuilder.append((cpvBuilder.length() == 0 ? "" : " ") + cpv);
                    }
                    if (cpvPureAIBuilder.indexOf(cpv) == -1) {
                        cpvPureAIBuilder.append((cpvPureAIBuilder.length() == 0 ? "" : " ") + cpv);
                    }
                    //System.out.println("classify "+cpv + " "+title);
                } else {
                    //System.out.println("NOT classify "+cpv+ " "+title);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        // merge cpv wit AIcpv
        if (clist != null) {
            for (String cpv : clist) {
                if (cpvBuilder.indexOf(cpv) == -1)
                    cpvBuilder.append(" " + cpv);
            }
        }
    }
    static public Instance createEssentieSparseInstanceNominal(String title, String essence, FeatureList format, KnowledgeBase knowledgeBase, StringBuilder instanceString) {
        Instance sparseInstance = new Instance();
        Set<String> titleFeaturesNoSuffix = knowledgeBase.extractFeaturesAndConcepts(title);
        Set<String> titleFeatures = new HashSet<String>();
        for (String feature : titleFeaturesNoSuffix)  titleFeatures.add(feature+"_TITLE");
        Set<String> essenseFeatures = knowledgeBase.extractFeaturesAndConcepts(essence);
        Set<String> features = new HashSet<>(titleFeatures);
        features.addAll(essenseFeatures);
        sparseInstance.constructInstanceFromFeatures(features, format);
        if (sparseInstance.getFeatureList().size() == 0) {
            sparseInstance = null;
        } else {
            instanceString = instanceString.append(sparseInstance.toFeatureString(format));
        }
        return sparseInstance;
    }

    static public LibSvmClassifier loadLibSvmModel(String filename) throws FileNotFoundException, IOException, ClassNotFoundException {
        return new LibSvmClassifier(svm.svm_load_model(filename));
    }
}
