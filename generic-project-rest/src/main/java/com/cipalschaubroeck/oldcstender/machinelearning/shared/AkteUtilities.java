package com.cipalschaubroeck.oldcstender.machinelearning.shared;

import com.cipalschaubroeck.cstender.domain.AwardItem;
import com.cipalschaubroeck.cstender.domain.Contact;
import com.cipalschaubroeck.cstender.domain.Contractor;
import com.cipalschaubroeck.cstender.domain.Government;
import com.cipalschaubroeck.cstender.domain.Type;
import com.cipalschaubroeck.oldcstender.machinelearning.KnowledgeBase;
import com.cipalschaubroeck.oldcstender.machinelearning.KnowledgeCity;
import com.cipalschaubroeck.oldcstender.machinelearning.MLearningModels;
import com.cipalschaubroeck.oldcstender.shared.Util;
import com.cipalschaubroeck.oldcstender.shared.xml.Lot;
import com.cipalschaubroeck.oldcstender.shared.xml.Loten;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Utilities for extracting information from enotification XML files.
 */
public class AkteUtilities {
    final public static String AKTE_PRIOR_INFORMATION = "1";
    final public static String AKTE_CONTRACT = "2";
    final public static String AKTE_CONTRACT_AWARD = "3";
    final public static String AKTE_PRIOR_INFORMATION_UTILITIES = "4";
    final public static String AKTE_CONTRACT_UTILITIES = "5";
    final public static String AKTE_QUALIFICATION_SYSTEM_UTILITIES = "7";
    final public static String AKTE_BUYER_PROFILE = "8";
    final public static String AKTE_SIMPLIFIED_CONTRACT = "9";
    final public static String AKTE_CONCESSION = "10";
    final public static String AKTE_CONTRACT_CONCESSIONAIRE = "11";
    final public static String AKTE_DESIGN_CONTEST = "12";
    final public static String AKTE_VOLUNTARY_EX_ANTE_TRANSPARENCY_NOTICE = "15";
    final public static String AKTE_PRIOR_INFORMATION_DEFENCE = "16";
    final public static String AKTE_CONTRACT_DEFENCE = "17";
    final public static String AKTE_CONTRACT_SUB_DEFENCE = "19";

    static public String[] mapCategoryCodes = { "A", "A1", "B", "B1", "C", "C1", "C2", "C3", "C4", "C5", "C6", "C7", "D", "D1", "D2", "D3", "D4", "D5", "D6", "D7", "D8", "D9", "D10", "D11", "D12",
            "D13", "D14", "D15", "D16", "D17", "D18", "D19", "D20", "D21", "D22", "D23", "D24", "D25", "D26", "D27", "D28", "D29", "E", "E1", "E2", "E3", "E4", "F", "F1", "F2", "F3", "G", "G1", "G2", "G3",
            "G4", "G5", "H", "H1", "H2", "K", "K1", "K2", "K3", "L", "L1", "L2", "M", "M1", "N", "N1", "N2", "P", "P1", "P2", "P3", "P4", "S", "S1", "S2", "S3", "S4", "T", "T1", "T2", "T3", "T4", "T5",
            "T6", "T7", "T8", "T9", "T10", "U", "V", "X" };

    static private String[] mapSureCategory = { "klasse", "A1", "B1", "C1", "C2", "C3", "C4", "C5", "C6", "C7", "D1", "D2", "D3", "D4", "D5", "D6", "D7", "D8", "D9", "D10", "D11", "D12", "D13", "D14",
            "D15", "D16", "D17", "D18", "D19", "D20", "D21", "D22", "D23", "D24", "D25", "D26", "D27", "D28", "D29", "E1", "E2", "E3", "E4", "F1", "F2", "F3", "G1", "G2", "G3", "G4", "G5", "H1", "H2", "K",
            "K1", "K2", "K3", "L1", "L2", "M1", "N1", "N2", "P1", "P2", "P3", "P4", "S1", "S2", "S3", "S4", "T1", "T2", "T3", "T4", "T5", "T6", "T7", "T8", "T9", "T10" };

    static private String[] map5 = { "0", "1", "2", "3", "4", "5", "6", "7", "8" };
    static private String[] mapCategory = { "klasse", "Klasse", "classe", "Classe", "homologation", "Homologation", "Cat.", "cat.", "Erkenning", "erkenning", "ondercategorie", "Ondercategorie",
            "categorie", "Categorie", "catégories", "Catégories", "Catégorie", "catégorie", "Agréation", "agréation" };
    static private String[] mapCategorySmall = { "klasse", "categorie" };
    static private String[] mapClass = { "klasse", "Klasse", "classe", "Classe", "erkenningsklasse", "Erkenningsklasse" };
    static private String[] mapLot = { "lot", "perceel", "percelen", "loten" };

    private static Hashtable<String, String> erkDisciplineTable = null;
    private Document document = null;

    public String getEssentie(String taal) {
        Document doc = getDocument(taal);
        if (doc != null) {
            String e = XmlUtilities.getNodePlainText("//SHORT_CONTRACT_DESCRIPTION", doc);
            if (e.length() == 0)
                e = XmlUtilities.getNodePlainText("//SHORT_DESCRIPTION_CONTRACT", doc); // F12
            if (e.length() == 0)
                e = XmlUtilities.getNodePlainText("//DESCRIPTION", doc); // F7
            if (e.length() == 0)
                e = XmlUtilities.getNodePlainText("//QUANTITY_SCOPE_WORKS", doc);// F1
            return e;
        }
        return "";
    }

    public String getForm() {
        String form = null;
        String language = null;

        if (form == null) {
            form = XmlUtilities.getNodeValue("//ADDITIONAL_INFORMATION_CORRIGENDUM/@FORM", document); // F14
            language = XmlUtilities.getNodeValue("//ADDITIONAL_INFORMATION_CORRIGENDUM/@LG", document);
        }
        if (form == null) {
            form = XmlUtilities.getNodeValue("//PRIOR_INFORMATION/@FORM", document); // F1
            language = XmlUtilities.getNodeValue("//PRIOR_INFORMATION/@LG", document);
        }
        if (form == null) {
            form = XmlUtilities.getNodeValue("//CONTRACT/@FORM", document); // F2
            language = XmlUtilities.getNodeValue("//CONTRACT/@LG", document);
        }

        if (form == null) {
            form = XmlUtilities.getNodeValue("//CONTRACT_AWARD/@FORM", document); // F3
            language = XmlUtilities.getNodeValue("//CONTRACT_AWARD/@LG", document);
        }
        if (form == null) {
            form = XmlUtilities.getNodeValue("//PERIODIC_INDICATIVE_UTILITIES/@FORM", document); // F4
            language = XmlUtilities.getNodeValue("//PERIODIC_INDICATIVE_UTILITIES/@LG", document);
        }
        if (form == null) {
            form = XmlUtilities.getNodeValue("//CONTRACT_UTILITIES/@FORM", document); // F5
            language = XmlUtilities.getNodeValue("//CONTRACT_UTILITIES/@LG", document);
        }
        if (form == null) {
            form = XmlUtilities.getNodeValue("//CONTRACT_AWARD_UTILITIES/@FORM", document); // F6
            language = XmlUtilities.getNodeValue("//CONTRACT_AWARD_UTILITIES/@LG", document);
        }
        if (form == null) {
            form = XmlUtilities.getNodeValue("//QUALIFICATION_SYSTEM_UTILITIES/@FORM", document); // F7
            language = XmlUtilities.getNodeValue("//QUALIFICATION_SYSTEM_UTILITIES/@LG", document);
        }

        if (form == null) {
            form = XmlUtilities.getNodeValue("//SIMPLIFIED_CONTRACT/@FORM", document); // F9
            language = XmlUtilities.getNodeValue("//SIMPLIFIED_CONTRACT/@LG", document);
        }
        if (form == null) {
            form = XmlUtilities.getNodeValue("//CONCESSION/@FORM", document); // F10
            language = XmlUtilities.getNodeValue("//CONCESSION/@LG", document);
        }

        if (form == null) {
            form = XmlUtilities.getNodeValue("//CONTRACT_CONCESSIONAIRE/@FORM", document); // F11
            language = XmlUtilities.getNodeValue("//CONTRACT_CONCESSIONAIRE/@LG", document);
        }
        if (form == null) {
            form = XmlUtilities.getNodeValue("//DESIGN_CONTEST/@FORM", document); // F12
            language = XmlUtilities.getNodeValue("//DESIGN_CONTEST/@LG", document);
        }
        if (form == null) {
            form = XmlUtilities.getNodeValue("//RESULT_DESIGN_CONTEST/@FORM", document); // F13
            language = XmlUtilities.getNodeValue("//RESULT_DESIGN_CONTEST/@LG", document);
        }

        if (form == null) {
            form = XmlUtilities.getNodeValue("//VOLUNTARY_EX_ANTE_TRANSPARENCY_NOTICE/@FORM", document); // F15
            language = XmlUtilities.getNodeValue("//VOLUNTARY_EX_ANTE_TRANSPARENCY_NOTICE/@LG", document);
        }
        if (form == null) {
            form = XmlUtilities.getNodeValue("//PRIOR_INFORMATION_DEFENCE/@FORM", document); // F16
            language = XmlUtilities.getNodeValue("//PRIOR_INFORMATION_DEFENCE/@LG", document);
        }

        if (form == null) {
            form = XmlUtilities.getNodeValue("//CONTRACT_DEFENCE/@FORM", document); // F17
            language = XmlUtilities.getNodeValue("//CONTRACT_DEFENCE/@LG", document);
        }
        if (form == null) {
            form = XmlUtilities.getNodeValue("//CONTRACT_AWARD_DEFENCE/@FORM", document); // F18
            // ??
            // AWARD??
            language = XmlUtilities.getNodeValue("//CONTRACT_AWARD_DEFENCE/@LG", document);
        }
        if (form == null) {
            form = XmlUtilities.getNodeValue("//MARKET_PLACE/@FORM", document); // F50
            language = XmlUtilities.getNodeValue("//MARKET_PLACE/@LG", document);
        }

        if (form == null) {
            form = XmlUtilities.getNodeValue("//QUALIFICATION_SYSTEM/@FORM", document); // F53
            language = XmlUtilities.getNodeValue("//QUALIFICATION_SYSTEM/@LG", document);
        }
        if (form == null) {
            form = XmlUtilities.getNodeValue("//CANDIDATES_LIST/@FORM", document); // F55
            language = XmlUtilities.getNodeValue("//CANDIDATES_LIST/@LG", document);
        }
        if (form == null) {
            form = XmlUtilities.getNodeValue("//NOTICE_OF_PURCHASE/@FORM", document); // F56
            language = XmlUtilities.getNodeValue("//NOTICE_OF_PURCHASE/@LG", document);
        }
        if (form == null) {
            form = XmlUtilities.getNodeValue("//FORM_FREE_TEXT/@FORM", document); // F57
            language = XmlUtilities.getNodeValue("//FORM_FREE_TEXT/@LG", document);
        }
        if (form == null) {
            form = XmlUtilities.getNodeValue("//REQUEST_FOR_INFORMATION/@FORM", document); // F58
            language = XmlUtilities.getNodeValue("//REQUEST_FOR_INFORMATION/@LG", document);
        }
        if (form == null) {
            System.out.println("OOOOOOOOOOOOOOOOOOOOOOOOO");
            System.out.println("FORM is null for ");
            System.out.println("OOOOOOOOOOOOOOOOOOOOOOOOO");
        }
        return form;
    }

    public String getLanguage() {
        String form = null;
        String language = null;
        form = XmlUtilities.getNodeValue("//CONTRACT/@FORM", document); // F2
        language = XmlUtilities.getNodeValue("//CONTRACT/@LG", document);

        if (form == null) {
            form = XmlUtilities.getNodeValue("//ADDITIONAL_INFORMATION_CORRIGENDUM/@FORM", document); // F14
            language = XmlUtilities.getNodeValue("//ADDITIONAL_INFORMATION_CORRIGENDUM/@LG", document);
        }
        if (form == null) {
            form = XmlUtilities.getNodeValue("//PRIOR_INFORMATION/@FORM", document); // F1
            language = XmlUtilities.getNodeValue("//PRIOR_INFORMATION/@LG", document);
        }
        if (form == null) {
            form = XmlUtilities.getNodeValue("//CONTRACT_AWARD/@FORM", document); // F3
            language = XmlUtilities.getNodeValue("//CONTRACT_AWARD/@LG", document);
        }
        if (form == null) {
            form = XmlUtilities.getNodeValue("//PERIODIC_INDICATIVE_UTILITIES/@FORM", document); // F4
            language = XmlUtilities.getNodeValue("//PERIODIC_INDICATIVE_UTILITIES/@LG", document);
        }
        if (form == null) {
            form = XmlUtilities.getNodeValue("//CONTRACT_UTILITIES/@FORM", document); // F5
            language = XmlUtilities.getNodeValue("//CONTRACT_UTILITIES/@LG", document);
        }
        if (form == null) {
            form = XmlUtilities.getNodeValue("//CONTRACT_AWARD_UTILITIES/@FORM", document); // F6
            language = XmlUtilities.getNodeValue("//CONTRACT_AWARD_UTILITIES/@LG", document);
        }
        if (form == null) {
            form = XmlUtilities.getNodeValue("//QUALIFICATION_SYSTEM_UTILITIES/@FORM", document); // F7
            language = XmlUtilities.getNodeValue("//QUALIFICATION_SYSTEM_UTILITIES/@LG", document);
        }

        if (form == null) {
            form = XmlUtilities.getNodeValue("//SIMPLIFIED_CONTRACT/@FORM", document); // F9
            language = XmlUtilities.getNodeValue("//SIMPLIFIED_CONTRACT/@LG", document);
        }
        if (form == null) {
            form = XmlUtilities.getNodeValue("//CONCESSION/@FORM", document); // F10
            language = XmlUtilities.getNodeValue("//CONCESSION/@LG", document);
        }

        if (form == null) {
            form = XmlUtilities.getNodeValue("//CONTRACT_CONCESSIONAIRE/@FORM", document); // F11
            language = XmlUtilities.getNodeValue("//CONTRACT_CONCESSIONAIRE/@LG", document);
        }
        if (form == null) {
            form = XmlUtilities.getNodeValue("//DESIGN_CONTEST/@FORM", document); // F12
            language = XmlUtilities.getNodeValue("//DESIGN_CONTEST/@LG", document);
        }
        if (form == null) {
            form = XmlUtilities.getNodeValue("//RESULT_DESIGN_CONTEST/@FORM", document); // F13
            language = XmlUtilities.getNodeValue("//RESULT_DESIGN_CONTEST/@LG", document);
        }
        if (form == null) {
            form = XmlUtilities.getNodeValue("//VOLUNTARY_EX_ANTE_TRANSPARENCY_NOTICE/@FORM", document); // F15
            language = XmlUtilities.getNodeValue("//VOLUNTARY_EX_ANTE_TRANSPARENCY_NOTICE/@LG", document);
        }
        if (form == null) {
            form = XmlUtilities.getNodeValue("//PRIOR_INFORMATION_DEFENCE/@FORM", document); // F16
            language = XmlUtilities.getNodeValue("//PRIOR_INFORMATION_DEFENCE/@LG", document);
        }
        if (form == null) {
            form = XmlUtilities.getNodeValue("//CONTRACT_DEFENCE/@FORM", document); // F17
            language = XmlUtilities.getNodeValue("//CONTRACT_DEFENCE/@LG", document);
        }
        if (form == null) {
            form = XmlUtilities.getNodeValue("//CONTRACT_AWARD_DEFENCE/@FORM", document); // F18
            // ??
            // AWARD??
            language = XmlUtilities.getNodeValue("//CONTRACT_AWARD_DEFENCE/@LG", document);
        }
        if (form == null) {
            form = XmlUtilities.getNodeValue("//MARKET_PLACE/@FORM", document); // F50
            language = XmlUtilities.getNodeValue("//MARKET_PLACE/@LG", document);
        }

        if (form == null) {
            form = XmlUtilities.getNodeValue("//QUALIFICATION_SYSTEM/@FORM", document); // F53
            language = XmlUtilities.getNodeValue("//QUALIFICATION_SYSTEM/@LG", document);
        }
        if (form == null) {
            form = XmlUtilities.getNodeValue("//CANDIDATES_LIST/@FORM", document); // F55
            language = XmlUtilities.getNodeValue("//CANDIDATES_LIST/@LG", document);
        }
        if (form == null) {
            form = XmlUtilities.getNodeValue("//NOTICE_OF_PURCHASE/@FORM", document); // F56
            language = XmlUtilities.getNodeValue("//NOTICE_OF_PURCHASE/@LG", document);
        }
        if (form == null) {
            form = XmlUtilities.getNodeValue("//FORM_FREE_TEXT/@FORM", document); // F57
            language = XmlUtilities.getNodeValue("//FORM_FREE_TEXT/@LG", document);
        }
        if (form == null) {
            form = XmlUtilities.getNodeValue("//REQUEST_FOR_INFORMATION/@FORM", document); // F58
            language = XmlUtilities.getNodeValue("//REQUEST_FOR_INFORMATION/@LG", document);
        }
        if (form == null) {
            System.out.println("OOOOOOOOOOOOOOOOOOOOOOOOO");
            System.out.println("FORM is null for ");
            System.out.println("OOOOOOOOOOOOOOOOOOOOOOOOO");
        }
        return language;
    }

    protected static String constructLocation(String line, String nuts, List<KnowledgeCity> cities) {
        StringBuilder location = new StringBuilder();
        line = " " + line.toLowerCase() + " ";
        for (KnowledgeCity city : cities) {
            boolean add = false;
            if (KnowledgeBase.indexOf2(line, city.getZipCode()) != -1 && location.indexOf(city.getZipCode()) == -1)
                add = true;
            if (add) {
                location.append(":l" + city.getZipCode() + ":l" + city.getNuts() + ":l" + city.getProvince());
            }
        }
        if (location.length() == 0)
            for (KnowledgeCity city : cities) {
                boolean add = false;
                for (String name : city.getNames())
                    if (KnowledgeBase.indexOf2(line, name.toLowerCase()) != -1 && city.isCompatibleWithNuts(nuts)) {
                        line = line.replaceAll(name.toLowerCase(), " ");
                        add = true;
                    }
                if (add && location.indexOf(city.getZipCode()) == -1) {
                    location.append(":l" + city.getZipCode() + ":l" + city.getNuts() + ":l" + city.getProvince());
                }
            }
        if (location.length() == 0 && nuts != null && nuts.length() > 0) {
            // use nuts
            String[] ns = nuts.split(" ");
            for (String n : ns) {
                if (n.length() == 5) { // add arr and province
                    location.append(":l" + n + ":l" + n.substring(0, 4));
                } else if (n.length() == 4) { // add province
                    location.append(":l" + n);
                } else if ("BE1".equals(n)) { // Brussel
                    location.append(":lBE100:lBE10");
                } else if ("BE2".equals(n)) { // Vlaanderen
                    location.append(":lBE21:lBE22:lBE23:lBE24:lBE25");
                } else if ("BE3".equals(n)) { // Wallonië
                    location.append(":lBE31:lBE32:lBE33:lBE34:lBE35");
                }
            }
        }

        if (location.length() > 0)
            location.append(":");
        return location.toString();
    }

    /**
     *
     * @param cities
     *          (order of the length of the city name is important)
     * @return
     */
    public String constructLocation(String nuts, List<KnowledgeCity> cities) {
        String line = null;
        String form = getForm();

        if (AKTE_CONTRACT_UTILITIES.equals(form))
            line = XmlUtilities.getNodePlainText("//CONTRACT_OBJECT_DESCRIPTION/LOCATION_NUTS/LOCATION", document);
        else if (AKTE_CONTRACT_AWARD.equals(form))
            line = XmlUtilities.getNodePlainText("//DESCRIPTION_AWARD_NOTICE_INFORMATION/LOCATION_NUTS/LOCATION", document);
        else if (AKTE_PRIOR_INFORMATION.equals(form))
            line = XmlUtilities.getNodePlainText("//TYPE_CONTRACT_PLACE_DELIVERY/SITE_OR_LOCATION/LABEL", document);
        else
            line = XmlUtilities.getNodePlainText("//DESCRIPTION_CONTRACT_INFORMATION/LOCATION_NUTS/LOCATION", document);

        line = " " + line + " ";

        String l = constructLocation(line, nuts, cities) + " (" + line + ")";
        // if (l.length() == 0) l = line;
        int ll = l.length();
        if (ll > 253)
            ll = 253;
        l = l.substring(0, ll);
        return l;
    }

    public Date getPublicationDate() {
        return XmlUtilities.getDate("//NOTICE_DISPATCH_DATE", document);
    }

    public Date getOpeningDate() {
        Date d = null;
        if (AKTE_CONTRACT.equals(getForm()))
            d = XmlUtilities.getDate("//CONTRACT/FD_CONTRACT/PROCEDURE_DEFINITION_CONTRACT_NOTICE/ADMINISTRATIVE_INFORMATION_CONTRACT_NOTICE/CONDITIONS_FOR_OPENING_TENDERS/DATE_TIME", document);
        else if (AKTE_CONTRACT_UTILITIES.equals(getForm()))
            d = XmlUtilities.getDate(
                    "//CONTRACT_UTILITIES/FD_CONTRACT_UTILITIES/PROCEDURE_DEFINITION_CONTRACT_NOTICE_UTILITIES/ADMINISTRATIVE_INFORMATION_CONTRACT_UTILITIES/CONDITIONS_FOR_OPENING_TENDERS/DATE_TIME", document);
        else if (AKTE_CONTRACT_DEFENCE.equals(getForm()))
            d = XmlUtilities.getDate(
                    "//CONTRACT_DEFENCE/FD_CONTRACT_DEFENCE/PROCEDURE_DEFINITION_CONTRACT_NOTICE_DEFENCE/ADMINISTRATIVE_INFORMATION_CONTRACT_NOTICE_DEFENCE/CONDITIONS_FOR_OPENING_TENDERS/DATE_TIME", document);
        else
            d = XmlUtilities.getDate("//CONDITIONS_FOR_OPENING_TENDERS/DATE_TIME", document);

        if (d == null) {
            if (AKTE_CONTRACT.equals(getForm()))
                d = XmlUtilities.getDate("//CONTRACT/FD_CONTRACT/PROCEDURE_DEFINITION_CONTRACT_NOTICE/ADMINISTRATIVE_INFORMATION_CONTRACT_NOTICE/RECEIPT_LIMIT_DATE", document);
            else if (AKTE_CONTRACT_UTILITIES.equals(getForm()))
                d = XmlUtilities
                        .getDate("//CONTRACT_UTILITIES/FD_CONTRACT_UTILITIES/PROCEDURE_DEFINITION_CONTRACT_NOTICE_UTILITIES/ADMINISTRATIVE_INFORMATION_CONTRACT_UTILITIES/RECEIPT_LIMIT_DATE", document);
            else if (AKTE_CONTRACT_DEFENCE.equals(getForm()))
                d = XmlUtilities.getDate("//CONTRACT_DEFENCE/FD_CONTRACT_DEFENCE/PROCEDURE_DEFINITION_CONTRACT_NOTICE_DEFENCE/ADMINISTRATIVE_INFORMATION_CONTRACT_NOTICE_DEFENCE/RECEIPT_LIMIT_DATE", document);
            else
                d = XmlUtilities.getDate("//RECEIPT_LIMIT_DATE", document);
        }
        return d;
    }

    public String[] getLoten(String taal) {
        String[] loten = null;
        Document doc = getDocument(taal);
        if (doc != null) {
            String[] lotenTitels = null;
            lotenTitels = XmlUtilities.getNodePlainTextArray("//LOT_TITLE", doc);
            String[] lotenEssentie = null;
            lotenEssentie = XmlUtilities.getNodePlainTextArray("//LOT_DESCRIPTION", doc);
            if (lotenTitels != null && lotenTitels.length > 0) {
                loten = new String[lotenTitels.length];
                int i = 0;
                for (String lotTitle : lotenTitels) {
                    // TODO ADD essentie to correct title
                    if (lotenEssentie != null && i < lotenEssentie.length)
                        loten[i] = lotTitle + ". @@@" + lotenEssentie[i];
                    else
                        loten[i] = lotTitle + ". @@@ ";
                    i++;
                }
            }
        }

        return loten;
    }

    public String getLot(String taal) {
        String lotXmlString = null;
        Document doc = getDocument(taal);
        Node firstNode = XmlUtilities.getFirstNode("//LOT_TITLE", doc);
        if (firstNode != null) {
            String parentName = firstNode.getParentNode().getNodeName();
            NodeList nodeList = XmlUtilities.getNodes("//" + parentName, doc);
            if (nodeList != null && nodeList.getLength() > 0) {
                Loten loten = new Loten();
                List<Lot> lotList = new ArrayList<>();
                for (int i = 0; i < nodeList.getLength(); i++) {
                    Node lotNode = nodeList.item(i);
                    NodeList children = lotNode.getChildNodes();
                    Lot lot = new Lot();
                    for (int j = 0; j < children.getLength(); j++) {
                        Node child = children.item(j);
                        if ("LOT_NUMBER".equals(child.getNodeName()))
                            lot.setNumber(XmlUtilities.getPlainText(child).trim());
                        else if ("LOT_TITLE".equals(child.getNodeName()))
                            lot.setTitle(XmlUtilities.getPlainText(child).trim());
                        else if ("LOT_DESCRIPTION".equals(child.getNodeName()))
                            lot.setEssence(XmlUtilities.getPlainText(child).trim());
                    }
                    lotList.add(lot);
                }
                loten.setLotList(lotList);
                try {
                    java.io.StringWriter sw = new StringWriter();
                    JAXBContext jaxbContext = JAXBContext.newInstance(Loten.class);
                    Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
                    jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, false);
                    jaxbMarshaller.setProperty(Marshaller.JAXB_FRAGMENT, true);
                    jaxbMarshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
                    jaxbMarshaller.marshal(loten, sw);
                    lotXmlString = sw.toString();
                } catch (JAXBException e) {
                    e.printStackTrace();
                }
            }
        }
        return lotXmlString;
    }

    public String[] getLotenTitels(String taal) {
        String[] loten = null;
        Document doc = getDocument(taal);
        if (doc != null) {
            loten = XmlUtilities.getNodePlainTextArray("//LOT_TITLE", doc);
        }
        return loten;
    }

    public String[] getLotenOmschrijving(String taal) {
        String[] loten = null;
        Document doc = getDocument(taal);
        if (doc != null) {
            loten = XmlUtilities.getNodePlainTextArray("//LOT_DESCRIPTION", doc);

        }
        return loten;
    }

    public String getSoortNormalized(String taal) {
        String soort = getSoort(taal);
        if (soort.startsWith("TYPE_SERVICES"))
            soort = "TYPE_SERVICES:";
        if (soort.endsWith("SERVICES:"))
            soort = "TYPE_SERVICES:";
        if (soort.equals("TYPE_OBJECT_WORKS_PRIOR_INFORMATION:"))
            soort = "TYPE_WORKS:";
        if (soort.startsWith("TYPE_SUPPLIES"))
            soort = "TYPE_SUPPLIES:";
        if (soort.startsWith("TYPE_OBJECT_SUPPLIES_SERVICES_PRIOR_INFORMATION"))
            soort = "TYPE_SUPPLIES:";
        if (soort.startsWith("TYPE_NOTICE_OF_PURCHASE"))
            soort = "TYPE_SUPPLIES:";
        if (soort.equals("TYPE_"))
            soort = "TYPE_WORKS:";
        if (soort.equals("TYPE_WORKS:WORKS:"))
            soort = "TYPE_WORKS:";
        if (soort.startsWith("TYPE_WORKS:"))
            soort = "TYPE_WORKS:";
        return soort;
    }

    public String getSoort(String taal) {
        String soort = "";
        Document doc = getDocument(taal);
        if (doc != null) {
            String w = XmlUtilities.getNodePlainTextNoDoubles("//OBJECT_WORKS_PRIOR_INFORMATION", doc);
            if (w != null && w.length() > 0)
                soort += "OBJECT_WORKS_PRIOR_INFORMATION:";

            w = XmlUtilities.getNodePlainTextNoDoubles("//OBJECT_SUPPLIES_SERVICES_PRIOR_INFORMATION", doc);
            if (w != null && w.length() > 0)
                soort += "OBJECT_SUPPLIES_SERVICES_PRIOR_INFORMATION:";

            w = XmlUtilities.getNodePlainTextNoDoubles("//FD_MARKET/@CTYPE", doc);
            if (w != null && w.length() > 0)
                soort += w + ":";

            w = XmlUtilities.getNodePlainTextNoDoubles("//TYPE_CONTRACT_LOCATION_WORKS/@CONTRACT_TYPE", doc);
            if (w != null && w.length() > 0)
                soort += w + ":";

            w = XmlUtilities.getNodePlainTextNoDoubles("//TYPE_CONTRACT_LOCATION_WORKS/@SERVICES_CATEGORY", doc);
            if (w != null && w.length() > 0)
                soort += w + ":";

            w = XmlUtilities.getNodePlainTextNoDoubles("//F07_CONTRACT_TYPE/@VALUE", doc);
            if (w != null && w.length() > 0)
                soort += w + ":";

            w = XmlUtilities.getNodePlainTextNoDoubles("//TYPE_CONTRACT/@VALUE", doc);
            if (w != null && w.length() > 0)
                soort += w + ":";

            w = XmlUtilities.getNodePlainTextNoDoubles("//TYPE_WORK_CONTRACT/@VALUE", doc);
            if (w != null && w.length() > 0)
                soort += w + ":";

            w = XmlUtilities.getNodePlainTextNoDoubles("//SUPPLIES", doc);
            if (w != null && w.length() > 0)
                soort += w + "SUPPLIES:";

            w = XmlUtilities.getNodePlainTextNoDoubles("//SERVICE_CATEGORY", doc);
            if (w != null && w.length() > 0)
                soort += w + "SERVICES:";
        }

        return "TYPE_" + soort;

    }

    public String getBDA(String taal) {
        Document doc = getDocument(taal);
        if (doc != null)
            return XmlUtilities.getNodePlainText("//NO_DOC_EXT", doc);
        return null;
    }

    public String getLotTitels(String taal) {
        Document doc = getDocument(taal);
        if (doc != null)
            return XmlUtilities.getNodePlainText("//LOT_TITLE", doc);
        return "";
    }

    public String getLotEssenties(String taal) {
        Document doc = getDocument(taal);
        if (doc != null)
            return XmlUtilities.getNodePlainText("//LOT_DESCRIPTION", doc);
        return "";
    }

    public static List<String> constructParent(String cpv) {
        List<String> results = new ArrayList<String>();
        String[] post = { "000000", "00000", "0000", "000", "00", "0" };
        for (int i = 7; i > 1; i--) {
            if (cpv.charAt(i) != '0')
                results.add(cpv.substring(0, i) + post[i - 2]);
        }
        return results;
    }

    public static List<String> constructNutsParent(String nuts) {
        List<String> results = new ArrayList<String>();
        if (!"Buitenland".equals(nuts) && !"BEL".equals(nuts))
            if (nuts.length() > 2) {
                for (int i = nuts.length(); i > 1; i--) {
                    String n = nuts.substring(0, i);
                    if ("BE".equals(n))
                        n = "BEL";
                    results.add(n);
                }
            }
        return results;
    }

    public static String constructNutsIndex(String tekst, boolean addParent) {
        StringBuffer index = new StringBuffer("");
        tekst = tekst.replace('\r', ' ').replace('\n', ' ').replaceAll("\\.", "");
        List<String> indexes = null;
        indexes = getFound(tekst, "(?<=NUTS CODE=.).{1,5}(?='|\"/)");
        for (String i : indexes) {
            if (index.indexOf(i) == -1) {
                if ("BE".equals(i))
                    i = "BEL";
                index.append(i + " ");
                if (addParent) {
                    List<String> parents = constructNutsParent(i);
                    for (String p : parents) {
                        if (index.indexOf(p + " ") == -1)
                            index.append(p + " ");
                    }
                }
            }
        }
        return index.toString().trim();
    }

    public static String constructCpvIndex(String tekst, boolean addParent) {
        StringBuffer index = new StringBuffer("");
        tekst = tekst.replace('\r', ' ').replace('\n', ' ').replaceAll("\\.", "");
        List<String> indexes = getFound(tekst, "(?<=CPV_CODE CODE=.)\\d{8}(?!\\d)");
        for (String i : indexes) {
            if (index.indexOf(i) == -1) {
                index.append(i + " ");
                if (addParent) {
                    List<String> parents = constructParent(i);
                    for (String p : parents) {
                        if (index.indexOf(p) == -1)
                            index.append(p + " ");
                    }
                }
            }
        }
        return index.toString().trim();
    }

    public static String addCpvParents(String cpv) {
        if (cpv == null || cpv.length() < 3)
            return cpv;
        cpv = cpv.trim();
        StringBuilder index = new StringBuilder(cpv);
        String[] cpvs = cpv.split(" ");
        for (String i : cpvs) {
            List<String> parents = constructParent(i);
            for (String p : parents) {
                if (index.indexOf(p) == -1)
                    index.append(" " + p);
            }
        }
        return index.toString().trim();
    }

    public String getOmschrijving(String taal) {
        String titel = "";
        Document doc = getDocument(taal);
        if (doc != null) {
            titel = XmlUtilities.getNodePlainText("//TITLE_CONTRACT", doc);
            if (titel.length() == 0)
                titel = XmlUtilities.getNodePlainText("//TITLE_DESIGN_CONTACT_NOTICE", doc); // F12
            if (titel.length() == 0)
                titel = XmlUtilities.getNodePlainText("//TITLE_QUALIFICATION_SYSTEM", doc); // F7

            if (titel != null && titel.length() > 1) {
                titel = titel.trim();
                if (titel.endsWith(".") && titel.length() > 1) {
                    titel = titel.substring(0, titel.length() - 1).trim();
                }
                if (titel.length() > 1) {
                    titel = titel.substring(0, 1).toUpperCase() + titel.substring(1);
                }
            }
        }
        return titel;
    }

    public String getContractor(Node node) {
        String contractor_name = XmlUtilities.getNodePlainText("ECONOMIC_OPERATOR_NAME_ADDRESS/CONTACT_DATA_WITHOUT_RESPONSIBLE_NAME/ORGANISATION/OFFICIALNAME", node);
        return contractor_name;
    }

    public String getGovernmentName(Node node) {
        String name = XmlUtilities.getNodePlainText("ORGANISATION/OFFICIALNAME", node);
        return name;
    }

    public String getGovernmentLocation(Node node, KnowledgeBase knowledgeBase) {
        String city = XmlUtilities.getNodePlainText("TOWN", node);
        String zip = XmlUtilities.getNodePlainText("POSTAL_CODE", node);
        String country = XmlUtilities.getNodePlainTextNoDoubles("COUNTRY/@VALUE", node);

        KnowledgeCity kc = knowledgeBase.getCity(zip, country);
        if (kc != null)
            return ":l" + kc.getZipCode() + ":l" + kc.getNuts() + ":l" + kc.getProvince() + ":";
        else
            return ":l" + zip + ":l" + country + ":";
    }

    public String getGovernmentStreet(Node node) {
        String contractor_name = XmlUtilities.getNodePlainText("ADDRESS", node);
        return contractor_name;
    }

    public String getGovernmentEmails(Node node) {
        String emails = XmlUtilities.getNodePlainText("E_MAILS", node);
        emails = emails.replaceAll("    ", " ");
        emails = emails.replaceAll("   ", " ");
        emails = emails.replaceAll("  ", " ");
        return emails.replaceAll(" ", ";");
    }

    public String getGovernmentContact(Node node) {
        String emails = XmlUtilities.getNodePlainText("CONTACT_POINT", node);
        return emails;
    }

    public String getGovernmentAttention(Node node) {
        String emails = XmlUtilities.getNodePlainText("ATTENTION", node);
        return emails;
    }

    public String getGovernmentPhone(Node node) {
        String emails = XmlUtilities.getNodePlainText("PHONE", node);
        return emails;
    }

    public String getGovernmentFax(Node node) {
        String emails = XmlUtilities.getNodePlainText("FAX", node);
        return emails;
    }

    public String getContractorStreet(Node node) {
        String contractor_name = XmlUtilities.getNodePlainText("ECONOMIC_OPERATOR_NAME_ADDRESS/CONTACT_DATA_WITHOUT_RESPONSIBLE_NAME/ADDRESS", node);
        return contractor_name;
    }

    public String getContractorLocation(Node node, KnowledgeBase knowledgeBase) {
        String city = XmlUtilities.getNodePlainText("ECONOMIC_OPERATOR_NAME_ADDRESS/CONTACT_DATA_WITHOUT_RESPONSIBLE_NAME/TOWN", node);
        String zip = XmlUtilities.getNodePlainText("ECONOMIC_OPERATOR_NAME_ADDRESS/CONTACT_DATA_WITHOUT_RESPONSIBLE_NAME/POSTAL_CODE", node);
        String country = XmlUtilities.getNodePlainTextNoDoubles("ECONOMIC_OPERATOR_NAME_ADDRESS/CONTACT_DATA_WITHOUT_RESPONSIBLE_NAME/COUNTRY/@VALUE", node);

        KnowledgeCity kc = knowledgeBase.getCity(zip, country);
        if (kc != null)
            return ":l" + kc.getZipCode() + ":l" + kc.getNuts() + ":l" + kc.getProvince() + ":";
        else
            return ":l" + zip + ":l" + country + ":";
    }

    public String getAwardNumber(Node node) {
        String contractor_name = XmlUtilities.getNodePlainText("CONTRACT_NUMBER", node);
        return contractor_name;
    }

    public String getAwardTitle(Node node) {
        String contractor_name = XmlUtilities.getNodePlainText("CONTRACT_TITLE", node);
        return contractor_name;
    }

    public String getAwardValue(Node node) {
        String value = XmlUtilities.getNodePlainText("CONTRACT_VALUE_INFORMATION/COSTS_RANGE_AND_CURRENCY_WITH_VAT_RATE/VALUE_COST", node);
        return value;
    }

    public String getContractorEmails(Node node) {
        String emails = XmlUtilities.getNodePlainText("ECONOMIC_OPERATOR_NAME_ADDRESS/CONTACT_DATA_WITHOUT_RESPONSIBLE_NAME/E_MAILS", node);

        return emails.replaceAll(" ", ";");
    }

    public String getContractorPhone(Node node) {
        String phone = XmlUtilities.getNodePlainText("ECONOMIC_OPERATOR_NAME_ADDRESS/CONTACT_DATA_WITHOUT_RESPONSIBLE_NAME/PHONE", node);
        return phone;
    }

    public String getContractorFax(Node node) {
        String fax = XmlUtilities.getNodePlainText("ECONOMIC_OPERATOR_NAME_ADDRESS/CONTACT_DATA_WITHOUT_RESPONSIBLE_NAME/FAX", node);
        return fax;
    }

    public String getAwardCurrency(Node node) {
        String value = XmlUtilities.getNodePlainText("CONTRACT_VALUE_INFORMATION/COSTS_RANGE_AND_CURRENCY_WITH_VAT_RATE/@CURRENCY", node);
        return value;
    }

    public boolean getAwardExclVat(Node node) {
        return XmlUtilities.getNodeExists("CONTRACT_VALUE_INFORMATION/COSTS_RANGE_AND_CURRENCY_WITH_VAT_RATE/EXCLUDING_VAT", node);
    }

    public String getGovernmentActivity(Node node) {
        String value = XmlUtilities.getNodePlainTextNoDoubles("//TYPE_OF_ACTIVITY_OTHER/@OTHER", node);
        if (value == null || value.length() == 0) {
            value = XmlUtilities.getNodePlainTextNoDoubles("//TYPE_OF_ACTIVITY/@VALUE", node);
        }
        return value;
    }

    protected void updateGovernmentNameType(Government government, String name) {
        government.setName(name);
        String type = null;

        String[] words = name.split(" ");
        for (int t = 0; t < Government.TYPES.length && type == null; t++) {
            for (int k = 0; k < Government.TYPES_KEYS[t].length && type == null; k++) {
                String key = Government.TYPES_KEYS[t][k];
                if (key.indexOf(" ") == -1) {
                    for (String w : words) {
                        if (w.equalsIgnoreCase(key)) {
                            type = Government.TYPES[t];
                            break;
                        }
                    }
                } else {
                    if (name.toLowerCase().indexOf(key) != -1)
                        type = Government.TYPES[t];
                }
            }
        }
        government.setType(Util.TYPE_PREFIX + type);
    }

    protected void updateContractorNameType(Contractor contractor, String name) {
        contractor.setName(name);
        String type = null;
        String[] nvs = { "nv ", "n.v. ", "NV ", "N.V. ", "sa ", "s.a. ", "SA ", "S.A. " };
        String[] nve = { " nv", " n.v.", " NV", " N.V.", " sa", " s.a.", " SA", " S.A.", " sa/nv", " SA/NV" };
        String[] bvbas = { "bvba ", "b.v.b.a. ", "BVBA ", "B.V.B.A ", "sprl ", "s.p.r.l. ", "SPRL ", "S.P.R.L. " };
        String[] bvbae = { " bvba", " b.v.b.a.", " BVBA", " B.V.B.A", " sprl", " s.p.r.l.", " SPRL", " S.P.R.L." };

        for (String t : nvs) {
            if (name.startsWith(t)) {
                name = name.substring(t.length());
                type = "NV";
            }
        }
        for (String t : bvbas) {
            if (name.startsWith(t)) {
                name = name.substring(t.length());
                type = "BVBA";
            }
        }
        for (String t : nve) {
            if (name.endsWith(t)) {
                name = name.substring(0, name.length() - t.length());
                type = "NV";
            }
        }
        for (String t : bvbae) {
            if (name.endsWith(t)) {
                name = name.substring(0, name.length() - t.length());
                type = "BVBA";
            }
        }
        contractor.setName(name);
        contractor.setType(type);
    }

    public Government getGovernment(KnowledgeBase knowledgeBase) {
        Document doc = getDocument("");
        Government government = null;
        NodeList nodes = XmlUtilities.getNodes("//CA_CE_CONCESSIONAIRE_PROFILE", doc);
        if (nodes != null && nodes.getLength() > 0) {
            Node node = nodes.item(0);
            government = new Government();
            government.setActivity(Util.ACTIVITY_PREFIX + getGovernmentActivity(doc));
            updateGovernmentNameType(government, getGovernmentName(node));
            government.setStreet(getGovernmentStreet(node));
            government.setLocation(getGovernmentLocation(node, knowledgeBase));
            Contact contact = new Contact();
            government.getContacts().add(contact);
            contact.setEmails(getGovernmentEmails(node));
            contact.setName(getGovernmentContact(node));
            contact.setAttention(getGovernmentAttention(node));
            contact.setPhones(getGovernmentPhone(node));
            contact.setFaxes(getGovernmentFax(node));
            contact.setGovernment(government);
        }
        return government;
    }

    public Type getF14Type() {
        Type type = null;
        if (XmlUtilities.containsNode("//CORRECTION", document))
            type = Type.CORRECTION;
        else if (XmlUtilities.containsNode("//ADDITIONAL_INFO", document))
            type = Type.ADDITIONAL_INFO;
        else if (XmlUtilities.containsNode("//INCOMPLETE_PROCEDURE", document))
            type = Type.INCOMPLETE_PROCEDURE;
        return type;
    }

    public String getF14Info() {
        Document document = getDocument("");
        String info = XmlUtilities.getNodePlainText("//OTHER_ADDITIONAL_INFO", document);
        return info;
    }

    public List<AwardItem> getAwardItems(KnowledgeBase knowledgeBase) {
        Document doc = getDocument("");
        List<AwardItem> awardItems = null;
        NodeList nodes = XmlUtilities.getNodes("//AWARD_OF_CONTRACT", doc);
        if (nodes != null && nodes.getLength() > 0) {
            awardItems = new ArrayList<>();
            for (int i = 0; i < nodes.getLength(); i++) {
                AwardItem item = new AwardItem();
                Contractor contractor = new Contractor();
                Node node = nodes.item(i);
                item.setTitle(getAwardTitle(node));
                item.setNumber(getAwardNumber(node));
                item.setContractorName(getContractor(node));
                item.setValue(getAwardValue(node));
                item.setCurrency(getAwardCurrency(node));
                item.setExclusiveVat(getAwardExclVat(node));

                updateContractorNameType(contractor, item.getContractorName());

                contractor.setStreet(getContractorStreet(node));
                contractor.setLocation(getContractorLocation(node, knowledgeBase));
                contractor.setEmails(getContractorEmails(node));
                contractor.setPhones(getContractorPhone(node));
                contractor.setFaxes(getContractorFax(node));
                item.setContractor(contractor);

                awardItems.add(item);
            }
        }
        return awardItems;
    }

    private Document getDocument(String taal) {
        return getDocument();
    }

    public Document getDocument() {
        return document;
    }

    public void setDocument(Document document) {
        this.document = document;
    }

    private boolean noErk(String erk) {
        boolean result = true;
        if (erk != null && erk.length() > 0) {
            if (erk.length() < 100) { // gevaarlijk, dus zeker niet toelaten in lange teksten.
                for (int i = 0; i < mapCategoryCodes.length && result; i++) {
                    if (erk.indexOf(" " + mapCategoryCodes[i] + " ") != -1)
                        result = false;
                }
            }
            for (int i = 0; i < mapCategory.length && result; i++) {
                if (erk.indexOf(mapCategory[i]) != -1)
                    result = false;
            }
            for (int i = 0; i < mapClass.length && result; i++) {
                if (erk.indexOf(mapClass[i]) != -1)
                    result = false;
            }
        }
        return result;
    }


    public static Hashtable<String, String> getDisciplineTable() {
        if (erkDisciplineTable == null) {
            erkDisciplineTable = new Hashtable<>();
            erkDisciplineTable.put("A", "WA");
            erkDisciplineTable.put("A1", "WA");
            erkDisciplineTable.put("B", "WA");
            erkDisciplineTable.put("B1", "WA");
            erkDisciplineTable.put("C", "WR");
            erkDisciplineTable.put("C1", "WR");
            erkDisciplineTable.put("C2", "GW");
            erkDisciplineTable.put("C3", "WR");
            erkDisciplineTable.put("C5", "WR");
            erkDisciplineTable.put("C6", "GR");
            erkDisciplineTable.put("C7", "GR");
            erkDisciplineTable.put("D", "AB");
            erkDisciplineTable.put("D1", "AB");
            erkDisciplineTable.put("D4", "AB");
            erkDisciplineTable.put("D5", "SM");
            erkDisciplineTable.put("D6", "AB");
            erkDisciplineTable.put("D7", "IJM");
            erkDisciplineTable.put("D8", "DT");
            erkDisciplineTable.put("D10", "VM");
            erkDisciplineTable.put("D11", "AB");
            erkDisciplineTable.put("D12", "DT");
            erkDisciplineTable.put("D13", "SG");
            erkDisciplineTable.put("D14", "SG");
            erkDisciplineTable.put("D15", "VM");
            erkDisciplineTable.put("D16", "SA");
            erkDisciplineTable.put("D17", "CV");
            erkDisciplineTable.put("D18", "CV");
            erkDisciplineTable.put("D20", "IJM");
            erkDisciplineTable.put("D21", "AB");
            erkDisciplineTable.put("D22", "DT");
            erkDisciplineTable.put("D23", "AB");
            erkDisciplineTable.put("D24", "AB");
            erkDisciplineTable.put("D25", "VM");
            erkDisciplineTable.put("D29", "VM");
            erkDisciplineTable.put("E", "AB");
            erkDisciplineTable.put("E1", "WR");
            erkDisciplineTable.put("E2", "GR");
            erkDisciplineTable.put("E4", "GR");
            erkDisciplineTable.put("F", "IJM");
            erkDisciplineTable.put("F1", "IJM");
            erkDisciplineTable.put("F2", "IJM");
            erkDisciplineTable.put("F3", "IJM");
            erkDisciplineTable.put("G", "GR");
            erkDisciplineTable.put("G1", "GR");
            erkDisciplineTable.put("G2", "GR");
            erkDisciplineTable.put("G3", "BEP");
            erkDisciplineTable.put("G4", "GR");
            erkDisciplineTable.put("G5", "AF");
            erkDisciplineTable.put("H", "SP");
            erkDisciplineTable.put("H1", "SP");
            erkDisciplineTable.put("H2", "SP");
            erkDisciplineTable.put("K", "MHE");
            erkDisciplineTable.put("K1", "MHE");
            erkDisciplineTable.put("K2", "MHE");
            erkDisciplineTable.put("K3", "MHE");
            erkDisciplineTable.put("L", "MHE");
            erkDisciplineTable.put("L1", "MHE");
            erkDisciplineTable.put("L2", "MHE");
            erkDisciplineTable.put("M", "MHE");
            erkDisciplineTable.put("M1", "MHE");
            erkDisciplineTable.put("N", "MHE");
            erkDisciplineTable.put("N1", "MHE");
            erkDisciplineTable.put("N2", "MHE");
            erkDisciplineTable.put("P", "EL");
            erkDisciplineTable.put("P1", "EL");
            erkDisciplineTable.put("P2", "EL");
            erkDisciplineTable.put("P3", "EL");
            erkDisciplineTable.put("P4", "EL");
            erkDisciplineTable.put("S", "EL");
            erkDisciplineTable.put("S1", "EL");
            erkDisciplineTable.put("S2", "EL");
            erkDisciplineTable.put("S3", "EL");
            erkDisciplineTable.put("S4", "EL");
            erkDisciplineTable.put("T", "SPI");
            erkDisciplineTable.put("T2", "SPI");
            erkDisciplineTable.put("T3", "SPI");
            erkDisciplineTable.put("T4", "SPI");
            erkDisciplineTable.put("T6", "SPI");
            erkDisciplineTable.put("U", "SPI");
            erkDisciplineTable.put("V", "SPI");
        }
        return erkDisciplineTable;
    }

    private static List<String> getFound(String contents, String regex) {
        List<String> results = new ArrayList<String>();
        Pattern pattern = Pattern.compile(regex, Pattern.UNICODE_CASE);
        Matcher matcher = pattern.matcher(contents);
        while (matcher.find()) {
            if (matcher.groupCount() > 0) {
                results.add(matcher.group(1));
            } else {
                results.add(matcher.group());
            }
        }
        return results;
    }

    public static boolean containsErkenning(String message, String erkenning) {
        boolean contains = false;
        List<String> indexes = getFound(message, erkenning + "(?!\\d)");
        if (indexes.size() > 0)
            contains = true;
        return contains;
    }

    private String constructCategorySnippet() {
        Document doc = getDocument();
        String erk = "";
        String e = "";
        erk = XmlUtilities.getNodePlainText("//ECONOMIC_OPERATORS_PERSONAL_SITUATION", doc) + " ";
        if (!noErk(erk) && e.indexOf(erk) == -1)
            e += " " + erk;
        erk = XmlUtilities.getNodePlainText("//F05_CONDITIONS_FOR_PARTICIPATION/TECHNICAL_CAPACITY", doc) + " ";
        if (!noErk(erk) && e.indexOf(erk) == -1)
            e += " " + erk;
        erk = XmlUtilities.getNodePlainText("//TECHNICAL_CAPACITY_LEFTI/T_CAPACITY_MIN_LEVEL", doc) + " ";
        if (!noErk(erk) && e.indexOf(erk) == -1)
            e += " " + erk;
        erk = XmlUtilities.getNodePlainText("//TECHNICAL_CAPACITY_LEFTI/T_CAPACITY_INFORMATION", doc) + " ";
        if (!noErk(erk) && e.indexOf(erk) == -1)
            e += " " + erk;
        erk = XmlUtilities.getNodePlainText("//EAF_CAPACITY_INFORMATION", doc) + " ";
        if (!noErk(erk) && e.indexOf(erk) == -1)
            e += " " + erk;
        erk = XmlUtilities.getNodePlainText("//EAF_CAPACITY_MIN_LEVEL", doc) + " ";
        if (!noErk(erk) && e.indexOf(erk) == -1)
            e += " " + erk;

        erk = XmlUtilities.getNodePlainText("//QUALIFICATION_FOR_SYSTEM/CONDITIONS_ECONOMIC_OPERATORS", doc) + " ";
        if (!noErk(erk) && e.indexOf(erk) == -1)
            e += " " + erk;

        return e;
    }

    public String getNormalizedCategoryAndClass() {
        return getErkCategorieAkteNoWarning();
    }

    private static boolean containsSureCategory(String text) {

        for (String mapCat : mapSureCategory) {
            if (text.indexOf(mapCat) != -1)
                return true;
        }
        return false;
    }

    /**
     *
     * @return the category of the tender, eg C1
     */
    private String getErkCategorieAkteNoWarning() {
        String e = "";
        String eCategory = "";
        Document doc = getDocument();
        if (doc != null) {
            e = constructCategorySnippet();
            eCategory = getNormalizedCategoryAndClass(e);
        }
        return eCategory;
    }

    public static String getNormalizedCategoryAndClass(String text) {
        text = eliminateSpaceBetweenCategoryNumber(text);
        text = identifyCategoryAndClassTokens(text);
        if (containsSureCategory(text))
            text = "categorie " + text;
        text = contructErkenningNew(text);
        text = normalizeCategory(text);
        return text;
    }

    static public String[] mapCategoryHeadCodes = { "A", "B", "C", "D", "E", "F", "G", "H", "K", "L", "M", "N", "P", "S", "T", "U", "V", "X" };

    public static String eliminateSpaceBetweenCategoryNumber(String text) {
        text = " " + text.replaceAll("\\([^0-9]*\\)", " ") + " ";
        return text.replaceAll("([^A-Za-z])([A-X]) ([0-9])", "$1$2$3");
    }

    public static String identifyCategoryAndClassTokens(String text) {
        text = " " + text + " ";
        text = text.replaceAll("ë", "e");
        text = text
                .replaceAll(
                        "([^A-Za-z])(homologation|Homologation|Cat\\.|cat\\.|Erkenning|Erkenningen|erkenning|erkenningen|ondercategorie|ondercategorieen|Ondercategorie|Ondercategorieen|CATEGORIE|categorie|categorieen|Categorie|Categorieen|catégories|Catégories|Catégorie|Souscatégorie|souscatégorie|catégorie|Agréation|agréation)([^A-Za-z])",
                        "$1categorie$3");
        text = text.replaceAll("([^A-Za-z])(KLASSE|klasse|Klasse|classe|CLASSE|Classe|erkenningsklasse|Erkenningsklasse)([^A-Za-z])", "$1klasse$3");

        return text;
    }

    private static String contructErkenningNew(String e) {
        StringBuilder result = new StringBuilder();
        // remove certain tokens we do not need and cause confusion.
        String text = e.replace(" en ", " ");
        text = text.replace(" EN ", " ");
        text = text.replace(" et ", " ");
        text = text.replace(" ET ", " ");
        text = text.replace(" A cette ", " ");
        text = text.replace(" A titre ", " ");
        text = text.replace("requise", " ");
        text = text.replace("lot", " ");
        text = text.replace("Lot", " ");
        text = text.replace(" is ", " ");
        text = text.replace(" L'", " ");
        text = text.replace(" ou ", " ");
        text = text.replace(" OU ", " ");
        text = text.replace(" of ", " ");
        text = text.replace(" OF ", " ");
        text = text.replace("N.A.", " ");
        text = text.replace("K.B.", " ");
        text = text.replace("k.b.", " ");
        text = text.replace("N/A", " ");
        text = text.replace("n/a", " ");
        text = text.replace("n.a.", " ");
        text = text.replace(" en/of ", " ");
        text = text.replace(" et/ou ", " ");
        text = text.replaceAll("[^a-zA-Z0-9éèçàäëöüôâêïÉÈÇÀÄËÖÜÔÂÊÏ]", " ");
        text = text.replaceAll("    ", " ");
        text = text.replaceAll("   ", " ");
        text = text.replaceAll("  ", " ");
        String[] words = text.split(" ");
        int i = 0;
        boolean isClass = false;
        while (i < words.length) {
            if (isMapCategorySmall(words[i])) {
                i++;
                while (i < words.length && (isWordCategory(words[i]) || (isClass = isWordClass(words[i])))) {
                    result.append((isClass ? "kl. " : "") + words[i] + ", ");
                    i++;
                }
                i--;
            }
            i++;
        }
        return result.toString();
    }

    public static boolean isMapClass(String word) {
        for (String d : mapClass) {
            if (d.equalsIgnoreCase(word))
                return true;
        }
        return false;
    }

    public static boolean isMapCategorySmall(String word) {
        for (String d : mapCategorySmall) {
            if (d.equalsIgnoreCase(word))
                return true;
        }
        return false;
    }

    public static boolean isWordDiscipline(String word) {
        for (String d : MLearningModels.DISCIPLINES) {
            if (d.equalsIgnoreCase(word))
                return true;
        }
        return false;
    }

    public static boolean isWordNuts(String word) {
        return word.matches("BE[0-9]{1,3}");
    }

    public static boolean isWordCategory(String word) {
        for (String d : mapCategoryCodes) {
            if (d.equalsIgnoreCase(word))
                return true;
        }
        return false;
    }

    public static boolean isWordType(String word) {
        for (String d : Government.TYPES) {
            if (d.equalsIgnoreCase(word))
                return true;
        }
        return false;
    }

    public static boolean isWordClass(String word) {
        for (String d : map5) {
            if (d.equalsIgnoreCase(word))
                return true;
        }
        return false;
    }

    public static Pattern PATTERN_BDA = Pattern.compile("[0-9]{4}"+Util.HYPHEN_ESCAPE+"[0-9]{6}");

    public static boolean isWordBDA(String word) {
        if (word != null && word.length() == (10 + Util.HYPHEN_ESCAPE.length()) ) {
            return PATTERN_BDA.matcher(word).find();
        }
        return false;
    }

    static public String normalizeCategory(String category) {
        if (category == null)
            return null;
        StringBuilder normalized = new StringBuilder();
        category = category.replaceAll(";", ",");
        category = category.replaceAll("o/cat. ", "");
        category = " " + category + ",";

        for (int i = 0; i < mapCategoryCodes.length; i++) {
            if (category.indexOf(" " + mapCategoryCodes[i] + ",") != -1)
                normalized.append("yyy" + mapCategoryCodes[i] + ":");
        }
        for (int i = 0; i < map5.length; i++) {
            if (category.indexOf("kl. " + map5[i] + ",") != -1)
                normalized.append("zzz" + map5[i] + ":");
        }
        if (normalized.length() > 0)
            normalized.insert(0, ":");
        return normalized.toString();
    }

    public static Document getDocument(InputStream inputStream) throws ParserConfigurationException, SAXException, IOException {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbf.newDocumentBuilder();
        Document document = db.parse(inputStream);
        inputStream.close();
        return document;
    }

}
