package com.cipalschaubroeck.oldcstender.machinelearning.classify;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;

/**
 * List of Instances with a feature structure.
 */
@Getter
@Setter
public class InstanceList extends ArrayList<Instance> {
    public FeatureList structure = null;
}
