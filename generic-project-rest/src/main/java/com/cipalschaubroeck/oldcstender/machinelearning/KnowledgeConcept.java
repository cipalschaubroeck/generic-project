package com.cipalschaubroeck.oldcstender.machinelearning;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Pattern;

/**
 * A meaningful concept.
 * There are 2 types: plain text concepts and regular expression concepts.
 */
@Getter
@Setter
public class KnowledgeConcept {
    private List<String> synonymList;
    private List<String> categoryList;
    private List<String> excludeList;
    private HashMap<String, Pattern> patternMap = null;

    public KnowledgeConcept(String info) {
        String[] infoList = info.split(";");
        try {
            String[] array = infoList[0].split("(?<!\\\\),");
            for (int i = 0; i < array.length; i++) {
                array[i] = array[i].replaceAll("\\\\,", ",");
            }
            synonymList = Arrays.asList(array);
            categoryList = Arrays.asList(infoList[1].split(","));
            for (String category : categoryList) {
                if (category.startsWith("!")) {
                    if (excludeList == null) excludeList = new ArrayList<>();
                    excludeList.add(category.substring(1));
                }
            }
        } catch (Exception e) {
            System.out.println("error in db "+info);
            e.printStackTrace();
            System.exit(0);
        }
    }

    public String getKey() {
        return synonymList.get(0);
    }

    public boolean hasConcept(String text) {
        for (String c : synonymList) {
            if (c.startsWith("!") && text.indexOf(c.substring(1)) != -1) return false;
            if (c.endsWith("&") && c.startsWith("&")) {
                c= c.substring(1,c.length()-1);
                if (text.indexOf(" "+c+" ") != -1 || text.indexOf(","+c+" ") != -1 || text.indexOf("."+c+" ") != -1 ||
                        text.indexOf(" "+c+",") != -1 || text.indexOf(","+c+",") != -1 || text.indexOf("."+c+",") != -1 ||
                        text.indexOf(" "+c+".") != -1 || text.indexOf(","+c+".") != -1 || text.indexOf("."+c+".") != -1) return true;
            } else if (c.endsWith("&")) {
                c= c.substring(0,c.length()-1);
                if (text.indexOf(c+" ") != -1 || text.indexOf(c+",") != -1 || text.indexOf(c+".") != -1) return true;
            } else if (c.startsWith("@")) {
                Pattern pattern = retrievePattern(c.substring(1));
                if (pattern.matcher(text).matches()) return true;
            } else {
                if (text.indexOf(c) != -1) return true;
            }
        }
        return false;
    }

    private Pattern retrievePattern(String c) {
        if (patternMap == null) patternMap = new HashMap<String,Pattern>();
        Pattern pattern = patternMap.get(c);
        if (pattern == null) {
            pattern = Pattern.compile(c);
            patternMap.put(c,pattern);
        }
        return pattern;
    }
}
