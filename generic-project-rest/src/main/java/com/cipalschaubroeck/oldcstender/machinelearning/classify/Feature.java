package com.cipalschaubroeck.oldcstender.machinelearning.classify;

import libsvm.svm_node;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class Feature {
    private int index;
    private double value;
    private String name;

    public String toFeatureIndexString() {
        return index + " " + name;
    }

    public svm_node getLibSvmFeature() {
        svm_node node = new svm_node();
        node.index = index;
        node.value = value;
        return node;
    }
    public Feature deepCopy() {
        Feature copy = new Feature();
        copy.setName(name);
        copy.setValue(value);
        copy.setIndex(index);
        return copy;
    }
}
