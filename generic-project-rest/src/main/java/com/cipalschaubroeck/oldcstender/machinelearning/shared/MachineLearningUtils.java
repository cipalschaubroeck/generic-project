package com.cipalschaubroeck.oldcstender.machinelearning.shared;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class MachineLearningUtils {
    /**
     * Split a list in sublists
     *
     * @param list input list
     * @param size
     * @return List of sublists
     */
    public static List<List<String>> splitList(List list, int size) {
        List<List<String>> partitions = new LinkedList<List<String>>();
        int partitionSize = (int) Math.floor(list.size()/size);
        for (int i = 0; i < size; i++) {
            partitions.add(list.subList((partitionSize*i), i == size -1 ? list.size() : partitionSize*(i+1)));
        }
        return partitions;
    }

    /**
     * Sort an unsorted map
     *
     * @param unsortMap
     * @param order
     * @return sorted map
     */
    public static Map<String, Integer> sortByComparator(Map<String, Integer> unsortMap, final boolean order) {

        List<Map.Entry<String, Integer>> list = new LinkedList<Map.Entry<String, Integer>>(unsortMap.entrySet());

        // Sorting the list based on values
        Collections.sort(list, new Comparator<Map.Entry<String, Integer>>() {
            public int compare(Map.Entry<String, Integer> o1, Map.Entry<String, Integer> o2) {
                if (order) {
                    return o1.getValue().compareTo(o2.getValue());
                } else {
                    return o2.getValue().compareTo(o1.getValue());

                }
            }
        });

        // Maintaining insertion order with the help of LinkedList
        Map<String, Integer> sortedMap = new LinkedHashMap<String, Integer>();
        for (Map.Entry<String, Integer> entry : list) {
            sortedMap.put(entry.getKey(), entry.getValue());
        }

        return sortedMap;
    }

    public static void printMap(Map<String, Integer> map) {
        int count = 0;
        for (Map.Entry<String, Integer> entry : map.entrySet()) {
            System.out.println(entry.getKey() + " " + entry.getValue());
            count++;
        }
    }

    public static void printMap(Map<String, Integer> map, Map<String, Integer> info, Map<String, Integer> info2) {
        int count = 0;
        for (Map.Entry<String, Integer> entry : map.entrySet()) {
            System.out.println(count + " Key : " + entry.getKey() + " Value : " + entry.getValue() + " Info : " + info.get(entry.getKey()) + "/" + info2.get(entry.getKey()));
            count++;
        }
    }

    public static void printMap(Map<String, Integer> map, Map<String, Integer> info, Map<String, Integer> info2, Map<String, Integer> info3, Map<String, Double> info4, Writer writer) throws IOException {
        printMap(map, info, info2, info3, info4, writer, "");
    }

    public static void printMap(Map<String, Integer> map, Map<String, Integer> info, Map<String, Integer> info2, Map<String, Integer> info3, Map<String, Double> info4, Writer writer, String prefix)
            throws IOException {
        int count = 0;
        for (Map.Entry<String, Integer> entry : map.entrySet()) {
            System.out.println(prefix + count + " Key : " + entry.getKey() + " Value : " + entry.getValue() + " Info : " + info.get(entry.getKey()) + "/" + info2.get(entry.getKey()) + "/"
                    + info3.get(entry.getKey()) + "/" + info4.get(entry.getKey()));
            count++;
            writer.write(prefix + entry.getKey() + " Percent : " + entry.getValue() + " Info : " + info.get(entry.getKey()) + "/" + info2.get(entry.getKey()) + "/" + info3.get(entry.getKey()) + "/"
                    + info4.get(entry.getKey()) + "\n");
        }
    }
    public static void writeMap(Map<String, Integer> map, String fileName) {
        try (Writer outLot = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fileName), StandardCharsets.UTF_8));) {

            for (String key : map.keySet()) {
                if (key != null && !"null".equals(key))
                    outLot.write(key + ";" + key + " " + map.get(key) + "\n");
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
