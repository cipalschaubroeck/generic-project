package com.cipalschaubroeck.oldcstender.machinelearning.shared;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * CpvUpgradeElement representation.
 */
@Getter
@Setter
public class CpvUpgradeElement {
    private String keywords = null;
    private String cpv = "";
    private Integer occurence = new Integer(0);
    private int atomicOccurence = 1;

    public CpvUpgradeElement(String keyword, String cpv, Integer occurence) {
        super();
        this.keywords = keyword;
        this.cpv = cpv;
        this.occurence = occurence;
    }

    public CpvUpgradeElement(String line) {
        super();
        String[] info = line.split(" ");
        this.keywords = info[0];
        //System.out.println(line);
        if (info.length > 2) {
            this.cpv = info[1];
            this.occurence = new Integer(info[2]);
        } else if (info.length == 2) {
            if (info[1].length() > 7)
                this.cpv = info[1];
            else
                this.occurence = new Integer(info[1]);
        }
    }

    public String toString() {
        return keywords + " " + (cpv.length() == 0 ? "00000000" : cpv) + " " + occurence;
    }

    public List<String> constructKeywordList() {
        String[] kArray = keywords.split(",");
        return Arrays.asList(kArray);
    }

    public List<String> constructAtomicKeywordList() {
        List<String> atomicKeywordList = new ArrayList<String>();
        for (String key : constructKeywordList()) {
            if (!key.startsWith("#")) {
                int pos = key.indexOf("(");
                if (pos != -1) {
                    int end = key.indexOf(")", pos);
                    if (end != -1) {
                        String prefix = key.substring(0, pos);
                        String[] keys = key.substring(pos + 1, end).split("\\|");
                        String ne = key.substring(end + 2);
                        for (String k : keys) {
                            atomicKeywordList.add(prefix + k + "_" + ne);
                        }
                    }
                } else {
                    atomicKeywordList.add(key);
                }
            }
        }
        atomicOccurence = occurence / atomicKeywordList.size();
        return atomicKeywordList;
    }

    public void addCpvString(String cpvNoParent) {
        String[] cpvArray = cpvNoParent.trim().split(" ");
        for (String c : cpvArray) {
            if (cpv.indexOf(c) == -1) cpv += "_" + c;
        }
    }
}
