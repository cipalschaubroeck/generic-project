package com.cipalschaubroeck.oldcstender.machinelearning;

import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.Map;

/**
 * A Relation
 */
@Getter
@Setter
public class KnowledgeRelation {
    private List<String> verbs;
    private List<KnowledgeEntity> entities;
    private String entitieString;

    public KnowledgeRelation(List<String> verbs, List<KnowledgeEntity> entities, String entitieString) {
        this.verbs = verbs;
        this.entities = entities;
        this.entitieString = entitieString;
    }

    public String constructEntities() {
        StringBuilder sb = new StringBuilder();
        for (KnowledgeEntity v : entities)
            sb.append((sb.length() > 0 ? "/" : "") + v);
        return sb.toString();
    }

    public String constructVerbs() {
        StringBuilder sb = new StringBuilder();
        for (String v : verbs)
            sb.append((sb.length() > 0 ? "/" : "") + v);
        return sb.toString();
    }

    public void addFeatures(Map<String, Integer> features, Map<String, KnowledgeEntity> entityMap) {
        for (String verb : getVerbs()) {
            for (KnowledgeEntity entity : getEntities()) {
                String e = entity.getEntity();
                KnowledgeEntity ke = entityMap.get(e);
                String group = null;
                String category = null;
                if (ke != null) {
                    group = ke.getGroup();
                    category = ke.getCategory();
                }
                if (group != null)
                    e = group;
                String feature = "REL_" + verb + "_" + e;
                //System.out.println(feature);
                if (!e.equals("")) KnowledgeBase.updateFeatureList(features, feature);
                if (category != null) {
                    feature = "REL_" + verb + "_CAT_" + category;
                    KnowledgeBase.updateFeatureList(features, feature);
                    if (entity.getAdjective() != null && entity.getAdjective().length() > 0) {
                        feature = "REL_" + verb + "_CAT_" + entity.getAdjective() + "_" + category;
                        KnowledgeBase.updateFeatureList(features, feature);
                    }
                }
                if (entity.getAdjective() != null && entity.getAdjective().length() > 0) {
                    e = entity.getAdjective() + "_" + entity.getEntity();
                    ke = entityMap.get(e);
                    group = null;
                    category = null;
                    if (ke != null) {
                        group = ke.getGroup();
                        category = ke.getCategory();
                    }
                    if (group != null)
                        e = group;
                    feature = "REL_" + verb + "_" + e;
                    if (!e.equals("")) KnowledgeBase.updateFeatureList(features, feature);
                    if (category != null) {
                        feature = "REL_" + verb + "_CAT_" + category;
                        KnowledgeBase.updateFeatureList(features, feature);
                        feature = "REL_" + verb + "_CAT_" + entity.getAdjective() + "_" + category;
                        KnowledgeBase.updateFeatureList(features, feature);
                    }
                }
            }
        }
    }

    @Override
    public int hashCode() {
        return (constructVerbs() + "@" + constructEntities()).hashCode();
    }

    public boolean equals(Object other) {
        if (other instanceof KnowledgeRelation) {
            KnowledgeRelation r = (KnowledgeRelation) other;
            return (constructVerbs() + "@" + constructEntities()).equals(r.constructVerbs() + "@" + r.constructEntities());
        }
        return false;
    }
}
