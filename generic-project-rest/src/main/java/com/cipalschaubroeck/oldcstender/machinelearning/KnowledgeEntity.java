package com.cipalschaubroeck.oldcstender.machinelearning;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

/**
 * A meaningful entity
 */
@Getter
@Setter
public class KnowledgeEntity {
    private String adjective = null;
    private String entity;
    private String group;
    private String groupBase;
    private String category;
    private List<String> synonymList = null;

    public KnowledgeEntity(String entitieString) {
        String[] info = entitieString.split(";");
        this.entity = info[0].trim();
        if (entity.indexOf(" ") != -1) {
            String[] ae = entity.split(" ");
            if (ae.length == 2) {
                entity = ae[1];
                adjective = ae[0];
            } else {
                entity = null;
            }
        }
        this.group = info[1].trim();
        if (group.indexOf(" ") != -1) {
            String[] ae = group.split(" ");
            if (ae.length == 2) {
                groupBase = ae[1];
            } else {
                groupBase = null;
            }
        } else {
            groupBase = this.group;
        }
        if (info.length == 3)
            this.category = info[2].trim();
    }

    public boolean equals(String adjective, String entity) {
        if (adjective != null) {
            return adjective.equals(this.adjective) && entity.equals(this.entity);
        } else
            return this.adjective == null && entity.equals(this.entity);
    }

    public KnowledgeEntity(String adjective, String entity) {
        if (adjective != null && adjective.length() > 0)
            this.adjective = adjective;
        else
            this.adjective = null;
        this.entity = entity;
        // TODO fix hyphen problems
        if (this.entity != null && this.entity.startsWith("-") && this.entity.length() > 1)
            this.entity = this.entity.substring(1);
    }

    public String toString() {
        return (getAdjective() != null ? getAdjective() + " " : "") + getEntity();
    }

    public void addSynonym(String synonym) {
        if (synonymList == null)
            synonymList = new ArrayList<>();
        synonymList.add(synonym);
    }
}
