package com.cipalschaubroeck.oldcstender.machinelearning;

import com.cipalschaubroeck.oldcstender.machinelearning.shared.MachineLearningTextUtils;

import java.util.Map;

/**
 * A text normalization utility.
 */

public class KnowledgeNormalize {
    protected KnowledgeBase knowledgeBase = null;

    public KnowledgeNormalize(KnowledgeBase knowledgeBase) {
        super();
        this.knowledgeBase = knowledgeBase;
    }

    public String[] tokens(String line) {
        line = line.replaceAll("\\)", " ) ");
        line = line.replaceAll("\\(", " ( ");
        line = line.replaceAll("   ", " ");
        line = line.replaceAll("  ", " ");
        line = line.replaceAll(" - en ", "- en ");
        line = line.replaceAll(" -en ", "- en ");
        line = line.replaceAll("-en ", "- en ");
        line = line.replaceAll("-& ", "- en ");
        line = line.replaceAll("- & ", "- en ");
        line = line.replaceAll("-of ", "- of ");
        line = line.replaceAll("-en/of ", "- en/of ");
        line = line.replaceAll("- en de ", "- en ");
        line = line.replaceAll("- en het ", "- en ");
        line = line.replaceAll("- en een ", "- en ");
        line = line.replaceAll(" EN ", " en ");
        line = line.replaceAll(" OF ", " of ");
        line = line.replaceAll(" EN/OF ", " en ");
        line = line.replaceAll("-EN ", "- en ");
        line = line.replaceAll("-OF ", "- of ");
        line = line.replaceAll("-EN/OF ", "- en/of ");
        line = line.replaceAll("- EN DE ", "- en ");
        line = line.replaceAll("- EN HET ", "- en ");
        line = line.replaceAll("- EN EEN ", "- en ");
        line = line.replaceAll("weg- en rioleringswerken", "wegen- en rioleringswerken");
        line = line.replaceAll("Weg- en rioleringswerken", "wegen- en rioleringswerken");
        return line.split(" ");
    }

    static final private String[] normalizeAdjectives = {"brandmeld","branddetectie","sanitaire","elektrische","hvac","cv","sprinkler","elektromechanische","televisie"};
    static final private String[] normalizeNouns = {"installaties","installatie","uitrustingen","uitrusting"};

    /**
     * Normalize some frequent constructs like
     * Onderhoud van de sanitaire, sprinkler-, elektrische en HVAC-installaties van het C.C.N.-gebouw
     * TODO add to confiog file
     */
    public String normalizeMultipleAdjectiveAndConcat(String line) {
        //
        line = " "+line+" ";
        for (String w : normalizeAdjectives) {
            int endPos = line.indexOf(w);
            String noun = null;
            if (endPos != -1) {
                endPos += w.length();
                boolean concat = false;
                if (line.substring(endPos,endPos+1).equals("-")) {
                    concat = true;
                    endPos++;
                }
                if (line.substring(endPos,endPos+1).equals(",") || (line.length() > (endPos+4) && (line.substring(endPos,endPos+4).equals(" en ")|| line.substring(endPos,endPos+4).equals(" of ")))) {
                    for (String n : normalizeNouns) {
                        int startNoun = line.indexOf(n,endPos);
                        if (startNoun != -1 && line.substring(endPos, startNoun).indexOf(".") == -1 && line.substring(endPos, startNoun).indexOf(")") == -1) {
                            noun = n;
                            break;
                        }
                    }
                }
                if (noun != null) {
                    if (concat)
                        line = line.substring(0,endPos-1) +noun + line.substring(endPos);
                    else
                        line = line.substring(0,endPos) + " "+noun + line.substring(endPos);
                }
            }
        }

        return line.substring(1, line.length()-1);
    }

    /**
     * Normalize text like: ADJ en ADJ NOUN -> ADJ NOUN en ADJ NOUN
     * eg sanitaire en HVAC installaties -> sanitaire installaties en HVAC installaties
     * @param line
     * @return
     */
    public String normalizeAdjectiveAndConcat(String line) {
        line = " "+line+" ";
        int pos = 0;
        do {
            pos = MachineLearningTextUtils.indexOfBetweenNotLetter(line, "(en|EN|OF|of|/|\\+)", pos);
            int l = 4;
            if (line.substring(pos+1,pos+2).equals("/") || line.substring(pos+1,pos+2).equals("+")) l = 3;
            if (pos != -1) {
                int start = line.substring(0,pos).lastIndexOf(" ");
                if (start != -1) {
                    String adjective = line.substring(start+1,pos).trim().toLowerCase();
                    if (knowledgeBase.isNamedAdjective(adjective)) {
                        int startNounPos = line.indexOf(" ",pos+l)+1;
                        if (startNounPos != 0) {
                            int endNounPos =  MachineLearningTextUtils.indexOf(line, "( |\\.|,)", startNounPos);
                            if (endNounPos == -1) endNounPos = line.length();
                            String noun = line.substring(startNounPos,endNounPos);
                            if (knowledgeBase.namedEntity(noun.toLowerCase()) != null) {
                                line = line.substring(0,pos) + " "+noun+line.substring(pos);
                                pos = pos + 1 + noun.length();
                            }
                        }
                    }
                }
                pos = pos+1;
            }
        } while (pos != -1);
        return line.substring(1, line.length()-1);
    }

    public String normalizeHyphenConcat(String line, Map<String,Integer> hyphenWords) {
        int pos = 0;
        line = " "+line+" ";
        String[] words = tokens(line);

        while (pos < words.length-2) {
            if ((words[pos].endsWith("-") && (words[pos+1].equals("en") || words[pos+1].equals("en/of") || words[pos+1].equals("of")))) {
                //if (!words[pos].endsWith("-")) words[pos]= words[pos]+" ";
                String word = replaceHyphen( words[pos],  words[pos+2], ( pos+3  < words.length ? words[pos+3] : null), hyphenWords);
                if (!words[pos].equals(word)) {
                    words[pos] = word;
                    int beforePos = pos -1;
                    while (beforePos > 0) {
                        word = words[beforePos];
                        if (word.endsWith("-,")) word = word.substring(0,word.length()-1);
                        if (word.endsWith("-")) {
                            word = replaceHyphen( word, words[pos], null, hyphenWords);
                            if (!words[beforePos].equals(word)) words[beforePos] = word+",";
                            beforePos--;
                        } else {
                            beforePos = 0;
                        }
                    }
                } else {
                    //System.out.println(word + " "+words[pos+2]);
                }
            }
            pos++;
        }
        StringBuilder sb = new StringBuilder();
        for (String w : words) {
            if (w.indexOf("-") != -1) w =  cleanHyphens(w);
            sb.append((sb.length() != 0 ? " " : "")+ w);
        }

        String result =  sb.toString();
        return result;
    }

    private String cleanHyphens(String word) {
        String cleanWord = word;
        String postFix = "";
        int pos = word.indexOf(".");
        if (pos == -1) pos = word.indexOf(",");
        if (pos == -1) pos = word.indexOf(";");
        if (pos == -1) pos = word.indexOf("!");
        if (pos != -1) {
            postFix = word.substring(pos);
            cleanWord = word.substring(0,pos);
        }
        if (knowledgeBase.namedEntity(cleanWord) != null) return cleanWord+postFix;
        return cleanWord.replace("-", ",")+postFix;
    }

    private String replaceHyphen(String word1, String word2, String word3, Map<String,Integer> hyphenWords) {
        String word = word1.substring(0,word1.length()-1).toLowerCase();
        while (word2.length() > 2 && (word2.substring(word2.length()-1).equals(".") || word2.substring(word2.length()-1).equals(",")|| word2.substring(word2.length()-1).equals(":") || word2.substring(word2.length()-1).equals(";"))) word2 = word2.substring(0,word2.length()-1);

        boolean word2Found = false;
        for (KnowledgeEntity namedEntity : knowledgeBase.getNamedEntityList()) {
            if (namedEntity.getEntity().startsWith(word) && namedEntity.getEntity().length() -2 > word.length()) {
                String suffix= namedEntity.getEntity().substring(word.length());
                if (knowledgeBase.namedAdjective(word2, word3) == null) {
                    if (word2.toLowerCase().endsWith(suffix)) {
                        return word+suffix;
                    }
                } else {
                    if (word3 != null) {
                        return word+word3;
                    }
                }
            }
        }
        for (KnowledgeEntity namedEntity : knowledgeBase.getNamedEntityList()) {
            if (word2.toLowerCase().equals(namedEntity.getEntity())) word2Found = true;
        }
        if (hyphenWords != null) knowledgeBase.updateFeatureList(hyphenWords, word1.toLowerCase() + ";"+word2.toLowerCase());
        if (!word2Found && hyphenWords !=null) knowledgeBase.updateFeatureList(hyphenWords, word2.toLowerCase() + ";"+word2.toLowerCase());
        return word1;
    }

    /**
     * Normalize some frequent named entities like the name of a bridge.
     * TODO add to config file
     *
     * @param line
     * @return
     */
    public static String normalizeNamedEntity(String line) {
        line = line.replaceAll("Zeetijger", "ship");
        line = line.replaceAll("ms Westdiep", "ship");
        line = line.replaceAll("ms Simon Stevin", "ship");

        line = line.replaceAll("Albertkanaal", "kanaal");

        line = line.replaceAll(" [A-Z][a-z]*(kerk|kathedraal)[ \\,\\;\\.]", " church ");
        line = line.replaceAll(" [A-Z][a-z]*(dok)[ \\,\\;\\.]", " dock ");
        line = line.replaceAll(" [A-Z][a-z]*(sluis)[ \\,\\;\\.]", " sluice ");
        line = line.replaceAll(" [A-Z][a-z]*(tunnel)[ \\,\\;\\.]", " tunnel ");
        line = line.replaceAll(" [A-Z][a-z]*(brug)[ \\,\\;\\.]", " bridge ");
        line = line.replaceAll(" [A-Z][a-z]*(park|parkje)[ \\,\\;\\.]", " park ");
        line = line.replaceAll(" [A-Z][a-z]*(straat|weg|laan) [0-9]*", " street ");
        return line.replaceAll(" [A-Z][a-z]*(straat|weg|laan)[ \\,\\;\\.]", " street ");
    }
}
