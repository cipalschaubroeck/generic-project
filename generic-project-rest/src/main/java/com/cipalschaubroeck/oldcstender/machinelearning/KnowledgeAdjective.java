package com.cipalschaubroeck.oldcstender.machinelearning;

import lombok.Getter;
import lombok.Setter;

/**
 * An adjective
 */
@Getter
@Setter
public class KnowledgeAdjective {
    private String adjective;

    public KnowledgeAdjective(String adjective) {
        this.adjective = adjective;
    }
}
