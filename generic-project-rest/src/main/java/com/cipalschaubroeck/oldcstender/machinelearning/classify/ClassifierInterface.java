package com.cipalschaubroeck.oldcstender.machinelearning.classify;

/**
 * Interface for classifying an instance.
 */
public interface ClassifierInterface {
    double classifyInstance(Instance instance);
}
