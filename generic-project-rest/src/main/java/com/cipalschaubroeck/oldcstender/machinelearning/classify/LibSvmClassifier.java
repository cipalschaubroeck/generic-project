package com.cipalschaubroeck.oldcstender.machinelearning.classify;

import libsvm.svm;
import libsvm.svm_model;

/**
 * Wrapper for the LibSVM classifier.
 */
public class LibSvmClassifier implements ClassifierInterface {
    protected svm_model smoModel = null;

    public LibSvmClassifier(svm_model smoModel) {
        super();
        this.smoModel = smoModel;
    }

    @Override
    public double classifyInstance(Instance sparseInstance) {
        return svm.svm_predict(smoModel, sparseInstance.getLibSvmInstance());
    }
}
