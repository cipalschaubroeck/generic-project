package com.cipalschaubroeck.oldcstender.report.jasper;

import com.cipalschaubroeck.cstender.domain.Tender;
import com.vaadin.addon.tableexport.TemporaryFileDownloadResource;
import com.vaadin.ui.UI;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.export.JRPrintServiceExporter;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimplePrintServiceExporterConfiguration;

import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.HashPrintServiceAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;
import javax.print.attribute.PrintServiceAttributeSet;
import javax.print.attribute.standard.MediaSizeName;
import javax.print.attribute.standard.PrinterName;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Export a Vaadin table with optionally a filter (fieldgroup) to Jasper.
 * PDF export and print to the default printer are supported.
 * @author jurrien
 */
public class JasperTenderExport  {
    private static final long serialVersionUID = -1683452748731184723L;
    private static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(JasperTenderExport.class);
    private JasperPrint jasperPrint = null;
    private List<Tender> tenderList = null;
    protected String mimeType;
    /**
     * The title of the "report" of the table contents.
     */
    protected String reportTitle ="";
    /**
     * The Filter of the report
     */
    protected String reportFilter = "";
    /**
     *
     * @param tenderList
     */
    public JasperTenderExport(List<Tender> tenderList) {
        this.tenderList = tenderList;
    }

    /**
     * Create and export the Table contents as a PDF. Only the export()
     * method needs to be called. If the user wishes to manipulate the converted object to export,
     * then convertTable() should be called separately, and, after manipulation, sendConverted().
     */
    public void export() {
        convertTable();
        sendConverted();
    }
    /**
     * Create and print the Table contents. Only the print()
     * method needs to be called. If the user wishes to manipulate the converted object to print,
     * then convertTable() should be called separately, and, after manipulation, printConverted().
     * @param printerName name of the printer to print on, or null for the system-default printer
     */
    public void print(String printerName) {
        convertTable();
        printConverted(printerName);
    }

    public void convertTable() {
        InputStream is = null;
        try {
            JRBeanCollectionDataSource tenderJRBeanCollectionDataSource = new JRBeanCollectionDataSource(tenderList);
            is = getClass().getClassLoader().getResourceAsStream("/com/sygel/tender/report/jasper/TenderMaster.jrxml");
            JasperDesign jasperMasterDesign = JRXmlLoader.load(is);
            JasperReport jasperMasterReport = JasperCompileManager.compileReport(jasperMasterDesign);

            Map<String, Object> params = new HashMap<String, Object>();
            params.put("REPORT_TITLE", getReportTitle());
            params.put("filterParameter", getReportFilter());
            logger.trace("Filling the report");

            this.jasperPrint = JasperFillManager.fillReport(jasperMasterReport, params, tenderJRBeanCollectionDataSource);
        } catch (JRException e) {
            logger.error("JRException error. " + e);
        }
    }

    /**
     * Export the workbook to the end-user.
     * <p/>
     * Code obtained from: http://vaadin.com/forum/-/message_boards/view_message/159583
     *
     * @return true, if successful
     */
    public boolean sendConverted() {
        File tempFile = null;
        FileOutputStream fileOut = null;
        try {
            tempFile = File.createTempFile("tmp", ".pdf");
            fileOut = new FileOutputStream(tempFile);
            logger.trace("Exporting the report to pdf");
            JasperExportManager.exportReportToPdfStream(this.jasperPrint, fileOut);
            if (null == mimeType) {
                setMimeType("application/pdf");
            }
            final boolean success = sendConvertedFileToUser(UI.getCurrent(), tempFile, "pdfexport.pdf");
            return success;
        } catch (final IOException e) {
            logger.error("Converting to PDF failed with IOException " + e);
            return false;
        } catch (JRException e) {
            logger.error("Converting to PDF failed with JRException " + e);
            return false;
        } finally {
            tempFile.deleteOnExit();
            try {
                fileOut.close();
            } catch (final IOException e) {
            }
        }
    }
    /**
     * Print report to a printer.
     * @param printerName the name of the printer to print on, or null for the system default printer
     */
    public void printConverted(String printerName) {
        long start = System.currentTimeMillis();
        PrintRequestAttributeSet printRequestAttributeSet = new HashPrintRequestAttributeSet();
        printRequestAttributeSet.add(MediaSizeName.ISO_A4);

        PrintServiceAttributeSet printServiceAttributeSet = new HashPrintServiceAttributeSet();
        if (printerName != null)
            printServiceAttributeSet.add(new PrinterName(printerName, null));

        JRPrintServiceExporter exporter = new JRPrintServiceExporter();
        exporter.setExporterInput(new SimpleExporterInput(this.jasperPrint));
        SimplePrintServiceExporterConfiguration configuration = new SimplePrintServiceExporterConfiguration();
        configuration.setPrintRequestAttributeSet(printRequestAttributeSet);
        configuration.setPrintServiceAttributeSet(printServiceAttributeSet);
        configuration.setDisplayPageDialog(false);
        configuration.setDisplayPrintDialog(false);
        exporter.setConfiguration(configuration);
        try {
            exporter.exportReport();
        } catch (JRException e) {
            logger.error("JRException error. " + e);
        }
        logger.trace("Printing time : " + (System.currentTimeMillis() - start));
    }

    public String getReportTitle() {
        return reportTitle;
    }
    public void setReportTitle(String reportTitle) {
        this.reportTitle = reportTitle;
    }


    /**
     * Utility method to send the converted object to the user, if it has been written to a
     * temporary File.
     *
     * Code obtained from: http://vaadin.com/forum/-/message_boards/view_message/159583
     *
     * @return true, if successful
     */
    public boolean sendConvertedFileToUser(final UI app, final File fileToExport,
                                           final String exportFileName, final String mimeType) {
        setMimeType(mimeType);
        return sendConvertedFileToUser(app, fileToExport, exportFileName);

    }

    protected boolean sendConvertedFileToUser(final UI app, final File fileToExport,
                                              final String exportFileName) {
        TemporaryFileDownloadResource resource;
        try {
            resource =
                    new TemporaryFileDownloadResource(app, exportFileName, mimeType, fileToExport);
            app.getPage().open(resource, null, false);
        } catch (final FileNotFoundException e) {
            logger.info("Sending file to user failed with FileNotFoundException " + e);
            return false;
        }
        return true;
    }



    public String getMimeType() {
        return this.mimeType;
    }

    public void setMimeType(final String mimeType) {
        this.mimeType = mimeType;
    }

    public String getReportFilter() {
        return reportFilter;
    }

    public void setReportFilter(String reportFilter) {
        this.reportFilter = reportFilter;
    }

}
