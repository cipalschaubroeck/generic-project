package com.cipalschaubroeck.oldcstender.report.jasper;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MasterDataSource {
    private DynamicColumnDataSource listDataSource = null;
    private DynamicColumnDataSource filterDataSource = null;
}
