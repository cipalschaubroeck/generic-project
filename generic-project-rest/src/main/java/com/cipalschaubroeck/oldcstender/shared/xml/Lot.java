package com.cipalschaubroeck.oldcstender.shared.xml;

import lombok.Getter;
import lombok.Setter;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Getter
@Setter
@XmlRootElement(name="l")
public class Lot {
    private String number;
    private String title;
    private String essence;

    @XmlElement(name="n")
    public void setNumber(String number) {
        this.number = number;
    }

    @XmlElement(name="t")
    public void setTitle(String title) {
        this.title = title;
    }

    @XmlElement(name="e")
    public void setEssence(String essence) {
        this.essence = essence;
    }

    public String toString() {
        return number + " " + title + " " + essence;
    }
}
