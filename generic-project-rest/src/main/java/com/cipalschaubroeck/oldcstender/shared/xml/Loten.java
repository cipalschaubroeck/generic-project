package com.cipalschaubroeck.oldcstender.shared.xml;

import lombok.Getter;
import lombok.Setter;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@Getter
@Setter
@XmlRootElement(name="x")
public class Loten {
    private List<Lot> lotList = null;

    @XmlElement(name="s")
    public void setLotList(List<Lot> lotList) {
        this.lotList = lotList;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (Lot lot : lotList)
            sb.append(lot.toString());
        return sb.toString();
    }
}
