package com.cipalschaubroeck.oldcstender.security;

import lombok.Getter;
import lombok.Setter;

import java.security.Principal;

@Getter
@Setter
public class UserPrincipal implements Principal {
    private String name;

    public UserPrincipal(String name) {
        super();
        this.name = name;
    }
}
