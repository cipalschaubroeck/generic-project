package com.cipalschaubroeck.oldcstender.security;

import lombok.Getter;
import lombok.Setter;

import java.security.Principal;

@Getter
@Setter
public class RolePrincipal implements Principal {
    private String name;

    public RolePrincipal(String name) {
        super();
        this.name = name;
    }
}
