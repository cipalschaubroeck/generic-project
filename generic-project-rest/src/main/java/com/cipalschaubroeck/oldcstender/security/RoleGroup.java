package com.cipalschaubroeck.oldcstender.security;

import java.security.Principal;
import java.security.acl.Group;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;

public class RoleGroup implements Group {
    private String groupName;
    private HashMap<Principal,Principal> members;

    public RoleGroup(String name) {
        groupName = name;
        members = new HashMap<Principal, Principal>();
    }

    @Override
    public String getName() {
        return groupName;
    }

    @Override
    public boolean addMember(Principal user) {
        boolean isMember = members.containsKey(user);
        if( isMember == false )
            members.put(user, user);
        return isMember == false;
    }

    @Override
    public boolean removeMember(Principal user) {
        Object prev = members.remove(user);
        return prev != null;
    }

    @Override
    public boolean isMember(Principal member) {
        return members.containsKey(member);
    }

    @Override
    public Enumeration<? extends Principal> members() {
        return Collections.enumeration(members.values());
    }
}
