package com.cipalschaubroeck.oldcstender.security;

import com.cipalschaubroeck.cstender.domain.User;
import com.cipalschaubroeck.oldcstender.deprecatedservice.UserService;
import com.cipalschaubroeck.oldcstender.shared.CdiHelper;
import com.cipalschaubroeck.oldcstender.shared.PasswordHash;

import javax.naming.NamingException;
import javax.security.auth.Subject;
import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.NameCallback;
import javax.security.auth.callback.PasswordCallback;
import javax.security.auth.callback.UnsupportedCallbackException;
import javax.security.auth.login.LoginException;
import javax.security.auth.spi.LoginModule;
import java.io.IOException;
import java.security.Principal;
import java.security.acl.Group;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Security module for CDI / JPA based username/password/role checks.
 */
public class TenderLoginModule implements LoginModule {
    //@Inject
    UserService userService;
    String roles = null;

    private CallbackHandler handler;
    private Subject subject;
    private UserPrincipal userPrincipal;
    private RolePrincipal rolePrincipal;
    private String login;
    private List<String> userGroups;
    private boolean loginOk = false;

    public void initialize(Subject subject, CallbackHandler callbackHandler, Map sharedState, Map options) {
        // We could read options passed via <module-option> in standalone.xml if there were any here
        // For an example see http://docs.redhat.com/docs/en-US/JBoss_Enterprise_Application_Platform/5/html/Security_Guide/sect-Custom_LoginModule_Example.html

        // LoginModule is not managed bean so we have to do a manual inject.
        // http://stackoverflow.com/questions/29845689/unable-to-inject-a-dao-in-custom-login-module
        if (userService == null) {
            try {
                CdiHelper.programmaticInjection(TenderLoginModule.class, this);
            } catch (NamingException e) {
                e.printStackTrace();
            }
        }
        handler = callbackHandler;
        this.subject = subject;
    }

    @Override
    public boolean login() throws LoginException {
        Callback[] callbacks = new Callback[2];
        callbacks[0] = new NameCallback("login");
        callbacks[1] = new PasswordCallback("password", true);

        try {
            handler.handle(callbacks);
            String username = ((NameCallback) callbacks[0]).getName();
            String password = String.valueOf(((PasswordCallback) callbacks[1])
                    .getPassword());

            User user = userService.findUser(username);

            if (user != null &&
                    password != null
                    &&	PasswordHash.validatePassword(password, user.getPasswordHash()) ) {

                login = username;
                userGroups = new ArrayList<String>();

                userGroups.add(User.ROLE_USER);
                if (user.getRoles() !=null && user.getRoles().indexOf(":a:") != -1)
                    userGroups.add(User.ROLE_ADMIN);
                loginOk = true;
                return true;
            }
            loginOk = false;
            // If credentials are NOT OK we throw a LoginException
            throw new LoginException("Authentication failed");

        } catch (IOException e) {
            loginOk = false;
            throw new LoginException(e.getMessage());
        } catch (UnsupportedCallbackException e) {
            loginOk = false;
            throw new LoginException(e.getMessage());
        }
    }

    @Override
    public boolean commit() throws LoginException {
        System.out.println("Commit S");
        if( loginOk == false )
            return false;
        userPrincipal = new UserPrincipal(login);
        subject.getPrincipals().add(userPrincipal);

        // jboss type roles
        Set<Principal> principals = subject.getPrincipals();
        Group subjectGroup = createGroup("Roles", principals);


        if (userGroups != null && userGroups.size() > 0) {
            for (String groupName : userGroups) {
                rolePrincipal = new RolePrincipal(groupName);
                subject.getPrincipals().add(rolePrincipal);
                // for jboss
                subjectGroup.addMember(rolePrincipal);
            }
        }
        return true;
    }

    /**
     * Find or create a Group with the given name.
     *
     * @return A named Group from the principals set.
     */
    protected Group createGroup(String name, Set<Principal> principals)
    {
        Group roles = null;
        Iterator<Principal> iter = principals.iterator();
        while( iter.hasNext() )
        {
            Object next = iter.next();
            if( (next instanceof Group) == false )
                continue;
            Group grp = (Group) next;
            if( grp.getName().equals(name) )
            {
                roles = grp;
                break;
            }
        }
        // If we did not find a group create one
        if( roles == null )
        {
            roles = new RoleGroup(name);
            principals.add(roles);
        }
        return roles;
    }

    @Override
    public boolean abort() throws LoginException {
        return false;
    }

    @Override
    public boolean logout() throws LoginException {
        subject.getPrincipals().remove(userPrincipal);
        subject.getPrincipals().remove(rolePrincipal);
        return true;
    }

}
