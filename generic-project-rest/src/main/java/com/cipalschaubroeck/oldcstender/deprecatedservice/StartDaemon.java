package com.cipalschaubroeck.oldcstender.deprecatedservice;

import org.apache.log4j.Logger;

import javax.annotation.PostConstruct;

// TODO JAL: Remove import javax.ejb.Singleton;
// TODO JAL: Remove import javax.ejb.Startup;
// TODO JAL: Remove import javax.ejb.TransactionAttribute;
// TODO JAL: Remove import javax.ejb.TransactionAttributeType;

/**
 * Deamon for test and maintenance tasks
 */
// TODO JAL: Remove @Startup
// TODO JAL: Remove @Singleton
// TODO JAL: Remove @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
public class StartDaemon {
    //@Inject
    OldImportService importService;

    //@Inject
    ImportPreService importPreService;

    //@Inject
    TrainingService trainingService;

    //@Inject
    UserService userService;

    //@Inject
    PushService pushService;

    private final Logger logger = Logger.getLogger(StartDaemon.class);

    @PostConstruct
    void init() {
        logger.info("init create user");
        //userService.createUser("js@sygel.com","test","jurrien","saelens");
    }

//    public static void main(String[] args) {
//        SpringApplication.run(StartDaemon.class, args);
//    }
}
