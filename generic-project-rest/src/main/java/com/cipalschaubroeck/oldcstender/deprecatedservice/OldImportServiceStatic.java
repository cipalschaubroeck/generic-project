package com.cipalschaubroeck.oldcstender.deprecatedservice;

import com.cipalschaubroeck.oldcstender.machinelearning.BuildModels;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

public class OldImportServiceStatic {

    public static final String DATA_FOLDER = BuildModels.rootDataFolder + "data/tender/be";

    public static String getDoneFileName(File file) {
        String dateString = new SimpleDateFormat("ddMMyyyyHHmmss").format(new Date());
        return file.getName().substring(0, file.getName().length() - 4) + "_" + dateString + ".zip";
    }

}
