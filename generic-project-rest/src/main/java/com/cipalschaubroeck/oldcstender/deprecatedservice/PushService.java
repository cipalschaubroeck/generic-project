package com.cipalschaubroeck.oldcstender.deprecatedservice;

import com.cipalschaubroeck.cstender.domain.Filter;
import com.cipalschaubroeck.cstender.domain.FilterTender;
import com.cipalschaubroeck.cstender.domain.PushTenderFilter;
import com.cipalschaubroeck.cstender.domain.Tender;
import com.cipalschaubroeck.cstender.domain.User;
import com.cipalschaubroeck.oldcstender.machinelearning.KnowledgeBase;
import org.apache.log4j.Logger;

import javax.annotation.Resource;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.persistence.EntityManager;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Set;

// TODO JAL: Remove import javax.ejb.AsyncResult;
// TODO JAL: Remove import javax.ejb.Asynchronous;
// TODO JAL: Remove import javax.ejb.Local;
// TODO JAL: Remove import javax.ejb.Stateless;

/**
 * Create and send emails to all users based on their filters
 */
// TODO JAL: Remove @Stateless
// TODO JAL: Remove @Local
public class PushService {
    @Resource(mappedName = "java:comp/env/mail/Default")
    //@Resource(name = "java:jboss/mail/Default")
    private Session session;

    //@PersistenceContext(unitName = "tender")
    private EntityManager entityManager;

    //@Inject
    I18Service i18Service;

    private final Logger logger = Logger.getLogger(PushService.class);

    public final static String BASEURL = KnowledgeBase.serverProtocol+"://"+KnowledgeBase.serverName+(KnowledgeBase.serverPort.equals("80") || KnowledgeBase.serverPort.equals("443") ? "" : ":"+KnowledgeBase.serverPort+"")+"/tender/secure#!MYTENDER/";
    public final static String MAILURL = KnowledgeBase.serverProtocol+"://"+KnowledgeBase.serverName+(KnowledgeBase.serverPort.equals("80") || KnowledgeBase.serverPort.equals("443") ? "" : ":"+KnowledgeBase.serverPort+"")+"/tender/myemail.jsp";

    //@Inject
    UserService userService;

    //@Inject
    SearchService searchService;

    /**
     * Create new EmailTender objects for every filter of every user.
     * Only creates the emailTenders after minimum 5 days and if there are tenders for that filter.
     */
    // TODO JAL: Remove @Asynchronous
    public String emailUsers(Progress progress) {
        Calendar emailTestDate = Calendar.getInstance();
        emailTestDate.add(Calendar.DATE, -5); // try again after minimum 5 days.
        List<User> users = userService.findAllUsers();
        int counter  = 0;
        int emails  = 0;
        for (User user : users) {
            Set<Filter> filters = user.getFilters();
            StringBuilder emailMessage = new StringBuilder(i18Service.getMessage("PUSH_EMAIL_HEADER"));
            boolean sendEmail = false;
            for (Filter filter : filters) {
                if (filter instanceof PushTenderFilter && filter.getSendEmail() && emailTestDate.getTime().after(filter.getLastPushMailTo()) ) { // try again after minimum 5 days.
                    String searchString = filter.getSearchString();
                    logger.trace("Email filter: " + filter.getName());
                    List<Tender> tenders = searchService.findTenders(searchString, false, filter.getLastPushMailTo());
                    if (tenders != null && tenders.size() > 0) {// only mail if there are tenders
                        emailTenders(user,(PushTenderFilter)filter,tenders, emailMessage);
                        sendEmail = true;
                    }
                }
            }
            if (sendEmail) {
                emailMessage.append(i18Service.getMessage("PUSH_EMAIL_FOOTER"));
                send(user.getEmail(), i18Service.getMessage("PUSH_EMAIL_TITLE"), emailMessage.toString());
                emails++;
            }
            counter++;
            if (progress != null)
                progress.setProgress(((float) counter) / users.size());
        }
        return new Long(emails)+"";
    }

    private void emailTenders(User user, PushTenderFilter filter, List<Tender> tenders, StringBuilder emailMessage) {
        filter.setLastPushMailFrom(filter.getLastPushMailTo());
        filter.setLastPushMailTo(new Date());
        // BULK DELETE, should be faster I hope.
        int deleted = entityManager.createNamedQuery("FilterTender.delete").setParameter("filter", filter).executeUpdate();
        entityManager.flush();
        logger.trace("EmailTender size b " + deleted);
        Set<FilterTender> filterTenders = filter.getFilterTenders();
        logger.trace("EmailTender size a " + filterTenders.size());
        for (Tender tender : tenders) {
            FilterTender filterTender = new FilterTender();
            entityManager.persist(filterTender);
            filterTender.setTenderFilter(filter);
            filterTender.setTender(tender);
            filterTenders.add(filterTender);
        }
        String filterName = filter.getName();
        try {
            filterName = URLEncoder.encode(filter.getName(), "UTF-8");
        } catch (UnsupportedEncodingException e) {
        }
        emailMessage.append("<a href=\""+MAILURL+"?filter="+filterName+"\">"+filter.getDisplayName()+ " ("+tenders.size()+")</a><br/>");
    }

    private void send(String addresses, String topic, String textMessage) {
        try {
            Message message = new MimeMessage(session);
            message.setFrom(InternetAddress.parse(i18Service.getMessage("PUSH_EMAIL_FROM"))[0]);
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse("jurrien.saelens@gmail.com"));
            message.setSubject(topic);
            message.setContent(textMessage, "text/html; charset=utf-8");
            Transport.send(message);
        } catch (MessagingException e) {
            logger.error("Could not send mail",e);
        }
    }
}
