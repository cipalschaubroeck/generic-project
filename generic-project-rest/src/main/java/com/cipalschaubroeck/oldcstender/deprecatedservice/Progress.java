package com.cipalschaubroeck.oldcstender.deprecatedservice;

import lombok.Getter;
import lombok.Setter;

/**
 * Defines the progress of a report.
 */
@Getter
@Setter
public class Progress {
    // Volatile because read in another thread. Progress value between 0 and 1
    private volatile float progress = 0;
}
