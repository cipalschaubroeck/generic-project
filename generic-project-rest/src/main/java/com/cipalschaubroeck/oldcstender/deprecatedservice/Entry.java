package com.cipalschaubroeck.oldcstender.deprecatedservice;

import org.w3c.dom.Document;

import java.util.Date;

public class Entry implements Comparable<Entry> {
    Document document = null;
    String content = null;
    String form = null;
    String language = null;
    Date publicationDate = null;
    String name = null;
    String title = null;
    String government = null;

    @Override
    public int compareTo(Entry o) {
        if (publicationDate == null)
            return -1;
        if (o.publicationDate == null)
            return 1;
        return publicationDate.compareTo(o.publicationDate);
    }
}
