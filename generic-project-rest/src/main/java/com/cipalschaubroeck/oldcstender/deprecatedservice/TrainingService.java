package com.cipalschaubroeck.oldcstender.deprecatedservice;

import com.cipalschaubroeck.cstender.domain.Tender;
import com.cipalschaubroeck.oldcstender.machinelearning.BuildModels;
import com.cipalschaubroeck.oldcstender.machinelearning.KnowledgeBase;
import com.cipalschaubroeck.oldcstender.machinelearning.MLearningModels;
import com.cipalschaubroeck.oldcstender.machinelearning.classify.ClassifierInterface;
import com.cipalschaubroeck.oldcstender.machinelearning.classify.FeatureList;
import com.cipalschaubroeck.oldcstender.machinelearning.shared.AkteUtilities;
import com.cipalschaubroeck.oldcstender.machinelearning.shared.CpvUpgrade;
import com.cipalschaubroeck.oldcstender.machinelearning.shared.CpvUpgradeElement;
import com.cipalschaubroeck.oldcstender.machinelearning.shared.MachineLearningFileUtils;
import com.cipalschaubroeck.oldcstender.machinelearning.shared.MachineLearningUtils;
import com.cipalschaubroeck.oldcstender.shared.Util;
import org.apache.log4j.Logger;

import javax.persistence.EntityManager;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

// TODO JAL: Remove import javax.ejb.Local;
// TODO JAL: Remove import javax.ejb.Stateless;
// TODO JAL: Remove import javax.ejb.TransactionAttribute;
// TODO JAL: Remove import javax.ejb.TransactionAttributeType;

/**
 * Training Data deprecatedservice
 */
// TODO JAL: Remove @Stateless
// TODO JAL: Remove @Local
public class TrainingService {
    //@PersistenceContext(unitName = "tender")
    private EntityManager entityManager;

    private final Logger logger = Logger.getLogger(TrainingService.class);

    //@Inject
    KnowledgeService knowledgeService;

    //@Inject
    SearchService searchService;

    //@Inject
    TrainingSelfService trainingSelfService;

    public static final String DATA_FOLDER = BuildModels.rootDataFolder + "data";

    public static final int IMPORT_BEGIN = 180000; // 180000
    public static final int IMPORT_END = 250000;   // 250000
    public static final int IMPORT_STEP = (IMPORT_END - IMPORT_BEGIN < 1000) ? IMPORT_END - IMPORT_BEGIN : 1000;
    public static final int IMPORT_STEP_SMALL = (IMPORT_END - IMPORT_BEGIN < 100) ? IMPORT_END - IMPORT_BEGIN : 100;
    public static final int IMPORT_STEP_SUPER_SMALL = (IMPORT_END - IMPORT_BEGIN < 10) ? IMPORT_END - IMPORT_BEGIN : 10;

    /**
     * Construct the corpus from all tender titles and essence and also the CPV translation list
     */
    // TODO JAL: Remove @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public void contructCorpus() {
        File arff = new File(DATA_FOLDER, "tender_corpus.txt");

        appendArff(arff, "", false);

        KnowledgeBase knowledgebase = knowledgeService.getKnowledgeBase();

        Map<String,String> cpvTranslationMap = knowledgebase.getCpvTranslations();
        for (String cpv : cpvTranslationMap.keySet()) {
            String description = cpvTranslationMap.get(cpv);
            appendArff(arff, knowledgebase.normalizeLine(description), true);
        }
        for (Tender tender : searchService.findTenderAll()) {
            //for (int i = IMPORT_BEGIN; i < IMPORT_END; i = i + 1) {
            //	Tender tender = searchService.findTenderByNoticeId(i + "");
            if (tender != null) {
                tender.setLanguage("NL");
                String title = tender.getTitle1();
                if (title != null && title.length() > 2) {
                    title = title.trim();
                    if (!title.endsWith(".")) title = title + ".  ";
                    if (title.endsWith(".")) title = title + "  ";
                    String essence = tender.getEssence1();
                    if (essence != null) {
                        if (!essence.endsWith(".")) essence = essence + ".  ";
                        if (essence.endsWith(".")) essence = essence + "  ";
                    }
                    appendArff(arff, knowledgebase.normalizeLine(title+essence), true);
                }
            }
        }
    }

    /**
     * Construct training data in ARFF format.
     * Use the Build class to generate the models.
     */
    // TODO JAL: Remove @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public void contructTraningDataCpv() {
        File arff = new File(DATA_FOLDER, "atender_cpv.arff");

        Map<String, List<String>> newDiscardedTenderDisciplineMap = new HashMap<String, List<String>>();
        for (String discipline : MLearningModels.DISCIPLINES) {
            newDiscardedTenderDisciplineMap.put(discipline, new ArrayList<String>());
        }

        appendArff(arff, "@relation 'Tender CPV'", false);
        appendArff(arff, "@attribute id string", true);
        appendArff(arff, "@attribute title string", true);
        appendArff(arff, "@attribute essence string", true);
        appendArff(arff, "@attribute type string", true);
        appendArff(arff, "@attribute category string", true);
        appendArff(arff, "@attribute cpvKnowledgeBase string", true);
        appendArff(arff, "@attribute cpvNoParent string", true);
        appendArff(arff, "@data", true);
        constructTrainingDataFromCpvList(arff, newDiscardedTenderDisciplineMap);
        constructTrainingDataFromTenderDB(arff, newDiscardedTenderDisciplineMap);
        for (String discipline : MLearningModels.DISCIPLINES) {
            File discardedTenderFile = new File(DATA_FOLDER, "adisciplineDiscarded_"+discipline+"_nl_be.txt");
            appendArff(discardedTenderFile, "# discarded tenders for discipline "+discipline, false);
            for (String message : newDiscardedTenderDisciplineMap.get(discipline))
                appendArff(discardedTenderFile, message, true);
        }
    }

    private void constructTrainingDataFromTenderDB(File arff, Map<String,List<String>> discardedTenderDisciplineMap) {
        for (Tender tender : searchService.findTenderAll()) {
            //for (int i = IMPORT_BEGIN; i < IMPORT_END; i = i + 1) {
            //  Tender tender = searchService.findTenderByNoticeId(i + "");
            if (tender != null) {
                tender.setLanguage("NL");
                String titel = tender.getTitle1();
                if (titel != null && titel.length() > 2 && tender.getLot() == null) {
                    //logger.trace(tender.getNoticeId() + " " + tender.getBda());
                    StringBuilder cpvKnowledgeBase = new StringBuilder();
                    String cpvNoParent = upgradeCpv(tender.getBda(), tender.getCpvNoParent(),tender.getTitle(), tender.getEssence(), knowledgeService.getKnowledgeBase(),cpvKnowledgeBase, discardedTenderDisciplineMap);
                    if (cpvNoParent.length() == 8 && cpvNoParent.endsWith("000000")
                            && !cpvNoParent.equals("48000000") && !cpvNoParent.equals("72000000") // SOFTWARE, DO TAKE the general CPV as training data
                            && !cpvNoParent.equals("44000000") // BO
                            && !cpvNoParent.equals("16000000") && !cpvNoParent.equals("31000000") && !cpvNoParent.equals("34000000") && !cpvNoParent.equals("38000000") && !cpvNoParent.equals("42000000") && !cpvNoParent.equals("43000000") // MM
                    ) {
                        //logger.trace("Ignore "+cpvNoParent);
                    } else {
                        appendArff(arff,
                                "'" + tender.getId() + "','"
                                        + escapeArff(titel) + "','"
                                        + escapeArff(tender.getEssence1()) + "','"
                                        + escapeArff(tender.getType()) + "','"
                                        + escapeArff(tender.getCategoryAI()) + "','"
                                        + escapeArff(cpvKnowledgeBase.toString()) + "','"
                                        + escapeArff(cpvNoParent) + "'", true);
                    }
                }
            }
        }
    }

    private void  constructTrainingDataFromCpvList(File arff, Map<String,List<String>> discardedTenderDisciplineMap) {
        Map<String,String> cpvTranslationMap = knowledgeService.getKnowledgeBase().getCpvTranslations();
        int id = 0;
        for (String cpv : cpvTranslationMap.keySet()) {
            String description = cpvTranslationMap.get(cpv);
            StringBuilder cpvKnowledgeBase = new StringBuilder();
            String cpvNoParent = upgradeCpv(cpv, cpv,description, "", knowledgeService.getKnowledgeBase(),cpvKnowledgeBase, discardedTenderDisciplineMap);
            appendArff(arff,
                    "'cpv" + id++ + "','"
                            + escapeArff(description) + "','"
                            + escapeArff("") + "','"
                            + escapeArff("cpv") + "','"
                            + escapeArff("") + "','"
                            + escapeArff(cpvKnowledgeBase.toString()) + "','"
                            + escapeArff(cpvNoParent) + "'", true);
        }
    }

    private void updateFeatureCount(Set<String> fSet, HashMap<String, Integer> featureCount) {
        if (fSet != null && fSet.size() > 0) {
            for (String feature : fSet) {
                if (feature.startsWith("REL_") || feature.startsWith("NE_")
                    //&& !feature.startsWith("REL_aanleggen_")
                    //&& !feature.startsWith("REL_heraanleggen_")
                    //&& !feature.startsWith("REL_renoveren_")
                    //&& !feature.startsWith("REL_beplanten_")
                    //&& !feature.startsWith("REL_afbreken_")
                    //&& !feature.startsWith("REL_bouwen_")
                    //&& !feature.startsWith("REL_graven_")
                    //&& !feature.startsWith("REL_plaatsen_")
                    //&& !feature.startsWith("REL_verbouwen_")
                    //&& !feature.startsWith("REL_renoveren_")
                    //&& !feature.startsWith("REL_uitbreiden_")
                    //&& !feature.startsWith("REL_restaureren_")
                    //&& !feature.startsWith("REL_isoleren_")
                    //&& !feature.startsWith("REL_inrichten_")
                    //&& !feature.startsWith("REL_herinrichten_")
                    //&& !feature.startsWith("REL_uitbaten_")
                    //&& !feature.startsWith("REL_financieren_")
                    //&& !feature.startsWith("REL_verzekeren_")
                    //&& !feature.startsWith("REL_vervangen_")
                    //&& !feature.startsWith("REL_aankopen_")
                    //&& !feature.startsWith("REL_onderhouden_")							
                ) {
                    String f = feature;
                    if (f.endsWith("_TITLE")) f = f.substring(0,f.length() - 6);
                    //System.out.println("f "+f);
                    Integer c = featureCount.get(f);
                    if (c == null)
                        featureCount.put(f, new Integer(1));
                    else
                        featureCount.put(f, new Integer(c+1));
                }
            }
        }
    }

    // TODO JAL: Remove @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public void createCpvUpgradeReport(String discipline) {
        try {
            File arff = new File(DATA_FOLDER, "acpvUpgrade_"+discipline.toUpperCase()+"_nl_be.txt");

            appendArff(arff, "# upgrade", false);

            List<String> cpvArray = knowledgeService.getKnowledgeBase().constructCpvDisciplineList(discipline);
            HashMap<String, Integer> featureCountNotInAllCPV = new HashMap<>();

            Map<String, CpvUpgrade> cpvList = new HashMap<>();
            StringBuilder allCpvBuilder = new StringBuilder();
            for (String c : cpvArray) {
                CpvUpgrade cpvUpgrade = new CpvUpgrade(c, knowledgeService.getKnowledgeBase().getCpvTranslations().get(c));
                cpvList.put(c,cpvUpgrade);
                allCpvBuilder.append(":"+c);
            }

            for (int i = IMPORT_BEGIN; i < IMPORT_END /*220000*/; i = i + 1) {
                Tender tender = searchService.findTenderByNoticeId(i + "");
                if (tender != null) {
                    tender.setLanguage("NL");
                    String titel = tender.getTitle1();
                    if (titel != null && titel.length() > 2 && tender.getLot() == null) {
                        boolean notInAllCpvs = true;
                        Set<String> fSet = knowledgeService.getKnowledgeBase().extractFeaturesAndConcepts(tender.getTitle() + " " + tender.getEssence());
                        for (String cpv : cpvList.keySet()) {
                            CpvUpgrade cpvUpgrade = cpvList.get(cpv);
                            if (tender.getCpvNoParent().indexOf(cpv) != -1) {
                                updateFeatureCount(fSet, cpvUpgrade.getFeatureCount());
                                notInAllCpvs = false;
                            }
                        }
                        if (notInAllCpvs) updateFeatureCount(fSet, featureCountNotInAllCPV);
                    }
                }
            }
            List<String> keys = new ArrayList(new TreeSet(cpvList.keySet()));
            Collections.sort(keys);
            for (String c : keys) {
                CpvUpgrade cpvUpgrade = cpvList.get(c);
                appendArff(arff, "# "+c+" "+cpvUpgrade.getDescription(), true);
                for (Map.Entry<String, Integer> entry : cpvUpgrade.constructMergedFeatures().entrySet())
                {
                    String feature = entry.getKey();
                    appendArff(arff,feature  + " "+ c +" "+ entry.getValue()+ " "+featureCountNotInAllCPV.get(feature), true) ;
                }
            }
        } catch (Exception e) {
            logger.error(e);
            e.printStackTrace();
        }
    }

    // TODO JAL: Remove @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public void createCpvUpgradeReportFromCpvTranslations() {
        try {
            File arff = new File(DATA_FOLDER, "acpvUpgrade_cpv_nl_be.txt");

            appendArff(arff, "# upgrade", false);

            List<String> cpvArray = knowledgeService.getKnowledgeBase().getModelCpvList();
            HashMap<String, Integer> featureCountNotInAllCPV = new HashMap<>();

            Map<String, CpvUpgrade> cpvList = new HashMap<>();
            for (String c : cpvArray) {
                CpvUpgrade cpvUpgrade = new CpvUpgrade(c, knowledgeService.getKnowledgeBase().getCpvTranslations().get(c));
                cpvList.put(c,cpvUpgrade);
            }

            Map<String,String> cpvTranslationMap = knowledgeService.getKnowledgeBase().getCpvTranslations();
            for (String cpvCode : cpvTranslationMap.keySet()) {
                String description = cpvTranslationMap.get(cpvCode);
                cpvCode = addCpvParentIfNotInExeptionList(cpvCode.trim(), knowledgeService.getKnowledgeBase());
                boolean notInAllCpvs = true;
                Set<String> fSet = knowledgeService.getKnowledgeBase().extractFeaturesAndConcepts(description);
                for (String cpv : cpvList.keySet()) {
                    CpvUpgrade cpvUpgrade = cpvList.get(cpv);
                    if (cpvCode.indexOf(cpv) != -1) {
                        updateFeatureCount(fSet, cpvUpgrade.getFeatureCount());
                        notInAllCpvs = false;
                    }
                }
                if (notInAllCpvs) updateFeatureCount(fSet, featureCountNotInAllCPV);
            }

            List<String> keys = new ArrayList(new TreeSet(cpvList.keySet()));
            Collections.sort(keys);
            for (String c : keys) {
                CpvUpgrade cpvUpgrade = cpvList.get(c);
                appendArff(arff, "# "+c+" "+cpvUpgrade.getDescription(), true);
                for (Map.Entry<String, Integer> entry : cpvUpgrade.constructMergedFeatures().entrySet())
                {
                    String feature = entry.getKey();
                    appendArff(arff,feature  + " "+ c +" "+ entry.getValue()+ " "+featureCountNotInAllCPV.get(feature), true) ;
                }
            }

        } catch (Exception e) {
            logger.error(e);
            e.printStackTrace();
        }
    }

    /**
     * Construct a list of features for a specific discipline
     *
     * @param discipline
     */
    // TODO JAL: Remove @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public void createDisciplineUpgradeReport(String discipline) {
        try {
            //File arff = new File(DATA_FOLDER, "adisciplineUpgrade_"+discipline.toUpperCase()+"_nl_be.txt");
            Map<String, CpvUpgradeElement> atomicKeywordMap = knowledgeService.getKnowledgeBase().getDisciplineAtomicKeywordMap().get(discipline);
            Map<String, CpvUpgradeElement> atomicStopwordMap = knowledgeService.getKnowledgeBase().getDisciplineStopwordMap().get(discipline);
            //appendArff(arff, "# upgrade", false);
            Map<String,CpvUpgradeElement> featureMap = new HashMap<String, CpvUpgradeElement>();
            List<String> cpvArray = knowledgeService.getKnowledgeBase().getCpvDisciplineMap().get(discipline);
            for (Tender tender : searchService.findTenderAll()) {
                if (tender != null) {
                    tender.setLanguage("NL");
                    String titel = tender.getTitle1();
                    String cpvNoParent = "";
                    boolean tenderInDiscipline = false;
                    for (String cpv : tender.getCpvNoParent().split(" ")) {
                        if (cpvArray.contains(cpv)) {
                            tenderInDiscipline = true;
                            cpvNoParent += " "+cpv;
                        }
                    }
                    if (titel != null && titel.length() > 2 && tender.getLot() == null && tenderInDiscipline) {
                        Set<String> fSet = knowledgeService.getKnowledgeBase().extractFeaturesAndConcepts(tender.getTitle() + " " + tender.getEssence());
                        for (String feature : fSet) {
                            if (!atomicKeywordMap.containsKey(feature) && !atomicStopwordMap.containsKey(feature)) {
                                CpvUpgradeElement cpvUpgradeElement = featureMap.get(feature);
                                if (cpvUpgradeElement == null) {
                                    cpvUpgradeElement = new CpvUpgradeElement(feature, cpvNoParent.trim().replaceAll(" ", "_"), 1);
                                    featureMap.put(feature, cpvUpgradeElement);
                                } else {
                                    cpvUpgradeElement.setOccurence(cpvUpgradeElement.getOccurence() + 1);
                                    cpvUpgradeElement.addCpvString(cpvNoParent.trim());
                                }
                            }
                        }
                    }
                }
            }
            Map<String,CpvUpgradeElement> mergedFeatureMap = CpvUpgrade.sortByComparator(CpvUpgrade.constructMergedCpvUpgradeElements(featureMap),false);
            MachineLearningFileUtils.writeList(CpvUpgrade.convertToList(mergedFeatureMap), DATA_FOLDER+ "/adisciplineUpgrade_"+discipline.toUpperCase()+"_nl_be.txt");
			/*
			List<String> keys = new ArrayList(new TreeSet(cpvList.keySet()));
			Collections.sort(keys);
			Map<String,Integer> featureOccurence = new HashMap<String,Integer>();
			for (String c : keys) {				
				CpvUpgrade cpvUpgrade = cpvList.get(c);
				//appendArff(arff, "# "+c+" "+cpvUpgrade.getDescription(), true);				
				for (Entry<String, Integer> entry : cpvUpgrade.constructMergedFeatures().entrySet())
        {
					String feature = entry.getKey();			
					Integer occurence = featureOccurence.get(feature);
					if (occurence == null) 
						occurence = entry.getValue();
					else
						occurence += entry.getValue();
					featureOccurence.put(feature, occurence);
					//appendArff(arff,feature  + " "+ c +" "+ entry.getValue()+ " "+featureCountNotInAllCPV.get(feature), true) ;           
        }				
			}		
			for (String feature : featureOccurence.keySet()) {
				if (!keywordMap.containsKey(feature) && !stopwordMap.containsKey(feature)) 
				  appendArff(arff,feature  +" "+featureOccurence.get(feature), true) ; 
			}
			
			featureOccurence = MachineLearningUtils.sortByComparator(featureOccurence, false);
			for (String feature : featureOccurence.keySet()) {
				if (!keywordMap.containsKey(feature) && !stopwordMap.containsKey(feature)) 
				  appendArff(arff,feature  +" "+featureOccurence.get(feature), true) ; 
			}
			*/

        } catch (Exception e) {
            logger.error(e);
            e.printStackTrace();
        }
    }

    /**
     * Update the CPV AI using the models deprecatedbuild by the Build.java class.
     * Update the discipline AI by using the CPV AI
     */
    // TODO JAL: Remove @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void updateCpvDisciplineAI(int tenderId, HashMap<String, List<String>> cpvDisciplineMap, Map<String,String> categoryCpvMap, KnowledgeBase knowledgeBase, FeatureList dataStructure, HashMap<String, ClassifierInterface> classifiers) {
        //trainingSelfService.clearCpvAI(tenderId, tenderId+1);
        trainingSelfService.updateCpvAI(tenderId, tenderId+1,knowledgeBase, dataStructure, classifiers, cpvDisciplineMap, categoryCpvMap);
        //trainingSelfService.updateDiscipline(tenderId,tenderId+1, cpvDisciplineMap, categoryCpvMap, true);
    }

    /**
     * Add CPV codes coming from the Rule based knowledge base.
     * If a CPV code is not in the exeptionList than take the parent, until a CPV in the execptionList is found that maps to a discipline.
     * Remove CPV codes that have no text equivalent.
     *
     * @param cpvNoParent
     * @param title
     * @param essence
     * @param knowledgeBase
     * @return
     */
    public static String upgradeCpv(String id, String cpvNoParent, String title, String essence, KnowledgeBase knowledgeBase, StringBuilder cpvKnowledegeBase, Map<String,List<String>> discardedTenderDisciplineMap) {
        String cpv = cpvNoParent;
        String text = title + " " + essence;
        Set<String> features = knowledgeBase.extractFeaturesAndConcepts(text);
        Map<String, String> cpvUpgradeMap = knowledgeBase.getCpvUpgradeMap();
        for (String cpvKeys : cpvUpgradeMap.keySet()) {
            String c = cpvUpgradeMap.get(cpvKeys);
            //if (cpv.indexOf(c) == -1) {
            boolean isValid = true;
            String[] cpvKeyArray = cpvKeys.split(",");
            for (String cpvKey : cpvKeyArray) {
                boolean keyValid = cpvKey.startsWith("!");
                boolean keyNegative = false;
                for (String feature : features) {
                    if (cpvKey.startsWith("!") && feature.equals(cpvKey.substring(1)) ) {
                        keyNegative = true;
                        break;
                    } else if (!cpvKey.startsWith("!") && feature.equals(cpvKey) ) {
                        keyValid = true;
                        break;
                    }
                }
                if (!keyValid || keyNegative) {
                    isValid = false;
                    break;
                }
            }
            if (isValid) {
                if (cpv.indexOf(c) == -1) {
                    cpv = cpv + " " + c;
                    if (c.equals("45332400")) cpv = cpv.replaceAll(" 45232460", "");
                }
                if (cpvKnowledegeBase.indexOf(c) == -1)
                    cpvKnowledegeBase.append(c + " ");
            }
            //}
        }
        cpv = addCpvParentIfNotInExeptionList(cpv.trim(), knowledgeBase);
        cpv = removeCpvIfNoDisciplineFeatureFound(id, cpv, cpvKnowledegeBase, features, knowledgeBase, text, discardedTenderDisciplineMap);
        return cpv;
    }

    /**
     * Remove cpv values without text feature
     * eg text "Aanleggen kruispunt" with CPV codes from government "SA cpv code",
     * remove these cpv codes which have nothing to do with the text
     *
     * @param cpvlist
     * @param cpvKnowledegeBase
     * @param features
     * @param knowledgeBase
     * @param text
     * @return cleaned cpv list
     */
    private static String removeCpvIfNoDisciplineFeatureFound(String id,String cpvlist, StringBuilder cpvKnowledegeBase, Set<String> features, KnowledgeBase knowledgeBase, String text, Map<String,List<String>> discardedTenderDisciplineMap) {
        StringBuilder cleanedCpvList = new StringBuilder();

        for (String cpv : cpvlist.split(" ")) {
            boolean notInList = true;
            for (String discipline : MLearningModels.DISCARD_CPV_DISCIPLINE) {
                List<String> saCpvs = knowledgeBase.getCpvDisciplineMap().get(discipline);
                List<String> newDiscardedTendersSA = null;
                List<String> discardedTendersSA = knowledgeBase.getDiscardedTenderDisciplineMap().get(discipline);
                if (discardedTenderDisciplineMap != null) newDiscardedTendersSA = discardedTenderDisciplineMap.get(discipline);
                Map<String, CpvUpgradeElement> atomicKeywordMap = knowledgeBase.getDisciplineAtomicKeywordMap().get(discipline);
                if (cpvKnowledegeBase.indexOf(cpv) == -1 && saCpvs.contains(cpv)) {
                    notInList = false;
                    if (disciplineContainsFeature(features,atomicKeywordMap)) {
                        if (cleanedCpvList.indexOf(cpv) == -1) cleanedCpvList.append(" " + cpv);
                    } else {
                        if (id != null && !isBDAPresentInList(id, discardedTendersSA)) {
                            StringBuilder featuresText = new StringBuilder("");
                            for (String feature : features) featuresText.append(" "+feature);
                            if (newDiscardedTendersSA != null) newDiscardedTendersSA.add(id+" "+cpv+" "+text+" / "+featuresText);
                            if (cleanedCpvList.indexOf(cpv) == -1) cleanedCpvList.append(" " + cpv);
                        }
                    }
                } else {
                    //if (cleanedCpvList.indexOf(cpv) == -1) cleanedCpvList.append(" " + cpv);
                }
            }
            if (notInList && cleanedCpvList.indexOf(cpv) == -1) cleanedCpvList.append(" " + cpv);
        }
        return cleanedCpvList.toString().trim();
    }

    private static boolean isBDAPresentInList(String bdalist, List<String> textList) {
        boolean present = false;
        for (String text : textList) {
            if (isBDAPresent(bdalist, text)) {
                present = true;
                break;
            }
        }
        return present;
    }

    private static boolean isBDAPresent(String bdalist, String text) {
        boolean present = false;
        String[] bdaArray = bdalist.trim().split(" ");
        for (String bda : bdaArray) {
            if (text.indexOf(bda) != -1) {
                present = true;
                break;
            }
        }
        return present;
    }

    private static boolean disciplineContainsFeature(Set<String> features, Map<String, CpvUpgradeElement> atomicKeywordMap) {
        boolean contains = false;
        for (String feature : features) {
            if (atomicKeywordMap.containsKey(feature)) {
                contains = true;
                break;
            }
        }
        return contains;
    }

    /**
     * Add parent CPV if there is no model for this CPV code, in other words it is not present in the Model CPV list.
     * @param cpv
     * @param knowledgeBase
     * @return
     */
    private static String addCpvParentIfNotInExeptionList(String cpv, KnowledgeBase knowledgeBase) {
        String[] cpvs = cpv.split(" ");
        List<String> usedCpvList = knowledgeBase.getModelCpvList();

        for (String c : cpvs) {
            while (c != null && !usedCpvList.contains(c)) {
                c = MLearningModels.constructParentCpv(c);
            }
            if (c != null && cpv.indexOf(c) == -1) cpv +=  " " + c;
        }

        return cpv;
    }

    private String escapeArff(String text) {
        if (text == null)
            return "";
        return text.replace('\\', ' ').replaceAll("'", "\\\\'").replaceAll("\n", " ").replaceAll("\r", " ");
    }

    private void appendArff(File arff, String line, boolean append) {
        try {
            PrintWriter out = new PrintWriter(new OutputStreamWriter(new FileOutputStream(arff, append), StandardCharsets.UTF_8), true);
            out.println(line);
            out.close();
        } catch (IOException e) {
        }
    }

    /**
     * Construct e list with all CPV codes within a Category
     */
    // TODO JAL: Remove @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public void contructCategoryCpvMapping() {
        Map<String,String> cpvTranslations = knowledgeService.getKnowledgeBase().getCpvTranslations();
        Map<String,String> categoryMap = knowledgeService.getKnowledgeBase().getCategoryTranslationMap();

        try (Writer writer = new BufferedWriter(new OutputStreamWriter(
                new FileOutputStream("/Users/jurrien/Documents/projects/ml/data/knowledgebase/aCategoryCpvMap.txt"), "utf-8"))) {

            for (String category : AkteUtilities.mapCategoryCodes) {
                Map<String,Integer> posFeatures = new HashMap<String,Integer>();
                Map<String,Integer> negFeatures = new HashMap<String,Integer>();
                Map<String,Integer> superPosFeatures = new HashMap<String,Integer>();
                Map<String,Double> modelFeatures = new HashMap<String,Double>();

                for (int i = IMPORT_BEGIN; i < IMPORT_END; i = i + 1) {
                    Tender tender = searchService.findTenderByNoticeId(i + "");
                    if (tender != null) {
                        tender.setLanguage("NL");
                        String titel = tender.getTitle1();
                        String tenderCategory = tender.getCategoryAI();
                        if (titel != null && titel.length() > 2 && tender.getLot() == null && tenderCategory != null) {
                            String[] cpvArray  = tender.getCpvNoParent().split(" ");
                            if (tenderCategory.indexOf(":"+ Util.CATEGORY_PREFIX+category.toUpperCase()+":") != -1) {
                                if (cpvArray != null && cpvArray.length > 0) BuildModels.updateFeatures(posFeatures, cpvArray, cpvTranslations);
                                if (cpvArray != null && cpvArray.length == 1 ) BuildModels.updateFeatures(superPosFeatures, cpvArray, cpvTranslations);
                            } else {
                                if (cpvArray != null && cpvArray.length > 0 ) BuildModels.updateFeatures(negFeatures, cpvArray, cpvTranslations);
                                //if (cpvArray != null && cpvArray.length == 1 && isOk) updateFeatures(superNegFeatures, cpvArray, cpvTranslations);
                            }
                        }
                    }
                }
                Map<String, Integer> featuresPercent = new HashMap<String, Integer>();
                for (Map.Entry<String, Integer> entry : posFeatures.entrySet())
                {
                    String key = entry.getKey();
                    Integer value = entry.getValue();
                    if (value != null) {
                        Integer negValue = negFeatures.get(key);
                        if (negValue == null) negValue = 0;
                        featuresPercent.put(key, (int)Math.round((value*1.0 / (value+negValue)*1.0)*100));
                    }
                }
                Map<String, Integer> sortedMapAsc = MachineLearningUtils.sortByComparator(featuresPercent, false);
                writer.write("# "+category + " " + categoryMap.get(category)+"\n");
                MachineLearningUtils.printMap(sortedMapAsc, posFeatures, superPosFeatures, negFeatures,modelFeatures,writer,category+"=");
            }
        } catch (UnsupportedEncodingException e) {
            logger.error(e);
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            logger.error(e);
            e.printStackTrace();
        } catch (IOException e) {
            logger.error(e);
            e.printStackTrace();
        }
    }    
}
