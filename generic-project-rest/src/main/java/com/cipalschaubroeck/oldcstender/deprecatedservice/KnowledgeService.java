package com.cipalschaubroeck.oldcstender.deprecatedservice;

import com.cipalschaubroeck.oldcstender.machinelearning.KnowledgeBase;
import lombok.Getter;
import lombok.Setter;

import javax.annotation.PostConstruct;
// TODO JAL: Remove import javax.ejb.Singleton;

/**
 * KnowledgeBase entry point.
 */
// TODO JAL: Remove @Singleton
@Getter
@Setter
public class KnowledgeService {
    KnowledgeBase knowledgeBase = null;

    @PostConstruct
    public void init() {
        knowledgeBase = new KnowledgeBase();
    }
}
