package com.cipalschaubroeck.oldcstender.deprecatedservice;

import com.cipalschaubroeck.cstender.domain.Tender;
import com.cipalschaubroeck.oldcstender.machinelearning.KnowledgeBase;
import com.cipalschaubroeck.oldcstender.machinelearning.MLearningModels;
import com.cipalschaubroeck.oldcstender.machinelearning.classify.ClassifierInterface;
import com.cipalschaubroeck.oldcstender.machinelearning.classify.FeatureList;
import org.apache.log4j.Logger;

import javax.persistence.EntityManager;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// TODO JAL: Remove import javax.ejb.Local;
// TODO JAL: Remove import javax.ejb.Stateless;
// TODO JAL: Remove import javax.ejb.TransactionAttribute;
// TODO JAL: Remove import javax.ejb.TransactionAttributeType;

// TODO JAL: Remove @Stateless
// TODO JAL: Remove @Local
public class TrainingSelfService {
    //@PersistenceContext(unitName = "tender")
    private EntityManager entityManager;

    private final Logger logger = Logger.getLogger(TrainingSelfService.class);

    //@Inject
    KnowledgeService knowledgeService;

    //@Inject
    SearchService searchService;

    // TODO JAL: Remove @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void updateCpvAI(int start, int end, KnowledgeBase filter, FeatureList dataStructure, HashMap<String, ClassifierInterface> classifiers, HashMap<String, List<String>> cpvDisciplineMap, Map<String, String> categoryCpvMap) {
        try {
            for (int i = start; i < end; i = i + 1) {
                logger.info("findTenderByNoticeId "+i);
                Tender tender = searchService.findTenderByNoticeId(i + "");
                if (tender != null) {
                    //System.out.println(tender.getNoticeId() + " " + tender.getBda());
                    tender.setLanguage("NL");
                    tender.setCpvAI("");
                    tender.setCpvNoParentAI("");
                    tender.setCpvPureAI("");
                    tender.setDisciplinesCpvAI("");

                    String titel = tender.getTitle1();
                    if (titel != null && titel.length() > 2) {
                        MLearningModels.updateTenderAI(tender, filter, dataStructure, classifiers, knowledgeService.getKnowledgeBase());
                        if (tender.getCpvNoParentAI() != null && tender.getCpvNoParentAI().trim().length() > tender.getCpvNoParent().trim().length()) {
                            logger.info("CPV ADDED "+tender.getNoticeId()+ " "+tender.getBda()+tender.getTitle() + " "+tender.getEssence() + " " + tender.getCpvNoParentAI()+ " oc: " + tender.getCpvNoParent());
                            if (tender.getLot() != null && tender.getLot().length()>2)
                                logger.info("LOT "+tender.getLot());
                        }
                        MLearningModels.updateDiscipline(tender, cpvDisciplineMap,categoryCpvMap, true);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
