package com.cipalschaubroeck.oldcstender.deprecatedservice;

import com.vaadin.server.VaadinSession;
import org.apache.log4j.Logger;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Locale;
import java.util.ResourceBundle;

// TODO JAL: Remove import javax.ejb.Singleton;

// TODO JAL: Remove @Singleton
public class I18Service {
    private final Logger logger = Logger.getLogger(I18Service.class);

    HashMap<Locale, ResourceBundle> resourceBundleMap;

    @PostConstruct
    public void init() {
        resourceBundleMap = new HashMap<>();
    }

    public String getMessage(String key) {
        return getMessage(key, VaadinSession.getCurrent().getLocale());
    }

    public String getMessage(String key, Locale locale) {
        //logger.trace("I18Service "+locale.toString());
        try {
            ResourceBundle resourceBundle = resourceBundleMap.get(locale);
            if (resourceBundle == null) {
                resourceBundle = ResourceBundle.getBundle("com/sygel/tender/resources/language",locale); // TODO JAL: ** REFACTOR ** Change path
                if (resourceBundle != null)
                    resourceBundleMap.put(locale, resourceBundle);
            }

            String translation = resourceBundle.getString(key);
            return translation;
        } catch (Exception e) {
            return key+" "+locale.toString();
        }
    }
}
