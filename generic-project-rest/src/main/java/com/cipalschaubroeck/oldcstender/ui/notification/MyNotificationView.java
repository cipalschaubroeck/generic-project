package com.cipalschaubroeck.oldcstender.ui.notification;

import com.cipalschaubroeck.cstender.domain.Filter;
import com.cipalschaubroeck.cstender.domain.FilterTender;
import com.cipalschaubroeck.cstender.domain.MyTenderFilter;
import com.cipalschaubroeck.cstender.domain.PushTenderFilter;
import com.cipalschaubroeck.cstender.domain.Tender;
import com.cipalschaubroeck.cstender.domain.TenderFilter;
import com.cipalschaubroeck.oldcstender.deprecatedservice.I18Service;
import com.cipalschaubroeck.oldcstender.deprecatedservice.UserService;
import com.cipalschaubroeck.oldcstender.ui.component.HelpWindow;
import com.cipalschaubroeck.oldcstender.ui.util.SessionUtil;
import com.vaadin.cdi.CDIView;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.FontAwesome;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.ui.AbstractSelect;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.ValoTheme;
import org.apache.log4j.Logger;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@CDIView(value = "MYTENDER")
@SuppressWarnings({ "serial", "unchecked" })
public final class MyNotificationView extends VerticalLayout implements View {

    private UserService userService;

    private I18Service i18Service;

    private NotificationResults notificationResultsLarge;

    boolean lastAdded = false;

    //@Inject
    HelpWindow helpWindow;

    private Button helpButton;

    protected Filter currentFilter = null;

    private ComboBox myTendersComboBox;

    private BeanItemContainer<Filter> filterContainer = null;

    private final Logger logger = Logger.getLogger(MyNotificationView.class);

    //@Inject
    public MyNotificationView(UserService userService, I18Service i18Service, NotificationResults notificationResultsLarge) {
        this.userService = userService;
        this.i18Service = i18Service;
        this.notificationResultsLarge = notificationResultsLarge;
        setSizeFull();
        addStyleName("transactions");
        // DashboardEventBus.register(this);
        setWidth("100%");

        addComponent(buildToolbar());
        addComponent(notificationResultsLarge);
        setExpandRatio(notificationResultsLarge, 2);
    }

    public void setFilter(Filter filter) {

    }

    protected void useFilter(String filtername) {
        TenderFilter filter = (TenderFilter) userService.findPushTenderFilter(SessionUtil.getUser().getId(), filtername);
        if (filter == null) {
            filter = (TenderFilter) userService.findMyTenderFilter(SessionUtil.getUser().getId(), MyTenderFilter.NAME);
            filter.setDescription(i18Service.getMessage("FAVOURITES"));
        }
        if (filter != null) {
            List<Tender> tenders = new ArrayList<Tender>();
            for(FilterTender filterTender : filter.getFilterTenders()) {
                tenders.add(filterTender.getTender());
            }
            notificationResultsLarge.setName(filter.getDisplayName());
            notificationResultsLarge.fillResults(tenders);
        }
    }

    @Override
    public void enter(final ViewChangeListener.ViewChangeEvent event) {
        String[] msgs = event.getParameters().split("/");
        if (msgs != null && msgs.length > 1) {
            String f = msgs[0];
            try {
                f = URLDecoder.decode(msgs[0],"UTF-8");
            } catch (UnsupportedEncodingException e) {
                logger.error(e);
            }
            logger.info("MyNotificationView::enter " + msgs[0] + " " + msgs[1]+ " decoded:"+f);
            useFilter(f);
            for (Filter filter : filterContainer.getItemIds()) {
                if (filter.getName().equals(f)) myTendersComboBox.setValue(filter);
            }
            try {
                notificationResultsLarge.selectNotification(new Long(msgs[1]));
            } catch (NumberFormatException nfe) {
            }
        }
    }

    @Override
    public void detach() {
        super.detach();
        // A new instance of TransactionsView is created every time it's
        // navigated to so we'll need to clean up references to it on detach.

        // DashboardEventBus.unregister(this);
    }

    private Component buildToolbar() {
        HorizontalLayout header = new HorizontalLayout();
        header.setMargin(new MarginInfo(true, true, false, true));
        header.setWidth("100%");

        Component filter = buildFilter();
        header.addComponent(filter);
        header.setExpandRatio(filter, 1.0f);

        helpButton = buildHelpButton();
        header.addComponent(helpButton);

        return header;
    }

    private Button buildHelpButton() {
        final Button helpQueryButton = new Button(i18Service.getMessage("HELP"));
        helpQueryButton.addStyleName(ValoTheme.BUTTON_QUIET);

        helpQueryButton.setIcon(FontAwesome.QUESTION_CIRCLE);
        helpQueryButton.setDescription(i18Service.getMessage("HELP_MYNOTIFICATION_QUERY_DESCRIPTION"));

        helpQueryButton.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(final Button.ClickEvent event) {
                helpWindow.setHelpText(i18Service.getMessage("HELP_MYNOTIFICATION_QUERY_TEXT"));
                if (!UI.getCurrent().getWindows().contains(helpWindow))
                    UI.getCurrent().addWindow(helpWindow);
                helpWindow.focus();
                helpWindow.addCloseListener(new Window.CloseListener() {
                    public void windowClose(Window.CloseEvent e) {
                    }
                });
            }
        });
        helpQueryButton.setEnabled(true);
        return helpQueryButton;
    }

    private Component buildFilter() {
        myTendersComboBox = new ComboBox();
        filterContainer = new BeanItemContainer<Filter>(Filter.class);
        Set<Filter> filters = userService.findUser(SessionUtil.getUser().getEmail(), true).getFilters();
        Filter myFilter = null;
        for (Filter filter : filters) {
            if (filter instanceof MyTenderFilter || (filter instanceof PushTenderFilter && filter.getSendEmail())) {
                if (filter instanceof MyTenderFilter) {
                    filter.setDescription(i18Service.getMessage("FAVOURITES"));
                    myFilter = filter;
                }
                filterContainer.addBean(filter);
            }
        }

        myTendersComboBox.setContainerDataSource(filterContainer);

        myTendersComboBox.setItemCaptionMode(AbstractSelect.ItemCaptionMode.PROPERTY);
        myTendersComboBox.setItemCaptionPropertyId("displayName");
        myTendersComboBox.setNullSelectionAllowed(false);
        myTendersComboBox.setWidth("100%");
        myTendersComboBox.addValueChangeListener(event -> {
            currentFilter = (Filter) event.getProperty().getValue();
            useFilter(currentFilter.getName());
        });
        myTendersComboBox.setTextInputAllowed(false);
        myTendersComboBox.setValue(myFilter);
        myTendersComboBox.setNewItemsAllowed(false);
        return myTendersComboBox;
    }
}
