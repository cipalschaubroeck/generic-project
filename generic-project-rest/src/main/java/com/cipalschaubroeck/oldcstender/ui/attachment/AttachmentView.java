package com.cipalschaubroeck.oldcstender.ui.attachment;

import com.cipalschaubroeck.cstender.domain.Attachment;
import com.cipalschaubroeck.oldcstender.deprecatedservice.I18Service;
import com.cipalschaubroeck.oldcstender.deprecatedservice.SearchService;
import com.cipalschaubroeck.oldcstender.ui.component.HelpWindow;
import com.vaadin.cdi.CDIView;
import com.vaadin.event.FieldEvents;
import com.vaadin.event.ShortcutAction;
import com.vaadin.event.ShortcutListener;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.FontAwesome;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.ValoTheme;

import java.util.Collection;

@CDIView(value = "ATTACHMENT")
@SuppressWarnings({ "serial", "unchecked" })
public final class AttachmentView extends VerticalLayout implements View {
    //@Inject
    private SearchService searchService;

    private I18Service i18Service;

    //@Inject
    private AttachmentResults attachmentResults;

    //@Inject
    HelpWindow helpWindow;

    private Button helpButton;

    private boolean first = true;

    //@Inject
    public AttachmentView(I18Service i18Service) {
        this.i18Service = i18Service;
        setSizeFull();
        addStyleName("transactions");
        // DashboardEventBus.register(this);
        setWidth("100%");
        addComponent(buildToolbar());
    }

    @Override
    public void enter(final ViewChangeListener.ViewChangeEvent event) {
        Collection<Attachment> attachments = searchService.findAttachments("");
        if (first) {
            first = false;
            addComponent(attachmentResults);
            setExpandRatio(attachmentResults, 2);
        }
        attachmentResults.fillResults(attachments);
    }

    @Override
    public void detach() {
        super.detach();
        // A new instance of TransactionsView is created every time it's
        // navigated to so we'll need to clean up references to it on detach.
        // DashboardEventBus.unregister(this);
    }

    private Component buildToolbar() {
        HorizontalLayout header = new HorizontalLayout();
        header.setMargin(new MarginInfo(true, true, false, true));
        header.setWidth("100%");
        // Responsive.makeResponsive(header);

        Component filter = buildFilter();
        header.addComponent(filter);

        header.setExpandRatio(filter, 1.0f);

        helpButton = buildHelpButton();
        header.addComponent(helpButton);


        return header;
    }

    private Button buildHelpButton() {
        final Button helpQueryButton = new Button(i18Service.getMessage("HELP"));
        helpQueryButton.addStyleName(ValoTheme.BUTTON_QUIET);

        helpQueryButton.setIcon(FontAwesome.QUESTION_CIRCLE);
        helpQueryButton.setDescription(i18Service.getMessage("HELP_ATTACHMENT_QUERY_DESCRIPTION"));

        helpQueryButton.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(final Button.ClickEvent event) {
                helpWindow.setHelpText(i18Service.getMessage("HELP_ATTACHMENT_QUERY_TEXT"));
                if (!UI.getCurrent().getWindows().contains(helpWindow))
                    UI.getCurrent().addWindow(helpWindow);
                helpWindow.focus();
                helpWindow.addCloseListener(new Window.CloseListener() {
                    public void windowClose(Window.CloseEvent e) {
                    }
                });
            }
        });
        helpQueryButton.setEnabled(true);
        return helpQueryButton;
    }

    private Component buildFilter() {
        final TextField filter = new TextField();
        filter.setWidth("100%");
        filter.addTextChangeListener(new FieldEvents.TextChangeListener() {
            @Override
            public void textChange(final FieldEvents.TextChangeEvent event) {
                Collection<Attachment> governmentItems = searchService.findAttachments(event.getText());
                attachmentResults.fillResults(governmentItems);
            }
        });

        filter.setInputPrompt(i18Service.getMessage("FIND_ATTACHMENT"));
        filter.setIcon(FontAwesome.SEARCH);
        filter.addStyleName(ValoTheme.TEXTFIELD_INLINE_ICON);
        filter.addShortcutListener(new ShortcutListener("Clear", ShortcutAction.KeyCode.ESCAPE, null) {
            @Override
            public void handleAction(final Object sender, final Object target) {
                filter.setValue("");
            }
        });
        return filter;
    }
}
