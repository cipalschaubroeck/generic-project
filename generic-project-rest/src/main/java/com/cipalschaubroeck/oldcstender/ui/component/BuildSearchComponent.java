package com.cipalschaubroeck.oldcstender.ui.component;

import com.cipalschaubroeck.cstender.domain.Filter;
import com.cipalschaubroeck.cstender.domain.PushTenderFilter;
import com.cipalschaubroeck.oldcstender.deprecatedservice.I18Service;
import com.cipalschaubroeck.oldcstender.deprecatedservice.KnowledgeService;
import com.cipalschaubroeck.oldcstender.deprecatedservice.UserService;
import com.cipalschaubroeck.oldcstender.machinelearning.KnowledgeCity;
import com.cipalschaubroeck.oldcstender.ui.util.SessionUtil;
import com.vaadin.data.Property;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.event.FieldEvents;
import com.vaadin.event.SelectionEvent;
import com.vaadin.server.FontAwesome;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.ui.AbstractSelect;
import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextField;
import com.vaadin.ui.TreeTable;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.renderers.HtmlRenderer;
import com.vaadin.ui.themes.ValoTheme;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class BuildSearchComponent extends VerticalLayout implements SelectionEvent.SelectionListener {
    Panel contentPanel = null;
    Grid disciplineTable;
    Grid categoryTable;
    Grid nutsTable;
    Grid classTable;
    Grid cpvTable;
    ComboBox filterComboBox;
    TextField nameTextField;
    CheckBox emailCheckBox;
    TreeTable cpvTreeTable;
    List<String> cpvs;
    List<String> nuts;
    List<KnowledgeCity> cities;
    Map<String,String> cpvTranslationMap;
    Map<String,String> nutsTranslationMap;
    Filter newTextFilter = null;
    String nutsSearchString = "";
    BeanItemContainer<Filter> filterContainer = null;

    IndexedContainer cpvContainer;
    IndexedContainer disciplineContainer;
    IndexedContainer categoryContainer;
    IndexedContainer nutsContainer;
    IndexedContainer classContainer;
    Filter searchFilter = new PushTenderFilter("","");
    String COLUMN_DISCIPLINE;
    String COLUMN_CPV;
    String COLUMN_CATEGORY;
    String COLUMN_NUTS;
    String COLUMN_CLASS;

    I18Service i18Service;
    KnowledgeService knowledgeService;
    UserService userService;

    //@Inject
    HelpWindow helpWindow;

    private Button helpButton;

    //@Inject
    public BuildSearchComponent(KnowledgeService knowledgeService, I18Service i18Service, UserService userService) {
        this.knowledgeService = knowledgeService;
        this.i18Service = i18Service;
        this.userService = userService;
        setMargin(true);
        setWidth("100%");
        setHeight("100%");
        contentPanel = new Panel();
        contentPanel.setWidth("100%");
        contentPanel.setHeight("100%");
    }

    @Override
    public void attach() {
        super.attach();
        cpvTranslationMap = knowledgeService.getKnowledgeBase().getCpvTranslations();
        nutsTranslationMap = knowledgeService.getKnowledgeBase().getNutsTranslationMap();
        cities = knowledgeService.getKnowledgeBase().getCityList();
        cpvs = new ArrayList<String>(cpvTranslationMap.keySet());
        Collections.sort(cpvs);
        nuts = new ArrayList<String>(nutsTranslationMap.keySet());
        Collections.sort(nuts);

        VerticalLayout left = new VerticalLayout();
        left.setMargin(new MarginInfo(true, true, true, true));
        contentPanel.setContent(left);
        addComponent(contentPanel);

        HorizontalLayout filterLayout = new HorizontalLayout();
        filterLayout.setMargin(new MarginInfo(false, false, true, false));
        filterLayout.setWidth("100%");
        left.addComponent(filterLayout);
        buildFilter(filterLayout);

        HorizontalLayout nameLayout = new HorizontalLayout();
        nameLayout.setMargin(new MarginInfo(false, false, true, false));
        nameLayout.setWidth("100%");
        left.addComponent(nameLayout);
        buildNameLayout(nameLayout);

        HorizontalLayout disciplineLayout = new HorizontalLayout();
        disciplineLayout.setMargin(new MarginInfo(false, false, true, false));
        disciplineLayout.setWidth("100%");
        left.addComponent(disciplineLayout);

        buildDisciplineList(disciplineLayout);
        buildCategoryList(disciplineLayout);

        HorizontalLayout locationLayout = new HorizontalLayout();
        locationLayout.setMargin(new MarginInfo(false, false, true, false));
        locationLayout.setWidth("100%");
        left.addComponent(locationLayout);

        buildNutsList(locationLayout);
        buildClassList(locationLayout);

        HorizontalLayout cpvLayout = new HorizontalLayout();
        cpvLayout.setMargin(new MarginInfo(false, false, true, false));
        cpvLayout.setWidth("100%");
        left.addComponent(cpvLayout);

        buildCpvSearch(cpvLayout);
        buildCpvTree(cpvLayout);
    }

    private void updateFilters() {
        filterContainer.removeAllItems();
        filterContainer.addBean(searchFilter);
        Filter newfilter = new PushTenderFilter("", "");
        newfilter.setDescription(i18Service.getMessage("NEW_FILTER"));
        if (!searchFilter.equals(newfilter)) {
            filterContainer.addBean(newfilter);
        }
        Set<Filter> filters = userService.findUser(SessionUtil.getUser().getEmail(), true).getFilters();

        for (Filter filter : filters) {
            if (!searchFilter.equals(filter) && filter instanceof PushTenderFilter)
                filterContainer.addBean(filter);
        }
    }

    private void buildFilter(HorizontalLayout filterLayout) {
        filterComboBox = new ComboBox();
        filterContainer = new BeanItemContainer<Filter>(Filter.class);
        filterComboBox.setContainerDataSource(filterContainer);
        filterComboBox.setItemCaptionMode(AbstractSelect.ItemCaptionMode.PROPERTY);
        filterComboBox.setItemCaptionPropertyId("shortDisplayName");
        filterComboBox.setTextInputAllowed(false);
        filterComboBox.setNullSelectionAllowed(false);
        filterComboBox.setNewItemsAllowed(false);
        filterComboBox.setWidth("100%");
        updateFilters();

        filterComboBox.addValueChangeListener(event -> {
            Filter f = (Filter) event.getProperty().getValue();
            if (f != null && !f.equals(searchFilter)) {
                searchFilter.init(f);
                updateDisciplineDatasource();
                updateNutsContainer();
                updateClassDatasource();
                updateCategoryDatasource();
                nameTextField.setValue(searchFilter.getName());
                emailCheckBox.setValue(searchFilter.getSendEmail());
            }
            updateFilters();
        });
        filterComboBox.setValue(searchFilter);
        filterLayout.addComponent(filterComboBox);
        filterLayout.setExpandRatio(filterComboBox, 1.0f);


        helpButton = buildHelpButton();
        filterLayout.addComponent(helpButton);

    }

    private Button buildHelpButton() {
        final Button helpQueryButton = new Button(i18Service.getMessage("HELP"));
        helpQueryButton.addStyleName(ValoTheme.BUTTON_QUIET);

        helpQueryButton.setIcon(FontAwesome.QUESTION_CIRCLE);
        helpQueryButton.setDescription(i18Service.getMessage("HELP_NOTIFICATION_QUERY_DESCRIPTION"));

        helpQueryButton.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(final Button.ClickEvent event) {
                helpWindow.setHelpText(i18Service.getMessage("HELP_NOTIFICATION_QUERY_TEXT"));
                if (!UI.getCurrent().getWindows().contains(helpWindow))
                    UI.getCurrent().addWindow(helpWindow);
                helpWindow.focus();
                helpWindow.addCloseListener(new Window.CloseListener() {
                    public void windowClose(Window.CloseEvent e) {
                    }
                });
            }
        });
        helpQueryButton.setEnabled(true);
        return helpQueryButton;
    }


    private void buildNameLayout(HorizontalLayout nameLayout) {
        nameTextField = new TextField(i18Service.getMessage("FILTER_NAME"));
        nameLayout.addComponent(nameTextField);
        nameTextField.setValue(searchFilter.getName());
        nameTextField.addTextChangeListener(event -> searchFilter.setName(event.getText()));

        emailCheckBox = new CheckBox(i18Service.getMessage("FILTER_EMAIL"));
        nameLayout.addComponent(emailCheckBox);
        emailCheckBox.setValue(searchFilter.getSendEmail());
        emailCheckBox.addValueChangeListener(event -> searchFilter.setSendEmail((Boolean)event.getProperty().getValue()));
    }

    private void buildNutsList(HorizontalLayout locationLayout) {
        nutsTable = new Grid();
        nutsTable.setWidth("90%");
        nutsTable.setHeight("500px");
        COLUMN_NUTS = i18Service.getMessage("NUTS");
        nutsTable.setSelectionMode(Grid.SelectionMode.MULTI);
        nutsTable.removeHeaderRow(0);
        nutsTable.addColumn(COLUMN_NUTS, String.class).setRenderer(new HtmlRenderer());
        Grid.HeaderRow filteringHeader = nutsTable.appendHeaderRow();

        final TextField filter = new TextField();
        filter.addTextChangeListener(new FieldEvents.TextChangeListener() {
            @Override
            public void textChange(final FieldEvents.TextChangeEvent event) {
                nutsSearchString = event.getText();
                updateNutsContainer();
            }
        });
        filter.setInputPrompt(i18Service.getMessage("FIND_LOCATION"));
        filter.setIcon(FontAwesome.SEARCH);
        filter.addStyleName(ValoTheme.TEXTFIELD_SMALL);
        filter.addStyleName(ValoTheme.TEXTFIELD_INLINE_ICON);

        filteringHeader.getCell(COLUMN_NUTS).setComponent(filter);
        filteringHeader.getCell(COLUMN_NUTS).setStyleName("filter-header");
        nutsContainer = new IndexedContainer();
        nutsContainer.addContainerProperty(COLUMN_NUTS, String.class, "");
        nutsTable.setContainerDataSource(nutsContainer);
        nutsTable.addSelectionListener(this);
        initNutsDatasource();
        locationLayout.addComponent(nutsTable);
    }

    private void updateNutsContainer()
    {
        nutsTable.removeSelectionListener(this);
        if (nutsSearchString == null || nutsSearchString.length() == 0) {
            for (Object id : nutsTable.getSelectedRows()) nutsTable.deselect(id); // WORKAROUND FOR BUG https://dev.vaadin.com/ticket/16195
            nutsContainer.removeAllItems();
            initNutsDatasource();
        } else if (nutsSearchString != null && nutsSearchString.length() > 2) {
            nutsSearchString = nutsSearchString.toLowerCase();
            for (Object id : nutsTable.getSelectedRows()) nutsTable.deselect(id); // WORKAROUND FOR BUG https://dev.vaadin.com/ticket/16195
            nutsContainer.removeAllItems();
            initNutsDatasource();
            for (KnowledgeCity city : cities) {
                String nutDescriptionString = city.toZipCodeNameString().toLowerCase();
                if (nutDescriptionString.contains(nutsSearchString)) {
                    Object id = nutsContainer.addItemAt(0);
                    nutsContainer.getContainerProperty(id, COLUMN_NUTS).setValue(city.toZipCodeNameString());
                    if (searchFilter.containsSearchItem(city.getZipCode()))
                        nutsTable.select(id);
                }
            }
        }
        nutsTable.addSelectionListener(this);
    }

    private void initNutsDatasource()
    {
        for (String nut : nuts) {
            Object id = nutsContainer.addItem();
            nutsContainer.getContainerProperty(id, COLUMN_NUTS).setValue(nut + " " +nutsTranslationMap.get(nut));
            if (searchFilter.containsSearchItem(nut))
                nutsTable.select(id);
        }

    }

    private void buildCpvSearch(HorizontalLayout cpvLayout) {
        cpvTable = new Grid();
        cpvTable.setWidth("90%");
        cpvTable.setHeight("500px");
        COLUMN_CPV = i18Service.getMessage("CPV");
        cpvTable.setSelectionMode(Grid.SelectionMode.MULTI);
        cpvTable.removeHeaderRow(0);
        cpvTable.addColumn(COLUMN_CPV, String.class).setRenderer(new HtmlRenderer());
        Grid.HeaderRow filteringHeader = cpvTable.appendHeaderRow();

        final TextField filter = new TextField();
        filter.addTextChangeListener(new FieldEvents.TextChangeListener() {
            @Override
            public void textChange(final FieldEvents.TextChangeEvent event) {
                updateCpvContainer(event.getText());
            }
        });
        filter.setInputPrompt(i18Service.getMessage("FIND_CPV"));
        filter.setIcon(FontAwesome.SEARCH);
        filter.addStyleName(ValoTheme.TEXTFIELD_SMALL);
        filter.addStyleName(ValoTheme.TEXTFIELD_INLINE_ICON);

        filteringHeader.getCell(COLUMN_CPV).setComponent(filter);
        filteringHeader.getCell(COLUMN_CPV).setStyleName("filter-header");

        cpvContainer = createDummyDatasource();
        cpvTable.setContainerDataSource(cpvContainer);

        cpvLayout.addComponent(cpvTable);
    }

    private void updateCpvContainer(String searchString)
    {
        cpvTable.removeSelectionListener(this);
        if (searchString == null || searchString.length() == 0) {
            for (Object id : cpvTable.getSelectedRows()) cpvTable.deselect(id); // WORKAROUND FOR BUG https://dev.vaadin.com/ticket/16195
            cpvContainer.removeAllItems();
        } else if (searchString != null && searchString.length() > 2) {
            searchString = searchString.toLowerCase();
            for (Object id : cpvTable.getSelectedRows()) cpvTable.deselect(id); // WORKAROUND FOR BUG https://dev.vaadin.com/ticket/16195
            cpvContainer.removeAllItems();
            for (String cpv : cpvs) {
                String cpvString = cpvTranslationMap.get(cpv).toLowerCase();
                String cpvDescriptionString = cpv + " "+cpvString;
                if (cpvDescriptionString.contains(searchString)) {
                    Object id = cpvContainer.addItem();
                    cpvContainer.getContainerProperty(id, COLUMN_CPV).setValue(cpv + " "+cpvString);
                    if (searchFilter.containsSearchItem(cpv))
                        cpvTable.select(id);
                }
            }
        }
        cpvTable.addSelectionListener(this);
    }

    private IndexedContainer createDummyDatasource()
    {
        IndexedContainer ic = new IndexedContainer();
        ic.addContainerProperty(COLUMN_CPV, String.class, "");
        Object id = ic.addItem();
        ic.getContainerProperty(id, COLUMN_CPV).setValue(i18Service.getMessage("FIND_CPV"));
        return ic;
    }

    private void buildCpvTree(HorizontalLayout cpvLayout) {
        cpvTreeTable = new TreeTable();
        cpvTreeTable.addContainerProperty(COLUMN_CPV, Label.class, "");
        cpvTreeTable.setWidth("90%");
        cpvTreeTable.setHeight("500px");
        addTreeTableElements(cpvTreeTable,cpvTranslationMap);
        cpvLayout.addComponent(cpvTreeTable);
    }

    private void buildClassList(HorizontalLayout locationLayout) {
        classTable = new Grid();

        classTable.setHeight("500px");
        COLUMN_CLASS = i18Service.getMessage("CLASS");
        classTable.setSelectionMode(Grid.SelectionMode.MULTI);
        classTable.addColumn(COLUMN_CLASS, String.class).setRenderer(new HtmlRenderer());
        classTable.setWidth("90%");

        classContainer = new IndexedContainer();
        classContainer.addContainerProperty(COLUMN_CLASS, String.class, "");
        classTable.setContainerDataSource(classContainer);
        updateClassDatasource();

        locationLayout.addComponent(classTable);
    }

    private void updateClassDatasource()
    {
        classTable.removeSelectionListener(this);
        for (Object id : classTable.getSelectedRows()) classTable.deselect(id); // WORKAROUND FOR BUG https://dev.vaadin.com/ticket/16195
        Map<String,String> classTranslationMap = knowledgeService.getKnowledgeBase().getClassTranslationMap();
        classContainer.removeAllItems();
        List<String> classes = new ArrayList<String>(classTranslationMap.keySet());
        Collections.sort(classes);
        for (String cl : classes) {
            Object id = classContainer.addItem();
            classContainer.getContainerProperty(id, COLUMN_CLASS).setValue(cl + " " +classTranslationMap.get(cl));
            if (searchFilter.containsSearchItem(cl))
                classTable.select(id);
        }
        classTable.addSelectionListener(this);
    }

    private void buildCategoryList(HorizontalLayout disciplineLayout) {
        categoryTable = new Grid();
        categoryTable.setHeight("500px");
        COLUMN_CATEGORY = i18Service.getMessage("CATEGORY");
        categoryTable.setSelectionMode(Grid.SelectionMode.MULTI);
        categoryTable.addColumn(COLUMN_CATEGORY, String.class).setRenderer(new HtmlRenderer());
        categoryTable.setWidth("90%");

        categoryContainer = new IndexedContainer();
        categoryContainer.addContainerProperty(COLUMN_CATEGORY, String.class, "");
        categoryTable.setContainerDataSource(categoryContainer);
        updateCategoryDatasource();
        disciplineLayout.addComponent(categoryTable);
    }

    private void updateCategoryDatasource()
    {
        categoryTable.removeSelectionListener(this);
        for (Object id : categoryTable.getSelectedRows()) categoryTable.deselect(id); // WORKAROUND FOR BUG https://dev.vaadin.com/ticket/16195
        Map<String,String> categoryTranslationMap = knowledgeService.getKnowledgeBase().getCategoryTranslationMap();
        categoryContainer.removeAllItems();
        List<String> categories = new ArrayList<String>(categoryTranslationMap.keySet());
        Collections.sort(categories);
        for (String category : categories) {
            Object id = categoryContainer.addItem();
            categoryContainer.getContainerProperty(id, COLUMN_CATEGORY).setValue(category + " " +categoryTranslationMap.get(category));
            if (searchFilter.containsSearchItem(category))
                categoryTable.select(id);
        }
        categoryTable.addSelectionListener(this);
    }

    private void buildDisciplineList(HorizontalLayout disciplineLayout) {
        disciplineTable = new Grid();
        disciplineTable.setHeight("500px");
        COLUMN_DISCIPLINE = i18Service.getMessage("DISCIPLINE");
        disciplineTable.setSelectionMode(Grid.SelectionMode.MULTI);
        disciplineTable.addColumn(COLUMN_DISCIPLINE, String.class).setRenderer(new HtmlRenderer());
        disciplineTable.setWidth("90%");
        disciplineContainer = new IndexedContainer();
        disciplineContainer.addContainerProperty(COLUMN_DISCIPLINE, String.class, "");
        disciplineTable.setContainerDataSource(disciplineContainer);
        updateDisciplineDatasource();

        disciplineLayout.addComponent(disciplineTable);
    }

    protected void updateDisciplineDatasource()
    {
        disciplineTable.removeSelectionListener(this);
        for (Object id : disciplineTable.getSelectedRows()) disciplineTable.deselect(id); // WORKAROUND FOR BUG https://dev.vaadin.com/ticket/16195
        disciplineContainer.removeAllItems();
        Map<String,String> disciplineTranslationMap = knowledgeService.getKnowledgeBase().getDisciplineTranslationMap();
        List<String> disciplines = new ArrayList<String>(disciplineTranslationMap.keySet());
        Collections.sort(disciplines);
        for (String discipline : disciplines) {
            Object id = disciplineContainer.addItem();
            disciplineContainer.getContainerProperty(id, COLUMN_DISCIPLINE).setValue(discipline + " " +disciplineTranslationMap.get(discipline));
            if (searchFilter.containsSearchItem(discipline))
                disciplineTable.select(id);
        }
        disciplineTable.addSelectionListener(this);
    }

    private void addTreeTableElements(TreeTable treeTable, Map<String,String> cpvTranslationMap) {
        List<String> cpvs = new ArrayList<String>(cpvTranslationMap.keySet());
        Collections.sort(cpvs);
        int i = 0;
        int currentParent = 0;
        int level0 = 0;
        int level1 = 0;
        int level2 = 0;
        int level3 = 0;
        int level4 = 0;
        int level5 = 0;
        for (String cpv : cpvs) {
            if (cpv.endsWith("000000")) {//level 0
                currentParent = i;
                level0 = i;
            } else if (cpv.endsWith("00000")) {//level 1
                currentParent = i;
                level1 = i;
            } else if (cpv.endsWith("0000")) {//level 2
                currentParent = i;
                level2 = i;
            } else if (cpv.endsWith("000")) {//level 3
                currentParent = i;
                level3 = i;
            } else if (cpv.endsWith("00")) {//level 4
                currentParent = i;
                level4 = i;
            } else if (cpv.endsWith("0")) {//level 5
                currentParent = i;
                level5 = i;
            }
            treeTable.addItem(new Object[]{ new Label(cpv + " "+cpvTranslationMap.get(cpv))}, i);
            if (i == level1) treeTable.setParent(i, level0);
            else if (i == level2) treeTable.setParent(i, level1);
            else if (i == level3) treeTable.setParent(i, level2);
            else if (i == level4) treeTable.setParent(i, level3);
            else if (i == level5) treeTable.setParent(i, level4);
            else if (i != currentParent) treeTable.setParent(i, currentParent);
            i++;
        }
    }

    @Override
    public void select(SelectionEvent event) {
        if (event.getSource().equals(disciplineTable)) {
            updateSearchItem(event,COLUMN_DISCIPLINE, disciplineContainer);
        } else if (event.getSource().equals(cpvTable)) {
            updateSearchItem(event,COLUMN_CPV, cpvContainer);
        } else if (event.getSource().equals(categoryTable)) {
            updateSearchItem(event,COLUMN_CATEGORY, categoryContainer);
        } else if (event.getSource().equals(classTable)) {
            updateSearchItem(event,COLUMN_CLASS, classContainer);
        } else if (event.getSource().equals(nutsTable)) {
            updateSearchItem(event,COLUMN_NUTS, nutsContainer);
        }
    }

    private void updateSearchItem(SelectionEvent event, String COLUMN, IndexedContainer container) {
        Set<Object> selectedSet = event.getSelected();
        for(Object selected : selectedSet) {
            Property o =  container.getContainerProperty(selected, COLUMN);
            if (o != null) {
                String selectedDiscipline = (String) o.getValue();
                int endPos = selectedDiscipline.indexOf(" ");
                selectedDiscipline = selectedDiscipline.substring(0,endPos);
                if (!searchFilter.containsSearchItem(selectedDiscipline)) searchFilter.addSearchItem(selectedDiscipline);;
            }
        }

        Set<Object> removedSet = event.getRemoved();
        for(Object removed : removedSet) {
            String removedDiscipline = (String) container.getContainerProperty(removed, COLUMN).getValue();
            int endPos = removedDiscipline.indexOf(" ");
            removedDiscipline = removedDiscipline.substring(0,endPos);
            searchFilter.removeSearchItem(removedDiscipline);
        }
    }

    public Filter getSearchFilter() {
        return searchFilter;
    }

    public void setSearchFilter(Filter filter) {
        this.searchFilter = filter;

    }
}
