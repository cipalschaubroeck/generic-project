package com.cipalschaubroeck.oldcstender.ui;

import com.vaadin.cdi.CDIUI;
import com.vaadin.server.VaadinRequest;
import com.vaadin.ui.Button;
import com.vaadin.ui.Label;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

import javax.inject.Named;

@CDIUI("open")
@Named("OpenUI")
public class OpenUI extends UI {

    @Override
    protected void init(VaadinRequest request) {
        final VerticalLayout layout = new VerticalLayout();
        layout.setMargin(true);
        setContent(layout);

        layout.addComponent(new Label("unsecure UI"));

        Button b = new Button("Go to secure part");
        b.addClickListener(new Button.ClickListener() {

            @Override
            public void buttonClick(Button.ClickEvent event) {
                String currentURI = getPage().getLocation().toString();
                getPage().setLocation(currentURI + "secure");
            }
        });
        layout.addComponent(b);
    }
}
