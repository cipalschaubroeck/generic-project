package com.cipalschaubroeck.oldcstender.ui;

import com.vaadin.annotations.Theme;
import com.vaadin.cdi.CDIUI;
import com.vaadin.server.VaadinRequest;
import com.vaadin.ui.Label;
import com.vaadin.ui.UI;

import javax.inject.Named;

@Theme("tender")
@SuppressWarnings("serial")
@CDIUI("login")
@Named("LoginUI")
public class LoginUI extends UI {
    @Override
    protected void init(VaadinRequest request) {
        Label field = new Label("hello");
        setContent(field);
    }

}
