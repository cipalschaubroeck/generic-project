package com.cipalschaubroeck.oldcstender.ui.deprecatedbuild;

import com.cipalschaubroeck.cstender.domain.Tender;
import com.cipalschaubroeck.oldcstender.deprecatedservice.OldImportService;
import com.cipalschaubroeck.oldcstender.deprecatedservice.SearchService;
import com.cipalschaubroeck.oldcstender.ui.notification.NotificationDetailWindow;
import com.vaadin.cdi.CDIView;
import com.vaadin.event.FieldEvents;
import com.vaadin.event.ShortcutAction;
import com.vaadin.event.ShortcutListener;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.FontAwesome;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.ui.Component;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

import java.util.List;

@CDIView(value = "BUILD_TENDER")
@SuppressWarnings({ "serial", "unchecked" })
public final class BuildTenderView extends VerticalLayout implements View {
    //@Inject
    private OldImportService importService;

    //@Inject
    private SearchService searchService;

    private boolean first = true;

    Panel contentPanel = null;
    VerticalLayout main = null;

    //@Inject
    private TitleResults titleResults = null;
    //@Inject
    private CpvResults cpvResults = null;
    //@Inject
    private CategoryResults categoryResults = null;
    //@Inject
    NotificationDetailWindow notificationDetailWindow;

    public BuildTenderView() {
        setSizeFull();
        addStyleName("transactions");
        // DashboardEventBus.register(this);
        setWidth("100%");
        setMargin(false);

        contentPanel = new Panel();
        contentPanel.setWidth("100%");
        contentPanel.setHeight("100%");
        main = new VerticalLayout();
        main.setMargin(false);
        //main.setMargin(new MarginInfo(true, true, true, true));
        main.addComponent(buildToolbar());
        contentPanel.setContent(main);
        addComponent(contentPanel);

        setExpandRatio(contentPanel, 1);
    }

    @Override
    public void enter(final ViewChangeListener.ViewChangeEvent event) {
        List<Tender> tenders = importService.findTenders();
        if (first) {
            first = false;
            main.addComponent(titleResults);
            main.addComponent(cpvResults);
            main.addComponent(categoryResults);
        }
        titleResults.fillResults(tenders, notificationDetailWindow);
        cpvResults.fillResults(searchService.constructCpvList(tenders));
        categoryResults.fillResults(searchService.constructCategoryList(tenders));
    }

    @Override
    public void detach() {
        super.detach();
        // A new instance of TransactionsView is created every time it's
        // navigated to so we'll need to clean up references to it on detach.

        // DashboardEventBus.unregister(this);
    }

    private Component buildToolbar() {
        HorizontalLayout header = new HorizontalLayout();
        header.setMargin(new MarginInfo(true, true, true, true));
        header.setWidth("100%");
        // Responsive.makeResponsive(header);

        Component filter = buildFilter();
        header.addComponent(filter);

        return header;
    }

    private Component buildFilter() {
        final TextField filter = new TextField();
        filter.setWidth("100%");
        filter.addTextChangeListener(new FieldEvents.TextChangeListener() {

            @Override
            public void textChange(final FieldEvents.TextChangeEvent event) {
                List<Tender> tenders = searchService.findTenders(event.getText(), false);

                titleResults.fillResults(tenders, notificationDetailWindow);
                cpvResults.fillResults(searchService.constructCpvList(tenders));
                categoryResults.fillResults(searchService.constructCategoryList(tenders));
            }
        });

        filter.setInputPrompt("Filter Aanbestedingen");
        filter.setIcon(FontAwesome.SEARCH);
        filter.addStyleName(ValoTheme.TEXTFIELD_INLINE_ICON);
        filter.addShortcutListener(new ShortcutListener("Clear", ShortcutAction.KeyCode.ESCAPE, null) {
            @Override
            public void handleAction(final Object sender, final Object target) {
                filter.setValue("");
            }
        });
        return filter;
    }
}
