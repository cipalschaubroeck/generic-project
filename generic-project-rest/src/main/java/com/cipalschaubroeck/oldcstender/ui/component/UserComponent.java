package com.cipalschaubroeck.oldcstender.ui.component;

import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;

public class UserComponent extends VerticalLayout {

    Panel contentPanel = null;
    VerticalLayout  contentLayout = null;

    public UserComponent() {
        super();
        setMargin(true);
        setWidth("100%");
        setHeight("100%");

        contentPanel = new Panel();

        contentPanel.setWidth("100%");
        contentPanel.setHeight("100%");
        contentLayout = new VerticalLayout();
        contentLayout.setMargin(new MarginInfo(true, true, true, true));
        contentPanel.setContent(contentLayout);
        addComponent(contentPanel);

    }

    public VerticalLayout getContentLayout() {
        return contentLayout;
    }
}
