package com.cipalschaubroeck.oldcstender.ui.event;

import com.cipalschaubroeck.oldcstender.ui.TenderViewType;

public class TenderEvent {
    public static final class PostViewChangeEvent {
        private final TenderViewType view;

        public PostViewChangeEvent(final TenderViewType view) {
            this.view = view;
        }

        public TenderViewType getView() {
            return view;
        }
    }
}
