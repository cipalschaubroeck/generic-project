package com.cipalschaubroeck.oldcstender.ui.component;

import com.vaadin.data.Container;
import com.vaadin.data.Item;
import com.vaadin.server.PaintException;
import com.vaadin.server.PaintTarget;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.Component;
import com.vaadin.ui.Table;
import org.apache.log4j.Logger;

import java.io.Serializable;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

/**
 * Table that also send ColumnCollapsedEvents and ColumnSortedEvents.
 * based on suggestion in http://dev.vaadin.com/ticket/6914
 */
public class BasicTable extends Table
{
    private static Logger log = Logger.getLogger(BasicTable.class);

    public static final String COLUMN_COLLAPSED_EVENT_ID = "columnCollapsed";
    public static final String COLUMN_SORTED_EVENT_ID = "columnSorted";

    public static interface ColumnSortedEventListener extends Serializable
    {
        /**
         * Triggered when collapsed columns amount changes
         *
         * @param newCollapsedColumns
         *            - List of collapsed propertyIds
         */
        void sortedColumnChanged(ColumnSortedEvent event);
    }

    public static class ColumnSortedEvent extends Component.Event
    {
        private static final long serialVersionUID = 1L;
        public static final Method METHOD;

        static
        {
            try
            {
                METHOD = ColumnSortedEventListener.class.getDeclaredMethod("sortedColumnChanged",
                        new Class[] { ColumnSortedEvent.class });
            }
            catch (final java.lang.NoSuchMethodException e)
            {
                // This should never happen
                throw new java.lang.RuntimeException(e);
            }
        }

        public ColumnSortedEvent(Component source)
        {
            super(source);
        }
    }

    public static interface ColumnCollapsedEventListener extends Serializable
    {
        /**
         * Triggered when collapsed columns amount changes
         *
         * @param newCollapsedColumns
         *            - List of collapsed propertyIds
         */
        void collapsedColumnsChanged(ColumnCollapsedEvent event);
    }

    public static class ColumnCollapsedEvent extends Component.Event
    {
        private static final long serialVersionUID = 1L;
        public static final Method METHOD;

        static
        {
            try
            {
                METHOD = ColumnCollapsedEventListener.class.getDeclaredMethod("collapsedColumnsChanged",
                        new Class[] { ColumnCollapsedEvent.class });
            }
            catch (final java.lang.NoSuchMethodException e)
            {
                // This should never happen
                throw new java.lang.RuntimeException(e);
            }
        }

        public ColumnCollapsedEvent(Component source)
        {
            super(source);
        }
    }

    private List<Object> collapsedColumns = new ArrayList<Object>();
    private Object sortedColumn = null;
    private boolean ascending = true;

    public void addColumnCollapsedListener(ColumnCollapsedEventListener l)
    {
        addListener(COLUMN_COLLAPSED_EVENT_ID, ColumnCollapsedEvent.class, l, ColumnCollapsedEvent.METHOD);
    }

    public void addColumnSortedListener(ColumnSortedEventListener l)
    {
        addListener(COLUMN_SORTED_EVENT_ID, ColumnSortedEvent.class, l, ColumnSortedEvent.METHOD);
    }

    @Override
    public void paintContent(PaintTarget target) throws PaintException
    {
        //synchronized (this)
        {
            super.paintContent(target);

            // For workaround of TreeTable/Table 'bug' to inform about collapse
            // event of columns
            List<Object> collapsedColumnPropertyIds = getCollapsedColumns();
            if (collapsedColumns.size() != collapsedColumnPropertyIds.size())
            {
                // then there was change in collapsed columns
                collapsedColumns = collapsedColumnPropertyIds;
                VaadinSession.getCurrent().access(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        fireEvent(new ColumnCollapsedEvent(BasicTable.this));
                    }
                });
            }
            boolean newAscending = isSortAscending();
            Object newSortedColumn = getSortContainerPropertyId();
            if (ascending != newAscending ||
                    (sortedColumn == null && newSortedColumn != null) ||
                    (sortedColumn != null && !sortedColumn.equals(newSortedColumn)))
            {
                log.info("Sort column now "+newSortedColumn+(newAscending?" ASC":" DESC"));
                // then there was change in sorted column
                sortedColumn = newSortedColumn;
                ascending = newAscending;
                VaadinSession.getCurrent().access(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        fireEvent(new ColumnSortedEvent(BasicTable.this));
                    }
                });
            }
        }
    }

    public List<Object> getCollapsedColumns()
    {
        List<Object> collapsedColumnPropertyIds = new ArrayList<Object>();
        List<Object> propertyIds = new ArrayList<Object>(getContainerPropertyIds());
        for (Object propertyId : propertyIds)
        {
            if (isColumnCollapsed(propertyId))
                collapsedColumnPropertyIds.add(propertyId);
        }
        return collapsedColumnPropertyIds;
    }

    @Override
    public void setContainerDataSource(Container newDataSource)
    {
        super.setContainerDataSource(newDataSource);
        resize();
    }

    public void resize()
    {
        setPageLength(getItemIds().size());
    }

    @Override
    public Object addItem(Object[] cells, Object itemId) throws UnsupportedOperationException
    {
        Object retval = super.addItem(cells, itemId);
        resize();
        return retval;
    }

    @Override
    public boolean removeAllItems()
    {
        boolean retval = super.removeAllItems();
        resize();
        return retval;
    }

    @Override
    public boolean removeItem(Object itemId)
    {
        boolean retval = super.removeItem(itemId);
        resize();
        return retval;
    }

    @Override
    public Object addItemAfter(Object previousItemId) throws UnsupportedOperationException
    {
        Object retval = super.addItemAfter(previousItemId);
        resize();
        return retval;
    }

    @Override
    public Item addItemAfter(Object previousItemId, Object newItemId) throws UnsupportedOperationException
    {
        Item retval = super.addItemAfter(previousItemId, newItemId);
        resize();
        return retval;
    }
}
