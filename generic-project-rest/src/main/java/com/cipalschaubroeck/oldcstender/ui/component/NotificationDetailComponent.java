package com.cipalschaubroeck.oldcstender.ui.component;

import com.cipalschaubroeck.oldcstender.deprecatedservice.I18Service;
import com.vaadin.server.FontAwesome;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

public class NotificationDetailComponent extends VerticalLayout {
    Label title = null;
    Label essence = null;
    Label lot = null;
    Label features = null;
    Label award = null;
    Label preInformation = null;
    Label cpv = null;
    Label openingDate = null;
    Label publication = null;
    Label category = null;
    Label government = null;
    Label governmentPublished = null;
    Panel contentPanel = null;
    Button tenderToPdfButton = null;
    Button tenderToXmlButton = null;
    Button awardToPdfButton = null;
    Button governmentButton = null;
    CheckBox rememberTenderCheckBox = null;
    HorizontalLayout preInformationLayout = null;
    Button preInformationToPdfButton = null;
    HorizontalLayout awardLayout = null;
    VerticalLayout rectificationLayout = null;
    VerticalLayout awardVerticalLayout = null;

    Attachments attachments = null;
    I18Service i18Service = null;

    //@Inject
    public NotificationDetailComponent(I18Service i18Service) {
        this.i18Service= i18Service;
        setMargin(true);
        setWidth("100%");
        setHeight("100%");
        contentPanel = new Panel();

        contentPanel.setWidth("100%");
        contentPanel.setHeight("100%");
        VerticalLayout left = new VerticalLayout();
        left.setMargin(new MarginInfo(true, true, true, true));
        contentPanel.setContent(left);
        addComponent(contentPanel);

        HorizontalLayout titleLayout = new HorizontalLayout();
        titleLayout.setWidth("100%");
        left.addComponent(titleLayout);

        openingDate = new Label("", ContentMode.HTML);
        openingDate.addStyleName(ValoTheme.LABEL_LARGE);
        titleLayout.addComponent(openingDate);
        titleLayout.setExpandRatio(openingDate, 1.0f);

        rememberTenderCheckBox = new CheckBox();
        rememberTenderCheckBox.setIcon(FontAwesome.SAVE);
        titleLayout.addComponent(rememberTenderCheckBox);

        tenderToPdfButton = new Button();
        tenderToPdfButton.setStyleName(ValoTheme.BUTTON_QUIET);
        //tenderToPdfButton.setStyleName(BaseTheme.BUTTON_LINK);
        tenderToPdfButton.setIcon(FontAwesome.FILE_PDF_O /*new ClassResource("/com/sygel/tender/resources/images/pdficon_large.png")*/);
        titleLayout.addComponent(tenderToPdfButton);

        tenderToXmlButton = new Button();
        tenderToXmlButton.setStyleName(ValoTheme.BUTTON_QUIET);
        //tenderToXmlButton.setStyleName(BaseTheme.BUTTON_LINK);
        tenderToXmlButton.setIcon(FontAwesome.FILE_TEXT /*new ClassResource("/com/sygel/tender/resources/images/xlsicon_large.png")*/);
        titleLayout.addComponent(tenderToXmlButton);


        title = new Label("");
        title.addStyleName(ValoTheme.LABEL_HUGE);
        left.addComponent(title);

        essence = new Label("");
        left.addComponent(essence);

        lot = new Label("", ContentMode.HTML);
        left.addComponent(lot);

        features = new Label("", ContentMode.HTML);
        left.addComponent(features);


        Label gap = new Label();
        gap.setHeight(".5em");
        left.addComponent(gap);
        cpv = new Label("", ContentMode.HTML);
        cpv.addStyleName(ValoTheme.LABEL_LIGHT);
        left.addComponent(cpv);

        category = new Label("", ContentMode.HTML);
        left.addComponent(category);

        Label gap2 = new Label();
        gap.setHeight(".5em");
        left.addComponent(gap2);

        HorizontalLayout governmentLayout = new HorizontalLayout();
        left.addComponent(governmentLayout);
        government = new Label("", ContentMode.HTML);
        governmentLayout.addComponent(government);

        governmentButton = new Button("");
        governmentButton.setHtmlContentAllowed(true);

        governmentButton.setStyleName(ValoTheme.BUTTON_LINK);
        governmentLayout.addComponent(governmentButton);

        governmentPublished = new Label("",ContentMode.HTML);
        governmentLayout.addComponent(governmentPublished);



        preInformationLayout = new HorizontalLayout();
        preInformationLayout.setMargin(new MarginInfo(true, false, false, false));
        preInformationLayout.setWidth("100%");
        left.addComponent(preInformationLayout);

        preInformation = new Label("", ContentMode.HTML);
        preInformationLayout.addComponent(preInformation);
        preInformationLayout.setExpandRatio(preInformation, 1.0f);

        preInformationToPdfButton = new Button(FontAwesome.FILE_PDF_O);
        //preInformationToPdfButton.setStyleName(BaseTheme.BUTTON_LINK);
        //preInformationToPdfButton.setIcon(new ClassResource("/com/sygel/tender/resources/images/pdficon_large.png"));
        preInformationLayout.addComponent(preInformationToPdfButton);

        awardLayout = new HorizontalLayout();
        awardLayout.setMargin(new MarginInfo(true, false, false, false));
        awardLayout.setWidth("100%");
        left.addComponent(awardLayout);

        awardVerticalLayout = new VerticalLayout();
        awardVerticalLayout.setWidth("100%");
        awardLayout.addComponent(awardVerticalLayout);
        awardLayout.setExpandRatio(awardVerticalLayout, 1.0f);

        awardToPdfButton = new Button(FontAwesome.FILE_PDF_O);
        //awardToPdfButton.setStyleName(BaseTheme.BUTTON_LINK);
        //awardToPdfButton.setIcon(new ClassResource("/com/sygel/tender/resources/images/pdficon_large.png"));
        awardLayout.addComponent(awardToPdfButton);

        rectificationLayout = new VerticalLayout();
        rectificationLayout.setMargin(new MarginInfo(true, false, false, false));
        rectificationLayout.setWidth("100%");
        left.addComponent(rectificationLayout);

        attachments = new Attachments(i18Service);

        left.addComponent(attachments);
    }

    public Label getTitle() {
        return title;
    }

    public void setTitle(Label title) {
        this.title = title;
    }

    public Label getEssence() {
        return essence;
    }

    public void setEssence(Label essence) {
        this.essence = essence;
    }

    public Label getCpv() {
        return cpv;
    }

    public void setCpv(Label cpv) {
        this.cpv = cpv;
    }

    public Label getOpeningDate() {
        return openingDate;
    }

    public void setOpeningDate(Label openingDate) {
        this.openingDate = openingDate;
    }

    public Label getPublication() {
        return publication;
    }

    public void setPublication(Label publication) {
        this.publication = publication;
    }

    public Label getCategory() {
        return category;
    }

    public void setCategory(Label category) {
        this.category = category;
    }

    public Attachments getAttachments() {
        return attachments;
    }

    public void setAttachments(Attachments attachments) {
        this.attachments = attachments;
    }

    public Label getLot() {
        return lot;
    }

    public void setLot(Label lot) {
        this.lot = lot;
    }

    public Label getAward() {
        return award;
    }

    public void setAward(Label award) {
        this.award = award;
    }

    public Button getTenderToPdfButton() {
        return tenderToPdfButton;
    }

    public void setTenderToPdfButton(Button tenderToPdfButton) {
        this.tenderToPdfButton = tenderToPdfButton;
    }

    public Button getAwardToPdfButton() {
        return awardToPdfButton;
    }

    public void setAwardToPdfButton(Button awardToPdfButton) {
        this.awardToPdfButton = awardToPdfButton;
    }

    public HorizontalLayout getAwardLayout() {
        return awardLayout;
    }

    public void setAwardLayout(HorizontalLayout awardLayout) {
        this.awardLayout = awardLayout;
    }

    public VerticalLayout getRectificationLayout() {
        return rectificationLayout;
    }

    public void setRectificationLayout(VerticalLayout rectificationLayout) {
        this.rectificationLayout = rectificationLayout;
    }

    public Label getPreInformation() {
        return preInformation;
    }

    public void setPreInformation(Label preInformation) {
        this.preInformation = preInformation;
    }

    public HorizontalLayout getPreInformationLayout() {
        return preInformationLayout;
    }

    public void setPreInformationLayout(HorizontalLayout preInformationLayout) {
        this.preInformationLayout = preInformationLayout;
    }

    public Button getPreInformationToPdfButton() {
        return preInformationToPdfButton;
    }

    public void setPreInformationToPdfButton(Button preInformationToPdfButton) {
        this.preInformationToPdfButton = preInformationToPdfButton;
    }

    public Label getGovernment() {
        return government;
    }

    public void setGovernment(Label government) {
        this.government = government;
    }

    public Label getGovernmentPublished() {
        return governmentPublished;
    }

    public void setGovernmentPublished(Label governmentPublished) {
        this.governmentPublished = governmentPublished;
    }

    public Button getGovernmentButton() {
        return governmentButton;
    }

    public void setGovernmentButton(Button governmentButton) {
        this.governmentButton = governmentButton;
    }

    public VerticalLayout getAwardVerticalLayout() {
        return awardVerticalLayout;
    }

    public void setAwardVerticalLayout(VerticalLayout awardVerticalLayout) {
        this.awardVerticalLayout = awardVerticalLayout;
    }

    public Button getTenderToXmlButton() {
        return tenderToXmlButton;
    }

    public void setTenderToXmlButton(Button tenderToXmlButton) {
        this.tenderToXmlButton = tenderToXmlButton;
    }

    public Label getFeatures() {
        return features;
    }

    public void setFeatures(Label features) {
        this.features = features;
    }

    public CheckBox getRememberTenderCheckBox() {
        return rememberTenderCheckBox;
    }

    public void setRememberTenderCheckBox(CheckBox rememberTenderCheckBox) {
        this.rememberTenderCheckBox = rememberTenderCheckBox;
    }
}
