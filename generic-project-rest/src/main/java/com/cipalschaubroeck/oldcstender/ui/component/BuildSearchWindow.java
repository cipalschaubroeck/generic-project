package com.cipalschaubroeck.oldcstender.ui.component;

import com.cipalschaubroeck.cstender.domain.Filter;
import com.cipalschaubroeck.oldcstender.deprecatedservice.I18Service;
import com.cipalschaubroeck.oldcstender.deprecatedservice.KnowledgeService;
import com.cipalschaubroeck.oldcstender.deprecatedservice.SearchService;
import com.vaadin.ui.Window;

@SuppressWarnings({ "serial", "unchecked" })
public class BuildSearchWindow extends Window {
    //@Inject
    SearchService searchService;
    //@Inject
    KnowledgeService knowledgeService;
    //@Inject
    I18Service i18Service;

    //@Inject
    BuildSearchComponent buildSearchComponent;

    public BuildSearchWindow() {
        setClosable(true);
        // setSizeFull();
        setWidth("90%");
        setHeight("90%");
        center();
        addStyleName("transactions");
        // DashboardEventBus.register(this);

    }

    @Override
    public void attach() {
        super.attach();
        setCaption(i18Service.getMessage("BUILD_QUERY"));
        setContent(buildSearchComponent);
    }

    public void initSearchFilter(Filter searchFilter) {
        buildSearchComponent.setSearchFilter(searchFilter);
    }

    public Filter getSearchFilter() {
        return buildSearchComponent.getSearchFilter();
    }
}
