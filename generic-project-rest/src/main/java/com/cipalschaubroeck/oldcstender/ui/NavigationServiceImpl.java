package com.cipalschaubroeck.oldcstender.ui;

import com.vaadin.cdi.CDIViewProvider;
import com.vaadin.cdi.NormalUIScoped;
import com.vaadin.navigator.Navigator;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.UI;

import javax.annotation.PostConstruct;
import javax.inject.Named;

// TODO JAL: Remove import javax.enterprise.event.Observes;

@NormalUIScoped
public class NavigationServiceImpl implements NavigationService {

    private static final long serialVersionUID = 1L;

    //@Inject
    private CDIViewProvider viewProvider;

    //@Inject
    private TenderMenu tenderMenu;


    private CssLayout mainPanel = new CssLayout();

    //@Inject
    private @Named("TenderUI")
    UI ui;

    @PostConstruct
    public void initialize() {
        //UI ui = UI.getCurrent();
        if (ui.getNavigator() == null) {

            mainPanel.setStyleName("mainPanel");
            mainPanel.setSizeFull();

            final HorizontalLayout layout = new HorizontalLayout();
            ui.setContent(layout);
            layout.setSizeFull();

            ui.getPage().setTitle("Sygel");

            layout.addComponent(tenderMenu);
            layout.addComponent(mainPanel);
            layout.setExpandRatio(mainPanel, 1);

            Navigator navigator = new TenderNavigator(ui, mainPanel);
            navigator.addProvider(viewProvider);
        }
    }

    // TODO JAL: Remove
//    @Override
//    public void onNavigationEvent(@Observes NavigationEvent event) {
//        try {
//            //UI ui = UI.getCurrent();
//            ui.getNavigator().navigateTo(event.getNavigateTo());
//        } catch (Exception e) {
//            throw new RuntimeException(e);
//        }
//    }

}
