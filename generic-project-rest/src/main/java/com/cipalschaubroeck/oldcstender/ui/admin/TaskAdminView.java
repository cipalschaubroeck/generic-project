package com.cipalschaubroeck.oldcstender.ui.admin;

import com.cipalschaubroeck.cstender.domain.Filter;
import com.cipalschaubroeck.cstender.domain.User;
import com.cipalschaubroeck.cstender.domain.UserFilter;
import com.cipalschaubroeck.oldcstender.deprecatedservice.I18Service;
import com.cipalschaubroeck.oldcstender.deprecatedservice.ImportPreService;
import com.cipalschaubroeck.oldcstender.deprecatedservice.OldImportServiceStatic;
import com.cipalschaubroeck.oldcstender.deprecatedservice.Progress;
import com.cipalschaubroeck.oldcstender.deprecatedservice.PushService;
import com.vaadin.cdi.CDIView;
import com.vaadin.data.Property;
import com.vaadin.event.UIEvents;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.FontAwesome;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.ProgressBar;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;
import org.apache.log4j.Logger;

import javax.annotation.security.RolesAllowed;
import java.io.File;

/**
 * Admin tasks
 * Import notice from zip files.
 * Generate Email users
 */
@CDIView(value = "TASK_ADMIN")
@SuppressWarnings({ "serial", "unchecked" })
@RolesAllowed({User.ROLE_ADMIN})
public final class TaskAdminView extends VerticalLayout implements View, UIEvents.PollListener {
    private final Logger logger = Logger.getLogger(TaskAdminView.class);

    private I18Service i18Service;
    //@Inject
    ImportPreService importPreService;

    //@Inject
    PushService pushService;

    private Button importTenderButton;
    private Button emailTenderButton;

    Filter searchFilter = new UserFilter("", "");
    Filter currentTextFilter = null;
    boolean lastAdded = false;

    private boolean importBusy = false;
    private boolean emailBusy = false;

    private HorizontalLayout importTenderLayout = null;
    private HorizontalLayout emailTenderLayout = null;

    // TODO JAL: Remove
    //private transient Future<String> importResult = null;
    private transient String importResult = null;
    private ProgressBar progressBar = null;
    // Volatile because read in another thread
    private volatile Progress progress = null;
    private Label importResultLabel = new Label();
    private Label emailResultLabel = new Label();

    //@Inject
    public TaskAdminView(I18Service i18Service) {
        this.i18Service = i18Service;
        setSizeFull();
        addStyleName("transactions");
        // DashboardEventBus.register(this);
        setWidth("100%");
        // Create the indicator, disabled until progress is started
        progressBar = new ProgressBar();
        progressBar.setEnabled(false);
        importTenderLayout = buildImportToolbar();
        emailTenderLayout = buildEmailToolbar();
        addComponent(importTenderLayout);
        addComponent(emailTenderLayout);
    }

    @Override
    public void enter(final ViewChangeListener.ViewChangeEvent event) {
    }

    @Override
    public void detach() {
        super.detach();
        // A new instance of TransactionsView is created every time it's
        // navigated to so we'll need to clean up references to it on detach.

        // DashboardEventBus.unregister(this);
    }

    private HorizontalLayout buildImportToolbar() {
        HorizontalLayout header = new HorizontalLayout();
        header.setMargin(new MarginInfo(true, true, false, true));
        header.setWidth("100%");
        // Responsive.makeResponsive(header);
        importTenderButton = buildImportTenderButton();
        header.addComponent(importTenderButton);
        header.addComponent(importResultLabel);
        return header;
    }

    private HorizontalLayout buildEmailToolbar() {
        HorizontalLayout header = new HorizontalLayout();
        header.setMargin(new MarginInfo(true, true, false, true));
        header.setWidth("100%");
        // Responsive.makeResponsive(header);
        emailTenderButton = buildEmailTenderButton();
        header.addComponent(emailTenderButton);
        header.addComponent(emailResultLabel);
        return header;
    }

    private Button buildImportTenderButton() {
        final Button userButton = new Button(i18Service.getMessage("TASK_IMPORT"));
        userButton.addStyleName(ValoTheme.BUTTON_QUIET);

        userButton.setIcon(FontAwesome.DOWNLOAD);

        userButton.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(final Button.ClickEvent event) {
                progress = new Progress();
                importBusy = true;
                importTenderLayout.addComponent(progressBar);
                importTenderLayout.removeComponent(importResultLabel);
                progressBar.setEnabled(true);
                // Disable the button until the work is done
                importTenderButton.setEnabled(false);
                emailTenderButton.setEnabled(false);
                importResult = importPreService.importTenderFolder(new File(OldImportServiceStatic.DATA_FOLDER +"/enot/"), progress);
                // start polling to update the UI progressBar
                UI.getCurrent().setPollInterval(1000);
                UI.getCurrent().addPollListener(TaskAdminView.this);
            }
        });
        userButton.setEnabled(true);
        return userButton;
    }

    private Button buildEmailTenderButton() {
        final Button emailButton = new Button(i18Service.getMessage("TASK_EMAIL"));
        emailButton.addStyleName(ValoTheme.BUTTON_QUIET);
        emailButton.setIcon(FontAwesome.ENVELOPE);
        emailButton.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(final Button.ClickEvent event) {
                progress = new Progress();
                emailBusy = true;
                importTenderLayout.addComponent(progressBar);
                importTenderLayout.removeComponent(importResultLabel);
                progressBar.setEnabled(true);
                // Disable the buttons until the work is done
                importTenderButton.setEnabled(false);
                emailTenderButton.setEnabled(false);
                importResult = pushService.emailUsers(progress);
                // start polling to update the UI progressBar
                UI.getCurrent().setPollInterval(1000);
                UI.getCurrent().addPollListener(TaskAdminView.this);
            }
        });
        emailButton.setEnabled(true);
        return emailButton;
    }


    @Override
    public void poll(UIEvents.PollEvent event) {
        //if (importResult != null && importResult.isDone())
        if (importResult != null)
        {
            // Restore the state to initial
            progressBar.setValue(new Float(0.0));
            progressBar.setEnabled(false);
            // Stop polling
            UI.getCurrent().setPollInterval(-1);
            // Update result
            importTenderButton.setEnabled(true);
            emailTenderButton.setEnabled(true);
            importTenderLayout.removeComponent(progressBar);
            try {
                if (importBusy)
                    //importResultLabel.setValue(i18Service.getMessage("IMPORT_NOTICE_COUNT")+ " "+ importResult.get());
                    importResultLabel.setValue(i18Service.getMessage("IMPORT_NOTICE_COUNT")+ " "+ importResult);
                if (emailBusy)
                    //emailResultLabel.setValue(i18Service.getMessage("EMAIL_COUNT")+ " "+ importResult.get());
                    emailResultLabel.setValue(i18Service.getMessage("EMAIL_COUNT")+ " "+ importResult);
                importResult = null;
                emailBusy = false;
                importBusy = false;
            } catch (Property.ReadOnlyException e) {
                logger.error(e);
            }
//            } catch (InterruptedException e) {
//                logger.error(e);
//            } catch (ExecutionException e) {
//                logger.error(e);
//            }
            importTenderLayout.addComponent(importResultLabel);
        } else {
            progressBar.setValue(new Float(progress.getProgress()));
        }
    }
}
