package com.cipalschaubroeck.oldcstender.ui.event;

import com.cipalschaubroeck.oldcstender.ui.TenderUI;
import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.SubscriberExceptionContext;
import com.google.common.eventbus.SubscriberExceptionHandler;

public class TenderEventBus implements SubscriberExceptionHandler {

    private final EventBus eventBus = new EventBus(this);

    public static void post(final Object event) {
        TenderUI.getTenderEventbus().eventBus.post(event);
    }

    public static void register(final Object object) {
        TenderUI.getTenderEventbus().eventBus.register(object);
    }

    public static void unregister(final Object object) {
        TenderUI.getTenderEventbus().eventBus.unregister(object);
    }

    @Override
    public final void handleException(final Throwable exception, final SubscriberExceptionContext context) {
        exception.printStackTrace();
    }
}
