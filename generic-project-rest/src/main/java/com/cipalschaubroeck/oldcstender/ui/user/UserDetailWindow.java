package com.cipalschaubroeck.oldcstender.ui.user;

import com.cipalschaubroeck.cstender.domain.User;
import com.cipalschaubroeck.oldcstender.deprecatedservice.I18Service;
import com.cipalschaubroeck.oldcstender.deprecatedservice.UserService;
import com.cipalschaubroeck.oldcstender.ui.component.UserComponent;
import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.ui.Button;
import com.vaadin.ui.Window;

@SuppressWarnings({ "serial", "unchecked" })
public class UserDetailWindow extends Window {
    UserService userService;

    I18Service i18Service;

    User user = null;

    final BeanFieldGroup<User> binder = new BeanFieldGroup<User>(User.class);

    UserComponent userComponent;

    //@Inject
    public UserDetailWindow(I18Service i18Service, UserComponent userComponent, UserService userService) {
        this.i18Service = i18Service;
        this.userComponent = userComponent;
        this.userService = userService;
        setClosable(true);
        // setSizeFull();
        setWidth("70%");
        setHeight("70%");
        center();
        addStyleName("transactions");
        // DashboardEventBus.register(this);

        // Form for editing the bean
        userComponent.getContentLayout().addComponent(binder.buildAndBind(i18Service.getMessage("USER_FIRSTNAME"), "firstname"));
        userComponent.getContentLayout().addComponent(binder.buildAndBind(i18Service.getMessage("USER_NAME"), "name"));
        userComponent.getContentLayout().addComponent(binder.buildAndBind(i18Service.getMessage("USER_COMPANY"), "company"));
        userComponent.getContentLayout().addComponent(binder.buildAndBind(i18Service.getMessage("USER_EMAIL"), "email"));
        userComponent.getContentLayout().addComponent(binder.buildAndBind(i18Service.getMessage("USER_STREET"), "street"));
        userComponent.getContentLayout().addComponent(binder.buildAndBind(i18Service.getMessage("USER_ZIPCODE"), "zipcode"));
        userComponent.getContentLayout().addComponent(binder.buildAndBind(i18Service.getMessage("USER_CITY"), "city"));
        userComponent.getContentLayout().addComponent(binder.buildAndBind(i18Service.getMessage("USER_COUNTRY"), "country"));

        // Buffer the form content
        binder.setBuffered(true);

        Button okButton = new Button("OK");
        okButton.addClickListener(
                event -> {
                    try {
                        binder.commit();
                        userService.saveUser(binder.getItemDataSource().getBean());
                        close();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }});
        userComponent.getContentLayout().addComponent(okButton);
    }

    @Override
    public void attach() {
        super.attach();
        setCaption(i18Service.getMessage("USER_EDIT"));
        setContent(userComponent);
    }

    public void initUser(User user) {
        user = userService.findUser(user.getEmail(), false);
        binder.setItemDataSource(user);
    }
    public void initUser(Long userId) {
        user = userService.findUser(userId);
        binder.setItemDataSource(user);
    }
}
