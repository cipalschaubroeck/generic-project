package com.cipalschaubroeck.oldcstender.ui;

import com.cipalschaubroeck.oldcstender.ui.event.TenderEvent;
import com.cipalschaubroeck.oldcstender.ui.event.TenderEventBus;
import com.vaadin.navigator.Navigator;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.ui.ComponentContainer;
import com.vaadin.ui.UI;

public class TenderNavigator extends Navigator {
    // Provide a Google Analytics tracker id here
    private static final String TRACKER_ID = null;// "UA-658457-6";
    // private GoogleAnalyticsTracker tracker;

    private static final TenderViewType ERROR_VIEW = TenderViewType.CONTRACTOR;

    // private ViewProvider errorViewProvider;

    public TenderNavigator(UI ui, final ComponentContainer container) {
        super(ui, container);

        String host = getUI().getPage().getLocation().getHost();
        if (TRACKER_ID != null && host.endsWith("www.sygel.com")) {
            initGATracker(TRACKER_ID);
        }
        initViewChangeListener();
        // initViewProviders();

    }

    private void initGATracker(final String trackerId) {
        // tracker = new GoogleAnalyticsTracker(trackerId, "www.sygel.com");

        // GoogleAnalyticsTracker is an extension add-on for UI so it is
        // initialized by calling .extend(UI)
        // tracker.extend(UI.getCurrent());
    }

    private void initViewChangeListener() {
        addViewChangeListener(new ViewChangeListener() {
            private static final long serialVersionUID = 1L;

            @Override
            public boolean beforeViewChange(final ViewChangeEvent event) {
                // Since there's no conditions in switching between the views
                // we can always return true.
                return true;
            }

            @Override
            public void afterViewChange(final ViewChangeEvent event) {
                TenderViewType view = TenderViewType.getByViewName(event.getViewName());
                // Appropriate events get fired after the view is changed.
                TenderEventBus.post(new TenderEvent.PostViewChangeEvent(view));
                // TenderEventBus.post(new BrowserResizeEvent());
                // TenderEventBus.post(new CloseOpenWindowsEvent());

                // if (tracker != null) {
                // The view change is submitted as a pageview for GA tracker
                // tracker.trackPageview("/dashboard/" + event.getViewName());
                // }
            }
        });
    }
}
