package com.cipalschaubroeck.oldcstender.ui.award;

import com.cipalschaubroeck.cstender.domain.AwardItem;
import com.cipalschaubroeck.cstender.domain.Contractor;
import com.cipalschaubroeck.cstender.domain.Tender;
import com.cipalschaubroeck.oldcstender.deprecatedservice.I18Service;
import com.cipalschaubroeck.oldcstender.deprecatedservice.KnowledgeService;
import com.cipalschaubroeck.oldcstender.deprecatedservice.SearchService;
import com.cipalschaubroeck.oldcstender.ui.component.ContractorDetailComponent;
import com.cipalschaubroeck.oldcstender.ui.notification.NotificationDetailWindow;
import com.vaadin.ui.Window;

import java.util.HashSet;
import java.util.Set;

@SuppressWarnings({ "serial", "unchecked" })
public class ContractorDetailWindow extends Window {
    //@Inject
    SearchService searchService;
    //@Inject
    KnowledgeService knowledgeService;
    //@Inject
    I18Service i18Service;

    //@Inject
    ContractorDetailComponent contractorDetailComponent;

    NotificationDetailWindow notificationDetailWindow;

    public ContractorDetailWindow() {
        setClosable(true);
        setWidth("90%");
        setHeight("90%");
        center();
        addStyleName("transactions");
        // DashboardEventBus.register(this);
    }

    @Override
    public void attach() {
        super.attach();
        setCaption(i18Service.getMessage("CONTRACTOR_DETAIL"));
        setContent(contractorDetailComponent);
    }

    public void initContractor(Long contractorId, NotificationDetailWindow notificationDetailWindow) {
        this.notificationDetailWindow =notificationDetailWindow;
        Contractor contractor = searchService.findContractorById(contractorId);
        init(contractor);
    }

    public void init(Contractor contractor) {
        //
        if (contractor !=null) {
            contractorDetailComponent.getName().setValue(contractor.getName());
            Set<Tender> tenders = new HashSet<>();
            for (AwardItem awardItem : contractor.getAwardItems()) {
                Tender tender =awardItem.getAward().getTender();
                tender.setLanguage("NL");
                tenders.add(tender);
            }

            contractorDetailComponent.getTitleResults().fillResults(tenders, notificationDetailWindow);
            StringBuilder adressBuilder = new StringBuilder();
            if (contractor.getStreet() != null && contractor.getStreet().length() >0)
                adressBuilder.append(contractor.getStreet()+"</br>");
            if (contractor.getLocation() != null && contractor.getLocation().length() > 0)
                adressBuilder.append(knowledgeService.getKnowledgeBase().translateLocation(contractor.getLocation(),null)+"</br>");

            if (contractor.getPhones()!= null && contractor.getPhones().length() > 0)
                adressBuilder.append(contractor.getPhones()+"</br>");
            if (contractor.getFaxes()!= null && contractor.getFaxes().length() > 0)
                adressBuilder.append(contractor.getFaxes()+"</br>");
            if (contractor.getEmails()!= null && contractor.getEmails().length() > 0)
                adressBuilder.append(contractor.getEmails()+"</br>");

            contractorDetailComponent.getDetails().setValue(adressBuilder.toString());
        }
    }
}
