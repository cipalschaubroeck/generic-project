package com.cipalschaubroeck.oldcstender.ui.attachment;

import com.cipalschaubroeck.cstender.domain.Attachment;
import com.cipalschaubroeck.cstender.domain.Tender;
import com.cipalschaubroeck.oldcstender.deprecatedservice.I18Service;
import com.cipalschaubroeck.oldcstender.ui.component.BasicTable;
import com.cipalschaubroeck.oldcstender.ui.notification.NotificationDetailWindow;
import com.vaadin.addon.tableexport.ExcelExport;
import com.vaadin.data.Item;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.event.Action;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.ui.Button;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Table;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.Reindeer;

import java.io.Serializable;
import java.util.Collection;

public class AttachmentResults extends CustomComponent implements Serializable
{
    private static final long serialVersionUID = 1L;
    private VerticalLayout screen = null;
    private BasicTable table = null;
    private Label count = new Label("");
    private I18Service i18Service;

    //@Inject
    NotificationDetailWindow notificationDetailWindow;

    //@Inject
    public AttachmentResults(I18Service i18Service)
    {
        this.i18Service = i18Service;
        table = new BasicTable();
        table.setSizeFull();
        table.setSelectable(true);
        table.addActionHandler(new TransactionsActionHandler());
        table.addItemClickListener(new ItemClickEvent.ItemClickListener()
        {
            private static final long serialVersionUID = 1L;
            @Override
            public void itemClick(ItemClickEvent event)
            {
            }
        });

        Button downloadButton = new Button(i18Service.getMessage("TO_XLS"));
        downloadButton.setStyleName(Reindeer.BUTTON_LINK);
        downloadButton.addClickListener(new Button.ClickListener()
        {
            private static final long serialVersionUID = 1L;
            private ExcelExport excelExport;

            public void buttonClick(final Button.ClickEvent event)
            {
                excelExport = new ExcelExport(table);
                excelExport.excludeCollapsedColumns();
                excelExport.setReportTitle(i18Service.getMessage("ATTACHMENTS"));
                excelExport.export();
            }
        });

        HorizontalLayout bottom = new HorizontalLayout(count, downloadButton);
        bottom.setSpacing(true);
        screen = new VerticalLayout(table, bottom);
        screen.setMargin(true);
        screen.setSpacing(true);
        setSizeFull();
        screen.setSizeFull();
        screen.setExpandRatio(table, 1);
        setCompositionRoot(screen);
    }

    public void fillResults(Collection<Attachment> attachmentsFound)
    {
        BeanItemContainer<Attachment> ds = new BeanItemContainer<Attachment>(Attachment.class);
        table.setContainerDataSource(ds);
        table.setVisibleColumns("name","textSnippet");

        table.setColumnHeaders(i18Service.getMessage("FILE_NAME"),
                i18Service.getMessage("TEXT"));
        Object[] columnIds = table.getVisibleColumns();
        table.setColumnWidth(columnIds[0], 300);
        table.setColumnWidth(columnIds[1], 800);

        ds.addAll(attachmentsFound);
        count.setValue(i18Service.getMessage("ENTRIES_FOUND")+":" + attachmentsFound.size());
    }

    private class TransactionsActionHandler implements Action.Handler {

        private final Action details = new Action(i18Service.getMessage("TENDER_DETAIL"));

        @Override
        public void handleAction(final Action action, final Object sender,
                                 final Object target) {
            if (action == details) {
                Item item = ((Table) sender).getItem(target);
                if (item != null) {
                    Tender tender = (Tender) item.getItemProperty("tender").getValue();
                    if (!UI.getCurrent().getWindows().contains(notificationDetailWindow))
                        UI.getCurrent().addWindow(notificationDetailWindow);
                    notificationDetailWindow.initTender(tender.getId());
                    notificationDetailWindow.focus();
                }
            }
        }

        @Override
        public Action[] getActions(final Object target, final Object sender) {
            return new Action[] { details };
        }
    }
}
