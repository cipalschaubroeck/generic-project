package com.cipalschaubroeck.oldcstender.ui.util;

import com.cipalschaubroeck.cstender.domain.User;
import com.vaadin.ui.UI;

public class SessionUtil {
    static public void storeUser(User user) {
        UI.getCurrent().getSession().setAttribute("USER", user);
    }

    static public User getUser() {
        return (User) UI.getCurrent().getSession().getAttribute("USER");
    }
}
