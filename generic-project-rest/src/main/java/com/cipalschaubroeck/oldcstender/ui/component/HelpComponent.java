package com.cipalschaubroeck.oldcstender.ui.component;

import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;

public class HelpComponent extends VerticalLayout {

    Panel contentPanel = null;
    VerticalLayout  contentLayout = null;
    Label htmlText = null;

    public HelpComponent() {
        super();
        setMargin(true);
        setWidth("100%");
        setHeight("100%");

        contentPanel = new Panel();

        contentPanel.setWidth("100%");
        contentPanel.setHeight("100%");
        contentLayout = new VerticalLayout();
        contentLayout.setMargin(new MarginInfo(true, true, true, true));
        contentPanel.setContent(contentLayout);
        addComponent(contentPanel);
        htmlText = new Label("", ContentMode.HTML);
        contentLayout.addComponent(htmlText);
    }

    public VerticalLayout getContentLayout() {
        return contentLayout;
    }

    public void setHelpText(String helpText) {
        htmlText.setValue(helpText);
    }
}
