package com.cipalschaubroeck.oldcstender.ui.util;

import com.cipalschaubroeck.cstender.domain.Tender;
import com.vaadin.addon.tableexport.ExcelExport;
import com.vaadin.data.Property;
import com.vaadin.data.util.ObjectProperty;
import com.vaadin.ui.Table;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Hyperlink;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.util.CellUtil;

import java.math.BigDecimal;
import java.util.Date;

public class HyperlinkExcelExport extends ExcelExport {
    String baseUrl = null;

    public HyperlinkExcelExport(Table table) {
        super(table);
    }

    /**
     * This method is ultimately used by either addDataRows() or
     * addHierarchicalDataRows() to actually add the data to the Sheet.
     *
     * @param rootItemId
     *          the root item id
     * @param row
     *          the row
     */
    protected void addDataRow(final Sheet sheetToAddTo, final Object rootItemId, final int row) {
        final Row sheetRow = sheetToAddTo.createRow(row);
        Property prop;
        Object propId;
        Object value;
        Cell sheetCell;
        // System.out.println("addDataRow "+rootItemId.getClass().getCanonicalName());
        for (int col = 0; col < getPropIds().size(); col++) {
            propId = getPropIds().get(col);
            prop = getProperty(rootItemId, propId);
            if (null == prop) {
                value = null;
            } else {
                value = prop.getValue();
            }
            sheetCell = sheetRow.createCell(col);
            final CellStyle cs = getCellStyle(rootItemId, row, col, false);
            sheetCell.setCellStyle(cs);
            final Short poiAlignment = getTableHolder().getCellAlignment(propId);
            CellUtil.setAlignment(sheetCell, workbook, poiAlignment);
            if (null != value) {
                if (!isNumeric(prop.getType())) {
                    if (java.util.Date.class.isAssignableFrom(prop.getType())) {
                        sheetCell.setCellValue((Date) value);
                    } else {
                        sheetCell.setCellValue(createHelper.createRichTextString(value.toString()));
                        if (baseUrl != null) {
                            Hyperlink link = createHelper.createHyperlink(Hyperlink.LINK_URL);
                            String item = "";
                            if (rootItemId instanceof Tender)
                                item = "/" + ((Tender) rootItemId).getId();
                            link.setAddress(baseUrl + item);
                            sheetCell.setHyperlink(link);
                        }
                    }
                } else {
                    try {
                        // parse all numbers as double, the format will determine how they
                        // appear
                        final Double d = Double.parseDouble(value.toString());
                        sheetCell.setCellValue(d);
                    } catch (final NumberFormatException nfe) {
                        sheetCell.setCellValue(createHelper.createRichTextString(value.toString()));
                    }
                }
            }
        }
    }

    private Property getProperty(final Object rootItemId, final Object propId) {
        Property prop;
        if (getTableHolder().isGeneratedColumn(propId)) {
            prop = getTableHolder().getPropertyForGeneratedColumn(propId, rootItemId);
        } else {
            prop = getTableHolder().getContainerDataSource().getContainerProperty(rootItemId, propId);
            if (useTableFormatPropertyValue) {
                if (getTableHolder().isExportableFormattedProperty()) {
                    final String formattedProp = getTableHolder().getFormattedPropertyValue(rootItemId, propId, prop);
                    if (null == prop) {
                        prop = new ObjectProperty<String>(formattedProp, String.class);
                    } else {
                        final Object val = prop.getValue();
                        if (null == val) {
                            prop = new ObjectProperty<String>(formattedProp, String.class);
                        } else {
                            if (!val.toString().equals(formattedProp)) {
                                prop = new ObjectProperty<String>(formattedProp, String.class);
                            }
                        }
                    }
                } else {
                }
            }
        }
        return prop;
    }

    /**
     * Utility method to determine whether value being put in the Cell is numeric.
     *
     * @param type
     *          the type
     * @return true, if is numeric
     */
    private boolean isNumeric(final Class<?> type) {
        if (isIntegerLongShortOrBigDecimal(type)) {
            return true;
        }
        if (isDoubleOrFloat(type)) {
            return true;
        }
        return false;
    }

    /**
     * Utility method to determine whether value being put in the Cell is
     * integer-like type.
     *
     * @param type
     *          the type
     * @return true, if is integer-like
     */
    private boolean isIntegerLongShortOrBigDecimal(final Class<?> type) {
        if ((Integer.class.equals(type) || (int.class.equals(type)))) {
            return true;
        }
        if ((Long.class.equals(type) || (long.class.equals(type)))) {
            return true;
        }
        if ((Short.class.equals(type)) || (short.class.equals(type))) {
            return true;
        }
        if ((BigDecimal.class.equals(type)) || (BigDecimal.class.equals(type))) {
            return true;
        }
        return false;
    }

    /**
     * Utility method to determine whether value being put in the Cell is
     * double-like type.
     *
     * @param type
     *          the type
     * @return true, if is double-like
     */
    private boolean isDoubleOrFloat(final Class<?> type) {
        if ((Double.class.equals(type)) || (double.class.equals(type))) {
            return true;
        }
        if ((Float.class.equals(type)) || (float.class.equals(type))) {
            return true;
        }
        return false;
    }

    public String getBaseUrl() {
        return baseUrl;
    }

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }
}
