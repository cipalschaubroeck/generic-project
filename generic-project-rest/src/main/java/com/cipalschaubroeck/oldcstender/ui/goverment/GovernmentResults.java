package com.cipalschaubroeck.oldcstender.ui.goverment;

import com.cipalschaubroeck.cstender.domain.Government;
import com.cipalschaubroeck.oldcstender.deprecatedservice.I18Service;
import com.cipalschaubroeck.oldcstender.deprecatedservice.KnowledgeService;
import com.cipalschaubroeck.oldcstender.ui.component.BasicTable;
import com.cipalschaubroeck.oldcstender.ui.notification.NotificationDetailWindow;
import com.vaadin.addon.tableexport.ExcelExport;
import com.vaadin.data.Item;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.event.Action;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.ui.Button;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Table;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.Reindeer;

import java.io.Serializable;
import java.util.Collection;

public class GovernmentResults extends CustomComponent implements Serializable
{
    private static final long serialVersionUID = 1L;
    private VerticalLayout screen = null;
    private BasicTable table = null;
    private Label count = new Label("");

    //@Inject
    GovernmentDetailWindow governmentDetailWindow;

    //@Inject
    NotificationDetailWindow notificationDetailWindow;

    //@Inject
    KnowledgeService knowledgeService;


    I18Service i18Service;

    //@Inject
    public GovernmentResults(I18Service i18Service)
    {
        this.i18Service = i18Service;
        table = new BasicTable();
        table.setSizeFull();
        table.setSelectable(true);
        table.addActionHandler(new TransactionsActionHandler());
        table.addItemClickListener(new ItemClickEvent.ItemClickListener()
        {
            private static final long serialVersionUID = 1L;

            @Override
            public void itemClick(ItemClickEvent event)
            {
            }
        });

        Button downloadButton = new Button(i18Service.getMessage("TO_XLS"));
        downloadButton.setStyleName(Reindeer.BUTTON_LINK);
        downloadButton.addClickListener(new Button.ClickListener()
        {
            private static final long serialVersionUID = 1L;
            private ExcelExport excelExport;

            public void buttonClick(final Button.ClickEvent event)
            {
                excelExport = new ExcelExport(table);
                excelExport.excludeCollapsedColumns();
                excelExport.setReportTitle(i18Service.getMessage("GOVERNMENTS"));
                excelExport.export();
            }
        });


        HorizontalLayout bottom = new HorizontalLayout(count, downloadButton);
        bottom.setSpacing(true);
        screen = new VerticalLayout(table, bottom);
        screen.setMargin(true);
        screen.setSpacing(true);
        setSizeFull();
        screen.setSizeFull();
        screen.setExpandRatio(table, 1);
        setCompositionRoot(screen);
    }

    public void fillResults(Collection<Government> governmentsFound)
    {
        for (Government government : governmentsFound) {
            government.setLocationString(knowledgeService.getKnowledgeBase().translateLocation(government.getLocation(), null));
            government.setTypeString((!"tnull".equals(government.getType()) && government.getType() != null && government.getType().length()>1) ? i18Service.getMessage(government.getType().substring(1)) : "");
        }
        BeanItemContainer<Government> governmentContainer = new BeanItemContainer<Government>(Government.class);
        table.setContainerDataSource(governmentContainer);
        table.setVisibleColumns("name","street","locationString","typeString");
        table.setColumnHeaders(i18Service.getMessage("GOVERNMENT"),i18Service.getMessage("STREET"),i18Service.getMessage("LOCATION"),i18Service.getMessage("TYPE"));
        Object[] columnIds = table.getVisibleColumns();

        table.setColumnWidth(columnIds[0], 300);
        table.setColumnWidth(columnIds[1], 200);
        table.setColumnWidth(columnIds[2], 200);
        table.setColumnWidth(columnIds[3], 200);

        governmentContainer.addAll(governmentsFound);

        count.setValue(i18Service.getMessage("ENTRIES_FOUND")+":" + governmentContainer.size());
    }

    private class TransactionsActionHandler implements Action.Handler {

        private final Action details = new Action(i18Service.getMessage("GOVERNMENT_DETAILS"));

        @Override
        public void handleAction(final Action action, final Object sender,
                                 final Object target) {
            if (action == details) {
                Item item = ((Table) sender).getItem(target);
                if (item != null) {
                    Long governmentId = (Long) item.getItemProperty("id").getValue();
                    if (!UI.getCurrent().getWindows().contains(governmentDetailWindow))
                        UI.getCurrent().addWindow(governmentDetailWindow);
                    governmentDetailWindow.focus();
                    governmentDetailWindow.initGovernment(governmentId, notificationDetailWindow);
                }
            }
        }

        @Override
        public Action[] getActions(final Object target, final Object sender) {
            return new Action[] { details };
        }
    }
}
