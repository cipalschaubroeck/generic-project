package com.cipalschaubroeck.oldcstender.ui.component;

import com.cipalschaubroeck.cstender.domain.Attachment;
import com.cipalschaubroeck.cstender.domain.Tender;
import com.cipalschaubroeck.oldcstender.deprecatedservice.I18Service;
import com.cipalschaubroeck.oldcstender.deprecatedservice.SearchService;
import com.cipalschaubroeck.oldcstender.ui.util.TemporaryFileDownloadResource;
import com.vaadin.data.Item;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.event.Action;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Table;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.Collection;

public class Attachments extends CustomComponent implements Serializable {
    private static final long serialVersionUID = 1L;
    private VerticalLayout screen = null;
    private BasicTable table = null;
    private Label count = new Label("");
    private SearchService searchService;
    private Tender tender;
    private I18Service i18Service;

    public Attachments(I18Service i18Service) {
        this.i18Service = i18Service;
        table = new BasicTable();
        table.setSizeFull();
        table.setSelectable(true);
        table.addActionHandler(new AttachmentActionHandler());

        table.addItemClickListener(new ItemClickEvent.ItemClickListener() {
            private static final long serialVersionUID = 1L;

            @Override
            public void itemClick(ItemClickEvent event) {
                Item attachment = event.getItem();
                selectAttachment(attachment);
            }
        });

        HorizontalLayout bottom = new HorizontalLayout(count);
        bottom.setSpacing(true);
        screen = new VerticalLayout(table, bottom);
        screen.setMargin(new MarginInfo(true, false, false, false));
        screen.setSpacing(true);
        setSizeFull();
        screen.setSizeFull();
        screen.setExpandRatio(table, 1);
        setCompositionRoot(screen);
    }

    public void fillResults(Collection<Attachment> attachments, SearchService searchService, Tender tender) {
        this.searchService = searchService;
        this.tender = tender;
        BeanItemContainer<Attachment> ds = new BeanItemContainer<Attachment>(Attachment.class);
        table.setContainerDataSource(ds);
        table.setVisibleColumns("name", "fileType");

        table.setColumnHeaders(i18Service.getMessage("FILE_NAME"), i18Service.getMessage("TYPE"));
        Object[] columnIds = table.getVisibleColumns();

        ds.addAll(attachments);
        count.setValue(i18Service.getMessage("ENTRIES_FOUND")+":" + attachments.size());
    }

    public void selectAttachment(Item attachment) {
        String name = (String) attachment.getItemProperty("name").getValue();
        String fileType = (String) attachment.getItemProperty("fileType").getValue();
        File tempFile = searchService.createAttachmentFile(tender.getNoticeId(), name, fileType);
        downloadResource(name, fileType, tempFile);
    }

    static public void downloadResource(String name, String fileType, File tempFile) {
        try {
            String mimeType = "application/pdf";
            if (fileType.equals("docx"))
                mimeType = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
            if (fileType.equals("doc"))
                mimeType = "application/msword";
            if (fileType.equals("xlsx"))
                mimeType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            if (fileType.equals("xls"))
                mimeType = "application/vnd.ms-excel";
            TemporaryFileDownloadResource resource = new TemporaryFileDownloadResource(name, mimeType, tempFile);
            UI.getCurrent().getPage().open(resource, null, false);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }



    private class AttachmentActionHandler implements Action.Handler {

        private final Action details = new Action("open");

        @Override
        public void handleAction(final Action action, final Object sender, final Object target) {
            if (action == details) {
                Item item = ((Table) sender).getItem(target);
                if (item != null) {
                    selectAttachment(item);
                }
            }
        }

        @Override
        public Action[] getActions(final Object target, final Object sender) {
            return new Action[] { details };
        }
    }
}
