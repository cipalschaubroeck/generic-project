package com.cipalschaubroeck.oldcstender.ui;

import com.cipalschaubroeck.cstender.domain.User;
import com.cipalschaubroeck.oldcstender.ui.admin.TaskAdminView;
import com.cipalschaubroeck.oldcstender.ui.admin.UserAdminView;
import com.cipalschaubroeck.oldcstender.ui.attachment.AttachmentView;
import com.cipalschaubroeck.oldcstender.ui.award.AwardView;
import com.cipalschaubroeck.oldcstender.ui.contractor.ContractorView;
import com.cipalschaubroeck.oldcstender.ui.notification.MyNotificationView;
import com.cipalschaubroeck.oldcstender.ui.notification.NotificationView;
import com.vaadin.navigator.View;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.Resource;
import org.apache.log4j.Logger;

public enum TenderViewType {
    TENDER("TENDER", NotificationView.class, FontAwesome.TABLE, true, User.ROLE_USER),
    MYTENDER("MYTENDER", MyNotificationView.class, FontAwesome.TABLE, true, User.ROLE_USER),
    AWARD("AWARD", AwardView.class, FontAwesome.TABLE, true, User.ROLE_USER),
    CONTRACTOR("CONTRACTOR", ContractorView.class, FontAwesome.TABLE, true, User.ROLE_USER),
    GOVERNMENT("GOVERNMENT", AwardView.class, FontAwesome.TABLE, true, User.ROLE_USER),
    ATTACHMENT("ATTACHMENT", AttachmentView.class, FontAwesome.TABLE, true, User.ROLE_USER),
    //BUILD_TENDER("BUILD_TENDER", BuildTenderView.class, FontAwesome.TABLE, true, User.ROLE_USER),
    USER_ADMIN("USER_ADMIN", UserAdminView.class, FontAwesome.USER, true, User.ROLE_ADMIN),
    TASK_ADMIN("TASK_ADMIN", TaskAdminView.class, FontAwesome.TASKS, true, User.ROLE_ADMIN)
    ;
    private final Logger logger = Logger.getLogger(TenderViewType.class);
    private final String viewName;
    private final Class<? extends View> viewClass;
    private final Resource icon;
    private final boolean stateful;
    private final String role;
    private TenderViewType(final String viewName,
                           final Class<? extends View> viewClass, final Resource icon,
                           final boolean stateful, String role) {
        this.viewName = viewName;
        this.viewClass = viewClass;
        this.icon = icon;
        this.stateful = stateful;
        this.role = role;
    }

    public boolean isStateful() {
        return stateful;
    }

    public String getViewName() {
        return viewName;
    }

    public Class<? extends View> getViewClass() {
        return viewClass;
    }

    public Resource getIcon() {
        return icon;
    }

    public String getRole() {
        return role;
    }

    public static TenderViewType getByViewName(final String viewName) {
        TenderViewType result = null;
        for (TenderViewType viewType : values()) {
            if (viewType.getViewName().equals(viewName)) {
                result = viewType;
                break;
            }
        }
        return result;
    }
}
