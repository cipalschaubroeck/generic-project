package com.cipalschaubroeck.oldcstender.ui.component;

import com.cipalschaubroeck.oldcstender.deprecatedservice.I18Service;
import com.vaadin.ui.Window;

@SuppressWarnings({ "serial", "unchecked" })
public class HelpWindow extends Window {
    //@Inject
    I18Service i18Service;

    //@Inject
    HelpComponent helpComponent;

    public HelpWindow() {
        setClosable(true);
        // setSizeFull();
        setWidth("90%");
        setHeight("90%");
        center();
        addStyleName("transactions");
        // DashboardEventBus.register(this);

    }

    @Override
    public void attach() {
        super.attach();
        setCaption(i18Service.getMessage("HELP"));
        setContent(helpComponent);
    }

    public void setHelpText(String helpText) {
        helpComponent.setHelpText(helpText);
    }

}
