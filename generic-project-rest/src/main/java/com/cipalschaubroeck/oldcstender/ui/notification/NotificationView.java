package com.cipalschaubroeck.oldcstender.ui.notification;

import com.cipalschaubroeck.cstender.domain.Filter;
import com.cipalschaubroeck.cstender.domain.PushTenderFilter;
import com.cipalschaubroeck.cstender.domain.Tender;
import com.cipalschaubroeck.oldcstender.deprecatedservice.I18Service;
import com.cipalschaubroeck.oldcstender.deprecatedservice.SearchService;
import com.cipalschaubroeck.oldcstender.deprecatedservice.UserService;
import com.cipalschaubroeck.oldcstender.ui.component.BuildSearchWindow;
import com.cipalschaubroeck.oldcstender.ui.util.SessionUtil;
import com.vaadin.cdi.CDIView;
import com.vaadin.data.Item;
import com.vaadin.event.Action;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.FontAwesome;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.ValoTheme;

import java.util.Calendar;
import java.util.List;

@CDIView(value = "TENDER")
@SuppressWarnings({ "serial", "unchecked" })
public final class NotificationView extends VerticalLayout implements View {
    //@Inject
    private SearchService searchService;

    //@Inject
    private UserService userService;

    //@Inject
    private I18Service i18Service;

    //@Inject
    private NotificationResults notificationResultsLarge;

    //@Inject
    BuildSearchWindow buildSearchWindow;

    private Button buildQueryButton;

    //private ComboBoxTextChange filter;
    private TextField filter;

    Filter searchFilter = new PushTenderFilter("", "");
    Filter currentTextFilter = null;
    boolean lastAdded = false;

    private boolean first = true;


    public NotificationView() {
        setSizeFull();
        addStyleName("transactions");
        // DashboardEventBus.register(this);
        setWidth("100%");

    }

    @Override
    public void enter(final ViewChangeListener.ViewChangeEvent event) {
        List<Tender> tenders = searchService.findTendersOrderByPublication();
        if (first) {
            first = false;
            addComponent(buildToolbar());
            addComponent(notificationResultsLarge);
            setExpandRatio(notificationResultsLarge, 2);
        }
        notificationResultsLarge.fillResults(tenders);
    }

    @Override
    public void detach() {
        super.detach();
        // A new instance of TransactionsView is created every time it's
        // navigated to so we'll need to clean up references to it on detach.

        // DashboardEventBus.unregister(this);
    }

    private Component buildToolbar() {
        HorizontalLayout header = new HorizontalLayout();
        header.setMargin(new MarginInfo(true, true, false, true));
        // header.addStyleName("viewheader");
        // header.setSpacing(true);
        header.setWidth("100%");
        // Responsive.makeResponsive(header);

        Component filter = buildFilter();
        header.addComponent(filter);
        header.setExpandRatio(filter, 1.0f);

        buildQueryButton = buildQueryButton();
        header.addComponent(buildQueryButton);

        return header;
    }

    private Button buildQueryButton() {
        final Button buildQueryButton = new Button(i18Service.getMessage("BUILD_QUERY"));
        buildQueryButton.addStyleName(ValoTheme.BUTTON_QUIET);

        buildQueryButton.setIcon(FontAwesome.GEARS);
        buildQueryButton.setDescription(i18Service.getMessage("BUILD_QUERY_DESCRIPTION"));

        buildQueryButton.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(final Button.ClickEvent event) {
                buildSearchWindow.initSearchFilter(searchFilter);
                if (!UI.getCurrent().getWindows().contains(buildSearchWindow))
                    UI.getCurrent().addWindow(buildSearchWindow);
                System.out.println("buttonClick " + searchFilter.getSearchString());

                buildSearchWindow.focus();
                buildSearchWindow.addCloseListener(new Window.CloseListener() {
                    public void windowClose(Window.CloseEvent e) {
                        searchFilter = buildSearchWindow.getSearchFilter();
                        if (searchFilter.getName() == null || searchFilter.getName().length() == 0)
                            buildQueryButton.setCaption(i18Service.getMessage("BUILD_QUERY"));
                        else
                            buildQueryButton.setCaption(searchFilter.getName());
                        saveFilter();
                        if (!currentTextFilter.equals(searchFilter)) {
                            currentTextFilter.init(searchFilter);

                            filter.setValue(searchFilter.getSearchString());
                            notificationResultsLarge.setName(searchFilter.getName() + " "+ searchFilter.getSearchString());
                            List<Tender> tenders = searchService.findTenders(searchFilter.getSearchString(), false);
                            notificationResultsLarge.fillResults(tenders);

                        }
                    }
                });
            }
        });
        buildQueryButton.setEnabled(true);
        return buildQueryButton;
    }

    private void saveFilter() {
        Calendar c = Calendar.getInstance();
        if (searchFilter.getLastPushMailTo() == null) {
            c.add(Calendar.MONTH, -1);
            //c.set(Calendar.MONTH, Calendar.SEPTEMBER);
            //c.set(Calendar.DATE, 1);
            searchFilter.setLastPushMailTo(c.getTime());
        }
        if (searchFilter.getId() != null) {
            if (searchFilter.getName().length() == 0)
                userService.deleteFilter(searchFilter);
            else
                userService.saveFilter(searchFilter);
            //updateFilters();
        } else if (searchFilter.getName().length() > 0) {
            userService.addUserFilter(SessionUtil.getUser().getEmail(), searchFilter);
            //updateFilters();
        }
    }

    private Component buildFilter() {
        filter = new TextField();
        currentTextFilter = new PushTenderFilter(searchFilter.getSearchString(), searchFilter.getSearchString());
        filter.setWidth("100%");
        filter.addTextChangeListener(event -> {
            String text = event.getText();
            if (text != null && !"".equals(text) && !searchFilter.getSearchString().equals(text)) {
                currentTextFilter.setSearchString(text);
                searchFilter.setSearchString(text);
                List<Tender> tenders = searchService.findTenders(searchFilter.getSearchString(), false);
                notificationResultsLarge.fillResults(tenders);
                saveFilter();
            }
        });

        filter.setInputPrompt(i18Service.getMessage("FIND_TENDER"));

        filter.setIcon(FontAwesome.SEARCH);
        filter.addStyleName(ValoTheme.TEXTFIELD_INLINE_ICON);

        return filter;
    }

    private boolean filterByProperty(final String prop, final Item item, final String text) {
        if (item == null || item.getItemProperty(prop) == null || item.getItemProperty(prop).getValue() == null) {
            return false;
        }
        String val = item.getItemProperty(prop).getValue().toString().trim().toLowerCase();
        if (val.contains(text.toLowerCase().trim())) {
            return true;
        }
        return false;
    }

    void createNewReportFromSelection() {
    }

    private class TransactionsActionHandler implements Action.Handler {
        private final Action report = new Action("Create Report");

        private final Action discard = new Action("Discard");

        private final Action details = new Action("XXX");

        @Override
        public void handleAction(final Action action, final Object sender, final Object target) {
            if (action == report) {
                createNewReportFromSelection();
            } else if (action == discard) {
                Notification.show("Not implemented in this demo");
            } else if (action == details) {
                Item item = ((Table) sender).getItem(target);
                if (item != null) {
                }
            }
        }

        @Override
        public Action[] getActions(final Object target, final Object sender) {
            return new Action[] { details, report, discard };
        }
    }

}
