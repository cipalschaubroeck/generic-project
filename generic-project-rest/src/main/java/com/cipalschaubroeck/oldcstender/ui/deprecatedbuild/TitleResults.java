package com.cipalschaubroeck.oldcstender.ui.deprecatedbuild;

import com.cipalschaubroeck.cstender.domain.Tender;
import com.cipalschaubroeck.oldcstender.deprecatedservice.I18Service;
import com.cipalschaubroeck.oldcstender.ui.component.BasicTable;
import com.cipalschaubroeck.oldcstender.ui.notification.NotificationDetailWindow;
import com.vaadin.addon.tableexport.ExcelExport;
import com.vaadin.data.Item;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.event.Action;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.ui.Button;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Table;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.Reindeer;

import java.io.Serializable;
import java.util.Collection;

public class TitleResults extends CustomComponent implements Serializable
{
    private static final long serialVersionUID = 1L;
    private VerticalLayout screen = null;
    private BasicTable table = null;
    private Label count = new Label("");

    private NotificationDetailWindow notificationDetailWindow;

    I18Service i18Service = null;

    //@Inject
    public TitleResults(I18Service i18Service)
    {
        this.i18Service = i18Service;
        table = new BasicTable();
        table.setHeight(2, Unit.INCH);
        table.setWidth("100%");
        table.setSelectable(true);
        table.addActionHandler(new TransactionsActionHandler());
        table.addItemClickListener(new ItemClickEvent.ItemClickListener()
        {
            private static final long serialVersionUID = 1L;

            @Override
            public void itemClick(ItemClickEvent event)
            {
            }
        });

        Button downloadButton = new Button(i18Service.getMessage("TO_XLS"));
        downloadButton.setStyleName(Reindeer.BUTTON_LINK);
        downloadButton.addClickListener(new Button.ClickListener()
        {
            private static final long serialVersionUID = 1L;
            private ExcelExport excelExport;

            public void buttonClick(final Button.ClickEvent event)
            {
                excelExport = new ExcelExport(table);
                excelExport.excludeCollapsedColumns();
                excelExport.setReportTitle("Aanbestedingen");
                excelExport.export();
            }
        });


        HorizontalLayout bottom = new HorizontalLayout(count, downloadButton);
        bottom.setSpacing(true);
        screen = new VerticalLayout(table, bottom);
        screen.setMargin(new MarginInfo(false, true, false, true));
        setSizeFull();
        screen.setSizeFull();
        screen.setExpandRatio(table, 1);
        setCompositionRoot(screen);
    }

    public void fillResults(Collection<Tender> tendersFound, NotificationDetailWindow notificationDetailWindow)
    {
        this.notificationDetailWindow = notificationDetailWindow;
        BeanItemContainer<Tender> ds = new BeanItemContainer<Tender>(Tender.class);
        table.setContainerDataSource(ds);
        table.setVisibleColumns("title");

        table.setColumnHeaders("Titel");
        Object[] columnIds = table.getVisibleColumns();


        ds.addAll(tendersFound);

        count.setValue(i18Service.getMessage("ENTRIES_FOUND")+":" + tendersFound.size());
    }

    private class TransactionsActionHandler implements Action.Handler {

        private final Action details = new Action("Aanbesteding details");

        @Override
        public void handleAction(final Action action, final Object sender,
                                 final Object target) {
            if (action == details) {
                Item item = ((Table) sender).getItem(target);
                if (item != null) {
                    Long tenderId = (Long) item.getItemProperty("id").getValue();
                    if (!UI.getCurrent().getWindows().contains(notificationDetailWindow))
                        UI.getCurrent().addWindow(notificationDetailWindow);
                    notificationDetailWindow.initTender(tenderId);
                    notificationDetailWindow.focus();

                }
            }
        }

        @Override
        public Action[] getActions(final Object target, final Object sender) {
            return new Action[] { details };
        }
    }

    public void setSideMargins(boolean sideMargin) {
        if (sideMargin)
            screen.setMargin(new MarginInfo(false, true, false, true));
        else {
            screen.setMargin(new MarginInfo(false, false, false, false));
            table.setHeight("100%");
            table.setWidth("100%");
            screen.setHeight("100%");
            //screen.setWidth("100%");
            //screen.setHeightUndefined();
            setHeight("100%");
        }


    }
}
