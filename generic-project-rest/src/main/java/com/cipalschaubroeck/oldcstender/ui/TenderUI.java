package com.cipalschaubroeck.oldcstender.ui;

import com.cipalschaubroeck.cstender.domain.User;
import com.cipalschaubroeck.oldcstender.deprecatedservice.UserService;
import com.cipalschaubroeck.oldcstender.ui.event.TenderEventBus;
import com.cipalschaubroeck.oldcstender.ui.util.SessionUtil;
import com.vaadin.annotations.Theme;
import com.vaadin.cdi.CDIUI;
import com.vaadin.server.Responsive;
import com.vaadin.server.VaadinRequest;
import com.vaadin.ui.UI;
import org.apache.log4j.Logger;

import javax.annotation.security.DeclareRoles;
import javax.inject.Named;
import java.util.Locale;

@Theme("tender")
@SuppressWarnings("serial")
@CDIUI("secure")
@Named("TenderUI")
@DeclareRoles({ User.ROLE_ADMIN, User.ROLE_ADMIN })
public class TenderUI extends UI {
    private final Logger logger = Logger.getLogger(TenderUI.class);

    // TODO JAL: Remove @Inject
    // TODO JAL: Remove private javax.enterprise.event.Event<NavigationEvent> navigationEvent;

    //@Inject
    UserService userService;

    private final TenderEventBus tenderEventbus = new TenderEventBus();

    @Override
    protected void init(VaadinRequest request) {
        // Page.getCurrent().getJavaScript().execute("initTender();");
        logger.info("init TenderUI pathinfo:"+request.getPathInfo());
        logger.info("init TenderUI getQuery:"+getPage().getLocation().getQuery());

        java.security.Principal p =request.getUserPrincipal();

        if (request.isUserInRole(User.ROLE_ADMIN) )
            logger.info("init TenderUI ADMIN ROLE");
        if (request.isUserInRole(User.ROLE_USER) )
            logger.info("init TenderUI USER ROLE");

        User user = userService.findUser( request.getUserPrincipal().getName(), true);
        SessionUtil.storeUser(user);
        Locale.setDefault(new Locale("nl", "BE"));
        Responsive.makeResponsive(this);
        TenderEventBus.register(this);
        String viewName = request.getParameter("v-loc");
        if (viewName == null)
            viewName = TenderViewType.TENDER.getViewName();
        else if (!viewName.contains("#!"))
            viewName = TenderViewType.TENDER.getViewName();
        else
            viewName = viewName.replaceFirst(".*#!", "");
        //if (viewName.contains("/")) viewName = viewName.substring(0,viewName.indexOf("/"));
        logger.info("initially navigating to " + viewName);
        // TODO JAL: Remove navigationEvent.fire(new NavigationEvent(viewName));
    }

    public static TenderEventBus getTenderEventbus() {
        return ((TenderUI) getCurrent()).tenderEventbus;
    }
}
