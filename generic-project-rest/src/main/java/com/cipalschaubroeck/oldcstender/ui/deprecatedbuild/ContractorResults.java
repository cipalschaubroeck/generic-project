package com.cipalschaubroeck.oldcstender.ui.deprecatedbuild;

import com.cipalschaubroeck.cstender.domain.Award;
import com.cipalschaubroeck.cstender.domain.AwardItem;
import com.cipalschaubroeck.oldcstender.ui.component.BasicTable;
import com.cipalschaubroeck.oldcstender.ui.notification.NotificationDetailWindow;
import com.vaadin.addon.tableexport.ExcelExport;
import com.vaadin.data.Item;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.event.Action;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.ui.Button;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Table;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.Reindeer;

import java.io.Serializable;
import java.util.Collection;

public class ContractorResults extends CustomComponent implements Serializable
{
    private static final long serialVersionUID = 1L;
    private VerticalLayout screen = null;
    private BasicTable table = null;
    private Label count = new Label("");

    //@Inject
    NotificationDetailWindow notificationDetailWindow;

    public ContractorResults()
    {
        table = new BasicTable();
        table.setHeight(2, Unit.INCH);
        table.setWidth("100%");
        table.setSelectable(true);
        table.addActionHandler(new TransactionsActionHandler());
        table.addItemClickListener(new ItemClickEvent.ItemClickListener()
        {
            private static final long serialVersionUID = 1L;

            @Override
            public void itemClick(ItemClickEvent event)
            {
            }
        });

        Button downloadButton = new Button("To Excel");
        downloadButton.setStyleName(Reindeer.BUTTON_LINK);
        downloadButton.addClickListener(new Button.ClickListener()
        {
            private static final long serialVersionUID = 1L;
            private ExcelExport excelExport;

            public void buttonClick(final Button.ClickEvent event)
            {
                excelExport = new ExcelExport(table);
                excelExport.excludeCollapsedColumns();
                excelExport.setReportTitle("Aanbestedingen");
                excelExport.export();
            }
        });


        HorizontalLayout bottom = new HorizontalLayout(count, downloadButton);
        bottom.setSpacing(true);
        screen = new VerticalLayout(table, bottom);
        screen.setMargin(new MarginInfo(false, true, false, true));
        setSizeFull();
        screen.setSizeFull();
        screen.setExpandRatio(table, 1);
        setCompositionRoot(screen);
    }

    public void fillResults(Collection<AwardItem> awardItemsFound)
    {
        BeanItemContainer<AwardItem> awardItemContainer = null;
        awardItemContainer = new BeanItemContainer<AwardItem>(AwardItem.class);

        table.setContainerDataSource(awardItemContainer);
        table.setVisibleColumns("contractorName","title","value");

        table.setColumnHeaders("Aannemer","Titel","Waarde");
        Object[] columnIds = table.getVisibleColumns();



        table.setColumnWidth(columnIds[0], 100);
        table.setColumnWidth(columnIds[1], 600);
        table.setColumnWidth(columnIds[2], 600);

        table.setSortContainerPropertyId(columnIds[2]);
        table.setSortAscending(true);

        awardItemContainer.addAll(awardItemsFound);
        count.setValue("entries found : " + awardItemsFound.size());
    }

    private class TransactionsActionHandler implements Action.Handler {

        private final Action details = new Action("Aanbesteding details");

        @Override
        public void handleAction(final Action action, final Object sender,
                                 final Object target) {
            if (action == details) {
                Item item = ((Table) sender).getItem(target);
                if (item != null) {
                    Award award = (Award) item.getItemProperty("award").getValue();
                    if (!UI.getCurrent().getWindows().contains(notificationDetailWindow))
                        UI.getCurrent().addWindow(notificationDetailWindow);
                    notificationDetailWindow.initTender(award.getTender().getId());
                    notificationDetailWindow.focus();

                }
            }
        }

        @Override
        public Action[] getActions(final Object target, final Object sender) {
            return new Action[] { details };
        }
    }
}
