package com.cipalschaubroeck.cstender.repository;

import com.cipalschaubroeck.cstender.domain.AwardItem;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AwardItemRepository extends PagingAndSortingRepository<AwardItem, Integer> {
}
