package com.cipalschaubroeck.cstender.repository;

import com.cipalschaubroeck.cstender.domain.PreInformation;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PreInformationRepository extends PagingAndSortingRepository<PreInformation, Integer> {
}
