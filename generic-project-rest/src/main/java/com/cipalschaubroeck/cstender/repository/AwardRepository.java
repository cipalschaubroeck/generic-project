package com.cipalschaubroeck.cstender.repository;

import com.cipalschaubroeck.cstender.domain.Award;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AwardRepository  extends PagingAndSortingRepository<Award, Integer> {
}
