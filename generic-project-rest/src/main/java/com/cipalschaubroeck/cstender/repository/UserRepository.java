package com.cipalschaubroeck.cstender.repository;

import com.cipalschaubroeck.cstender.domain.User;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends PagingAndSortingRepository<User, Integer> {
}
