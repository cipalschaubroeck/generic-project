package com.cipalschaubroeck.cstender.repository;

import com.cipalschaubroeck.cstender.domain.TenderFilter;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TenderFilterRepository extends PagingAndSortingRepository<TenderFilter, Integer> {
}
