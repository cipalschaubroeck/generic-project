package com.cipalschaubroeck.cstender.repository;


import com.cipalschaubroeck.cstender.domain.Country;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CountryRepository extends PagingAndSortingRepository<Country, Integer> {
}
