package com.cipalschaubroeck.cstender.repository;

import com.cipalschaubroeck.cstender.domain.Tender;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TenderRepository extends PagingAndSortingRepository<Tender, Long> {

    public Tender findByEreference(String ereference);
}
