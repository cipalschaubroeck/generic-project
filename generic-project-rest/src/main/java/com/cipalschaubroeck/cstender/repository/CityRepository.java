package com.cipalschaubroeck.cstender.repository;

import com.cipalschaubroeck.cstender.domain.City;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CityRepository extends PagingAndSortingRepository<City, Long> {
}
