package com.cipalschaubroeck.cstender.repository;

import com.cipalschaubroeck.cstender.domain.Government;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GovernmentRepository extends PagingAndSortingRepository<Government, Integer> {
}
