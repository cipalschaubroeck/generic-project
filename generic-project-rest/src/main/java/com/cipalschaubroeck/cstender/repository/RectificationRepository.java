package com.cipalschaubroeck.cstender.repository;

import com.cipalschaubroeck.cstender.domain.Rectification;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RectificationRepository extends PagingAndSortingRepository<Rectification, Integer> {
}
