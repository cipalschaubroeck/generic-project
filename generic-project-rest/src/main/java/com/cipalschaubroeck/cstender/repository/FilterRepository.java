package com.cipalschaubroeck.cstender.repository;

import com.cipalschaubroeck.cstender.domain.Filter;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FilterRepository extends PagingAndSortingRepository<Filter, Integer> {
}
