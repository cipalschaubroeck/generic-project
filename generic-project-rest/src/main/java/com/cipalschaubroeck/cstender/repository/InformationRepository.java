package com.cipalschaubroeck.cstender.repository;

import com.cipalschaubroeck.cstender.domain.Information;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface InformationRepository extends PagingAndSortingRepository<Information, Integer> {
}
