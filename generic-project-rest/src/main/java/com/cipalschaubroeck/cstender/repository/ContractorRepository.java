package com.cipalschaubroeck.cstender.repository;

import com.cipalschaubroeck.cstender.domain.Contractor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ContractorRepository extends PagingAndSortingRepository<Contractor, Integer> {
}
