package com.cipalschaubroeck.cstender.repository;

import com.cipalschaubroeck.cstender.domain.NoticeText;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface NoticeTextRepository extends PagingAndSortingRepository<NoticeText, Integer> {
}
