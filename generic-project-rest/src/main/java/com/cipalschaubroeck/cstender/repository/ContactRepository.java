package com.cipalschaubroeck.cstender.repository;

import com.cipalschaubroeck.cstender.domain.Contact;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ContactRepository extends PagingAndSortingRepository<Contact, Integer> {
}
