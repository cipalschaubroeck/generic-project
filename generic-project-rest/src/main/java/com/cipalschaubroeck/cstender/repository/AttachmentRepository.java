package com.cipalschaubroeck.cstender.repository;

import com.cipalschaubroeck.cstender.domain.Attachment;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AttachmentRepository extends PagingAndSortingRepository<Attachment, Integer> {
}
