package com.cipalschaubroeck.cstender.repository;

import com.cipalschaubroeck.cstender.domain.FilterTender;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FilterTenderRepository extends PagingAndSortingRepository<FilterTender, Integer> {
}
