package com.cipalschaubroeck.cstender;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import springfox.documentation.spring.data.rest.SpringDataRestConfiguration8;

@SpringBootApplication
@Import(SpringDataRestConfiguration8.class)
@ComponentScan({"com.cipalschaubroeck.cstender", "com.cipalschaubroeck.oldcstender"})
public class GenericProjectApplication {

    public static void main(String[] args) {
        SpringApplication.run(GenericProjectApplication.class, args);
    }

}

