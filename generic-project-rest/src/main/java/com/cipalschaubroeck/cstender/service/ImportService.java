package com.cipalschaubroeck.cstender.service;

public interface ImportService {
    void importTenderFolderDirectFromZip();
}
