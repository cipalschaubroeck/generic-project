package com.cipalschaubroeck.cstender.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.Arrays;
import java.util.Comparator;

@Service
public class ImportServiceImp implements ImportService {

    //@Inject
    @Autowired
    private com.cipalschaubroeck.oldcstender.deprecatedservice.OldImportService importService ;

    @Override
    public void importTenderFolderDirectFromZip() {

        File folder = new File("E:/zipFolder"); // TODO JAL: Change for an application param
        File[] files = folder.listFiles();
        Arrays.sort(files, new Comparator<File>(){
            public int compare(File f1, File f2)
            {
                return Long.valueOf(f1.lastModified()).compareTo(f2.lastModified());
            }
        });
        for (File file : files) {
            importService.importTenderDirectFromZip(file);
        }
    }
}
