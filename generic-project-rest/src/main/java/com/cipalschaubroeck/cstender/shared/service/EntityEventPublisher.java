package com.cipalschaubroeck.cstender.shared.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.event.AfterCreateEvent;
import org.springframework.data.rest.core.event.AfterDeleteEvent;
import org.springframework.data.rest.core.event.AfterSaveEvent;
import org.springframework.data.rest.core.event.BeforeCreateEvent;
import org.springframework.data.rest.core.event.BeforeDeleteEvent;
import org.springframework.data.rest.core.event.BeforeSaveEvent;
import org.springframework.stereotype.Component;

/**
 * utility component to avoid too much repetition of emitting before action event,
 * do the action, after action event
 */
@Component
public class EntityEventPublisher {

    @Autowired
    private ApplicationEventPublisher publisher;

    /**
     * emits a beforeCreateEvent, saves the entity and then emits an afterCreateEvent
     *
     * @param entity     entity to be saved for the first time
     * @param repository repository to save the entity
     * @param <T>        domain type
     * @param <ID>       id type
     * @return saved entity
     */
    public <T, ID> T create(T entity, CrudRepository<T, ID> repository) {
        publisher.publishEvent(new BeforeCreateEvent(entity));
        T saved = repository.save(entity);
        publisher.publishEvent(new AfterCreateEvent(saved));
        return saved;
    }

    /**
     * emits a beforeSaveEvent, saves the entity and then emits an afterSaveEvent
     *
     * @param entity     entity to be saved
     * @param repository repository to save the entity
     * @param <T>        domain type
     * @param <ID>       id type
     * @return saved entity
     */
    public <T, ID> T update(T entity, CrudRepository<T, ID> repository) {
        publisher.publishEvent(new BeforeSaveEvent(entity));
        T saved = repository.save(entity);
        publisher.publishEvent(new AfterSaveEvent(saved));
        return saved;
    }

    /**
     * emits a beforeDeleteEvent, deletes the entity and then emits an afterDeleteEvent
     *
     * @param entity     entity to be deleted
     * @param repository repository to delete the entity
     * @param <T>        domain type
     * @param <ID>       id type
     */
    public <T, ID> void delete(T entity, CrudRepository<T, ID> repository) {
        publisher.publishEvent(new BeforeDeleteEvent(entity));
        repository.delete(entity);
        publisher.publishEvent(new AfterDeleteEvent(entity));
    }
}
