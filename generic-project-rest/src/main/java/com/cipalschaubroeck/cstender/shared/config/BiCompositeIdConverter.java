package com.cipalschaubroeck.cstender.shared.config;

import org.apache.commons.lang3.StringUtils;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.rest.webmvc.spi.BackendIdConverter;

import java.io.Serializable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @param <T> id class
 */
public abstract class BiCompositeIdConverter<T extends Serializable> implements BackendIdConverter {

    private static final Pattern PATTERN = Pattern.compile("(\\d+)-(\\d+)");
    private static final String FORMAT = "%d-%d";

    protected abstract T createId(Integer id1, Integer id2);

    public abstract Class<T> getIdClass();

    @Override
    public Serializable fromRequestId(String id, Class<?> entityType) {
        return fromString(id);
    }

    private T fromString(String id) {
        if (StringUtils.isNotBlank(id)) {
            Matcher matcher = PATTERN.matcher(id);
            if (matcher.matches()) {
                return createId(Integer.parseInt(matcher.group(1)), Integer.parseInt(matcher.group(2)));
            }
        }
        return null;
    }

    protected abstract Integer getId1(T id);

    protected abstract Integer getId2(T id);

    @Override
    public String toRequestId(Serializable id, Class<?> entityType) {
        // safe because it is checked with supports
        @SuppressWarnings("unchecked")
        T t = (T) id;
        return fromId(t);
    }

    private String fromId(T t) {
        return String.format(FORMAT, getId1(t), getId2(t));
    }

    public Converter<String, T> fromStringConverter() {
        return this::fromString;
    }

    public Converter<T, String> fromIdConverter() {
        return this::fromId;
    }
}
