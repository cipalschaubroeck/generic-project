package com.cipalschaubroeck.cstender.shared.service;

import lombok.Data;

import java.io.Serializable;

@Data
public class EntityInfo {
    private Serializable id;
    private Class<?> domainType;
}
