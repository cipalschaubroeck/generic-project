package com.cipalschaubroeck.cstender.shared.controller;

import com.cipalschaubroeck.cstender.shared.service.EntityInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.convert.ConversionService;
import org.springframework.data.repository.support.Repositories;
import org.springframework.data.rest.core.mapping.ResourceMappings;
import org.springframework.data.rest.core.mapping.ResourceMetadata;
import org.springframework.data.rest.webmvc.PersistentEntityResource;
import org.springframework.data.rest.webmvc.PersistentEntityResourceAssembler;
import org.springframework.data.rest.webmvc.spi.BackendIdConverter;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.Resources;
import org.springframework.hateoas.core.EmbeddedWrapper;
import org.springframework.hateoas.core.EmbeddedWrappers;
import org.springframework.lang.Nullable;
import org.springframework.plugin.core.PluginRegistry;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

@Component
public class ControllerSupport {

    private static final EmbeddedWrappers WRAPPERS = new EmbeddedWrappers(false);

    @Autowired
    private ResourceMappings mappings;
    @Autowired
    private PluginRegistry<BackendIdConverter, Class<?>> idConverters;
    @Autowired
    private Repositories repositories;
    @Autowired
    @Qualifier("defaultConversionService")
    private ConversionService conversionService;

    private static Link getDefaultSelfLink() {
        return new Link(ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString());
    }

    public static Resources toResources(Iterable<?> source, PersistentEntityResourceAssembler assembler, Class<?> domainType) {
        if (!source.iterator().hasNext()) {
            List<EmbeddedWrapper> content = Collections.singletonList(WRAPPERS.emptyCollectionOf(domainType));
            return new Resources<>(content, getDefaultSelfLink());
        }

        List<PersistentEntityResource> resources = StreamSupport.stream(source.spliterator(), false).map(obj ->
                obj == null ? null : assembler.toResource(obj)).collect(Collectors.toList());

        return new Resources<>(resources, getDefaultSelfLink());
    }

    /**
     * expects a link for a particular entity, so the last two parts are repository path and id
     *
     * @param link               the link
     * @param allowedDomainTypes (optional) a collection of allowed domain types
     * @return the info or null if the class is not in the allowed domain types or there is no repository
     * for it
     */
    public EntityInfo linkToEntityInfo(@NotNull Link link, @Nullable Iterable<Class<?>> allowedDomainTypes) {
        String[] href = link.getHref().split("/");
        String itemId = href[href.length - 1];
        String repositoryPath = href[href.length - 2];

        Stream<ResourceMetadata> metadata;
        if (allowedDomainTypes != null) {
            metadata = StreamSupport.stream(allowedDomainTypes.spliterator(), false)
                    .map(mappings::getMetadataFor);
        } else {
            metadata = mappings.stream();
        }
        ResourceMetadata mapping = metadata.filter(m -> m.getPath().matches(repositoryPath))
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException(link.getHref() + " could not be match to a(n allowed) domain type"));

        EntityInfo info = new EntityInfo();
        info.setDomainType(mapping.getDomainType());
        info.setId(Optional.ofNullable(idConverters.getPluginFor(mapping.getDomainType()))
                .orElse(BackendIdConverter.DefaultIdConverter.INSTANCE)
                .fromRequestId(itemId, mapping.getDomainType()));
        if (info.getId() instanceof String) {
            // default id converter just passes the id, check that it is correct
            repositories.getRepositoryInformationFor(mapping.getDomainType()).ifPresent(repositoryInformation -> {
                if (!repositoryInformation.getIdType().isInstance(info.getId())) {
                    // safe cast since it's type of an id
                    info.setId((Serializable) conversionService.convert((Object) info.getId(), repositoryInformation.getIdType()));
                }
            });
        }

        return info;
    }
}
