package com.cipalschaubroeck.cstender.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Country")
@Getter
@Setter
public class Country {

    @Id
    private Long id;

    private String name;

    private String nameNL;

    private String nameFR;

    private String nameEN;

    private String nameDE;

    private String code;
}
