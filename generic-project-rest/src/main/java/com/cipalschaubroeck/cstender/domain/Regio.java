package com.cipalschaubroeck.cstender.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "Regio")
@Getter
@Setter
public class Regio {

    @Id
    private Long id;

    @ManyToOne
    @JoinColumn(name="country", referencedColumnName="id")
    private Country country;
}
