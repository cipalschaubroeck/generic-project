package com.cipalschaubroeck.cstender.domain;

import com.cipalschaubroeck.oldcstender.shared.PasswordHash;
import com.cipalschaubroeck.oldcstender.shared.Util;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedAttributeNode;
import javax.persistence.NamedEntityGraph;
import javax.persistence.NamedEntityGraphs;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name="user")
@Getter
@Setter

// TODO JAL: ** REFACTOR** move this query
@NamedNativeQueries({
        @NamedNativeQuery(name = "User.findByUserFilter", query = "SELECT * "
                + " FROM User WHERE MATCH (searchIndex) AGAINST (:searchString IN BOOLEAN MODE) order by name asc", resultClass = User.class)

})

// TODO JAL: ** REFACTOR** move this query
@NamedQueries({
        @NamedQuery(name = "User.findByEmail", query = "SELECT u FROM User u WHERE u.email LIKE :email"),
        @NamedQuery(name = "User.findAll", query = "SELECT u FROM User u")
})

// TODO JAL: ** REFACTOR** move this query
@NamedEntityGraphs({
        @NamedEntityGraph(name = "User.filters",
                attributeNodes = {
                        @NamedAttributeNode("id"),
                        @NamedAttributeNode("email"),
                        @NamedAttributeNode("passwordHash"),
                        @NamedAttributeNode("name"),
                        @NamedAttributeNode("firstname"),
                        @NamedAttributeNode("company"),
                        @NamedAttributeNode("vat"),
                        @NamedAttributeNode("street"),
                        @NamedAttributeNode("zipcode"),
                        @NamedAttributeNode("city"),
                        @NamedAttributeNode("country"),
                        @NamedAttributeNode("invoiceStreet"),
                        @NamedAttributeNode("invoiceZipcode"),
                        @NamedAttributeNode("invoiceCity"),
                        @NamedAttributeNode("invoiceCountry"),
                        @NamedAttributeNode("subscriptionType"),
                        @NamedAttributeNode("subscriptionStatus"),
                        @NamedAttributeNode("subscriptionStart"),
                        @NamedAttributeNode("subscriptionEnd"),
                        @NamedAttributeNode("lastWebLogin"),
                        @NamedAttributeNode("lastPushMail"),
                        @NamedAttributeNode("filters")
                })
})
public class User {

    public static final String ROLE_USER = "user";
    public static final String ROLE_ADMIN = "admin";

    @Id
    private Long id;

    private String email;
    private String passwordHash;

    private String name;
    private String firstname;

    private String company;
    private String vat;
    private String street;
    private String zipcode;
    private String city;
    private String country;

    private String invoiceStreet;
    private String invoiceZipcode;
    private String invoiceCity;
    private String invoiceCountry;

    private String roles;

    @Lob
    private String searchIndex;

    @Enumerated(EnumType.ORDINAL)
    private SubscriptionType subscriptionType = SubscriptionType.FREE;

    @Enumerated(EnumType.ORDINAL)
    private SubscriptionStatus subscriptionStatus = SubscriptionStatus.OK;

    private Date subscriptionStart;
    private Date subscriptionEnd;

    private Date lastWebLogin;
    private Date lastPushMail;

    @OneToMany(cascade = {CascadeType.REMOVE},mappedBy = "user")
    private Set<Filter> filters = new HashSet<>();

    @Transient
    String newPassword;

    @PreUpdate
    @PrePersist
    public void updateSearchIndex() {
        searchIndex = Util.constructDBIndex(name + " " + firstname+ " "+ street + " " +email+ " "+Util.LOCATION_PREFIX +zipcode+" "+city+" "+country+" "+invoiceStreet+ " "+Util.LOCATION_PREFIX+invoiceZipcode+" "+invoiceCity+ " "+invoiceCountry);
    }

    // TODO JAL: ** Refactor ** move this method
    public void storePassword(String password) {
        try {
            passwordHash = PasswordHash.createHash(password);
        } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
            passwordHash = null;
        }
    }

    // TODO JAL: ** Refactor ** move this method
    public boolean checkPassword(String password) {
        return PasswordHash.validatePassword(password, passwordHash);
    }

    // TODO JAL: ** Refactor ** move this method
    public boolean isUserInRole(String role) {
        if (role.equals(User.ROLE_ADMIN) && roles.indexOf(":a:") != -1) return true;
        if (role.equals(User.ROLE_USER) && roles.indexOf(":u:") != -1) return true;
        return false;
    }
}
