package com.cipalschaubroeck.cstender.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import java.util.Date;

@Entity
@Table(name="NoticeText")
@Getter
@Setter

public class NoticeText {

    @Id
    private Long id;

    @Size(max = 4)
    private String language;

    @Lob
    private String text;

    private Date publication;

    @Size(max = 4)
    private String form;

    @ManyToOne
    @JoinColumn(name="award", referencedColumnName="id")
    private Award award;

    @ManyToOne
    @JoinColumn(name="information", referencedColumnName="id")
    private Information information;

    @ManyToOne
    @JoinColumn(name="preInformation", referencedColumnName="id")
    private PreInformation preInformation;

    @ManyToOne
    @JoinColumn(name="rectification", referencedColumnName="id")
    private Rectification rectification;
}
