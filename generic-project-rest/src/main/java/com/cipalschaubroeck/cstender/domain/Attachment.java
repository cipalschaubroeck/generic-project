package com.cipalschaubroeck.cstender.domain;

import com.cipalschaubroeck.oldcstender.shared.Util;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.Size;

@Entity
@Table(name = "Attachment")
@Getter
@Setter
public class Attachment {
    @Id
    private Long id;

    @ManyToOne(fetch= FetchType.EAGER)
    @JoinColumn(name="tender", referencedColumnName="id")
    Tender tender;

    @Size(max = 255)
    private String url;

    @Size(max = 255)
    private String name;

    @Size(max = 255)
    private String fileType;

    private String features;

    private String text;

    private String reverseIndex;

    @Size(max = 255)
    private String location;

    @Size(max = 20)
    private String type;

    @Size(max = 255)
    private String categoryAI;

    @Size(max = 255)
    private String disciplinesAI;

    private String cpv;

    @Size(max = 11)
    private String governmentType;

    @Transient
    private String textSnippet;

    @PreUpdate
    @PrePersist
    public void updateReverseIndex() {
        reverseIndex = Util.constructDBIndex(text);
    }

    // TODO JAL: ** REFACTOR ** isExtraAttachment needs to be refactor in another class
    /**
     * Check if attachment is an F type document or an extra attachment
     *
     * @return true if this is an extra attachment, false F type document
     */
    public boolean isExtraAttachment() {
        if (getName() == null) return false;
        if (!getName().toLowerCase().endsWith(".pdf")) return true;
        String name = getName().substring(0,getName().length()-4);
        if (name.endsWith("fr_FR") || name.endsWith("nl_NL")) name = name.substring(0,getName().length()-5);
        if (containsWord(name)) return true;
        return false;
    }

    // TODO JAL: ** REFACTOR ** containsWord needs to be refactor in another class
    public static boolean containsWord(String word) {
        return word.matches(".*[A-Za-z]{3}.*");
    }
}
