package com.cipalschaubroeck.cstender.domain;

import com.cipalschaubroeck.oldcstender.shared.Util;
import com.cipalschaubroeck.oldcstender.shared.xml.Loten;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedAttributeNode;
import javax.persistence.NamedEntityGraph;
import javax.persistence.NamedEntityGraphs;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.Size;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Table(name="Tender")
@Getter
@Setter

// TODO JAL: ** REFACTOR** move this query
@NamedNativeQueries({
        @NamedNativeQuery(name = "Tender.findByQueryNoAI1", query = "SELECT * "
                + " FROM Tender WHERE MATCH (title1,essence1,lot1,features1,reverseIndex1,disciplines,location,cpv,categoryAI,governmentType) AGAINST (:searchString IN BOOLEAN MODE) and not publication is null order by publication desc", resultClass = Tender.class),
        @NamedNativeQuery(name = "Tender.findByQuery1", query = "SELECT * "
                + " FROM Tender WHERE (not title1 is null) AND MATCH (title1,essence1,lot1,features1,reverseIndex1,disciplinesCpvAI,location,cpvAI,categoryAI,governmentType) AGAINST (:searchString IN BOOLEAN MODE) order by publication desc", resultClass = Tender.class),
        @NamedNativeQuery(name = "Tender.findByQueryChangeDate1", query = "SELECT * "
                + " FROM Tender WHERE MATCH (title1,essence1,lot1,features1,reverseIndex1,disciplinesCpvAI,location,cpvAI,categoryAI,governmentType) AGAINST (:searchString IN BOOLEAN MODE) and changeDate > :changeDate order by publication desc", resultClass = Tender.class),
        @NamedNativeQuery(name = "Tender.findByQueryExactCategory1", query = "SELECT * "
                + " FROM Tender WHERE MATCH (title1,essence1,lot1,features1,reverseIndex1,disciplinesCpvAI,location,cpvNoParent,categoryAI,governmentType) AGAINST (:searchString IN BOOLEAN MODE) order by publication desc", resultClass = Tender.class),
        @NamedNativeQuery(name = "Tender.findByQueryWithAward", query = "SELECT * "
                + " FROM Tender WHERE MATCH (title1,essence1,lot1,features1,reverseIndex1,disciplinesCpvAI,location,cpvAI,categoryAI,governmentType) AGAINST (:searchString IN BOOLEAN MODE) and not award is null order by publication desc", resultClass = Tender.class)

})

// TODO JAL: ** REFACTOR** move this query
@NamedQueries({
        @NamedQuery(name = "Tender.findByPublicationDate", query = "SELECT t FROM Tender t WHERE t.publication >= :fromPublication AND t.publication <= :toPublication order by t.publication"),
        @NamedQuery(name = "Tender.findByNoticeIds", query = "SELECT t FROM Tender t WHERE t.noticeIds LIKE :noticeId"),
        @NamedQuery(name = "Tender.findByNoticeId", query = "SELECT t FROM Tender t WHERE t.noticeId = :noticeId"),
        @NamedQuery(name = "Tender.findByEreference", query = "SELECT t FROM Tender t WHERE t.ereference = :ereference"),
        @NamedQuery(name = "Tender.findAll", query = "SELECT t FROM Tender t order by openingDate desc"),
        @NamedQuery(name = "Tender.findAll1", query = "SELECT t FROM Tender t WHERE (not title1 is null) order by openingDate desc"),
        @NamedQuery(name = "Tender.findAllOrderPublication", query = "SELECT t FROM Tender t order by publication desc"),
        @NamedQuery(name = "Tender.findAllOrderPublication1", query = "SELECT t FROM Tender t WHERE (not title1 is null) order by publication desc")
})

// TODO JAL: ** REFACTOR** move this query
@NamedEntityGraphs({
        @NamedEntityGraph(name = "Tender.attachments", attributeNodes = { @NamedAttributeNode("reverseIndex1"),@NamedAttributeNode("reverseIndex2"),@NamedAttributeNode("reverseIndex3"),@NamedAttributeNode("reverseIndex4"),@NamedAttributeNode("id"), @NamedAttributeNode("bda"), @NamedAttributeNode("noticeId"), @NamedAttributeNode("noticeIds"),
                @NamedAttributeNode("year"), @NamedAttributeNode("cpv"), @NamedAttributeNode("cpvNoParent"), @NamedAttributeNode("nuts"), @NamedAttributeNode("nutsNoParent"), @NamedAttributeNode("location"),
                @NamedAttributeNode("form"), @NamedAttributeNode("ereference"), @NamedAttributeNode("type"), @NamedAttributeNode("openingDate"), @NamedAttributeNode("publication"),
                @NamedAttributeNode("changeDate"), @NamedAttributeNode("features1"), @NamedAttributeNode("features2"), @NamedAttributeNode("features3"), @NamedAttributeNode("features4"),
                @NamedAttributeNode("essence1"), @NamedAttributeNode("essence2"), @NamedAttributeNode("essence3"), @NamedAttributeNode("essence4"), @NamedAttributeNode("lot1"), @NamedAttributeNode("lot2"),
                @NamedAttributeNode("lot3"), @NamedAttributeNode("lot4"), @NamedAttributeNode("title1"), @NamedAttributeNode("title2"), @NamedAttributeNode("title3"), @NamedAttributeNode("title4"),
                @NamedAttributeNode("category"), @NamedAttributeNode("categoryAI"),
                @NamedAttributeNode("disciplines"),
                @NamedAttributeNode("preInformation"),@NamedAttributeNode("rectifications"),@NamedAttributeNode("attachments"),@NamedAttributeNode("award") }),
        @NamedEntityGraph(name = "Tender.award", attributeNodes = { @NamedAttributeNode("reverseIndex1"),@NamedAttributeNode("reverseIndex4"),@NamedAttributeNode("reverseIndex4"),@NamedAttributeNode("reverseIndex4"),@NamedAttributeNode("id"), @NamedAttributeNode("bda"), @NamedAttributeNode("noticeId"), @NamedAttributeNode("noticeIds"),
                @NamedAttributeNode("year"), @NamedAttributeNode("cpv"), @NamedAttributeNode("cpvNoParent"), @NamedAttributeNode("nuts"), @NamedAttributeNode("nutsNoParent"), @NamedAttributeNode("location"),
                @NamedAttributeNode("form"), @NamedAttributeNode("ereference"), @NamedAttributeNode("type"), @NamedAttributeNode("openingDate"), @NamedAttributeNode("publication"),
                @NamedAttributeNode("changeDate"), @NamedAttributeNode("features1"), @NamedAttributeNode("features2"), @NamedAttributeNode("features3"), @NamedAttributeNode("features4"),
                @NamedAttributeNode("essence1"), @NamedAttributeNode("essence2"), @NamedAttributeNode("essence3"), @NamedAttributeNode("essence4"), @NamedAttributeNode("lot1"), @NamedAttributeNode("lot2"),
                @NamedAttributeNode("lot3"), @NamedAttributeNode("lot4"), @NamedAttributeNode("title1"), @NamedAttributeNode("title2"), @NamedAttributeNode("title3"), @NamedAttributeNode("title4"),
                @NamedAttributeNode("category"), @NamedAttributeNode("categoryAI"),
                @NamedAttributeNode("disciplines"),

                @NamedAttributeNode("award") }) })

public class Tender {

    @Id
    private Long id;

    @Lob
    private String bda;
    @Size(max = 10)
    private String noticeId;
    @Lob
    private String noticeIds;
    private Integer year;
    @Lob
    private String cpv;

    @Lob
    private String cpvNoParent;
    @Lob
    private String cpvAI;
    @Lob
    private String cpvNoParentAI;
    @Lob
    private String cpvPureAI;
    @Lob
    private String nuts;
    @Lob
    private String nutsNoParent;
    @Lob
    private String location;
    @Size(max = 4)
    private String form;
    @Size(max = 11)
    private String governmentType;
    @Lob
    private String ereference;
    @Size(max = 20)
    private String type;


    private Date openingDate;
    private Date publication;
    private Date changeDate;

    @Lob
    private String features1;
    @Lob
    private String features2;
    @Lob
    private String features3;
    @Lob
    private String features4;

    @Lob
    private String essence1;
    @Lob
    private String essence2;
    @Lob
    private String essence3;
    @Lob
    private String essence4;

    @Lob
    private String lot1;
    @Lob
    private String lot2;
    @Lob
    private String lot3;
    @Lob
    private String lot4;

    @Lob
    private String title1;
    @Lob
    private String title2;
    @Lob
    private String title3;
    @Lob
    private String title4;

    @Size(max = 5)
    private String country;


    @Lob
    private String category;
    @Lob
    private String categoryAI;
    @Lob
    private String disciplines;
    @Lob
    private String disciplinesCpvAI;

    @Lob
    private String reverseIndex1;
    @Lob
    private String reverseIndex2;
    @Lob
    private String reverseIndex3;
    @Lob
    private String reverseIndex4;

    @OneToOne
    @JoinColumn(name = "award", referencedColumnName = "id")
    private Award award;

    @OneToOne
    @JoinColumn(name = "information", referencedColumnName = "id")
    private Information information;

    @OneToOne
    @JoinColumn(name = "preInformation", referencedColumnName = "id")
    private PreInformation preInformation;

    @OneToMany(mappedBy = "tender")
    private Set<Rectification> rectifications = new HashSet<>();

    @OneToMany(mappedBy = "tender")
    private Set<Attachment> attachments = new HashSet<>();

    @ManyToOne
    @JoinColumn(name = "city", referencedColumnName = "id")
    private City city;

    @ManyToOne
    @JoinColumn(name = "government", referencedColumnName = "id")
    private Government government;

    @Transient
    private String language = "NL";

    @Transient
    private String locationTranslation  = "";

    @Transient
    private String disciplineTranslation  = "";

    @Transient
    private String categoryTranslation  = "";

    @Transient
    private String description  = "";

    // TODO JAL: Convert into an Enum
    public static final String[] LANGUAGE_BE = { "NL", "FR", "DE", "EN" };

    @PreUpdate
    @PrePersist
    public void updateReverseIndex() {
        String bdaIndex = "";
        if (bda != null) {
            String[] bdaArray = bda.trim().split(" ");
            for (String b : bdaArray)
                bdaIndex += " " + Util.BDA_PREFIX+b.replace("-", Util.HYPHEN_ESCAPE);
        }
        reverseIndex1 = Util.constructDBIndex(title1 + " " +essence1)+bdaIndex;
        reverseIndex2 = Util.constructDBIndex(title2 + " " +essence2)+bdaIndex;
    }

    // TODO JAL: ** REFACTOR ** setTitle logic must be in another class
    public void setTitle(String language, String title) {
        for (int i = 0; i < LANGUAGE_BE.length; i++) {
            if (i == 0 && LANGUAGE_BE[i].equals(language))
                title1 = title;
            else if (i == 1 && LANGUAGE_BE[i].equals(language))
                title2 = title;
            else if (i == 2 && LANGUAGE_BE[i].equals(language))
                title3 = title;
            else if (i == 3 && LANGUAGE_BE[i].equals(language))
                title4 = title;
        }
    }

    // TODO JAL: ** REFACTOR ** getTitle logic must be in another class
    public String getTitle(String language) {
        for (int i = 0; i < LANGUAGE_BE.length; i++) {
            if (i == 0 && LANGUAGE_BE[i].equals(language))
                return title1;
            else if (i == 1 && LANGUAGE_BE[i].equals(language))
                return title2;
            else if (i == 2 && LANGUAGE_BE[i].equals(language))
                return title3;
            else if (i == 3 && LANGUAGE_BE[i].equals(language))
                return title4;
        }
        return null;
    }

    // TODO JAL: ** REFACTOR ** setEssence logic must be in another class
    public void setEssence(String language, String essence) {
        for (int i = 0; i < LANGUAGE_BE.length; i++) {
            if (i == 0 && LANGUAGE_BE[i].equals(language))
                essence1 = essence;
            else if (i == 1 && LANGUAGE_BE[i].equals(language))
                essence2 = essence;
            else if (i == 2 && LANGUAGE_BE[i].equals(language))
                essence3 = essence;
            else if (i == 3 && LANGUAGE_BE[i].equals(language))
                essence4 = essence;
        }
    }

    // TODO JAL: ** REFACTOR ** setLot logic must be in another class
    public void setLot(String language, String lot) {
        for (int i = 0; i < LANGUAGE_BE.length; i++) {
            if (i == 0 && LANGUAGE_BE[i].equals(language))
                lot1 = lot;
            else if (i == 1 && LANGUAGE_BE[i].equals(language))
                lot2 = lot;
            else if (i == 2 && LANGUAGE_BE[i].equals(language))
                lot3 = lot;
            else if (i == 3 && LANGUAGE_BE[i].equals(language))
                lot4 = lot;
        }
    }

    // TODO JAL: ** REFACTOR ** getLot logic must be in another class
    public String getLot(String language) {
        for (int i = 0; i < LANGUAGE_BE.length; i++) {
            if (i == 0 && LANGUAGE_BE[i].equals(language))
                return lot1;
            else if (i == 1 && LANGUAGE_BE[i].equals(language))
                return lot2;
            else if (i == 2 && LANGUAGE_BE[i].equals(language))
                return lot3;
            else if (i == 3 && LANGUAGE_BE[i].equals(language))
                return lot4;
        }
        return null;
    }

    public String getLot() {
        return getLot(language);
    }

    // TODO JAL: ** REFACTOR ** getEssence logic must be in another class
    public String getEssence(String language) {
        for (int i = 0; i < LANGUAGE_BE.length; i++) {
            if (i == 0 && LANGUAGE_BE[i].equals(language))
                return essence1;
            else if (i == 1 && LANGUAGE_BE[i].equals(language))
                return essence2;
            else if (i == 2 && LANGUAGE_BE[i].equals(language))
                return essence3;
            else if (i == 3 && LANGUAGE_BE[i].equals(language))
                return essence4;
        }
        return null;
    }

    // TODO JAL: ** REFACTOR ** getEssence logic must be in another class
    public String getEssence() {
        String t =  getEssence(language);
        if (t == null) {
            if (essence1 != null && essence1.length() > 0)
                t = essence1;
            else if (essence2 != null && essence2.length() > 0)
                t = essence2;
            else if (essence3 != null && essence3.length() > 0)
                t = essence3;
            else if (essence4 != null && essence4.length() > 0)
                t = essence4;
        }
        return t;
    }

    // TODO JAL: ** REFACTOR ** setFeatures logic must be in another class
    public void setFeatures(String language, String features) {
        for (int i = 0; i < LANGUAGE_BE.length; i++) {
            if (i == 0 && LANGUAGE_BE[i].equals(language))
                features1 = features;
            else if (i == 1 && LANGUAGE_BE[i].equals(language))
                features2 = features;
            else if (i == 2 && LANGUAGE_BE[i].equals(language))
                features3 = features;
            else if (i == 3 && LANGUAGE_BE[i].equals(language))
                features4 = features;
        }
    }

    // TODO JAL: ** REFACTOR ** getFeatures logic must be in another class
    public String getFeatures(String language) {
        for (int i = 0; i < LANGUAGE_BE.length; i++) {
            if (i == 0 && LANGUAGE_BE[i].equals(language))
                return features1;
            else if (i == 1 && LANGUAGE_BE[i].equals(language))
                return features2;
            else if (i == 2 && LANGUAGE_BE[i].equals(language))
                return features3;
            else if (i == 3 && LANGUAGE_BE[i].equals(language))
                return features4;
        }
        return null;
    }

    public String getFeatures() {
        return getFeatures(language);
    }

    // TODO JAL: ** REFACTOR ** addBda logic must be in another class
    public void addBda(String bdas) {
        String[] bdaArray = bdas.split(" ");
        for (String b:bdaArray) {
            b = b.trim();
            if (this.bda == null) this.bda= b;
            else if (this.bda.indexOf(b) == -1) this.bda = this.bda + " " + b;
        }
    }

    // TODO JAL: ** REFACTOR ** getTitle logic must be in another class
    public String getTitle() {
        String t = getTitle(language);
        if (t == null) {
            if (title1 != null && title1.length() > 0)
                t = title1;
            else if (title2 != null && title2.length() > 0)
                t = title2;
            else if (title3 != null && title3.length() > 0)
                t = title3;
            else if (title4 != null && title4.length() > 0)
                t = title4;
        }
        return t;
    }

    public List<Rectification> getRectificationsSorted() {
        List<Rectification> sorted = new ArrayList<>();
        sorted.addAll(rectifications);
        Collections.sort(sorted);
        return sorted;
    }

    // TODO JAL: ** REFACTOR ** getLoten logic must be in another class
    public Loten getLoten() {
        Loten loten = null;
        String l = getLot(language);
        if (l != null && l.length() > 0) {
            StringReader sr = new StringReader(l);
            JAXBContext jaxbContext;
            try {
                jaxbContext = JAXBContext.newInstance(Loten.class);
                Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
                loten = (Loten) jaxbUnmarshaller.unmarshal(sr);
            } catch (JAXBException e) {
                e.printStackTrace();
            }
        }
        return loten;
    }

    // TODO JAL: ** REFACTOR ** getCpvOnlyAI logic must be in another class
    @Transient
    public String getCpvOnlyAI() {
        StringBuilder cpvOnlyAI = new StringBuilder();
        if (cpvPureAI == null || cpvPureAI.length() < 2) return "";
        String[] cs = cpvPureAI.split(" ");
        for (String c : cs) {
            if (getCpvNoParent().indexOf(c) == -1) cpvOnlyAI.append(" "+c);
        }
        return cpvOnlyAI.toString().trim();
    }

    // TODO JAL: ** REFACTOR ** getDisciplinesString logic must be in another class
    @Transient
    public String getDisciplinesString() {
        if (this.getDisciplinesCpvAI() == null || this.getDisciplinesCpvAI().length() < 2) return "";
        StringBuilder db = new StringBuilder();
        String[] ds = this.getDisciplinesCpvAI().split("\\:");
        for (String d : ds) {
            if (d.length()>1)
                db.append(" "+d.substring(2));
        }
        return db.toString().trim();
    }

    // TODO JAL: ** REFACTOR ** getCategoryString logic must be in another class
    @Transient
    public String getCategoryString() {
        if (this.getCategoryAI() == null || this.getCategoryAI().length() < 2) return "";
        StringBuilder db = new StringBuilder();
        String[] ds = this.getCategoryAI().split("\\:");
        for (String d : ds) {
            if (d.startsWith(Util.CATEGORY_PREFIX))
                db.append(" "+d.substring(3));
            if (d.startsWith(Util.CLASS_PREFIX))
                db.append(" kl"+d.substring(3));
        }
        return db.toString().trim();
    }

    // TODO JAL: ** REFACTOR ** getBdaMaster logic must be in another class
    @Transient
    public String getBdaMaster() {
        String bdaMaster = bda;
        if (bdaMaster != null) {
            int end = bdaMaster.indexOf(" ");
            if (end != -1) bdaMaster = bdaMaster.substring(0,end);
        }
        return bdaMaster;
    }
}
