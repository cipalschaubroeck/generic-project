package com.cipalschaubroeck.cstender.domain;

public enum SubscriptionType {
    FREE,
    PAID
}
