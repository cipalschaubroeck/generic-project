package com.cipalschaubroeck.cstender.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Size;

@Entity
@Table(name="Contact")
@Getter
@Setter
public class Contact {

    @Id
    private Long id;

    @Size(max = 255)
    private String name = "";

    @Size(max = 255)
    private String attention = "";

    @Size(max = 255)
    private String emails;

    @Size(max = 255)
    private String phones;

    @Size(max = 255)
    private String faxes;

    @Size(max = 255)
    private String disciplines;

    @Size(max = 255)
    private String cpv;

    @ManyToOne
    @JoinColumn(name="government", referencedColumnName="id")
    private Government government;

    // TODO JAL: TODO JAL: ** REFACTOR ** setName logic must be in another class
    public void setName(String name) {
        if (name != null && name.length() > 254) name = name.substring(0,254);
        this.name = name;
    }

    // TODO JAL: TODO JAL: ** REFACTOR ** setAttention logic must be in another class
    public void setAttention(String attention) {
        if (attention != null && attention.length() > 254) attention = attention.substring(0,254);
        this.attention = attention;
    }

    // TODO JAL: TODO JAL: ** REFACTOR ** setEmails logic must be in another class
    public void setEmails(String emails) {
        while (emails != null && emails.length() > 254)
            emails = emails.substring(emails.indexOf(";")+1);
        this.emails = emails;
    }

    // TODO JAL: TODO JAL: ** REFACTOR ** setPhones logic must be in another class
    public void setPhones(String phones) {
        if (phones != null && phones.length() > 254) phones = phones.substring(0,254);
        this.phones = phones;
    }

    // TODO JAL: TODO JAL: ** REFACTOR ** setFaxes logic must be in another class
    public void setFaxes(String faxes) {
        if (faxes != null && faxes.length() > 254) faxes = faxes.substring(0,254);
        this.faxes = faxes;
    }

    // TODO JAL: TODO JAL: ** REFACTOR ** setDisciplines logic must be in another class
    public void setDisciplines(String disciplines) {
        if (disciplines != null && disciplines.length() > 254) disciplines = disciplines.substring(0,254);
        this.disciplines = disciplines;
    }

    // TODO JAL: TODO JAL: ** REFACTOR ** setCpv logic must be in another class
    public void setCpv(String cpv) {
        if (cpv != null && cpv.length() > 254) cpv = cpv.substring(0,254);
        this.cpv = cpv;
    }

    public boolean equals(Object other) {
        Contact otherContact = (Contact) other;
        if (other == null) return false;
        return getName().equals(otherContact.getName());
    }

    public String toString() {
        return name + " "+ attention + " "+emails+ " "+phones+ " "+faxes;
    }
}
