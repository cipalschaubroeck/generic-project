package com.cipalschaubroeck.cstender.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="City")
@Getter
@Setter
public class City {

    @Id
    private Long id;

    private String name;

    private String nameNL;

    private String nameFR;

    private String nameEN;

    private String nameDE;

    private String zipcode;

    @ManyToOne
    @JoinColumn(name="regio", referencedColumnName="id")
    private Regio regio;
}
