package com.cipalschaubroeck.cstender.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name="Filter")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@Getter
@Setter
public abstract class Filter implements Serializable {

    @Id
    private Long id;

    private String name = "";
    private String searchString = "";
    private Boolean sendEmail = false;
    protected Date lastPushMailFrom;
    protected Date lastPushMailTo;

    @Transient
    boolean selected = false;

    @Transient
    String description = null;

    @ManyToOne
    @JoinColumn(name="user", referencedColumnName="id")
    private User user;

    public boolean equals(Object other) {
        boolean equal = false;
        if (other != null && other instanceof Filter) {
            Filter otherFilter = (Filter) other;
            if (otherFilter.getName().equals(getName())
                    && otherFilter.getSearchString().equals(getSearchString())
                    && otherFilter.getSendEmail().equals(getSendEmail())
            ) equal = true;
        }
        return equal;
    }

    public Filter(String name, String searchString) {
        this.name = name;
        this.searchString = searchString;
    }

    public Filter() {}

    public void init(Filter filter) {
        id = filter.getId();
        name = filter.getName();
        searchString = filter.getSearchString();
        sendEmail = filter.getSendEmail();
        lastPushMailFrom = filter.getLastPushMailFrom();
        lastPushMailTo = filter.getLastPushMailTo();
        user = filter.getUser();
    }

    /**
     *
     * @param searchItem
     * @return true if the searchString contains the word searchItem (case insensitive)
     */
    public boolean containsSearchItem(String searchItem) {
        String tmp = (" "+searchString+" ").toLowerCase();
        if (tmp.indexOf(" "+searchItem.toLowerCase()+" ")!= -1) return true;
        return false;
    }

    /**
     * Removes the searchItem form the searchString
     * @param searchItem
     * @return true is something is actually removed from the searchString
     */
    public boolean removeSearchItem(String searchItem) {
        String tmp = (" "+searchString+" ").toLowerCase();
        String tmp2 = (" "+searchString+" ");
        int pos = tmp.indexOf(" "+searchItem.toLowerCase()+" ");
        if (pos != -1) {
            searchString = (tmp2.substring(0,pos)+tmp2.substring(pos+searchItem.length()+2)).trim();
            return true;
        }
        return false;
    }
    /**
     * Add a searchItem to the filter.
     * @param searchItem
     */
    public void addSearchItem(String searchItem) {
        searchString += " "+searchItem;
        searchString = searchString.trim();
    }

    @Transient
    public String getDisplayName() {
        if (getDescription() != null && !selected)
            return getDescription() + " " + getSearchString();
        else if (getName().length() > 0 && !selected)
            return getName() + ": " + getSearchString();
        else
            return getSearchString();
    }

    @Transient
    public String getShortDisplayName() {
        if (getDescription() != null)
            return getDescription() + " " + getSearchString();
        else if (getName().length() > 0)
            return getName() + ": " + getSearchString();
        else
            return getSearchString();
    }
}
