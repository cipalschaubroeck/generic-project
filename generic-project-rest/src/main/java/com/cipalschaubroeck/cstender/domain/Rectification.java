package com.cipalschaubroeck.cstender.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import java.util.Collection;
import java.util.Date;
import java.util.Vector;

@Entity
@Table(name="Rectification")
@Getter
@Setter
public class Rectification implements Comparable<Rectification> {

    @Id
    private Long id;

    @OneToMany(mappedBy = "rectification", cascade = CascadeType.REMOVE, orphanRemoval=true)
    private Collection<NoticeText> rectificationTexts = new Vector<>();

    @ManyToOne
    @JoinColumn(name="tender", referencedColumnName="id")
    private Tender tender;
    @Size(max = 40)
    private String attachmentName1;
    @Size(max = 40)
    private String attachmentName2;
    @Size(max = 40)
    private String attachmentName3;
    @Size(max = 40)
    private String attachmentName4;

    @Enumerated(EnumType.ORDINAL)
    private Type type = Type.CORRECTION;

    private Date publicationDate;

    // TODO JAL: ** REFACTOR ** setAttachmentName logic must be in another class
    public void setAttachmentName(String language, String attachmentName) {
        for (int i = 0; i < Tender.LANGUAGE_BE.length; i++) {
            if (i == 0 && Tender.LANGUAGE_BE[i].equals(language))
                attachmentName1 = attachmentName;
            else if (i == 1 && Tender.LANGUAGE_BE[i].equals(language))
                attachmentName2 = attachmentName;
            else if (i == 2 && Tender.LANGUAGE_BE[i].equals(language))
                attachmentName3 = attachmentName;
            else if (i == 3 && Tender.LANGUAGE_BE[i].equals(language))
                attachmentName4 = attachmentName;
        }
    }

    // TODO JAL: ** REFACTOR ** getAttachmentName logic must be in another class
    public String getAttachmentName(String language) {
        for (int i = 0; i < Tender.LANGUAGE_BE.length; i++) {
            if (i == 0 && Tender.LANGUAGE_BE[i].equals(language))
                return attachmentName1;
            else if (i == 1 && Tender.LANGUAGE_BE[i].equals(language))
                return attachmentName2;
            else if (i == 2 && Tender.LANGUAGE_BE[i].equals(language))
                return attachmentName3;
            else if (i == 3 && Tender.LANGUAGE_BE[i].equals(language))
                return attachmentName4;
        }
        return null;
    }

    // TODO JAL: ** REFACTOR ** getAttachmentName logic must be in another class
    public String getAttachmentName() {
        String t = getAttachmentName(tender.getLanguage());
        if (t == null) {
            if (attachmentName1 != null && attachmentName1.length() > 0)
                t = attachmentName1;
            else if (attachmentName2 != null && attachmentName2.length() > 0)
                t = attachmentName2;
            else if (attachmentName3 != null && attachmentName3.length() > 0)
                t = attachmentName3;
            else if (attachmentName4 != null && attachmentName4.length() > 0)
                t = attachmentName4;
        }
        return t;
    }

    // TODO JAL: ** REFACTOR ** getAttachmentPdfName logic must be in another class
    public String getAttachmentPdfName() {
        String n = getAttachmentName();
        if (n != null && n.endsWith(".xml")) n = n.substring(0,n.length()-3) + "pdf";
        return n;
    }

    @Override
    public int compareTo(Rectification o) {
        if (o.getPublicationDate() == null && getPublicationDate() == null) return 0;
        if (o.getPublicationDate() != null && getPublicationDate() == null) return -1;
        if (o.getPublicationDate() == null && getPublicationDate() != null) return 1;

        return getPublicationDate().compareTo(o.getPublicationDate());
    }
}
