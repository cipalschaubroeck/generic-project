package com.cipalschaubroeck.cstender.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name="FilterTender")
@Getter
@Setter

// TODO JAL: ** REFACTOR** move this query
@NamedQueries({
        @NamedQuery(name = "FilterTender.delete", query = "DELETE FROM FilterTender e WHERE e.tenderFilter = :tenderFilter"),
})
public class FilterTender {

    @Id
    private Long id;

    @ManyToOne
    @JoinColumn(name = "tender", referencedColumnName = "id")
    private Tender tender;

    @ManyToOne
    @JoinColumn(name="filter", referencedColumnName="id")
    private TenderFilter tenderFilter;
}
