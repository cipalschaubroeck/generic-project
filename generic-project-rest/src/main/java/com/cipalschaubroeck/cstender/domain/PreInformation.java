package com.cipalschaubroeck.cstender.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import java.util.Collection;
import java.util.Date;
import java.util.Vector;

@Entity
@Table(name="PreInformation")
@Getter
@Setter
public class PreInformation {

    @Id
    private Long id;

    @OneToMany(mappedBy = "preInformation")
    private Collection<NoticeText> preInformationTexts = new Vector<>();;

    @OneToOne(mappedBy = "preInformation")
    private Tender tender;

    private String form;

    private Date publication;

    @Size(max = 40)
    private String attachmentName1;
    @Size(max = 40)
    private String attachmentName2;
    @Size(max = 40)
    private String attachmentName3;
    @Size(max = 40)
    private String attachmentName4;

    // TODO JAL: ** REFACTOR ** getNoticeText logic must be in another class
    public NoticeText getNoticeText(String language) {
        Collection<NoticeText> noticeTexts = getPreInformationTexts();
        NoticeText text = null;
        if (noticeTexts != null)
            for (NoticeText noticeText : noticeTexts) {
                if (language.equals(noticeText.getLanguage())) {
                    return noticeText;
                }
            }
        return text;
    }

    // TODO JAL: ** REFACTOR ** setAttachmentName logic must be in another class
    public void setAttachmentName(String language, String attachmentName) {
        for (int i = 0; i < Tender.LANGUAGE_BE.length; i++) {
            if (i == 0 && Tender.LANGUAGE_BE[i].equals(language))
                attachmentName1 = attachmentName;
            else if (i == 1 && Tender.LANGUAGE_BE[i].equals(language))
                attachmentName2 = attachmentName;
            else if (i == 2 && Tender.LANGUAGE_BE[i].equals(language))
                attachmentName3 = attachmentName;
            else if (i == 3 && Tender.LANGUAGE_BE[i].equals(language))
                attachmentName4 = attachmentName;
        }
    }

    // TODO JAL: ** REFACTOR ** getAttachmentName logic must be in another class
    public String getAttachmentName() {
        return getAttachmentName(tender.getLanguage());
    }

    // TODO JAL: ** REFACTOR ** getAttachmentName logic must be in another class
    public String getAttachmentName(String language) {
        for (int i = 0; i < Tender.LANGUAGE_BE.length; i++) {
            if (i == 0 && Tender.LANGUAGE_BE[i].equals(language))
                return attachmentName1;
            else if (i == 1 && Tender.LANGUAGE_BE[i].equals(language))
                return attachmentName2;
            else if (i == 2 && Tender.LANGUAGE_BE[i].equals(language))
                return attachmentName3;
            else if (i == 3 && Tender.LANGUAGE_BE[i].equals(language))
                return attachmentName4;
        }
        return null;
    }

    // TODO JAL: ** REFACTOR ** getAttachmentPdfName logic must be in another class
    public String getAttachmentPdfName() {
        String n = getAttachmentName();
        if (n != null && n.endsWith(".xml")) n = n.substring(0,n.length()-3) + "pdf";
        return n;
    }
}
