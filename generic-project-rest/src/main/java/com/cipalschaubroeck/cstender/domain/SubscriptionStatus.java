package com.cipalschaubroeck.cstender.domain;

public enum SubscriptionStatus {
    OK,
    STOP
}
