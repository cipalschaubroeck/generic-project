package com.cipalschaubroeck.cstender.domain;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue(value = "UserFilter")
public class UserFilter extends Filter {
    public UserFilter(String name, String searchString) {
        super(name, searchString);
    }
    public UserFilter() {
        super();
    }
}
