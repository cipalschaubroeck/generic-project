package com.cipalschaubroeck.cstender.domain;

import com.cipalschaubroeck.oldcstender.shared.Util;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedAttributeNode;
import javax.persistence.NamedEntityGraph;
import javax.persistence.NamedEntityGraphs;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name="Government")
@Getter
@Setter

// TODO JAL: ** REFACTOR** move this query
@NamedNativeQueries({
        @NamedNativeQuery(name = "Government.findByQuery1", query = "SELECT * "
                + " FROM Government WHERE MATCH (reverseIndex) AGAINST (:searchString IN BOOLEAN MODE) order by name asc", resultClass = Government.class)

})

// TODO JAL: ** REFACTOR** move this query
@NamedQueries({
        @NamedQuery(name = "Government.findByNameLocation", query = "SELECT g FROM Government g WHERE UPPER(g.name) = :name AND g.location = :location"),
        @NamedQuery(name = "Government.findAll", query = "SELECT g FROM Government g")
})

// TODO JAL: ** REFACTOR** move this grapth
@NamedEntityGraphs({
        @NamedEntityGraph(name = "Government.tenders", attributeNodes = {
                @NamedAttributeNode("id"),
                @NamedAttributeNode("name"),
                @NamedAttributeNode("type"),
                @NamedAttributeNode("street"),
                @NamedAttributeNode("location"),
                @NamedAttributeNode("disciplines"),
                @NamedAttributeNode("cpv"),
                @NamedAttributeNode("tenders"),
                @NamedAttributeNode("contacts")
        }),
})

public class Government {

    // TODO JAL: ** REFACTOR ** convert into a enum
    public static final String[] ACTIVITY = {"GENERAL_PUBLIC_SERVICES",
            "SOCIAL_PROTECTION","EDUCATION","HEALTH","ENVIRONMENT","PUBLIC_ORDER_AND_SAFETY",
            "HOUSING_AND_COMMUNITY_AMENITIES","DEFENCE","ECONOMIC_AND_FINANCIAL_AFFAIRS","RECREATION_CULTURE_AND_RELIGION"};

    // TODO JAL: ** REFACTOR ** convert into a enum
    public static final String[] TYPES = {"city","police","railway","firedep","hospital","school","university","army","ocmw","traffic","building"};

    // TODO JAL: ** REFACTOR ** convert into a enum
    public static final String[][] TYPES_KEYS = {
            {"gemeente","gemeentebestuur","stad","stadsbestuur","ville d","commune d","administration communale","college van burgemeester"},
            {"politiezone","police","politie"},
            {"infrabel","sncb","nmbs"},
            {"brandweer"},
            {"ziekenhuis","ziekenhuizen","az","hospitalier","hospitalière","neurologique","clinique","cliniques"},
            {"ecole","scolaire","school","scholen","onderwijs","scholengroep","schoolbestuur","basisscholen","basisonderwijs","kleuterschool"},
            {"hogeschool","k.u.leuven","ku","ku leuven","universiteit"},
            {"defence","defensie"},
            {"ocmw","centre public d'action sociale","cpas","o.c.m.w.","c.p.a.s."},
            {"wegen en verkeer","vervoermaatschappij","mobiliteitsbedrijf","direction des routes","direction de la coordination des districts routiers","direction de la gestion du trafic routier"},
            {"regie der gebouwen","woningbeheer","woningfonds","régie des bâtiments","deprecatedservice bâtiments","deprecatedservice technique des bâtiments","sociale woningen","habitations sociales"}
    };

    @Id
    private Long id;

    @Size(max = 255)
    private String name;

    @Size(max = 11)
    private String type;

    @Size(max = 255)
    private String street;

    @Size(max = 255)
    private String location;

    @Size(max = 255)
    private String disciplines;

    @Size(max = 255)
    private String activity;

    @Size(max = 255)
    private String cpv;

    @Lob
    private String reverseIndex;

    @OneToMany(mappedBy = "government")
    private Set<Tender> tenders;

    @OneToMany(mappedBy = "government", cascade = {CascadeType.REMOVE,CascadeType.PERSIST}, orphanRemoval=true)
    private Set<Contact> contacts = new HashSet<>();

    @Transient
    private String locationString;

    @Transient
    private String typeString;

    @PreUpdate
    @PrePersist
    public void updateReverseIndex() {
        StringBuilder contactBuilder = new StringBuilder();
        if (getContacts() != null)
            for (Contact contact : getContacts()) {
                contactBuilder.append(contact.toString());
            }
        reverseIndex = Util.constructDBIndex(name + " " + street + " "+contactBuilder.toString())+" "+location+ (type != null ? " "+type : "");
    }

    // TODO JAL: TODO JAL: ** REFACTOR ** setName logic must be in another classv
    public void setName(String name) {
        if (name != null && name.length() > 254) name = name.substring(0,254);
        this.name = name;
    }

    // TODO JAL: TODO JAL: ** REFACTOR ** setStreet logic must be in another class
    public void setStreet(String street) {
        if (street != null && street.length() > 254) street = street.substring(0,254);
        this.street = street;
    }

    // TODO JAL: TODO JAL: ** REFACTOR ** setLocation logic must be in another class
    public void setLocation(String location) {
        if (location != null && location.length() > 254) location = location.substring(0,254);
        this.location = location;
    }

    // TODO JAL: TODO JAL: ** REFACTOR ** setDisciplines logic must be in another class
    public void setDisciplines(String disciplines) {
        if (disciplines != null && disciplines.length() > 254) disciplines = disciplines.substring(0,254);
        this.disciplines = disciplines;
    }

    // TODO JAL: TODO JAL: ** REFACTOR ** setCpv logic must be in another class
    public void setCpv(String cpv) {
        if (cpv != null && cpv.length() > 254) cpv = cpv.substring(0,254);
        this.cpv = cpv;
    }

    // TODO JAL: TODO JAL: ** REFACTOR ** setActivity logic must be in another class
    public void setActivity(String activity) {
        if (activity != null && activity.length() > 254) activity = activity.substring(0,254);
        this.activity = activity;
    }
}
