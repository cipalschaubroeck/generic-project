package com.cipalschaubroeck.cstender.domain;

public enum Type {
    CORRECTION,
    ADDITIONAL_INFO,
    INCOMPLETE_PROCEDURE
}
