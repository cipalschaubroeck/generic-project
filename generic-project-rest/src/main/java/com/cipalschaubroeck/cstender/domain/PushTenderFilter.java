package com.cipalschaubroeck.cstender.domain;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.NamedAttributeNode;
import javax.persistence.NamedEntityGraph;
import javax.persistence.NamedEntityGraphs;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.NamedSubgraph;
import javax.persistence.Transient;
import java.text.DateFormat;

@NamedEntityGraphs({
        @NamedEntityGraph(
                name = "PushTenderFilter.filterTenders",
                includeAllAttributes=true,
                subgraphs = {
                        @NamedSubgraph(
                                name = "filterTenders",
                                type = TenderFilter.class,
                                attributeNodes = {
                                        @NamedAttributeNode(value = "id"),
                                        @NamedAttributeNode(value = "tender")
                                }
                        )
                }

        )
})


@NamedQueries({
        @NamedQuery(name = "PushTenderFilter.findByUserAndFiltername", query = "SELECT f FROM PushTenderFilter f WHERE f.user.id = :userId AND f.name = :filtername"),
})

@Entity
@DiscriminatorValue(value = "PushTenderFilter")
public class PushTenderFilter extends TenderFilter {
    public PushTenderFilter(String name, String searchString) {
        super(name, searchString);
    }
    public PushTenderFilter() {
        super();
    }

    @Transient
    @Override
    public String getDisplayName() {
        String from = " ";
        String to = " ";
        if (lastPushMailFrom != null) from =  DateFormat.getDateInstance(DateFormat.SHORT).format(lastPushMailFrom);
        if (lastPushMailTo != null) to =  DateFormat.getDateInstance(DateFormat.SHORT).format(lastPushMailTo);
        return "Email - " + getName() + ": "+from+" - "+to+ " : "+ getSearchString();
    }
}
