package com.cipalschaubroeck.cstender.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="AwardItem")
@Getter
@Setter
public class AwardItem {

    @Id
    private Long id;

    @ManyToOne(fetch= FetchType.EAGER)
    @JoinColumn(name="award", referencedColumnName="id")
    private Award award;

    @ManyToOne
    @JoinColumn(name="contractor", referencedColumnName="id")
    private Contractor contractor;

    private String number;

    private String title;

    private String contractorName;

    private String value;

    private String currency;

    private boolean exclusiveVat;

    @Transient
    private String displayTitle;

    // TODO JAL: ** REFACTOR ** setNumber logic must be in another class
    public void setNumber(String number) {
        if (number.length() > 254) number = number.substring(0,254);
        this.number = number;
    }

    // TODO JAL: ** REFACTOR ** setTitle logic must be in another class
    public void setTitle(String title) {
        if (title.length() > 254) title = title.substring(0,254);
        this.title = title;
    }
}
