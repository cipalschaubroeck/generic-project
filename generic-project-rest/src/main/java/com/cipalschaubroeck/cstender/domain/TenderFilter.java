package com.cipalschaubroeck.cstender.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.util.HashSet;
import java.util.Set;

@Entity
@DiscriminatorValue(value = "TenderFilter")
@Getter
@Setter
public class TenderFilter extends Filter {

    public TenderFilter(String name, String searchString) {
        super(name, searchString);
    }

    public TenderFilter() {
        super();
    }

    @OneToMany(mappedBy = "tenderFilter")
    private Set<FilterTender> filterTenders = new HashSet<>();
}
