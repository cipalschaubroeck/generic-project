package com.cipalschaubroeck.cstender.configuration.swagger;

import com.fasterxml.classmate.TypeResolver;
import io.swagger.annotations.ApiModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.schema.ModelBuilderPlugin;
import springfox.documentation.spi.schema.contexts.ModelContext;
import springfox.documentation.spring.web.DescriptionResolver;
import springfox.documentation.swagger.common.SwaggerPluginSupport;

import static springfox.documentation.swagger.common.SwaggerPluginSupport.pluginDoesApply;

/**
 * This replaces ApiModelBuilder to allow use properties for ApiModel annotation
 */
@Component
@Order(SwaggerPluginSupport.SWAGGER_PLUGIN_ORDER + 1)
public class PropertyApiModelBuilder implements ModelBuilderPlugin {
    private final TypeResolver typeResolver;
    private final DescriptionResolver descriptions;

    @Autowired
    public PropertyApiModelBuilder(TypeResolver typeResolver, DescriptionResolver descriptions) {
        this.typeResolver = typeResolver;
        this.descriptions = descriptions;
    }

    @Override
    public void apply(ModelContext context) {
        ApiModel annotation = AnnotationUtils.findAnnotation(
                typeResolver.resolve(context.getType()).getErasedType(),
                ApiModel.class);
        if (annotation != null) {
            context.getBuilder()
                    .description(descriptions.resolve(annotation.description()))
                    .name(descriptions.resolve(annotation.value()));
        }
    }

    @Override
    public boolean supports(DocumentationType delimiter) {
        return pluginDoesApply(delimiter);
    }
}
