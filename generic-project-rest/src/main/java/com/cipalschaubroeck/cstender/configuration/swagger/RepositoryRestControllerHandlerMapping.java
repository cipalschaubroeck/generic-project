package com.cipalschaubroeck.cstender.configuration.swagger;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.annotation.AnnotatedElementUtils;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

// TODO: when springfox data rest includes RepositoryRestController, this class won't be necessary anymore
public class RepositoryRestControllerHandlerMapping extends RequestMappingHandlerMapping {

    @Value("${spring.data.rest.base-path}")
    private String prefix;

    @Override
    public void afterPropertiesSet() {
        if (StringUtils.isNotBlank(prefix)) {
            getPathPrefixes().put(prefix, this::isHandler);
        }
        super.afterPropertiesSet();
    }

    @Override
    protected boolean isHandler(Class<?> beanType) {
        return AnnotatedElementUtils.hasAnnotation(beanType, RepositoryRestController.class);
    }
}
