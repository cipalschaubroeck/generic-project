package com.cipalschaubroeck.cstender.configuration;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
@Profile("xperido")
@ComponentScan(basePackageClasses = com.greenvalley.docgen.client.DocumentHandlerService.class)
public class XPeridoConfig {
}
