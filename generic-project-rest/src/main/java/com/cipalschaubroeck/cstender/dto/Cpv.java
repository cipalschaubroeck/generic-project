package com.cipalschaubroeck.cstender.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
// TODO JAL: ** REFACTOR ** Cpv class and Category are the same
public class Cpv {
    private String code;
    private String name;
    private int occurance = 1;

    public Cpv(String code, String name,int occurance) {
        super();
        this.code = code;
        this.name = name;
        this.occurance = occurance;
    }

    public void addOccurance() {
        occurance++;
    }

    public boolean equals(Object other) {
        if (other == null) return false;
        return code.equals(((Cpv)other).code);
    }
}
