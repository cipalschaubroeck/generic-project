package com.cipalschaubroeck.cstender.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Category {
    private String code;
    private String name;
    private int occurance = 1;

    public Category(final String code, final String name, final int occurance) {
        this.code = code;
        this.name = name;
        this.occurance = occurance;
    }

    public void addOccurance() {
        occurance++;
    }

    public int hashCode() {
        return code.hashCode();
    }

    public boolean equals(Object other) {
        if (other == null) return false;
        return code.equals(((Category)other).code);
    }
}
