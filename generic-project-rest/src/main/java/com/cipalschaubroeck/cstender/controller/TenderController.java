package com.cipalschaubroeck.cstender.controller;

import com.cipalschaubroeck.cstender.service.ImportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TenderController {

    @Autowired
    private ImportService importService;


    @RequestMapping(value="/importFiles")
    public ResponseEntity<Object> importTenderFolderDirectFromZip() {
        importService.importTenderFolderDirectFromZip();
        return new ResponseEntity<>("Done!", HttpStatus.OK);
    }
}
