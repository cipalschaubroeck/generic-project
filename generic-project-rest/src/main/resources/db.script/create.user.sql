-- TODO Provide user name instead of user_generic_project
-- TODO Replace PASSWORD
CREATE USER 'user_tender'@'localhost' IDENTIFIED BY 'tender';

-- TODO Provide schema name instead of cstender
GRANT CREATE, ALTER, SELECT, DROP, UPDATE, INSERT, DELETE ON generic_project.* TO 'user_tender'@'localhost';
