#
# Copyright © Sygel - All Rights Reserved
# Unauthorized copying of this file, via any medium is strictly prohibited
# Unauthorized use of this source code is strictly prohibited
# Proprietary and confidential
#
TYPE_SERVICES:79500000 Ondersteuning voor kantoorwerk
TYPE_SERVICES:79600000 Recruteringsdiensten
TYPE_SERVICES:79721000 Diensten van detectivebureau
TYPE_SERVICES:79722000 Grafologische diensten
TYPE_SERVICES:85140000 Diverse gezondheidszorgdiensten
TYPE_SERVICES:79920000 Verpakkings- en aanverwante diensten
TYPE_SERVICES:79950000 Organiseren van tentoonstellingen, beurzen en congressen
TYPE_SERVICES:79960000 Fotografische en aanvullende diensten
TYPE_SERVICES:79972000 Diensten voor publicatie van woordenboeken
TYPE_SERVICES:79980000 Abonnementsdiensten
TYPE_SERVICES:79990000 Diverse zakelijke diensten
TYPE_SERVICES:79000000 Zakelijke dienstverlening: juridisch, marketing, consulting, drukkerij en beveiliging
TYPE_SERVICES:33610000 Geneesmiddelen voor maagdarmkanaal en metabolisme
TYPE_SERVICES:33620000 Geneesmiddelen voor het bloed, de bloedvormende organen en het hartvaatstelsel
TYPE_SERVICES:33630000 Geneesmiddelen voor dermatologie, spierstelsel en skelet
TYPE_SERVICES:33640000 Geneesmiddelen voor het urogenitaal stelsel en hormonen
TYPE_SERVICES:33650000 Algemene infectiewerende middelen voor systemisch gebruik, vaccins, antineoplastica en immunomodulerende middelen
TYPE_SERVICES:33660000 Geneesmiddelen voor zenuwstelsel en zintuigelijke organen
TYPE_SERVICES:33670000 Geneesmiddelen voor het ademhalingssysteem
TYPE_SERVICES:33680000 Farmaceutische artikelen
TYPE_SERVICES:33690000 Diverse geneesmiddelen
TYPE_SERVICES:33710000 Parfums, toiletartikelen en condooms
TYPE_SERVICES:33720000 Scheerapparaten en sets voor manicure of pedicure
TYPE_SERVICES:33730000 Oogverzorgingsproducten en contactlenzen
TYPE_SERVICES:33740000 Producten voor hand- en nagelverzorging
TYPE_SERVICES:33750000 Babyverzorgingsproducten
TYPE_SERVICES:33760000 Toiletpapier, zakdoekjes, handdoeken en servetten
TYPE_SERVICES:33770000 Papieren hygiëneartikelen
TYPE_SERVICES:33790000 Glaswerk voor laboratoria, voor hygiënisch of farmaceutisch gebruik
TYPE_SERVICES:33140000 Medische verbruiksartikelen
TYPE_SERVICES:37535200 speeltuinuitrusting
TYPE_SERVICES:70310000 verhuur of verkoop van gebouwen
TYPE_SERVICES:34942000 signalisatie-uitrusting
TYPE_SERVICES:39100000 Meubilair
TYPE_SERVICES:33192300 Medisch meubilair, met uitzondering van bedden en tafels
TYPE_SERVICES:33192120 Ziekenhuisbedden
TYPE_SERVICES:80530000 Beroepsopleidingsdiensten
TYPE_SERVICES:79992000 - Receptiediensten
TYPE_SERVICES:45343200 - Installeren van brandblusinstallaties
TYPE_SERVICES:35111000 - Brandblusuitrusting
TYPE_SERVICES:50413200 - Reparatie en onderhoud van brandblusinstallaties
TYPE_SERVICES:79710000 - Beveiligingsdiensten
TYPE_WORKS:35111500 Brandbestrijdingssysteem Percent : 100 Info : 1/1/null/1.0
TYPE_SERVICES:79710000 Beveiligingsdiensten Percent : 75 Info : 3/2/1/1.0
TYPE_SERVICES:32354110 Röntgenfilm  /  NE_röntgenfilm
TYPE_SERVICES:32354120 Blauwediazofilm  /  NE_blauwediazofilm
TYPE_SERVICES:32354100 Film voor radiologie  /  NE_film NE_radiologie
TYPE_SERVICES:31532910 Fluorescentiebuizen  /  NE_fluorescentiebuis
TYPE_SERVICES:45343100 45343000 Aanbrengen van brandwering  /  REL_aanbrengen_brandwering NE_brandwering
TYPE_SERVICES:44482200 44482000 Brandkranen  /  NE_brandkraan
TYPE_SERVICES:44482100 44482000 Brandslangen  /  NE_brandslang
TYPE_SERVICES:35121000 beveiligingsuitrusting
TYPE_WORKS:35120000 Bewakings- en beveiligingssystemen en ?apparatuur Percent : 67 Info : 2/null/1/0.0
TYPE_WORKS:79512000 telefonisch informatiecentrum
TYPE_WORKS:80410000 diverse schooldiensten