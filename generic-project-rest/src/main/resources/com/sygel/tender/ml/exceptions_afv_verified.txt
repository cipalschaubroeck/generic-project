#
# Copyright © Sygel - All Rights Reserved
# Unauthorized copying of this file, via any medium is strictly prohibited
# Unauthorized use of this source code is strictly prohibited
# Proprietary and confidential
#
TYPE_SERVICES:90511200 Ophalen van huisvuil Percent : 100 Info : 2/1/null/null
TYPE_SERVICES:90700000 Milieuzorgdiensten
TYPE_SERVICES:90513400 Diensten voor het verwerken van as Percent : 100 Info : 3/3/null/null
TYPE_SERVICES:90733300 Diensten voor bescherming van oppervlaktewater tegen vervuiling Percent : 100 Info : 1/null/null/null
TYPE_SERVICES:90733200 Opruimdiensten voor vervuild oppervlaktewater Percent : 100 Info : 1/null/null/null
TYPE_SERVICES:34928480 Afval- en vuilcontainers en bakken Percent : 100 Info : 1/null/null/null
TYPE_SUPPLIES:90513100 Verwijderen van huisvuil Percent : 100 Info : 1/1/null/null
TYPE_SERVICES:90524400 Ophalen, vervoeren en verwerken van ziekenhuisafval Percent : 100 Info : 1/null/null/null
TYPE_SUPPLIES:90900000 Schoonmaak- en afvalverwijderingsdiensten Percent : 100 Info : 1/1/null/null
TYPE_SUPPLIES:39713300 Afvalpersen Percent : 100 Info : 2/1/null/null
TYPE_SUPPLIES:90514000 Diensten voor afvalrecycling Percent : 100 Info : 1/1/null/null
TYPE_SERVICES:90440000 Diensten voor behandeling van beerputten Percent : 100 Info : 1/null/null/null
TYPE_SUPPLIES:39234000 Compostcontainers Percent : 100 Info : 2/2/null/null
TYPE_SUPPLIES:19640000 Afval- en vuilniszakken van polytheen Percent : 100 Info : 14/14/null/null
TYPE_SERVICES:90513000 Diensten voor het verwerken en storten van ongevaarlijk afval en vuilnis Percent : 100 Info : 5/5/null/null
TYPE_SERVICES:90510000 Afvalverzameling en ?verwerking Percent : 96 Info : 25/18/1/null
TYPE_SERVICES:90511000 Diensten voor ophalen van vuilnis Percent : 88 Info : 7/5/1/null
TYPE_SERVICES:90514000 Diensten voor afvalrecycling Percent : 80 Info : 4/3/1/null
TYPE_SERVICES:90520000 Diensten op het gebied van radioactief, toxisch, medisch en gevaarlijk afval Percent : 75 Info : 6/6/2/null
TYPE_SUPPLIES:39224340 Afvalbakken Percent : 75 Info : 3/2/1/null
TYPE_SERVICES:90512000 Diensten voor afvalvervoer Percent : 75 Info : 9/7/3/null
TYPE_SERVICES:90500000 Diensten op het gebied van vuilnis en afval Percent : 74 Info : 17/11/6/null
TYPE_SERVICES:90511100 Ophalen van vast stadsafval Percent : 67 Info : 2/2/1/null
TYPE_SUPPLIES:90510000 Afvalverzameling en ?verwerking Percent : 50 Info : 2/2/2/null
TYPE_SUPPLIES:34928480 Afval- en vuilcontainers en bakken Percent : 50 Info : 3/2/3/null
TYPE_SUPPLIES:44613800 Containers voor afvalmateriaal Percent : 40 Info : 2/null/3/null
TYPE_SERVICES:90900000 Schoonmaak- en afvalverwijderingsdiensten Percent : 40 Info : 2/2/3/null
TYPE_SERVICES:90511300 Diensten voor het verzamelen van straatafval Percent : 25 Info : 1/null/3/null
TYPE_SUPPLIES:34144512 Vuilniscompacteerwagens Percent : 17 Info : 1/1/5/null
TYPE_SERVICES:90610000 Straatreinigings- en veegdiensten Percent : 15 Info : 2/1/11/null
TYPE_SUPPLIES:34144430 Straatveegvoertuigen Percent : 100 Info : 13/7/null/null
TYPE_SUPPLIES:34144431 Veegzuigvoertuigen Percent : 100 Info : 6/4/null/null
TYPE_SUPPLIES:44616200 Afvalvaten Percent : 100 Info : 1/null/null/null
TYPE_SUPPLIES:90612000 Straatveegdiensten Percent : 100 Info : 1/1/null/null
TYPE_SUPPLIES:19600000 Afval van leer, textiel, rubber en kunststof Percent : 100 Info : 1/null/null/null
TYPE_SUPPLIES:34921100 Straatveegmachines Percent : 96 Info : 27/21/1/null
TYPE_SERVICES:90450000 Diensten voor behandeling van septische putten Percent : 50 Info : 1/null/1/null
TYPE_SUPPLIES:90533000 Beheer van afvalstortplaats Percent : 100 Info : 1/1/null/null
TYPE_SERVICES:90521500 Verpakken van radioactief afval
TYPE_SERVICES:90521510 Verpakken van lichtradioactief afval
TYPE_SERVICES:90521520 Verpakken van middelradioactief afval
TYPE_SERVICES:90000000 Diensten inzake afvalwater, afval, reiniging en milieu
TYPE_SERVICES:42968200 Automaten voor hygiënische benodigdheden
TYPE_SERVICES:44613700 Vuilnisbakken
TYPE_SERVICES:03416000 Houtafval
TYPE_SERVICES:90400000 diensten in verband met afvalwater
TYPE_SERVICES:90731400 Diensten voor toezicht op of meting van luchtvervuiling
TYPE_SERVICES:34144500 Voertuigen voor werkzaamheden in verband met vast afval en afvalwater
TYPE_SERVICES:90910000 Reinigingsdiensten
TYPE_WORKS:90600000 Verwijderen, schoonmaken en ontsmetten binnen een stedelijke en/of landelijke omgeving Percent : 100 Info : 1/null/null/null
TYPE_SERVICES:79723000 Diensten voor afvalanalyse
TYPE_SERVICES:45222100 bouwen van afvalverwerkingsinstallatie
TYPE_SUPPLIES:33770000 Papieren hygi�neartikelen Percent : 100 Info : 1/null/null/null
TYPE_SUPPLIES:33760000 Toiletpapier, zakdoekjes, handdoeken en servetten Percent : 50 Info : 1/1/1/null
