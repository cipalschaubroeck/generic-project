#
# Copyright © Sygel - All Rights Reserved
# Unauthorized copying of this file, via any medium is strictly prohibited
# Unauthorized use of this source code is strictly prohibited
# Proprietary and confidential
#
TYPE_SERVICES:45110000 Slopen en ontmantelen van gebouwen, en grondverzet Percent : 100 Info : 1/null/null/null
TYPE_SERVICES:45262660 Verwijderen van asbest Percent : 100 Info : 1/1/null/null
TYPE_WORKS:45110000 Slopen en ontmantelen van gebouwen, en grondverzet Percent : 83 Info : 33/22/7/null
TYPE_WORKS:45262660 Verwijderen van asbest Percent : 81 Info : 17/11/4/null
TYPE_WORKS:90650000 Diensten voor het verwijderen van asbest Percent : 67 Info : 2/1/1/null
TYPE_WORKS:45111000 Sloopwerkzaamheden, bouwrijp maken en ruimen van bouwterreinen Percent : 66 Info : 23/11/12/null
TYPE_SERVICES:90650000 Diensten voor het verwijderen van asbest Percent : 50 Info : 2/2/2/null
TYPE_WORKS:45111100 Sloopwerkzaamheden Percent : 47 Info : 18/12/20/null
TYPE_WORKS:45111200 Bouwrijp maken en ruimen van bouwterreinen Percent : 40 Info : 2/1/3/null
TYPE_WORKS:45111100 Sloopwerkzaamheden
TYPE_WORKS:45111200 Bouwrijp maken en ruimen van bouwterreinen
TYPE_WORKS:45111210 Steenvrij maken door middel van explosieven en dergelijke
TYPE_WORKS:45111220 Verwijderen van begroeiing op bouwterreinen
TYPE_WORKS:50243000 Slopen van schepen
TYPE_WORKS:50229000 Slopen van rollend materieel
TYPE_WORKS:50190000 Slopen van voertuigen
TYPE_WORKS:45262110 Ontmantelen van steigers
TYPE_WORKS:45112330 Ruimen van bouwterrein