#
# Copyright © Sygel - All Rights Reserved
# Unauthorized copying of this file, via any medium is strictly prohibited
# Unauthorized use of this source code is strictly prohibited
# Proprietary and confidential
#
TYPE_SUPPLIES:32400000 Netwerken
TYPE_SUPPLIES:72900000 Diensten voor computerbackup en converteren van computercatalogus
TYPE_SUPPLIES:72300000 Uitwerken van gegevens
TYPE_SUPPLIES:72400000 Internetdiensten
TYPE_SUPPLIES:72800000 Computeraudit- en testdiensten
TYPE_SUPPLIES:72600000 Diensten voor computerondersteuning en -advies
TYPE_SUPPLIES:48100000 Speciale software voor de industrie
TYPE_SUPPLIES:48200000 Software voor netwerken, internet en intranet
TYPE_SUPPLIES:48300000 Software voor het maken van documenten, tekeningen, afbeeldingen, dienstregelingen en productiviteit
TYPE_SUPPLIES:48400000 Software voor zakelijke transacties en persoonlijke zaken
TYPE_SUPPLIES:48600000 Database- en besturingssoftware
TYPE_SUPPLIES:48700000 Softwarevoorzieningen
TYPE_SUPPLIES:48450000 Software voor tijdregistratie of human reousrces Percent : 100 Info : 1/null/null/null
TYPE_SERVICES:72263000 Software-implementatiediensten Percent : 100 Info : 1/1/null/null
TYPE_SUPPLIES:30233300 Smartcardlezers Percent : 100 Info : 1/null/null/null
TYPE_SERVICES:72210000 Programmering van softwarepakketten Percent : 100 Info : 3/null/null/null
TYPE_SUPPLIES:72212450 Diensten voor ontwikkeling van software voor tijdregistratie of personele middelen Percent : 100 Info : 1/null/null/null
TYPE_SUPPLIES:31682210 Instrumenten- en besturingsuitrusting Percent : 100 Info : 1/null/null/null
TYPE_SERVICES:72211000 Programmering van systeem- en gebruikerssoftware Percent : 100 Info : 2/null/null/null
TYPE_SERVICES:50312000 Onderhoud en reparatie van computeruitrusting Percent : 100 Info : 2/null/null/null
TYPE_SUPPLIES:72267000 Onderhouds- en reparatiediensten voor software Percent : 100 Info : 1/null/null/null
TYPE_SERVICES:72242000 Maken van ontwerp modellen Percent : 100 Info : 1/null/null/null
TYPE_SERVICES:72260000 Diensten in verband met software Percent : 100 Info : 2/null/null/null
TYPE_SERVICES:72267100 Onderhoud van software voor informatietechnologie Percent : 100 Info : 2/null/null/null
TYPE_SUPPLIES:30237000 Onderdelen, toebehoren en benodigdheden voor computers Percent : 100 Info : 1/null/null/null
TYPE_SUPPLIES:48820000 Servers Percent : 100 Info : 1/null/null/null
TYPE_SERVICES:72200000 Softwareprogrammering en -advies Percent : 100 Info : 3/1/null/null
TYPE_SUPPLIES:48822000 Computerservers Percent : 100 Info : 1/null/null/null
TYPE_SUPPLIES:48000000 Software en informatiesystemen Percent : 100 Info : 16/5/null/null
TYPE_WORKS:32420000 Netwerkuitrusting Percent : 100 Info : 1/null/null/null
TYPE_SUPPLIES:72411000 Internetproviders (ISP) Percent : 100 Info : 1/null/null/null
TYPE_SUPPLIES:64216000 Elektronische berichten- en informatiediensten Percent : 100 Info : 1/null/null/null
TYPE_SERVICES:48219300 Administratieve software Percent : 100 Info : 1/1/null/null
TYPE_SERVICES:50312610 Onderhoud van uitrusting voor informatietechnologie Percent : 100 Info : 2/null/null/null
TYPE_SUPPLIES:48160000 Software voor bibliotheek Percent : 100 Info : 1/null/null/null
TYPE_SUPPLIES:30200000 Computeruitrusting en -benodigdheden Percent : 100 Info : 3/1/null/null
TYPE_SUPPLIES:30220000 Digitalecartografie-uitrusting
TYPE_SUPPLIES:72212318 Diensten voor ontwikkeling van scannersoftware Percent : 100 Info : 1/1/null/null
TYPE_SUPPLIES:30238000 Automatiseringsuitrusting voor bibliotheek Percent : 100 Info : 5/3/null/null
TYPE_SUPPLIES:32580000 Gegevensuitrusting Percent : 100 Info : 1/null/null/null
TYPE_SUPPLIES:30232000 Randapparatuur Percent : 100 Info : 2/1/null/null
TYPE_SERVICES:80420000 Diensten voor e-leren Percent : 100 Info : 1/null/null/null
TYPE_SERVICES:72000000 IT-diensten: adviezen, softwareontwikkeling, internet en ondersteuning Percent : 100 Info : 11/8/null/null
TYPE_SERVICES:48151000 Computerbesturingssysteem Percent : 100 Info : 1/1/null/null
TYPE_SUPPLIES:48990000 Software voor spreadsheets en verbetering Percent : 100 Info : 1/null/null/null
TYPE_SUPPLIES:48620000 Besturingssystemen Percent : 100 Info : 1/null/null/null
TYPE_SUPPLIES:30232100 Printers en plotters Percent : 100 Info : 2/null/null/null
TYPE_SUPPLIES:48330000 Software voor dienstregelingen en productiviteit Percent : 100 Info : 1/null/null/null
TYPE_SERVICES:48180000 Medische software Percent : 100 Info : 1/null/null/null
TYPE_SERVICES:72212220 Diensten voor ontwikkeling van software voor internet en intranet Percent : 100 Info : 1/null/null/null
TYPE_SUPPLIES:72000000 IT-diensten: adviezen, softwareontwikkeling, internet en ondersteuning Percent : 100 Info : 1/null/null/null
TYPE_SUPPLIES:48810000 Informatiesystemen Percent : 100 Info : 1/null/null/null
TYPE_SUPPLIES:30211100 Supercomputer Percent : 100 Info : 1/null/null/null
TYPE_SERVICES:72267000 Onderhouds- en reparatiediensten voor software Percent : 100 Info : 2/null/null/null
TYPE_SUPPLIES:30230000 Computerapparatuur Percent : 100 Info : 5/1/null/null
TYPE_SUPPLIES:38548000 Instrumenten voor voertuigen Percent : 100 Info : 1/null/null/null
TYPE_SUPPLIES:38570000 Instrumenten en apparatuur voor regeling en besturing Percent : 100 Info : 1/null/null/null
TYPE_SERVICES:48800000 Informatiesystemen en servers Percent : 100 Info : 2/null/null/null
TYPE_SERVICES:48000000 Software en informatiesystemen Percent : 100 Info : 9/3/null/null
TYPE_SERVICES:72500000 Informaticadiensten Percent : 100 Info : 2/null/null/null
TYPE_WORKS:72000000 IT-diensten: adviezen, softwareontwikkeling, internet en ondersteuning Percent : 100 Info : 1/null/null/null
TYPE_SUPPLIES:30210000 Machines voor dataprocessing (hardware) Percent : 100 Info : 1/null/null/null
TYPE_SERVICES:72100000 Advies inzake hardware Percent : 100 Info : 1/null/null/null
TYPE_SUPPLIES:51612000 Installatie van informatieverwerkende uitrusting Percent : 100 Info : 1/null/null/null
TYPE_SERVICES:72230000 Ontwikkeling van gebruikersspecifieke software Percent : 100 Info : 1/null/null/null
TYPE_SERVICES:72222300 Diensten in verband met informatietechnologie Percent : 100 Info : 1/null/null/null
TYPE_SERVICES:48450000 Software voor tijdregistratie of human reousrces Percent : 100 Info : 1/null/null/null
TYPE_SERVICES:48332000 Software voor het maken van dienstregelingen Percent : 100 Info : 1/null/null/null
TYPE_SUPPLIES:48151000 Computerbesturingssysteem Percent : 100 Info : 1/null/null/null
TYPE_SERVICES:72222000 Strategie- en planningsdiensten voor informatiesystemen of -technologie Percent : 100 Info : 1/1/null/null
TYPE_SUPPLIES:72700000 Computernetwerkdiensten Percent : 100 Info : 1/null/null/null
TYPE_SUPPLIES:48500000 Communicatie- en multimediasoftware Percent : 100 Info : 1/null/null/null
TYPE_SERVICES:48900000 Diverse software en computersystemen Percent : 100 Info : 1/null/null/null
TYPE_SUPPLIES:30213200 Tablet-pc's Percent : 100 Info : 1/null/null/null
TYPE_SERVICES:72314000 Verzamelen en collationeren van gegevens Percent : 100 Info : 2/null/null/null
TYPE_SERVICES:72262000 Ontwikkeling van software Percent : 100 Info : 2/null/null/null
TYPE_SUPPLIES:50300000 Reparatie, onderhoud en aanverwante diensten in verband met pc's, kantooruitrusting, telecommunicatie- en audiovisuele uitrusting Percent : 100 Info : 1/null/null/null
TYPE_SUPPLIES:32424000 Netwerkinfrastructuur
TYPE_SUPPLIES:35123300 Tijdregistratiesysteem  /  NE_tijdregistratiesysteem
TYPE_SUPPLIES:32425000 Netwerkbesturingssysteem  /  NE_netwerkbesturingssysteem PA_sturingkast
TYPE_SUPPLIES:32552200 Teleprinters  /  NE_teleprinter
TYPE_SUPPLIES:64212400 Wireless Application Protocol (WAP)-diensten  /  NE_dienst NE_protocol NE_wireless_application
TYPE_SUPPLIES:32412120 Intranetnetwerk  /  NE_intranetnetwerk
TYPE_SUPPLIES:32412110 Internetnetwerk  /  NE_internetnetwerk
TYPE_SUPPLIES:64222000 Telewerkdiensten  /  NE_telewerkdienst
TYPE_SUPPLIES:31712116 31700000 Microprocessors  /  NE_microprocessor