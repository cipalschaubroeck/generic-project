#
# Copyright © Sygel - All Rights Reserved
# Unauthorized copying of this file, via any medium is strictly prohibited
# Unauthorized use of this source code is strictly prohibited
# Proprietary and confidential
#
# upgrade
# 30200000 Computeruitrusting en -benodigdheden
NE_computermateriaal 30200000
NE_computeruitrusting 30200000
REL_leveren_met_software 30200000 1
REL_beveiligen_primaire_storage 30200000 1
REL_realiseren_virtualisatiecluster 30200000 2
REL_onderhouden_opslagsysteem 30200000 4
REL_leveren_laptops 30200000 3
REL_aankopen_hardware 30200000 1
REL_leveren_computermateriaal 30200000 2
REL_beveiligen_storage 30200000 1
REL_(installeren|leveren)_datacommunicatie 30200000 2
REL_vernieuwen_serverpark 30200000 2
REL_(aankopen|onderhouden)_san 30200000 4
REL_leveren_informatica 30200000 2
REL_vernieuwen_server 30200000 2
REL_leveren_computers 30200000 2
REL_(leveren|installeren)_kassasysteem 30200000 4
REL_vervangen_schijfeenheid 30200000 2
REL_(beheren|inrichten)_datacenters 30200000 3
REL_leveren_software 30200000 1
REL_aankopen_ict 30200000 2
# 30210000 Machines voor dataprocessing (hardware)
REL_leveren_computerapparatuur 30210000 1
REL_(leveren|aankopen)_werkstations 30210000 2
REL_(aankopen|leveren)_draagbare_werkstations 30210000 2
# 30211100 Supercomputer
# 30213200 Tablet-pc's
REL_aankopen_tablets 30213200 2
REL_aankopen_digitale_tablets 30213200 2
# 30230000 Computerapparatuur
NE_informaticamateriaal 30230000
NE_informatica_meterieel 30230000
NE_informatica_meteriaal 30230000
NE_it,NE_infrastructuur 30230000
REL_leveren_kvm 30230000 2
REL_(aankopen|onderhouden)_rsa 30230000 4
REL_leveren_laptops 30230000 2
REL_leveren_computermateriaal 30230000 4
REL_vernieuwen_lan 30230000 1
REL_leveren_it 30230000 1
REL_leveren_tft 30230000 2
REL_vernieuwen_serverpark 30230000 2
REL_leveren_pc's 30230000 1
# 30232000 Randapparatuur
# 30232100 Printers en plotters
REL_aankopen_digitale_drukpers 30232100 4
REL_onderhouden_scanners 30232100 2
REL_(huren|aankopen|onderhouden)_multifunctionals 30232100 10
REL_(huren|onderhouden)_kopieermachines 30232100 8
REL_huren_afdrukmateriaal 30232100 4
REL_huren_fotokopieertoestel 30232100 1
REL_(huren|leveren|installeren|onderhouden)_multifunctionele 30232100 16
REL_huren_afdrukapparaten 30232100 1
REL_(leveren|installeren)_latexprinter 30232100 2
# 30233300 Smartcardlezers
# 30237000 Onderdelen, toebehoren en benodigdheden voor computers
# 30238000 Automatiseringsuitrusting voor bibliotheek
REL_(onderhouden|leveren|plaatsen)_bibliotheekbeveiliging 30238000 8
# 31682210 Instrumenten- en besturingsuitrusting
# 31712116 31700000 Microprocessors  /  NE_microprocessor
NE_microprocessor 31712116
# 32420000 Netwerkuitrusting
NE_switch,NE_netwerk 32420000
NE_backbone 32420000
NE_data_infrastructuur 32420000
NE_data_infrastructuurwerken 32420000
NE_netwerkmateriaal 32420000
NE_netwerkuitrusting 32420000
NE_wifi 32420000
NE_cisco 32420000
NE_netwerkapparatuur 32420000
REL_(beheren|installeren|leveren|ondersteunen|bouwen|aankopen)_lan 32420000
REL_(beheren|installeren|leveren|ondersteunen|bouwen|aankopen)_wlan 32420000
REL_(beheren|installeren|leveren|ondersteunen|bouwen|aankopen)_wifi 32420000 2
REL_(beheren|installeren|leveren|ondersteunen|bouwen|aankopen)_wifi_netwerk 32420000 2
REL_aankopen_cisco 32420000 2
REL_leveren_gprs 32420000 2
REL_(installeren|leveren|ondersteunen|bouwen|aankopen)_netwerkmateriaal 32420000 5
REL_(plaatsen|leveren)_serverkast 32420000 4
REL_uitbreiden_racks 32420000 1
REL_(beheren|installeren|leveren|ondersteunen|bouwen|aankopen)_firewall 32420000 3
REL_(beheren|installeren|leveren|ondersteunen|bouwen|aankopen)_netwerkcomponenten 32420000 2
# 32424000 Netwerkinfrastructuur
NE_breedbandverbinding 32424000
NE_load_balancer 32424000
NE_ethernet 32424000
NE_router 32424000
NE_glasvezelnetwerk 32424000
# 32580000 Gegevensuitrusting
REL_installeren_datacenter 32580000
NE_noodinformaticacentrum 32580000
# 38548000 Instrumenten voor voertuigen
# 38570000 Instrumenten en apparatuur voor regeling en besturing
# 48000000 Software en informatiesystemen
NE_datainfrastructuurwerken 48000000
NE_informatica 48000000 11
NE_informaticaontwikkeling 48000000
NE_softwaretool 48000000
NE_webapplicatie 48000000
NE_webservice 48000000
NE_informaticatoepassing 48000000
NE_hardware,NE_software 48000000 8
REL_(aankopen|leveren)_softwarepakket 48000000 8
REL_onderhouden_website 48000000 1
REL_(aankopen|leveren)_microsoft 48000000 5
REL_(plaatsen|integreren|onderhouden|leveren)_hardware 48000000 11
REL_onderhouden_softwareapplicatie 48000000 1
REL_leveren_ict 48000000 1
REL_(leveren|aankopen|huren)_licenties 48000000 10
REL_leveren_informatica 48000000 2
REL_vervangen_informaticameterieel 48000000 2
REL_leveren_beheersoftware 48000000 2
REL_(implementeren|configureren|leveren|voorzien|ondersteunen|installeren|gebruiken|onderhouden)_software 48000000 14
REL_(ontwikkelen|aanpassen|leveren|uitbreiden|ondersteunen|installeren|indienststellen|integreren|wijzigen|plaatsen|configureren|voorzien)_software 48000000 18
REL_(installeren|aankopen|leveren|onderhouden|plaatsen|integreren)_software 48000000 24
REL_aankopen_citrix 48000000 5
REL_(installeren|leveren|onderhouden)_softwarepakket 48000000 6
REL_(installeren|leveren|onderhouden)_software 48000000 4
REL_(installeren|leveren|onderhouden)_softwareapplicatie 48000000 1
REL_(installeren|leveren|onderhouden)_afsprakenmodule 48000000
REL_(installeren|leveren|onderhouden)_klantenbegeleidingssysteem 48000000
# 48151000 Computerbesturingssysteem
REL_aankopen_microsoft 48151000 1
# 48160000 Software voor bibliotheek
NE_digitaal_archiefsysteem 48160000
REL_(onderhouden|plaatsen)_bibliotheekbeveiliging 48160000 4
# 48180000 Medische software
# 48219300 Administratieve software
# 48330000 Software voor dienstregelingen en productiviteit
REL_onderhouden_tijdsregistratiesysteem 48330000 2
# 48332000 Software voor het maken van dienstregelingen
REL_(beheren|onderhouden)_planningssoftware 48332000 2
REL_aankopen_planningssysteem 48332000 1
REL_(onderhouden|beheren)_planningstool 48332000 4
# 48450000 Software voor tijdregistratie of human reousrces
REL_implementeren_geïnformatiseerd_loonbeheerssysteem 48450000 2
REL_implementeren_loonbeheerssysteem 48450000 2
REL_onderhouden_tijdsregistratiesysteem 48450000 2
REL_beheren_tijdsregistratie 48450000 1
REL_aankopen_planningssysteem 48450000 1
REL_(leveren|installeren)_hr 48450000 4
# 48500000 Communicatie- en multimediasoftware
REL_(installeren|leveren)_media 48500000 2
REL_installeren_audiovisuele_infrastructuur 48500000 1
REL_installeren_conferentiesysteem 48500000 1
# 48620000 Besturingssystemen
REL_leveren_microsoft 48620000 1
# 48800000 Informatiesystemen en servers
REL_(leveren|onderhouden)_juniper 48800000 4
REL_aankopen_virtualisatiesoftware 48800000 2
REL_(onderhouden|aankopen)_unix 48800000 4
REL_(plaatsen|leveren)_serverkast 48800000 8
REL_(onderhouden|uitbreiden)_centrale_storage 48800000 8
REL_realiseren_mobiele_toegang 48800000 2
REL_vernieuwen_it 48800000 2
REL_beheren_servers 48800000 1
REL_(onderhouden|uitbreiden)_storage 48800000 8
REL_maken_rack 48800000 1
# 48810000 Informatiesystemen
# 48820000 Servers
NE_serverinfrastructuur 48820000
REL_(installeren|aankopen)_patching 48820000 4
REL_aankopen_data 48820000 4
REL_vervangen_servers 48820000 2
REL_(aankopen|installeren)_serverracks 48820000 2
REL_aankopen_hp 48820000 2
REL_aankopen_opslagmateriaal 48820000 2
REL_(installeren|aankopen)_intelligente_pdu's 48820000 4
REL_(installeren|aankopen)_racks 48820000 2
# 48822000 Computerservers
# 48900000 Diverse software en computersystemen
REL_leveren_applets 48900000 1
REL_(leveren|uitbreiden)_licenties 48900000 2
REL_maken_vmware 48900000 1
REL_leveren_beheersoftware 48900000 2
REL_leveren_hardware 48900000 2
REL_aankopen_itsm 48900000 2
# 48990000 Software voor spreadsheets en verbetering
# 50312000 Onderhoud en reparatie van computeruitrusting
# 50312610 Onderhoud van uitrusting voor informatietechnologie
# 51612000 Installatie van informatieverwerkende uitrusting
# 64216000 Elektronische berichten- en informatiediensten
# 72000000 IT-diensten: adviezen, softwareontwikkeling, internet en ondersteuning
REL_onderhouden_website 72000000 8
REL_uitbreiden_racks 72000000 1
REL_bouwen_websites 72000000 1
REL_inrichten_datacenter 72000000 2
REL_(leveren|aankopen)_ict 72000000 4
REL_maken_web 72000000 1
REL_(installeren|aankopen|leveren|uitrusten|herstellen)_software 72000000 9
REL_aankopen_licenties 72000000 1
REL_aankopen_softwarepakket 72000000 2
REL_onderhouden_hardware 72000000 3
REL_beheren_it 72000000 2
# 72100000 Advies inzake hardware
# 72200000 Softwareprogrammering en -advies
# 72210000 Programmering van softwarepakketten
REL_programmeren_softwarepakket 72210000 1
REL_programmeren_software 72210000 1
REL_leveren_applets 72210000 1
REL_programmeren_toepassing 72210000 1
# 72211000 Programmering van systeem- en gebruikerssoftware
# 72212220 Diensten voor ontwikkeling van software voor internet en intranet
# 72212318 Diensten voor ontwikkeling van scannersoftware
REL_installeren_boekscanner 72212318 1
# 72212450 Diensten voor ontwikkeling van software voor tijdregistratie of personele middelen
REL_onderhouden_tijdsregistratiesysteem 72212450 2
# 72222000 Strategie- en planningsdiensten voor informatiesystemen of -technologie
# 72222300 Diensten in verband met informatietechnologie
# 72230000 Ontwikkeling van gebruikersspecifieke software
REL_(aankopen|onderhouden)_software 72230000 4
REL_(beheren|onderhouden)_webshop 72230000 2
# 72242000 Maken van ontwerp modellen
# 72260000 Diensten in verband met software
REL_(onderhouden|realiseren|beheren)_sharepoint 72260000 6
REL_uitvoeren_informaticaproject 72260000 1
# 72262000 Ontwikkeling van software
# 72263000 Software-implementatiediensten
# 72267000 Onderhouds- en reparatiediensten voor software
REL_onderhouden_klantenbegeleidingssysteem 72267000 4
REL_leveren_sap 72267000 1
# 72267100 Onderhoud van software voor informatietechnologie
REL_onderhouden_licencies 72267100 1
# 72314000 Verzamelen en collationeren van gegevens
REL_(aankopen|leveren|onderhouden|realiseren|beheren)_registratiesysteem 72314000 1
NE_registratiesysteem 72314000 1
# 72411000 Internetproviders (ISP)
# 72500000 Informaticadiensten
REL_aankopen_cloud 72500000 1
REL_aankopen_mobile_office 72500000 1
REL_aankopen_checkpoint 72500000 2
REL_aankopen_licenties 72500000 2
REL_maken_virtualisatie 72500000 1
REL_(installeren|leveren)_applicatievirtualisatie 72500000 4
REL_installeren_itsm 72500000 1
REL_leveren_klantbegeleidingssuysteem 72500000 1
REL_(bevatten|beheren|analyseren|vervangen|aankopen|configureren)_firewall 72500000 10
REL_(maken|aankopen)_citrix 72500000 2
REL_onderhouden_servers 72500000 2
REL_bouwen_websites 72500000 1
# 72700000 Computernetwerkdiensten
NE_extranet 72700000
NE_dataverbinding 72700000
NE_wannetwerk 72700000
NE_datatransmissiedienst 72700000
REL_(leveren|installeren)_internet 72700000 4
REL_huren_cloudomgeving 72700000 2
REL_beheren_accespoints 72700000 1
# 80420000 Diensten voor e-leren
# 35123300 Tijdregistratiesysteem  /  
NE_tijdregistratiesysteem 35123300
# 32425000 Netwerkbesturingssysteem  /   PA_sturingkast
NE_netwerkbesturingssysteem 32425000
# 32552200 Teleprinters  /  
NE_teleprinter 32552200
# 64212400 Wireless Application Protocol (WAP)-diensten  /  NE_dienst NE_protocol NE_wireless_application
NE_wireless_application 64212400
# 32412120 Intranetnetwerk  /  NE_intranetnetwerk
# 32412110 Internetnetwerk  /  NE_internetnetwerk
# 64222000 Telewerkdiensten  /  NE_telewerkdienst