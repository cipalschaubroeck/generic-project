#
# Copyright © Sygel - All Rights Reserved
# Unauthorized copying of this file, via any medium is strictly prohibited
# Unauthorized use of this source code is strictly prohibited
# Proprietary and confidential
#
TYPE_SERVICES:50850000 Reparatie en onderhoud van meubilair Percent : 100 Info : 1/1/null/null
TYPE_WORKS:44210000 Assemblagestructuren en delen daarvan Percent : 100 Info : 1/null/null/null
TYPE_WORKS:45421111 Plaatsen van deurkozijnen Percent : 100 Info : 1/null/null/null
TYPE_WORKS:45212190 Aanbrengen van zonwering Percent : 100 Info : 1/null/null/null
TYPE_SERVICES:45262650 Bekledingswerkzaamheden Percent : 100 Info : 1/null/null/null
TYPE_WORKS:45421153 Plaatsen van inbouwmeubilair Percent : 82 Info : 9/7/2/null
TYPE_WORKS:45421110 Plaatsen van deur- en raamkozijnen Percent : 67 Info : 2/null/1/null
TYPE_WORKS:45421160 Werken aan hang- en sluitwerk Percent : 67 Info : 2/1/1/null
TYPE_WORKS:45422100 Houtwerk Percent : 63 Info : 5/3/3/null
TYPE_WORKS:45421131 Plaatsen van deuren Percent : 60 Info : 6/1/4/null
TYPE_WORKS:45422000 Timmer- en schrijnwerk Percent : 56 Info : 25/18/20/null
TYPE_WORKS:45432114 Leggen van houten vloerbedekkingen Percent : 50 Info : 1/null/1/null
TYPE_SUPPLIES:45422100 Houtwerk Percent : 50 Info : 1/null/1/null
TYPE_WORKS:45324000 Aanbrengen van gipsplaten Percent : 50 Info : 1/null/1/null
TYPE_WORKS:45420000 Bouwtimmerwerk en plaatsen van schrijnwerk Percent : 49 Info : 33/25/34/null
TYPE_WORKS:45421132 Plaatsen van ramen Percent : 48 Info : 12/6/13/null
TYPE_WORKS:44220000 Schrijnwerk voor de bouw Percent : 42 Info : 5/null/7/null
TYPE_WORKS:45421100 Plaatsen van deuren en ramen en bijbehorende elementen Percent : 38 Info : 19/11/31/null
TYPE_WORKS:45421145 Installeren van rolluiken Percent : 33 Info : 1/1/2/null
TYPE_WORKS:45421000 Bouwtimmerwerk Percent : 33 Info : 4/3/8/null
TYPE_WORKS:45421130 Plaatsen van deuren en ramen Percent : 30 Info : 6/null/14/null
TYPE_WORKS:45421152 Aanbrengen van scheidingswanden Percent : 29 Info : 2/null/5/null
TYPE_WORKS:45421151 Plaatsen van inbouwkeukens Percent : 25 Info : 1/null/3/null
TYPE_WORKS:45320000 Isolatiewerkzaamheden Percent : 21 Info : 3/null/11/null
TYPE_WORKS:45421141 Plaatsen van scheidingswanden Percent : 20 Info : 2/null/8/null
TYPE_WORKS:45421146 Aanbrengen van verlaagde plafonds Percent : 15 Info : 2/null/11/null
TYPE_WORKS:45261410 Dakisolatiewerkzaamheden Percent : 14 Info : 4/2/24/null
TYPE_WORKS:45321000 Warmte-isolatiewerkzaamheden Percent : 14 Info : 2/null/12/null
TYPE_WORKS:45340000 Plaatsen van hekken en leuningen en beveiligingsuitrusting Percent : 10 Info : 1/null/9/null
TYPE_WORKS:45342000 Plaatsen van hekwerk Percent : 9 Info : 1/null/10/null
TYPE_WORKS:45421144 Installeren van zonweringen
TYPE_WORKS:39154100 Tentoonstellingsstands