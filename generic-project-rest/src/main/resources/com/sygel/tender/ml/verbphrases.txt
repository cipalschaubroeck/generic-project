#
# Copyright © Sygel - All Rights Reserved
# Unauthorized copying of this file, via any medium is strictly prohibited
# Unauthorized use of this source code is strictly prohibited
# Proprietary and confidential
#
bepalen,bepaal
moderniseren,modernisatie
bereiden,bereid
aansturen,aansturing
berekenen,berekening
herbeginnen
bouwen,bouw,opbouw,opbouwen,wederopbouwen,wederopbouw,wederopbouwing,vervangbouw,gebouwd,heropbouw,bijbouwen,passiefbouw,oprichten,oprichting,bouwwerkzaamheden
ombouwen,ombouw,ombouwwerken
programmeren,programmeer,programmering
rooien,ontstronk,ontstronken
garanderen,garandeert
recycleren,recyclage
controle,controleren
boren,boring
betonneren,betonnering,betonneer
doorboren,doorboring
hangen,hang
dichtleggen
verdiepen
behandelen,behandeling
begeleiden,begeleiding,escorteren
reviseren,revisie
monitoren,monitoring
verhuren,verhuring,verhuur,leasen,leasing,langetermijnleasing,huur op lange termijn
versnipperen,versnippering,versnipper
verstevigen,verstevig,versteviging
opruimen,opruiming
opzuigen,opzuiging
werken
stallen,gestald
uitoefenen,uitoefening
keuren,keuringen,keuring,certificering,certificeren
afreien
afkoppelen,afkoppeling,afkoppelingen
hosten,hosting
ophogen,ophoging,verhogen,verhoging
valoriseren,valorisatie
verbreden,verbreding
dempen
drillen,drilling
afschepen
schouwen,schouwing
inplanten,inplanting
bevatten,bevat,omvatten,omvat
prefinancieren,prefinanciering
implementeren,implementatie
converteren,convertering
besturen,besturing
bestellen,bestelling
wegslepen
gevelherstel,gevelherstelling
rehabiliteren,rehabilitatie
ontwateren,ontwatering
waterdichtmaken,waterdicht maken
coördineren,coördinatie
leiden,leiding
leeghalen,lediging,leegmaken
herleggen,herleg,herlegging
verzenden,verzending
herstarten,herstarting
gebruiken,gebruik,gebruikt
editen,editing,edit
landen,landing
strekken
wedden
winnen,winning
wieden
uitwerken,uitwerking
wegschaven
voorbereiden,voorbereiding,voorbereidingen
vouwen
voorboren,voorboring,voorboringen
voorzien
verwarmen
vullen
erkennen
bemesten;bemesting
verkopen,verkocht
verwijden,verwijding
verlenen,verleend,verlening
verdampen,verdamping
sorteren,sortering
uittrekken,uittrekking
uitroeien,uitroeiing
touwklimmen
kopiëren
stutten
loggen
regelen,regeling
scannen,scanning
proeven
optrekken
trekken
opvullen,vullen,opvulling
naaien
opvangen,opvanging
opsporen,opsporing
ontsmetten,ontsmetting
opsommen,opsomming
opslagen,opslaan
oproepen,oproeping
opheffen,opheffing
opleggen
boogschieten
bewerken,bewerk,bewerking
mengen,meging
schieten
enten
koken
mixen
kopen
innen,inning
bewaken,bewaking
regenereren,geregenereerd
inbinden,inbinding
inspuiten,inspuiting
toedienen,toediening
kenmerken,gekenmerkt
kruiden,gekruid
gokken
malen,gemalen
openen,opening
brengen
drogen
schrijven,schrijf
distilleren,distillering,distilleer
upgraden
browsen
collationeren
aanzien
bunkeren
laden
afdrukken
drukken
bijtanken
aflezen,aflees
afdichten,afdichting
adviseren,advisering,adviseer
aandrijven,aangedreven
schakelen
impregneren
injecteren
insmeren
verbeteren,verbetering,bevorderen,bevordering
zuiveren,zuivering
geven
gevlochten
geweven
kweken
lamineren
leren
registreren,registratie
repareren,reparatie,reparaties
retoucheren
solderen
uitbalanceren
neutraliseren
organiseren,organiseer,organisering
parfumeren
positioneren
publiceren,publicatie
invoeren
invullen
inzetten
wisselen
laten
afhandelen,afhandeling,afhandel
conformeren,conform maken
bekleden,bekleding
selecteren,selectering
investeren,investering
verwerven,verwerving
verbranden,verbranding
inbouwen,inbouw
uitbouwen,uitbouw
behangen
ontvangen,ontvang
configureren,configuratie
beoordelen,beoordeling
beteugelen,beteugel,beteugeling
produceren,productie
heien
endosseren
coveren
detecteren,detecteer,dectering
controleren,controle
meten,meting
inhuren
studie
zandstralen
analyseren,analysering,analyse,analyses,laboratoriumanalyse
bemonsteren,bemonstering
selecteren,selectie
ontwikkelen,ontwikkeling,verwezenlijken,verwezenlijking
herontwikkelen,herontwikkeling,herontwikkel,herconditionering,herconditioneren
inspecteren,inspecties
verstrekken,verstrekking
afsluiten,afsluiting,aangaan
vervoeren,vervoer
vrijmaken,vrijmaking
wegsteken
verven
kalibreren,kalibratie
omvormen,omvorming
opnemen,opname
nivelleren,nivellering,nivelleer
metaliseren,metalisatie
verwerken,verwerking,verwerk
vernietigen,vernietiging,vernietig
verzamelen,verzameling,inzamelen,inzameling
verzekeren,verzekering
ophalen,ophaling
transporteren,transport,transportering
omruilen,omruiling,omruil
integreren,integratie
uitbouwen,uitbouw
regenereren,regeneratie
aflagen,aflaging
aanstellen,aanstelling,aanduiden,aanduiding
aanstorten,aanstorting
recupereren,recuperatie
effenen
schoonmaken,schoonmaking,schoonmaak
nazien,nazicht
overbruggen,overbrugging
onderbruggen,onderbrugging
onderbreken,onderbreking
ondersteunen,ondersteun,ondersteuning,support
wijzigen,wijziging
verlengen,verlenging
verstoppen,verstopping
spoelen,spoeling
verkorten,inkorten,verkorting,inkorting
conserveren
baggeren
begrazen,begrazing
galvaniseren
sorteren,sortering
gieten
metselen
knotten
ontginnen,ontginning
nieuwbouw
strooien,strooing,bestrooien
openspreiden
ontstoppen,ontstoppingen
reorganiseren
bevoorraden,bevoorrading
voorkomen,voorkoming
wegruimen,wegruiming
inbedrijfstellen,inbedrijfstelling,in bedrijf stellen
huren,huur,renting
legen,ledigen
aanbrengen,aanbrenging
beheren,beheer,opvolging,opvolgen
verwijderen,verwijdering,wegnemen,wegneming
opstarten,opstart
aanleggen,aanleg
leggen
heraanleggen,heraanleg,heraanlegging,heraangelegd
herbouwen,herbouw,herbouwing
graven,graaf,uitgraven,afgraving,afgraven,delven,uitgraving,uitgegraven,opgravingen,ontgraven
saneren,saneer,sanering
uitbreiden,uitbreidingen
aanpakken,aanpak
ontsluiten,ontsluiting
versterken,versterking
aanpassen,aanpassing,aanpassingswerken,aanpassingswerk,aanpassingen
renoveren,moderniseringswerken,opfrissing,moderniseringswerk,opknapwerk,opknapwerken,herstellingswerkzaamheden,verfraaiing,renovatiewerkzaamheden,vernieuwingen,opfrissen,herstellingswerkzaamheid,buitenrestauratie,binnenrestauratie,binnenrenovatie,binnenrenovaties,totaalrenovatie,renovaties,restauratiewerken,renoveer,renovatie,opfrissingswerk,opfrissingswerken,verbeteringswerken,herwaardering,verbeteringswerk,herbouwingswerken,herbouwingswerk,herbouwingwerken,herbouwingwerk,vernieuwingswerken,hernieuwen,hernieuwing,vernieuwen,vernieuwing,vernieuwbouw,vervangingsbouwen,herconditioneringswerken,vervangingsbouw,renovatiewerken,instandhoudingswerken,instandhouden,opknappen,gerenoveerd,herbestemming,herbestemmen,restaureren,restauratie,reconstrueren,recontrueer,reconstructie,bijwerken
dakrenovatie
aanwerven,recruteren,recrutering
herplaatsen
asfalteren
lozen,lozing
zaaien,inzaaien,bezaaiing,bezaaien,inzaaiing
opstellen,opgesteld
evalueren,evaluering
aanvullen
aansluiten,aansluiting
overstorten
storten,storting,gestort
afvoeren,afvoer,afvoering
schilderen,schilderwerken,herschilderen
profileren,profilering
ontwerpen,vormgeven,vormgeving,ontwerp,modellering,modelleren
affrezen,uitfrezen,frezen
isoleren,isolatiewerken,isoleringswerken,isolatie,isolering,naisolatie,naisoleren
uitbreken,uitbraak
bijplaatsen
bijsturen,bijsturing
bekomen
verharden
herprofileren,herprofilering
onderzoeken,onderzoek
realiseren,realisatie,gerealiseerd
moderniseren,modernisering
geschiktmaken,geschiktmaking
instandhoudingswerk
uitbreiden,uitbreidingswerken,uitbreiding,uitbreidingsverbouwingen,uitbreidingsverbouwing
aanpassen,aanpassing
verbouwen,verbouwing,verbouw,verbouwingswerken,verbouwingen,verbouwd
aanbouwen,aanbouw
vervangen,vervang,vervanging,vervangingen
construeren,constructie,creëren
herstellen,herstel,herstelling,herstellingen,herstellingswerken,herstelwerken,depannage,depanneren,depannering,depannages,depannage
onderhouden,onderhoud,onderhoudswerken
stabiliseren,stabiliseer
plaatsen,plaatsing,geplaatst
graveren,graveer,gravering
installeren,installatie,opzetten,geïnstalleerd
herinrichten,herinrichting,herindelen,herindeling
inrichten,inrichtingswerk,inrichting,totaalinrichting,totaalinrichten
geschiktmakingswerken
ruimen,ruiming
indienststellen,indienststelling
lassen,oplassen
monteren,montage
maaien
uitvoeren,uitvoering
leveren,levering,leveringen,gratis ter beschikking stellen,ter beschikking stellen,aanleveren,aanlevering,bijlevering,bijleveren,geleverd,opleveren,verschaffen,verschaffing,afhalen
afbreken,afbraak,slopen,gesloopt,sloop,sloping,deconstrueren,deconstructie,ontmantelen,ontmanteling,wegbreken,demontagewerkzaamheden,demonteren,demontage
opbreken,opbraak
aankopen,aankoop,groepsaankoop
uitrusten,uitrusting,uitrustingswerken,uitrustingswerk
filtreren,filtreer,filtrering
automatiseren,automatisatie,automatiseer,automatisering
beveiligen,beveiliging
hardsolderen
gebruiksklaarmaken,gebruiksklaarmaking,indienstelling,indienstellen
bewaken,bewaking
digitaliseren,digitaliseer
beplanten,planten,aanplanten,aanplanting,beplanting,aanplant,heraanplant,heraanplanten
borstelen,vegen,poetsen,kuisen,kuis
reinigen,reinig,reiniging,gereinigd
overbruggen
bestrijken,bestrijk
overlagen,overlaging
voegen
opladen
inventariseren
stockeren
optimaliseren,optimalisering,optimaliseer,optimalisatie
bestraten
financieren,financiering,gefinancierd,financieringen
exploiteren,exploitatie
verplaatsen,verplaats
uitbaten,uitbating
afwerken,afwerking,afgewerkt,voltooiingswerken,voltooiingswerk
maken,aanmaken,maak,aanmaak,opmaken,opmaak,fabriceren,fabriceer,vervaardigen,vervaardiging,op maat gemaakte
snoeien
scheren
vellen
kappen
hakken
Raamcontract voor elektriciteitwerken
bouwrijpmaken,bouwrijp maken
wassen
strijken
samenstellen,samenstelling
composteren;compostering