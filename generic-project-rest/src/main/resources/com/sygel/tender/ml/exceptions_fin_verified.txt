#
# Copyright © Sygel - All Rights Reserved
# Unauthorized copying of this file, via any medium is strictly prohibited
# Unauthorized use of this source code is strictly prohibited
# Proprietary and confidential
#
TYPE_SERVICES:64212200 64212000 Ems-diensten  /  NE_ems NE_dienst
TYPE_SERVICES:66000000 financiële en verzekeringsdiensten
TYPE_SERVICES:66100000 Diensten op het gebied van banken
TYPE_SERVICES:66510000 Verzekeringsdiensten
TYPE_SERVICES:66520000 Pensioendiensten
TYPE_SERVICES:66700000 Herverzekeringen
TYPE_SERVICES:79311400 Economisch onderzoek Percent : 100 Info : 2/null/null/null
TYPE_SERVICES:79212100 Uitvoeren van financiële audit Percent : 100 Info : 1/1/null/null
TYPE_SERVICES:71321100 Diensten voor bouweconomie Percent : 100 Info : 1/null/null/null
TYPE_SERVICES:71244000 Kostenberekening en -bewaking Percent : 100 Info : 2/null/null/null
TYPE_SERVICES:79300000 Markt- en economieonderzoek; enquêtes en statistieken Percent : 100 Info : 12/6/null/null
TYPE_SERVICES:66171000 Diensten voor financieel advies Percent : 100 Info : 2/null/null/null
TYPE_SERVICES:30161000 kredietkaarten
TYPE_SERVICES:66113000 kredietverlening
TYPE_SERVICES:66113100 diensten op het gebied van micro-k
TYPE_SERVICES:66517000 krediet- en borgstellingsverzekering
TYPE_SERVICES:66517100 kredietverzekeringen
TYPE_SERVICES:48440000 software voor financiële analyse en b
TYPE_SERVICES:48812000 financiële-informatiesystemen
TYPE_SERVICES:66114000 financiële leasing
TYPE_SERVICES:66122000 financiële diensten i.v.m. vennootsch
TYPE_SERVICES:66150000 diensten in verband met het beheer
TYPE_SERVICES:66170000 diensten op het gebied van financiël
TYPE_SERVICES:66171000 diensten voor financieel advies
TYPE_SERVICES:66172000 afhandelen van financiële transactie
TYPE_SERVICES:79211200 opstellen van financiële verslagen
TYPE_SERVICES:79212100 uitvoeren van financiële audit
TYPE_SERVICES:79412000 advies inzake financieel management
TYPE_SERVICES:66500000 diensten op het gebied van verzekeringen
TYPE_SERVICES:66700000 herverzekeringen
TYPE_SERVICES:75300000 diensten voor verplichte sociale ver
TYPE_SERVICES:66114000 financiële leasing
TYPE_SERVICES:70200000 verhuur of leasing van eigen onroere
TYPE_SERVICES:70210000 Residential property renting or leasing services
TYPE_SERVICES:70220000 Verhuur of leasing van niet voor bewoning bestemde eigen gebouwen
TYPE_SERVICES:79910000 Beheer van holdings
TYPE_SERVICES:79940000 Dienstverlening door incassobureaus
TYPE_SERVICES:79200000 Boekhoudkundige, audit- en fiscale diensten