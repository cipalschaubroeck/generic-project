#
# Copyright © Sygel - All Rights Reserved
# Unauthorized copying of this file, via any medium is strictly prohibited
# Unauthorized use of this source code is strictly prohibited
# Proprietary and confidential
#
# upgrade
# 34514200 Opvijzelbare platforms
# 34516000 Fenders voor de marine
REL_(aankopen|leveren)_schuimgevulde_fenders 34516000 2 null
REL_(aankopen|leveren)_fenders 34516000 2 null
# 34931000 Uitrusting voor havens
REL_(leveren|plaatsen)_havensignalisatie 34931000
REL_vervangen_ponton 34931000 1 null
# 38420000 Instrumenten voor het meten van debiet, peil en druk van vloeistoffen en gassen
# 38421000 Debietmeetuitrusting
NE_radardebietmeter 38421000
# 45232451 Afwaterings- en oppervlaktewerkzaamheden
REL_plaatsen_oeverversteviging 45232451 2 null
REL_(plaatsen|leveren)_dompelpomp 45232451 2 null
REL_ruimen_gracht 45232451 6 10
REL_ruimen_waterloop 45232451 2 3
REL_onderhouden_afwateringsstelsels 45232451 1 1
# 45233200 Diverse oppervlaktewerkzaamheden
REL_renoveren_wandelpad 45233200 1 1
REL_(ruimen|profileren)_sloot 45233200 5 null
REL_ruimen_CAT_gracht 45233200 4 1
REL_(onderhouden|profileren|reinigen|ruimen)_gracht 45233200 8 null
REL_(profileren|aflagen|onderhouden)_berm 45233200 9 null
# 45240000 Waterbouwwerkzaamheden
NE_stuwsluis 45240000
NE_sluice 45240000 3 null
NE_dock 45240000 3 null
NE_waterbouwkundige_kunstwerk 45240000 3 null
NE_waterbouwkundig_kunstwerk 45240000 3 null
REL_(profileren|ruimen|onderhouden)_wachtbekkens 45240000 3 null
REL_plaatsen_oeverversteviging 45240000 2 null
REL_bouwen_bufferbekkens 45240000 1 2
REL_(vervangen|renoveren)_oeververdediging 45240000 5 null
REL_(transporteren|verwerken)_baggerspecie 45240000 2 null
REL_aanleggen_ecologische_vijverpartij 45240000 1 null
REL_(plaatsen|leveren)_schotbalkenstuw 45240000 2 null
REL_(realiseren|aanleggen)_vispassage 45240000 3 null
REL_opbreken_stuwconstructies 45240000 1 null
REL_(leveren|heien)_damplanken 45240000 3 null
REL_herinrichten_zuunbeek 45240000 1 null
REL_(renoveren|aanleggen|herstellen|stabiliseren|realiseren)_kaaimuur 45240000 7 null
REL_(verwerken|afvoeren)_ruimingsspecie 45240000 2 null
REL_inrichten_ijzerlaankanaal 45240000 1 null
REL_aanpassen_staatssteiger 45240000 1 null
REL_realiseren_afwateringssystemen 45240000 2 1
REL_aanleggen_erosiebestrijdende_maatregelen 45240000 1 null
REL_bouwen_wildwaterbaan 45240000 1 null
REL_(herinrichten|verstevigen|herprofileren|herstellen|opbreken|aanleggen)_oever 45240000 7 null
REL_heraanleggen_waterkering 45240000 1 null
REL_(aansluiten|aanleggen|herstellen)_meander 45240000 3 null
REL_graven_poelen 45240000 1 1
REL_renoveren_linkeroever 45240000 1 null
REL_renoveren_bridge 45240000 1 4
REL_(wijzigen|herprofileren|herstellen|ruimen|aanleggen|werken|onderhouden|aansluiten)_gracht 45240000 5 null
REL_herstellen_onderwatertaluds 45240000 1 null
REL_(wijzigen|herprofileren|herstellen|ruimen|aanleggen|werken|onderhouden|aansluiten)_waterloop 45240000 20 null
REL_(wijzigen|herprofileren|herstellen|ruimen|aanleggen|werken|onderhouden|aansluiten)_beek 45240000
REL_ruimen_baangrachten 45240000 1 2
REL_aanleggen_vijverpartij 45240000 1 null
REL_(plaatsen|maken)_kopmuur 45240000 2 null
REL_(onderhouden|herstellen|werken)_onbevaarbare_waterloop 45240000 9 null
REL_opbreken_stuw 45240000 1 null
REL_(renoveren|bouwen)_kaai 45240000 4 null
REL_uitbreiden_zwin 45240000 1 null
REL_graven_nevengeul 45240000 1 null
REL_(opbreken|afbreken|renoveren)_sluis 45240000 3 null
REL_bouwen_sigmadijk 45240000 1 null
REL_(aansluiten|verwijderen|aanleggen|plaatsen)_damwand 45240000 9 null
REL_plaatsen_stalen_damwand 45240000 1 null
REL_construeren_oeverbescherming 45240000 1 null
REL_(bouwen|verwijderen)_damwandenscherm 45240000 2 null
REL_(leggen|aanleggen)_vispassages 45240000 3 null
REL_onderhouden_steiger 45240000 1 null
REL_(plaatsen|leveren)_peilschaallatten 45240000 2 null
NE_strandhoofd 45240000 1 null
REL_opbreken_strandhoofd 45240000 1 null
REL_(plaatsen|verwijderen|leveren)_dukdalven 45240000 6 null
REL_bouwen_scheldepromenade 45240000 1 null
REL_aanleggen_vistrap 45240000 1 null
REL_heien_stalen_damplanken 45240000 2 null
REL_(bouwen|leggen|renoveren|restaureren|realiseren|afbreken)_kaaimuur 45240000 11 null
REL_aanleggen_vistrappen 45240000 1 null
REL_heraanleggen_aquafin 45240000 1 2
# 45241000 Havenbouwwerkzaamheden
NE_havenbouwwerkzaamheden 45241000 1
NE_haveninfrastructuur 45241000 1
REL_inbouwen_wielfender 45241000 1 null
REL_inbouwen_rolfender 45241000 1 null
REL_afbreken_haveninfrastructuur 45241000 1 null
NE_dokkencomplex 45241000
# 45241100 Bouwen van kades
REL_(renoveren|bouwen|aanleggen|afbreken|realiseren)_kade 45241100
REL_(renoveren|bouwen|aanleggen|afbreken|realiseren)_kademuur 45241100
REL_heien_damplanken 45241100 2 null
REL_(renoveren|bouwen|aanleggen|afbreken|realiseren)_semi_keermuur 45241100 1 null
REL_(renoveren|bouwen|aanleggen|afbreken|realiseren)_kaaiplateau 45241100 1 null
REL_(renoveren|bouwen|aanleggen|afbreken|realiseren)_linkeroever 45241100 1 null
REL_(renoveren|bouwen|aanleggen|afbreken|realiseren)_kaaimuur 45241100 1 1
REL_(renoveren|bouwen|aanleggen|afbreken|realiseren)_kaai 45241100 5 null
REL_(renoveren|bouwen|aanleggen|afbreken|realiseren)_keermuur 45241100 1 2
REL_(renoveren|bouwen|aanleggen|afbreken|realiseren)_kaaimuur 45241100 2 null
REL_(renoveren|bouwen|aanleggen|afbreken|realiseren)_verticale_keermuur 45241100 1 null
REL_heien_stalen_damplanken 45241100 2 null
REL_(renoveren|bouwen|aanleggen|afbreken|realiseren)_onderwatertalud 45241100 1 null
# 45242210 Bouwen van jachthaven
NE_jachthaven 45242210 1
# 45243510 Aanleg van dijken en kades
REL_(renoveren|bouwen|aanleggen|afbreken|realiseren)_dijk 45243510 1 null
REL_(renoveren|bouwen|aanleggen|afbreken|realiseren)_ringdijk 45243510 1 null
REL_bouwen_scheldepromenade 45243510 1 null
# 45243600 Bouwen van kademuren
NE_kaaimuur 45243600
NE_kaaimuurkop 45243600
REL_construeren_oeverbescherming 45243600 1 null
REL_(vervangen|renoveren)_oeververdediging 45243600 3 null
REL_(heien|leveren)_damplanken 45243600 4 null
REL_(bouwen|verwijderen)_damwandenscherm 45243600 2 null
REL_(afbreken|bouwen)_kaaimuur 45243600 5 null
REL_(renoveren|bouwen|aanleggen|afbreken|realiseren|herstellen)_kaai 45243600 3 null
REL_(renoveren|bouwen|aanleggen|afbreken|realiseren|herstellen)_sluiswand 45243600 1 null
REL_heien_stalen_damplanken 45243600 2 null
NE_wrijfhouten 45243600 1
REL_(verwijderen|plaatsen|aansluiten)_damwand 45243600 3 null
# 45244000 Maritieme bouwwerkzaamheden
# 45244200 Havensteigers
NE_aanlegsteiger 45244200
NE_havensteigers 45244200
NE_veersteiger 45244200
# 45246000 Werkzaamheden voor rivierdebietregeling en hoogwaterbeheersing
NE_debietsbepaling 45246000
REL_aanleggen_vispaaiplaats 45246000 1 null
REL_(herstellen|plaatsen)_onbevaarbare_waterloop 45246000 3 null
REL_(onderhouden|herstellen|ruimen|plaatsen)_waterloop 45246000 7 null
REL_herstellen_dijk 45246000 1 null
REL_afbreken_klepstuw 45246000 1 null
REL_aanleggen_plas 45246000 1 null
# 45246200 Oeverbeschermingswerken
NE_oeverbeschermingswerk 45246200
NE_oeverherstel 45246200
REL_renoveren_rechteroever 45246200 1 null
REL_renoveren_oever 45246200 1 null
REL_(herstellen|inrichten)_oever 45246200 2 null
REL_aanleggen_oeverzwaluwwand 45246200 1 null
REL_aanleggen_vooroever 45246200 1 null
REL_aanpassen_oever 45246200 1 null
REL_graven_oevertalud 45246200 1 null
REL_verwijderen_oeververdediging 45246200 1 1
REL_plaatsen_oeverversteviging 45246200 2 null
REL_aanleggen_vooroever 45246200 1 null
REL_(beheren|ruimen)_waterloop 45246200 3 null
REL_construeren_oeverbescherming 45246200 1 null
REL_(plaatsen|verwijderen)_damwand 45246200 2 null
REL_vervangen_oeververdediging 45246200 1 null
REL_(onderhouden|herstellen|renoveren)_oever 45246200 3 null
# 45246400 Werkzaamheden voor hoogwaterpreventie
NE_meetinfrastructuur,NE_waterweg 45246400
NE_peilmeetpunt 45246400
NE_meettechnologie,NE_hydrografie 45246400
NE_waterpeilregeling 45246400
NE_meetpaal 45246400
NE_overstromingsgebied 45246400 1 null
REL_plaatsen_bolders 45246400 1 null
REL_aanleggen_bufferbekken 45246400 1 19
REL_(ruimen|herprofileren|heraanleggen|beveiligen)_rosdambeek 45246400 4 null
REL_bouwen_ringdijk 45246400 1 null
# 45246410 Onderhoud van hoogwaterkeringen
NE_hoogwaterkering 45246410
# 45247100 Bouwwerkzaamheden voor waterwegen
REL_plaatsen_visvriendelijke_vijzelpomp 45247100 1 null
REL_plaatsen_stuw 45247100 1 null
REL_aanleggen_langsgracht 45247100 1 null
REL_plaatsen_regelbare_stuw 45247100 1 null
REL_(afwerken|aanvullen)_landzijde 45247100 2 null
REL_herprofileren_CAT_gracht 45247100 1 null
REL_aanleggen_CAT_gracht 45247100 1 1
REL_(bouwen|aanpassen)_waterloop 45247100 1 1
REL_(bouwen|aanpassen)_damwand 45247100 1 null
REL_(bouwen|aanpassen)_waterweg 45247100 1 null
# 45247110 aanleg van kanaal
NE_kanaalverbreding 45247110
REL_(aanleggen|bouwen|realiseren)_kanaal 45247110
# 45247120 Waterwegen, met uitzondering van kanalen
REL_inrichten_overstromingsgebied 45247120 1 null
REL_(ruimen|onderhouden|herprofileren|aanleggen)_sloot 45247120 2 null
REL_(ruimen|onderhouden|herprofileren|aanleggen)_gracht 45247120 2 null
REL_(werken|onderhouden|herprofileren|aanleggen)_waterloop 45247120 7 null
REL_(werken|onderhouden|herprofileren|aanleggen)_onbevaarbare_waterloop 45247120 7 null
# 45247230 Bouwen van dijk
REL_aanleggen_uitwateringsconstructie 45247230 1 null
REL_aanleggen_broedvogeleilanden 45247230 1 null
REL_(aanleggen|bouwen)_dijk 45247230 3 null
REL_heraanleggen_waterkering 45247230 1 null
REL_aanleggen_waterkeringsdijk 45247230 1 null
REL_bouwen_ringdijk 45247230 1 null
REL_bouwen_sigmadijk 45247230 1 null
NE_dijklichaam 45247230 1
NE_dijkwerk 45247230 1
# 45248200 bouwen van droogdokken
NE_droogdok 45248200
# 45248300 Bouwwerkzaamheden voor drijvende dokken
REL_leveren_drijvende_droogdok 45248300 1 null
REL_leveren_drijvende_dok 45248300 1 null
# 45248400 Bouwen van landingssteigers
# 45252124 Bagger- en pompwerkzaamheden
NE_baggerwerk 45252124
NE_onderhoudsbaggerwerk 45252124
REL_afvoeren_slib 45252124 1 3
REL_uitvoeren_baggerwerk 45252124 1 null
REL_baggeren_doorgang 45252124 1 null
REL_baggeren_zone 45252124 2 null
REL_(transporteren|verwerken)_baggerspecie 45252124 4 null
REL_baggeren_betreffende 45252124 1 null
REL_(exploiteren|beheren)_kanaal 45252124 2 null
REL_(onderhouden|werken|herprofileren|aanleggen)_waterloop 45252124 4 null
REL_uitbreiden_zwin 45252124 1 null
REL_baggeren_doorgang 45252124 1 null
REL_(transporteren|baggeren)_specie 45252124 2 null
REL_onderhouden_onbevaarbare_waterloop 45252124 1 5
# 45262211 Heien
REL_heien_buispaal 45262211
# 50240000 Reparatie, onderhoud en aanverwante diensten in verband met scheepvaart en andere uitrusting
REL_onderhouden_ponton 50240000 2 null
# 50241000 Reparatie en onderhoud van schepen
NE_ship 50241000 1
REL_onderhouden_ship 50241000 1 null
REL_onderhouden_ponton 50241000 2 null
# 63721000 Exploitatie van havens en waterwegen en aanverwante diensten
# 63721300 Exploitatie van waterwegen
REL_(verwijderen|ruimen)_drijvend_hout 63721300 1 null
REL_(verwijderen|ruimen)_drijvend_vuil 63721300
# 71351923 Diensten voor bathymetrische metingen
NE_bathymetrische_meting 71351923 1
REL_uitvoeren_peiling 71351923 2 null
# 71354400 Hydrografische diensten
NE_hydrografische_dienst 71354400 1
NE_hydrografische_meting 71354400 1
REL_uitvoeren_hydrografische_peiling 71354400 2 null
REL_(uitvoeren|verwerken)_hydrografische_meting 71354400 5 null
REL_uitvoeren_hydrografische_singlebeam 71354400 1 null
REL_uitvoeren_hydrografische_peiling 71354400 1 null
REL_onderhouden_golfmeetboei 71354400 1 null
# 71353100 Hydrografisch onderzoek
NE_hydrografisch_onderzoek 71353100
# 90513600 Diensten voor slibverwijdering
NE_slibverwijdering 90513600
REL_ruimen_baangrachten 90513600 1 2
REL_(ruimen|onderhouden)_gracht 90513600 3 null
REL_werken_onbevaarbare_waterloop 90513600 1 1
# 90513800 Diensten voor slibbehandeling
NE_slibbehandeling 90513800
NE_slibverwerking 90513800
REL_(exploiteren|beheren)_kanaal 90513800 2 null
REL_(behandelen|verwerken)_slib 90513800 1 null
REL_baggeren_zone 90513800 1 null
REL_(transporteren|verwerken)_baggerspecie 90513800 3 null
REL_baggeren_doorgang 90513800 1 null
REL_baggeren_betreffende 90513800 1 null
REL_baggeren_vaartkom 90513800 1 null
# 90733200 Opruimdiensten voor vervuild oppervlaktewater
NE_vervuild_oppervlaktewater 90733200
# 90733300 Diensten voor bescherming van oppervlaktewater tegen vervuiling
# 90733400 Diensten voor behandeling van oppervlaktewater
NE_vijver,NE_waterbehandeling 90733400
NE_vijver,NE_waterbehandeling 90733400
REL_behandelen_oppervlaktewater 90733400
# 98363000 Duikdiensten
NE_duikopdracht 98363000
NE_duikdiensten 98363000
REL_uitvoeren_duikopdracht 98363000 2 null
# 45243000 Kustverdedigingswerken
NE_kustverdedigingswerken 45243000 1
# 45243400 Werkzaamheden ter voorkoming van strandafslag
REL_(onderhouden|nivelleren|vervoeren|wegsteken)_strand 45243400 1
REL_vrijmaken_strandhaag 45243400
REL_uitvoeren_strandsuppletie 45243400
REL_voorkomen_strandafslag 45243400
# 45243500 Bouwen van zeewering
REL_heraanleggen_waterkering 45247230 1 null
REL_bouwen_zeewering 45247230 1
# 45247270 Bouw van waterbekken Percent : 67 Info : 2/1/1/null
NE_waterbekken 45247270
# 45248100 bouwen van kanaalsluizen
NE_kanaalsluis 45248100
NE_sluisdeur 45248100
REL_bouwen_sluis 45248100
REL_bouwen_kanaalsluis 45248100
# 50246000 Onderhoud van havenuitrusting
# 50246200 onderhoud van boeien
NE_boei 50246200