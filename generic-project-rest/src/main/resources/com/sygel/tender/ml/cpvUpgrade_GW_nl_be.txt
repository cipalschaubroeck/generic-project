#
# Copyright © Sygel - All Rights Reserved
# Unauthorized copying of this file, via any medium is strictly prohibited
# Unauthorized use of this source code is strictly prohibited
# Proprietary and confidential
#
# upgrade
# 45330000 Installeren van gasmeters
REL_installeren_gasmeter 45330000
# 45333000 Installeren van gasfittings
NE_gasfitting 45333000
REL_installeren_gasfitting 45333000
# 09323000 Stadsverwarming ZIE CV
# 38418000 Calorimeters
REL_(leveren|plaatsen)_warmtemeters 38418000 2
# 38421100 Watermeters
NE_watermeter 38421100
NE_watermetermontage 38421100
REL_(leveren|plaatsen)_watermeter 38421100 2
REL_(leveren|plaatsen)_volumetrische_watermeter 38421100 1
# 39370000 Waterinstallaties
NE_waterleidinginstallatie 39370000
NE_waterinstallatie 39370000
# 39340000 Uitrusting voor gasnetwerk
NE_huisdrukregelaar 39340000 1
REL_(leveren|plaatsen)_huisdrukregelaar 39340000 1
# 41110000 Drinkwater
# 42130000 Kranen, afsluiters, kleppen en dergelijke ZIE SA
# 42131160 Hydranten
REL_(leveren|vervangen|onderhouden|plaatsen|verwijderen)_buitenhydrant 42131160 10
REL_(leveren|vervangen|onderhouden|plaatsen|verwijderen)_hydrant 42131160 10
REL_(leveren|vervangen|onderhouden|plaatsen|verwijderen)_ondergrondse_hydrant 42131160 2
# 44161200 Hoofdwaterleiding
REL_(verlengen|aanleggen|vernieuwen|plaatsen|renoveren)_waterleiding 44161200 2
REL_(verlengen|aanleggen|vernieuwen|plaatsen|renoveren)_CAT_waterleiding 44161200 1
# 44162200 Distributiepijpleiding
NE_drinkwaterdistributieleiding 44162200
NE_distributiepijpleiding 44162200
# 44162500 Drinkwaterleidingen
NE_drinkwateraftakking 44162500
NE_drinkwatervertakking 44162500 1
NE_drinkwaterleiding 44162500 1
NE_drinkwatervoorziening 44162500 1
REL_(aanleggen|boren|realiseren)_drinkwatervoorziening 44162500
REL_(aanleggen|boren|realiseren)_drinkwatervertakking 44162500
REL_(aanleggen|boren|realiseren)_drinkwateraftakking 44162500
REL_(aanleggen|boren|realiseren)_drinkwaterleiding 44162500 1
# 44163100 Pijpen
# 44163200 Pijpfittings
NE_verlengstuk,NE_gasmeter 44163200 1
NE_stalen_hulpstuk,NE_gas 44163200 1
NE_stalen_hulpstuk,NE_aardgas 44163200 1
NE_stalen_hulpstuk,NE_waterleiding 44163200 1
NE_stalen_hulpstuk,NE_gasleiding 44163200 1
NE_stalen_hulpstuk,NE_aardgasdistributienet 44163200 1
# 44163230 Pijpkoppelstukken
# 44167100 Koppelstukken
# 45221250 Ondergrondse werkzaamheden, met uitzondering van tunnels, schachten en onderdoorgangen
# 45231110 Bouwen van pijpleidingen
REL_(aanleggen|plaatsen)_hdpe 45231220 2
# 45231112 Aanleggen van buisleidingen
# 45231200 Bouwen van olie- en gaspijpleidingen
# 45231100 Algemene bouwwerkzaamheden voor pijpleidingen
# 45231220 Bouwen van gaspijpleidingen
REL_lassen_ontspanningsleidingen 45231220 1
NE_gaspijpleiding 45231220
REL_(aanleggen|leveren|plaatsen|bouwen)_gaspijpleiding 45231220 1
REL_(aanleggen|leveren|plaatsen|bouwen)_ondergrondse_gaspijpleiding 45231220 1
REL_(aanleggen|leveren|plaatsen|werken|bouwen)_gasleiding 45231220 2
REL_uitbreiden_gasnet 45231220 1
NE_gasnet 45231220 1
REL_(aanleggen|leveren|plaatsen|bouwen)_ontspanningsstations 45231220 1
# 45231221 Aanleggen van gastoevoerleiding
NE_aardgasleiding 45231221
NE_gasaansluiting 45231221
REL_lassen_ontspanningsleidingen 45231221 1
REL_(aanleggen|leveren)_gasleiding 45231221 2
REL_uitbreiden_gasnet 45231221 1
REL_bouwen_ontspanningsstations 45231221 1
# 45231223 Aanvullende werkzaamheden voor gasdistributie
NE_gasmeter 45231223 1
NE_gasmeterkranen 45231223 1
# 45232150 Werkzaamheden in verband met waterleidingen
REL_(aansluiten|verlengen|plaatsen)_waterleiding 45232150 4
# 45232151 Werkzaamheden voor het verbeteren van waterleidingen
REL_verlengen_waterleiding 45232151 2
# 45232210 Aanleg van bovengrondse leidingen
# 45251250 Bouwen van stadsverwarmingsinstallatie
# 50411100 Reparatie en onderhoud van watermeters
REL_plaatsen_CAT_watermeter 50411100 2
REL_plaatsen_watermeters 50411100 2
REL_plaatsen_watermeter 50411100 2
# 65110000 Waterdistributie
# 65111000 Drinkwaterdistributie
REL_aanleggen_waterleiding 65111000 2
# 65130000 Exploitatie van watervoorziening
# 76600000 Inspectie van pijpleidingen
NE_drukmeting,NE_gasnet 76600000