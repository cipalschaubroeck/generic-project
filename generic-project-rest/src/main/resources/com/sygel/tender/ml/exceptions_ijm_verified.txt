#
# Copyright © Sygel - All Rights Reserved
# Unauthorized copying of this file, via any medium is strictly prohibited
# Unauthorized use of this source code is strictly prohibited
# Proprietary and confidential
#
TYPE_WORKS:45262680 Lassen Percent : 100 Info : 5/5/null/null
TYPE_WORKS:45223110 Installeren van metalen constructies Percent : 100 Info : 3/null/null/null
TYPE_WORKS:45262670 Metaalbewerking Percent : 100 Info : 2/null/null/null
TYPE_SUPPLIES:44212000 Metaalbouwproducten en onderdelen, met uitzondering van prefabgebouwen Percent : 100 Info : 1/null/null/null
TYPE_SUPPLIES:39563300 Weefsels van metaaldraad Percent : 100 Info : 1/null/null/null
TYPE_SUPPLIES:14755000 Wolfraam Percent : 100 Info : 1/1/null/null
TYPE_SUPPLIES:14721000 Aluminium Percent : 100 Info : 1/1/null/null
TYPE_SUPPLIES:44312000 Omheiningsdraad Percent : 100 Info : 1/1/null/null
TYPE_SERVICES:45442200 Aanbrengen van anticorrosielagen Percent : 100 Info : 1/null/null/null
TYPE_WORKS:44212120 Metaalconstructies van bruggen Percent : 100 Info : 2/null/null/null
TYPE_WORKS:45223210 Constructiestaalbouw Percent : 64 Info : 7/3/4/null
TYPE_WORKS:45262400 Optrekken van staalconstructies Percent : 50 Info : 2/null/2/null
TYPE_SUPPLIES:45262400 Optrekken van staalconstructies Percent : 100 Info : 1/null/null/null
TYPE_WORKS:45223100 Monteren van metalen constructies Percent : 50 Info : 1/null/1/null
TYPE_WORKS:44316510 Smeedwerk Percent : 50 Info : 2/1/2/null
TYPE_SUPPLIES:44212120 Metaalconstructies van bruggen Percent : 50 Info : 1/1/1/null
TYPE_WORKS:45421140 Plaatsen van metalen bouwtimmerwerk, met uitzondering van deuren en ramen Percent : 43 Info : 3/2/4/null
TYPE_WORKS:44212000 Metaalbouwproducten en onderdelen, met uitzondering van prefabgebouwen Percent : 33 Info : 1/1/2/null
TYPE_SUPPLIES:44470000 Gietijzeren producten Percent : 33 Info : 1/1/2/null
TYPE_SUPPLIES:45262670 Metaalbewerking Percent : 33 Info : 1/null/2/null
TYPE_SUPPLIES:14700000 Basismetalen Percent : 29 Info : 2/2/5/null
TYPE_SUPPLIES:44316000 IJzerwaren Percent : 100 Info : 1/null/null/null
TYPE_SUPPLIES:14622000 Staal Percent : 100 Info : 1/null/null/null
TYPE_SUPPLIES:43840000 Uitrusting voor smederij Percent : 100 Info : 1/null/null/null
TYPE_SUPPLIES:14523300 Zilver Percent : 100 Info : 1/null/null/null
TYPE_SUPPLIES:44212315 Steigeruitrusting
TYPE_SUPPLIES:44212317 Steigerconstructies
TYPE_SUPPLIES:45262100 Steigerbouw
TYPE_WORKS:45421100 Plaatsen van deuren en ramen en bijbehorende elementen
TYPE_SUPPLIES:44481100 Brandladders
TYPE_SUPPLIES:45262680 Lassen Percent : 100 Info : 5/5/null/null
TYPE_SUPPLIES:44450000 Zacht staal
TYPE_SUPPLIES:44212220 Pylonen, palen en paaltjes
TYPE_SUPPLIES:35821100 vlaggenmast
TYPE_SUPPLIES:44115700 Externe zonnewering
TYPE_SUPPLIES:45421148 Plaatsen van poorten
TYPE_SUPPLIES:45237000 Bouwen van toneel/platforms