#
# Copyright © Sygel - All Rights Reserved
# Unauthorized copying of this file, via any medium is strictly prohibited
# Unauthorized use of this source code is strictly prohibited
# Proprietary and confidential
#
# upgrade
# 16400000 sproeimachines voor de land- of tuinbouw
NE_beregeningsinstallatie 16400000
# 03121100 Levende planten, bollen, wortels, stekken en enten
# 03400000 Producten van bosbouw en bosexploitatie
# 03411000 Naaldhout
# 03413000 Brandhout
# 03450000 Boomkwekerijproducten
# 03451000 Planten
# 03451200 Bloembollen
# 03452000 Bomen
# 45111220 Verwijderen van begroeiing op bouwterreinen
# 45112700 Werkzaamheden voor landschapsarchitectuur
REL_LOT_landschap 45112700 1
NE_natuurinrichtingsproject 45112700
REL_herinrichten_landschap 45112700 1
REL_(herinrichten|aanleggen)_omgeving 45112700 2
REL_LOT_omgevingsaanleg 45112700 2
# 45112710 Landschappelijk ontwerp voor groenzones
REL_herinrichten_moestuinsites 45112710 2
REL_ontwerpen_groenzone 45112710
# 45112711 Landschappelijk ontwerp voor parken
REL_inrichten_landschappelijke 45112711 1
REL_ontwerpen_park 45112711
# 45112722 Landschappelijk ontwerp voor paardrijterreinen
# 45112723 Landschappelijk ontwerp voor speelterreinen
REL_(vernieuwen|herinrichten|aanleggen|inrichten|uitbreiden|ontwerpen)_sportterrein 45112723 4
REL_(vernieuwen|herinrichten|aanleggen|inrichten|uitbreiden|ontwerpen)_speelterrein 45112723 6
# 45233200 Diverse oppervlaktewerkzaamheden
# 45233229 Onderhoud van bermen
NE_bermbeheer 45233229
REL_aanpassen_zijberm 45233229 6
REL_(onderhouden|zaaien|profileren)_berm 45233229 4
REL_(onderhouden|maaien)_berm 45233229 4
REL_maaien_taludvegetatie 45233229 3
REL_aanleggen_aardeberm 45233229 1
REL_(onderhouden|profileren)_berm 45233229 8
REL_(profileren|onderhouden|herprofileren|zaaien|maaien)_berm 45233229 28
REL_storten_berm- 45233229 1
REL_verwijderen_boombanden 45233229 2
REL_maaien_gracht 45233229 14
REL_maaien_baangrachten 45233229 4
REL_(onderhouden|beplanten)_houtkanten 45233229 2
REL_(leveren|plaatsen)_boompaalconstructies 45233229 6
# 45236100 Verharding voor diverse sportvoorzieningen
REL_aanleggen_atletiekpiste 45236100 2
REL_aanleggen_sportveld 45236100 1
REL_aanleggen_natuurgrasvelden 45236100 1
REL_aanleggen_finse 45236100 1
# 71421000 Landschaps- en tuinaanlegdiensten
NE_beplanting 71421000
NE_landschapswerk 71421000
REL_onderhouden_beplanting 71421000 3
REL_onderhouden_aanplanting 71421000 1
REL_beplanten_hagen 71421000 1
REL_beplanten_heesters 71421000 1
REL_beplanten_struikgewas 71421000 1
REL_beplanten_zone 71421000 1
REL_beplanten_beplanting 71421000 1
REL_beplanten_zone 71421000 1
REL_beplanten_boom 71421000 1
REL_heraanleggen_openbaar_domein 71421000 1
REL_heraanleggen_omgevingswerk 71421000 2
REL_heraanleggen_domein 71421000 1
REL_aanleggen_gazon 71421000 1
REL_aanleggen_grasmat 71421000 1
REL_LOT_landschap 71421000 1
REL_heraanleggen_kasteelpark 71421000 2
REL_(inrichten|realiseren)_park 71421000 3
# 77000000 Diensten voor land-, bos- en tuinbouw, aquicultuur en imkerij
NE_groenwerken 77000000
NE_groenonderhoudswerk 77000000
NE_heidebeheer 77000000
REL_uitvoeren_groenonderhoud 77000000 1
REL_maaien_groenzone 77000000 2
REL_aanleggen_vegetaties 77000000 1
REL_onderhouden_park 77000000 2
REL_maaien_bodemvegetatie 77000000 2
REL_snoeien_hoogstammen 77000000 1
REL_aanleggen_gazon 77000000 2
REL_beplanten_bosgoed 77000000 3
REL_aanleggen_grazige_vegetaties 77000000 1
REL_maaien_distels 77000000 2
REL_plaatsen_weideafsluitingen 77000000 1
REL_plaatsen_betuining 77000000 4
REL_(afbreken|plaatsen|herstellen)_afsluiting 77000000 10
REL_afvoeren_maaisel 77000000 3
REL_uitvoeren_natuurbeheerwerk 77000000 8
NE_natuurbeheerwerk 77000000
REL_zaaien_grazige_vegetatie 77000000 1
# 77112000 Verhuur van maaimachines of landbouwmaterieel met bedieningspersoneel
NE_huurcontract,NE_grasmaaier 77112000
REL_verhuren_maaimachine 77112000
REL_verhuren_grasmaaier 77112000
# 77120000 Composteerdiensten
# 77200000 Bosbouwdiensten
REL_beplanten_bosgoed 77200000 1
REL_(snoeien|beplanten|vellen|verwijderen)_boom 77200000 3
# 77211000 Diensten in verband met houtwinning
REL_verwijderen_dode 77211000 2
NE_hakhoutbeheer 77211000
# 77211300 Rooien van bomen
NE_ontbossen 77211300
NE_rooiingswerk 77211300
REL_verwijderen_loofboom 77211300 2
REL_verwijderen_wortelstronken 77211300 1
REL_(snoeien|vellen)_houtachtige_gewassen 77211300 2
REL_(snoeien|vellen)_gewassen 77211300 2
REL_uitvoeren_hakhoutbeheer 77211300 1
REL_verkorten_naaldbamen 77211300 2
REL_rooien_boom 77211300 29
# 77211400 Vellen van bomen
REL_(snoeien|vellen|rooien|hakken|kappen|snoeien|verwijderen)_hoogstammige 77211400 1
REL_(snoeien|vellen|rooien|hakken|kappen|snoeien)_lijnbomen 77211400 6
REL_(snoeien|vellen|rooien|hakken|kappen|snoeien|verwijderen)_loofboom 77211400 2
REL_(snoeien|vellen|rooien|hakken|kappen|snoeien|verwijderen)_boom 77211400 29
REL_(snoeien|vellen|rooien|hakken|kappen|snoeien|verwijderen)_struik 77211400 29
REL_uitvoeren_hakhoutbeheer 77211400 1
REL_verkorten_naaldbamen 77211400 2
REL_beheren_houtige_vegetatie 77211400 2
REL_verwijderen_boombanden 77211400 2
REL_(snoeien|vellen|rooien|hakken|kappen|snoeien|verwijderen)_gewassen 77211400 4
REL_(snoeien|vellen|rooien|hakken|kappen|snoeien|verwijderen)_wortelstronken 77211400 1
REL_(snoeien|vellen|rooien|hakken|kappen|snoeien|verwijderen)_houtachtige_gewassen 77211400 4
REL_(snoeien|vellen|rooien|hakken|kappen|snoeien|verwijderen)_lijnbomen 77211400 6
REL_(snoeien|vellen|rooien|hakken|kappen|snoeien|verwijderen)_loofboom 77211400 2
REL_(snoeien|vellen|rooien|hakken|kappen|snoeien|verwijderen)_houtachtige_gewassen 77211400 2
# 77211500 Onderhouden van bomen
NE_boomverjonging 77211500
REL_beheren_houtige_vegetatie 77211500 2
REL_onderhouden_aanplanting 77211500 1
REL_beheren_houtige_ruigte 77211500 1
REL_snoeien_houtachtige_gewassen 77211500 2
REL_verkorten_naaldbamen 77211500 2
REL_verkorten_naaldbomen 77211500 2
REL_onderhouden_bosgoed 77211500 2
# 77211600 Zaaien van bomen
REL_beplanten_boom 77211600 2
# 77230000 Diensten in verband met bosbouw
REL_(vellen|leveren|onderhouden|beplanten)_boom 45233229 15
# 77231000 Diensten voor bosbouwbeheer
# 77231100 Beheersdiensten van bosbestanden
# 77231300 Bosbeheersdiensten
# 77231600 Bebossingsdiensten
# 77231900 Sectorale planningsdiensten voor bosbouw
# 77300000 Tuinbouwdiensten
NE_glastuinbouw 77300000
REL_graven_teelaarde 77300000 1
REL_uitvoeren_tuinwerken 77300000 2
REL_(plaatsen|leveren|onderhouden)_heesters 77300000 2
REL_snoeien_heestermassieven 77300000 1
REL_snoeien_heggen 77300000 1
REL_uitvoeren_onkruidbestrijding 77300000 2
REL_(zaaien|snoeien)_gras 77300000 4
REL_(scheren|snoeien|onderhouden)_hagen 77300000 5
# 77310000 Beplanten en onderhouden van groengebieden
NE_aanplantingswerk 77310000
NE_maaibeurt 77310000
PA_groenonderhoud 77310000 2
NE_groenwerken 77310000
PA_beplantingswerk 77310000 2
NE_bezaaiingswerk 77310000 2
PA_maaiwerken 77310000 2
CON_begrazen 77310000 2
REL_beheren_groene_zones 77310000 2
REL_beheren_groene_zone 77310000 2
REL_uitvoeren_groenaanleg 77310000 2
REL_afvoeren_maaisel 77310000 2
REL_(plaatsen|leveren)_heesters 77310000 2
REL_uitvoeren_bezaaiingswerken 77310000 1
REL_(snoeien|vellen|verwijderen)_gewassen 77310000 4
REL_(snoeien|vellen|verwijderen)_gras 77310000 4
REL_(maaien|uitvoeren)_groenonderhoud 77310000 3
REL_(openspreiden|leveren|plaatsen)_meststoffen 77310000 8
REL_LOT_groenonderhoud 77310000 2
REL_LOT_groen 77310000 2
REL_maaien_grasperken 77310000 1
REL_(scheren|snoeien)_hagen 77310000 5
REL_(zaaien|maaien|snoeien|onderhouden)_gras 77310000 9
REL_(aanleggen|onderhouden)_groenvoorziening 77310000 37
REL_(uitvoeren|beplanten|onderhouden)_beplanting 77310000 8
REL_snoeien_heestermassieven 77310000 1
REL_(onderhouden|aanleggen)_groen 77310000 4
REL_beheren_domein 77310000 1
REL_beheren_vegetatie 77310000 2
REL_onderhouden_gemeentelijke_aanplanting 77310000 2
REL_verwijderen_onkruid 77310000 1
REL_(leveren|beplanten)_rozen 77310000 2
REL_beheren_openbaar_domein 77310000 1
REL_(maaien|uitvoeren)_groenonderhoud 77310000 3
REL_(snoeien|onderhouden|beplanten|vellen|leveren)_boom 77310000 23
REL_(onderhouden|beplanten)_houtkanten 77310000 2
REL_onderhouden_geplante 77310000 1
REL_(leveren|beplanten)_bosgoed 77310000 3
REL_onderhouden_plantsoenen 77310000 4
REL_(plaatsen|leveren)_heesters 77310000 2
REL_uitvoeren_bezaaiingswerken 77310000 1
REL_verwijderen_struiken 77310000 2
REL_(vellen|verwijderen|onderhouden|snoeien)_gewassen 77310000 5
REL_uitvoeren_groenbeheer 77310000 2
REL_(onderhouden|aanleggen)_gazon 77310000 4
REL_(onderhouden|herstellen)_lijnbeplantingen 77310000 2
REL_LOT_groenonderhoud 77310000 1
REL_(maaien|snoeien)_onderhoud 77310000 4
REL_aanleggen_vegetaties 77310000 1
REL_onderhouden_aanplanting 77310000 8
REL_(onderhouden|maaien)_terrein 77310000 7
REL_onderhouden_openbaar_groen 77310000 2
REL_aanleggen_groenzone 77310000 2
REL_aanleggen_gras-kunststofplaten 77310000 1
REL_scheren_manuele_onkruidbestrijding 77310000 1
REL_(onderhouden|beheren)_groene_ruimte 77310000 4
REL_beheren_houtige_vegetatie 77310000 2
REL_(zaaien|maaien)_berm 77310000 11
REL_snoeien_zone 77310000 4
REL_(beplanten|onderhouden)_perkplanten 77310000 4
REL_(plaatsen|leveren)_boompaalconstructies 77310000 6
REL_(onderhouden|maaien)_groenzone 77310000 6
REL_(leveren|openspreiden|plaatsen)_meststoffen 77310000 8
REL_onderhouden_groene_ruimte 77310000 2
REL_onderhouden_groenbestand 77310000 1
REL_onderhouden_groene_ruimtes 77310000 1
REL_onderhouden_groeninfrastructuur 77310000 1
REL_scheren_onkruidbestrijding 77310000 1
REL_verwijderen_struikgewassen 77310000 1
REL_aanleggen_beplantingszones 77310000 1
REL_(leveren|openspreiden)_teelaarde 77310000 2
REL_beplanten_groenelementen 77310000 1
REL_onderhouden_aanplantingswerken 77310000 1
# 77311000 Onderhouden van sier- en recreatietuinen
REL_onderhouden_groene_ruimte 77311000 2
REL_onderhouden_beplanting 77311000 1
REL_onderhouden_groen 77311000 2
REL_onderhouden_tuin 77311000 2
REL_onderhouden_CAT_tuin 77311000 2
REL_onderhouden_openbaar_groen 77311000 2
REL_onderhouden_groene_zone 77311000 2
REL_onderhouden_recreatietuin 77311000 2
REL_onderhouden_siertuin 77311000 2
# 77312000 Diensten voor het wieden van onkruid
NE_exotenbestrijding 77312000
PA_onkruid 77312000 1
REL_verwijderen_onkruid 77312000 1
REL_(ruimen|verwijderen)_bladeren 77312000 2
REL_onderhouden_groen 77312000 4
REL_onderhouden_openbaar_groen 77312000 2
REL_verwijderen_onkruid 77312000 1
REL_onderhouden_beplanting 77312000 2
REL_uitvoeren_groenonderhoud 77312000 1
REL_onderhouden_groene_ruimte 77312000 2
REL_onderhouden_groenbestand 77312000 1
REL_onderhouden_groeninfrastructuur 77312000 1
REL_onderhouden_aanplantingswerken 77312000 1
# 77312100 Onkruidverdelging
NE_onkruidverdelging 77312100
REL_uitvoeren_niet-chemische_onkruidbestrijding 77312100 2
REL_uitvoeren_onkruidbestrijding 77312100 2
REL_leveren_onkruidborstelmachine 77312100 2
NE_onkruidverwijdering 77312100
# 77313000 Parkonderhoud
NE_parkonderhoud 77313000
REL_onderhouden_park 77313000 2
REL_maaien_grasoppervlakten 77313000 1
REL_maaien_gazon 45233229 4
REL_onderhouden_aanplanting 77313000 4
REL_onderhouden_groene_ruimte 77313000 5
REL_(snoeien|onderhouden)_rozenstruiken 77313000 2
REL_onderhouden_openbaar_groen 77313000 2
REL_onderhouden_groeninfrastructuur 77313000 1
REL_beplanten_beplanting 77313000 1
# 77314000 Onderhoud van terreinen
REL_verwijderen_zwerfvuil 77314000 1
REL_(ruimen|verwijderen)_bladeren 77314000 3
REL_(onderhouden|snoeien)_beplanting 77314000 2
REL_onderhouden_groene_ruimte 77314000 2
REL_uitvoeren_natuurbeheerwerk 77314000 2
REL_LOT_natuurbeheerwerk 77200000 2
NE_natuurbeheerwerk 77314000
REL_(maaien|onderhouden)_terrein 77314000 7
# 77314100 Aanleggen van gazons
REL_(zaaien|onderhouden|regenereren|aanleggen)_voetbalveld 77314100 2
REL_(zaaien|onderhouden|regenereren|aanleggen)_gras 77314100 8
REL_(zaaien|onderhouden|regenereren|aanleggen)_grasperk 77314100 2
REL_(zaaien|onderhouden|regenereren|aanleggen)_terreingazon 77314100 2
REL_(zaaien|onderhouden|regenereren|aanleggen)_gazon 77314100 1
# 77315000 Zaaiwerkzaamheden
NE_zaaiwerkzaamheid 77315000
NE_grasbezaaiing 77315000
NE_bezaaiingswerk 77315000
REL_uitvoeren_bezaaiingswerk 77315000 1
REL_zaaien_gras 77315000 6
REL_zaaien_grasveld 77315000 6
REL_zaaien_afdelingen 77315000 2
REL_zaaien_berm 77315000 2
REL_zaaien_zone 77315000 1
REL_zaaien_terreingazon 77315000 2
# 77320000 Onderhouden van sportvelden
REL_(onderhouden|aanleggen|maaien|renoveren)_voetbalterreinen 77320000 1
REL_(onderhouden|aanleggen|maaien|renoveren)_voetbalveld 77320000 8
REL_(onderhouden|aanleggen|maaien|renoveren)_tennisterreinen 77320000 3
REL_(onderhouden|aanleggen|maaien|renoveren)_sportveld 77320000 3
# 77330000 Uitvoeren van bloemenarrangementen
NE_bloemenarrangement 77330000
# 77340000 Snoeien van bomen en heggen
REL_(verwijderen|snoeien|vellen)_houtachtige_gewassen 77340000 4
REL_scheren_knotwilgen 77340000 4
REL_onderhouden_aanplanting 77340000 1
REL_(ruimen|verwijderen)_bladeren 77340000 2
REL_verwijderen_dode 77340000 6
REL_(snoeien|vellen|leveren|onderhouden|beplanten)_boom 77340000 20
REL_verkorten_naaldbamen 77340000 2
REL_scheren_hagen 77340000 1
REL_(herstellen|onderhouden)_lijnbeplantingen 77340000 2
REL_onderhouden_beplanting 77340000 2
REL_(verwijderen|snoeien)_houtachtige_gewassen 77340000 2
REL_snoeien_sierbomen 77340000 4
REL_verwijderen_struikgewassen 77340000 1
REL_(onderhouden|beplanten)_houtkanten 77340000 2
REL_afvoeren_kap- 77340000 1
REL_(leveren|plaatsen)_boompaalconstructies 77340000 6
REL_verwijderen_struiken 77340000 2
REL_(snoeien|verwijderen)_gewassen 77340000 2
# 77341000 Snoeien van bomen
REL_beheren_houtige_vegetatie 77341000 2
REL_snoeien_lijnbomen 77341000 4
REL_snoeien_boom 77341000 12
REL_ruimen_houtafval 77341000 1
REL_snoeien_houtachtige_gewassen 77341000 2
REL_uitvoeren_hakhoutbeheer 77341000 1
REL_verkorten_naaldbamen 77341000 2
# 77342000 Snoeien van heggen
REL_snoeien_heg 77340000 1
REL_snoeien_haag 77340000 1
# 92533000 Diensten voor natuurreservaten
REL_beheren_dierenkudde 92533000 2
# 98371111 Diensten voor het onderhouden van begraafplaatsen
REL_onderhouden_begraafplaats 98371111